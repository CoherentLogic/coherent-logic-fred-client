package com.coherentlogic.fred.client.spark.integration;

import static com.coherentlogic.coherent.data.model.core.util.Utils.using;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.beans.IntrospectionException;
import java.util.Calendar;
import java.util.Date;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.junit4.SpringRunner;

import com.coherentlogic.fred.client.core.domain.Categories;
import com.coherentlogic.fred.client.core.domain.Observations;
import com.coherentlogic.fred.client.core.domain.ObservationsSparkBeanSpecification;
import com.coherentlogic.fred.client.core.domain.ReleaseDates;
import com.coherentlogic.fred.client.core.domain.ReleaseDatesSparkBeanSpecification;
import com.coherentlogic.fred.client.core.domain.Releases;
import com.coherentlogic.fred.client.core.domain.ReleasesSparkBeanSpecification;
import com.coherentlogic.fred.client.core.domain.Seriess;
import com.coherentlogic.fred.client.core.domain.SeriessSparkBeanSpecification;
import com.coherentlogic.fred.client.core.domain.SortOrder;
import com.coherentlogic.fred.client.core.domain.Sources;
import com.coherentlogic.fred.client.core.domain.SourcesSparkBeanSpecification;
import com.coherentlogic.fred.client.core.domain.Tags;
import com.coherentlogic.fred.client.core.domain.TagsSparkBeanSpecification;
import com.coherentlogic.fred.client.core.domain.VintageDates;
import com.coherentlogic.fred.client.core.domain.VintageDatesSparkBeanSpecification;
import com.coherentlogic.fred.client.core.factories.QueryBuilderFactory;
import com.coherentlogic.fred.client.spark.integration.builders.CategoriesDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.ObservationsDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.ReleaseDatesDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.ReleasesDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.SeriessDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.SourcesDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.TagsDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.VintageDatesDatasetFactory;

@RunWith(SpringRunner.class)
@SpringBootTest
@SpringBootConfiguration()
//@EnableAutoConfiguration
//@ComponentScan(basePackages="com.coherentlogic.fred.client")
//@Configuration
@ImportResource({
    "classpath*:/spring-test/api-key-beans.xml",
    "classpath*:/spring-test/h2-jpa-beans.xml",
    "classpath*:/spring-test/application-context.xml"
})
public class SparkIntModuleIntegrationTest {

    private static final Logger log = LoggerFactory.getLogger(SparkIntModuleIntegrationTest.class);

    @Autowired
    private QueryBuilderFactory queryBuilderFactory;

    private SparkSession sparkSession;

    @Before
    public void setUp () {

        String master = "local[*]";

        SparkConf sparkConf = new SparkConf()
            .setAppName(SparkIntModuleIntegrationTest.class.getName())
            .setMaster(master);

        JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);

        sparkSession = new SparkSession (javaSparkContext.sc());
    }

    @After
    public void tearDown () {
        sparkSession.close();
        sparkSession = null;
    }

    static Date getDate (int year, int month, int day) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        return calendar.getTime();
    }

    @Test
    public void testSeriessDataset() {

        Date realtimeStart = getDate (2001, Calendar.JANUARY, 20);
        Date realtimeEnd = getDate (2004, Calendar.MAY, 17);

        Seriess result = queryBuilderFactory.getInstance()
            .series()
            .withSeriesId("GNPCA")
            .withRealtimeStart(realtimeStart)
            .withRealtimeEnd(realtimeEnd)
            .doGetAsSeriess ();

        log.info("result: " + result);

        SeriessDatasetFactory seriessDatasetFactory = new SeriessDatasetFactory(sparkSession);

        Dataset<SeriessSparkBeanSpecification> resultantDataset = seriessDatasetFactory
            .addBeans(result).getInstance();

        for (String column : resultantDataset.columns())
            log.info ("resultantDataset.column: " + column);

        assertNotNull(resultantDataset);
        assertEquals(14, resultantDataset.columns().length);
    }

    @Test
    public void testCategoriesDataset () {

        Categories result = queryBuilderFactory.getInstance()
            .category()
            .withCategoryId(125)
            .doGetAsCategories();

        CategoriesDatasetFactory categoriesDatasetFactory = new CategoriesDatasetFactory(sparkSession);

        Dataset<Categories> resultantDataset = categoriesDatasetFactory
            .addBeans(result).getInstance();

        assertNotNull(resultantDataset);
        assertEquals(10, resultantDataset.columns().length);
    }

    @Test
    public void testObservationsDataset () throws IntrospectionException {

        Observations result = queryBuilderFactory.getInstance()
            .series()
            .observations()
            .withSeriesId("GNPCA")
            .doGetAsObservations();

        ObservationsDatasetFactory observationsDatasetFactory = new ObservationsDatasetFactory(sparkSession);

        Dataset<ObservationsSparkBeanSpecification> resultantDataset = observationsDatasetFactory
            .addBeans(result).getInstance();

        assertNotNull(resultantDataset);
        assertEquals(14, resultantDataset.columns().length);
    }

    @Test
    public void testReleasesDataset () {

        Releases result = queryBuilderFactory.getInstance()
            .releases()
            .doGetAsReleases();

        ReleasesDatasetFactory releasesDatasetFactory = new ReleasesDatasetFactory (sparkSession);

        Dataset<ReleasesSparkBeanSpecification> resultantDataset =
            releasesDatasetFactory.addBeans(result).getInstance();

        assertNotNull(resultantDataset);
        assertEquals(7, resultantDataset.columns().length);
    }

    @Test
    public void testReleaseDates () {

        ReleaseDates result = queryBuilderFactory.getInstance()
            .releases()
            .dates()
            .withRealtimeStart("2012-06-18")
            .withRealtimeEnd("2012-06-18")
            .doGetAsReleaseDates();

        ReleaseDatesDatasetFactory datasetFactory = new ReleaseDatesDatasetFactory (sparkSession);

        Dataset<ReleaseDatesSparkBeanSpecification> resultantDataset =
            datasetFactory.addBeans(result).getInstance();

        assertNotNull(resultantDataset);
        assertEquals(10, resultantDataset.columns().length);
    }

    @Test
    public void testSources () {

        Sources result = queryBuilderFactory.getInstance()
            .sources()
            .doGetAsSources ();

        SourcesDatasetFactory datasetFactory = new SourcesDatasetFactory (sparkSession);

        Dataset<SourcesSparkBeanSpecification> resultantDataset =
            datasetFactory.addBeans(result).getInstance();

        assertNotNull(resultantDataset);
        assertEquals(7, resultantDataset.columns().length);
    }

    @Test
    public void testTags () {

        Date realtimeStart = using (2001, Calendar.JANUARY, 20);
        Date realtimeEnd = using (2004, Calendar.MAY, 17);

        Tags result = queryBuilderFactory.getInstance()
            .series()
            .search()
            .tags()
            .withRealtimeStart(realtimeStart)
            .withRealtimeEnd(realtimeEnd)
            .withSeriesSearchText("monetary service index")
            .doGetAsTags ();

        TagsDatasetFactory datasetFactory = new TagsDatasetFactory (sparkSession);

        Dataset<TagsSparkBeanSpecification> resultantDataset =
            datasetFactory.addBeans(result).getInstance();

        assertNotNull(resultantDataset);
        assertEquals(8, resultantDataset.columns().length);
    }

    @Test
    public void testVintageDates () {

        Date realtimeStart = using (2001, Calendar.JANUARY, 20);
        Date realtimeEnd = using (2004, Calendar.MAY, 17);

        VintageDates result = queryBuilderFactory.getInstance()
            .series()
            .vintageDates()
            .withSeriesId("GNPCA")
            .withRealtimeStart(realtimeStart)
            .withRealtimeEnd(realtimeEnd)
            .withLimit(100)
            .withOffset(1)
            .withSortOrder(SortOrder.desc)
            .doGetAsVintageDates();

        VintageDatesDatasetFactory datasetFactory = new VintageDatesDatasetFactory (sparkSession);

        Dataset<VintageDatesSparkBeanSpecification> resultantDataset =
            datasetFactory.addBeans(result).getInstance();

        assertNotNull(resultantDataset);
        assertEquals(8, resultantDataset.columns().length);
    }
}
