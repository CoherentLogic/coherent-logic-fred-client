package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TopicTest {

    private Topic topic;

    @Before
    public void before () {
        topic = new Topic ();
    }

    @After
    public void after () {
        topic = null;
    }

    @Test
    public void testTopic () {

        TestUtil.util.doTest(
            topic,
            () -> {
                topic.setTopic("topic");
            }
        );
    }

    @Test
    public void testRecordInfo () {

        TestUtil.util.doTest(
            topic,
            () -> {
                topic.setRecordInfo(new RecordInfo ());
            }
        );
    }
}
