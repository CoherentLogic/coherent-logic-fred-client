package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IdentifyResponseTest {

    private IdentifyResponse identifyResponse;

    @Before
    public void before () {
        identifyResponse = new IdentifyResponse ();
    }

    @After
    public void after () {
        identifyResponse = null;
    }

    @Test
    public void testSetIdentify () throws Throwable {

        TestUtil.util.doTest(
            identifyResponse,
            () -> {
                identifyResponse.setIdentify (new Identify ());
            }
        );
    }
}
