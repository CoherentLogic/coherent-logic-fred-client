package com.coherentlogic.fraser.client.core.builders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * Unit test for the {@link com.coherentlogic.fraser.client.core.builders.QueryBuilder QueryBuilder} class.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class QueryBuilderTest {

    static final String FOO = "foo";

    private QueryBuilder queryBuilder;

    @Before
    public void beforeTest () {
        queryBuilder = new QueryBuilder (null);
    }

    @After
    public void tearDown () {
        queryBuilder = null;
    }

    @Test
    public void testWithVerb() {

        String uri = queryBuilder.withVerb(FOO).getEscapedURI();

        assertEquals ("https://fraser.stlouisfed.org/oai?verb=foo", uri);
    }

    @Test
    public void testWithVerbAsIdentify() {

        String uri = queryBuilder.withVerbAsIdentify().getEscapedURI();

        assertEquals ("https://fraser.stlouisfed.org/oai?verb=Identify", uri);
    }

    @Test
    public void testWithVerbAsListMetadataFormats() {

        String uri = queryBuilder.withVerbAsListMetadataFormats().getEscapedURI();

        assertEquals ("https://fraser.stlouisfed.org/oai?verb=ListMetadataFormats", uri);
    }

    @Test
    public void testWithVerbAsListRecords() {

        String uri = queryBuilder.withVerbAsListRecords().getEscapedURI();

        assertEquals ("https://fraser.stlouisfed.org/oai?verb=ListRecords", uri);
    }

    @Test
    public void testWithVerbAsListIdentifiers() {

        String uri = queryBuilder.withVerbAsListIdentifiers().getEscapedURI();

        assertEquals ("https://fraser.stlouisfed.org/oai?verb=ListIdentifiers", uri);
    }

    @Test
    public void testWithVerbAsListSets() {

        String uri = queryBuilder.withVerbAsListSets().getEscapedURI();

        assertEquals ("https://fraser.stlouisfed.org/oai?verb=ListSets", uri);
    }

    @Test
    public void testWithVerbAsGetRecord() {

        String uri = queryBuilder.withVerbAsGetRecord().getEscapedURI();

        assertEquals ("https://fraser.stlouisfed.org/oai?verb=GetRecord", uri);
    }

    @Test
    public void testWithMetadataPrefix() {

        String uri = queryBuilder.withMetadataPrefix(FOO).getEscapedURI();

        assertEquals ("https://fraser.stlouisfed.org/oai?metadataPrefix=foo", uri);
    }

    @Test
    public void testWithMetadataPrefixAsMODS() {

        String uri = queryBuilder.withMetadataPrefixAsMODS().getEscapedURI();

        assertEquals ("https://fraser.stlouisfed.org/oai?metadataPrefix=MODS", uri);
    }

    @Test
    public void testWithSet() {

        String uri = queryBuilder.withSet("FOO").getEscapedURI();

        assertEquals ("https://fraser.stlouisfed.org/oai?set=FOO", uri);
    }

    @Test
    public void testWithResumptionToken() {

        String uri = queryBuilder.withResumptionToken("FOO").getEscapedURI();

        assertEquals ("https://fraser.stlouisfed.org/oai?resumptionToken=FOO", uri);
    }
}
