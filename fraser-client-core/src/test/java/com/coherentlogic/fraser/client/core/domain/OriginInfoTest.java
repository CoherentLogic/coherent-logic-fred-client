package com.coherentlogic.fraser.client.core.domain;

import java.util.ArrayList;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class OriginInfoTest {

    private OriginInfo originInfo;

    @Before
    public void before () {
        originInfo = new OriginInfo ();
    }

    @After
    public void after () {
        originInfo = null;
    }

    @Test
    public void testEdition () throws Throwable {

        TestUtil.util.doTest(
            originInfo,
            () -> {
                originInfo.setEdition ("edition");
            }
        );
    }

    @Test
    public void testPlace () throws Throwable {

        TestUtil.util.doTest(
            originInfo,
            () -> {
                originInfo.setPlace ("place");
            }
        );
    }

    @Test
    public void testIssuance () throws Throwable {

        TestUtil.util.doTest(
            originInfo,
            () -> {
                originInfo.setIssuance ("issuance");
            }
        );
    }

    @Test
    public void testDateOther () throws Throwable {

        TestUtil.util.doTest(
            originInfo,
            () -> {
                originInfo.setDateOther ("date");
            }
        );
    }

    @Test
    public void testSortDate () throws Throwable {

        TestUtil.util.doTest(
            originInfo,
            () -> {
                originInfo.setSortDate (new Date ());
            }
        );
    }

    @Test
    public void testFrequency () throws Throwable {

        TestUtil.util.doTest(
            originInfo,
            () -> {
                originInfo.setFrequency ("feequency");
            }
        );
    }

    @Test
    public void testPublisher () throws Throwable {

        TestUtil.util.doTest(
            originInfo,
            () -> {
                originInfo.setPublisher ("publisher");
            }
        );
    }

    @Test
    public void testDateIssuedList () throws Throwable {

        TestUtil.util.doTest(
            originInfo,
            () -> {
                originInfo.setDateIssuedList (new ArrayList<String> ());
            }
        );
    }

}
