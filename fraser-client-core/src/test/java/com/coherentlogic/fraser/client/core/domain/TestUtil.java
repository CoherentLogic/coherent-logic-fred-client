package com.coherentlogic.fraser.client.core.domain;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.atomic.AtomicBoolean;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

public class TestUtil {

    public static final TestUtil util = new TestUtil ();

    public void doTest (SerializableBean target, Runnable runnable) {

        final UnidirectionalAtomicBoolean flag = new UnidirectionalAtomicBoolean ();

        target.addPropertyChangeListener(
            (propertyChangeEvent) -> {
                flag.setAsTrue();
            }
        );

        runnable.run();

        assertTrue (flag.get());
    }

    static class UnidirectionalAtomicBoolean {

        final AtomicBoolean flag = new AtomicBoolean (false);

        void setAsTrue () {

            if (!flag.get ())
                flag.set (true);
            else
                throw new RuntimeException ("The flag has already been set to true.");
        }

        boolean get () {
            return flag.get();
        }
    }
}
