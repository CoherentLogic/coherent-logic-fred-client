package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GeographicTest {

    private Geographic geographic;

    @Before
    public void before () {
        geographic = new Geographic ();
    }

    @After
    public void after () {
        geographic = null;
    }

    @Test
    public void testSetAuthority () {

        TestUtil.util.doTest(
            geographic,
            () -> {
                geographic.setGeographic("geographic");
            }
        );
    }

    @Test
    public void testSetRecordInfo () throws Throwable {

        TestUtil.util.doTest(
            geographic,
            () -> {
                geographic.setRecordInfo(new RecordInfo ());
            }
        );
    }
}
