package com.coherentlogic.fraser.client.core.domain;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SubjectTest {

    private Subject subject;

    @Before
    public void before () {
        subject = new Subject ();
    }

    @After
    public void after () {
        subject = null;
    }

    @Test
    public void testThemeList () throws Throwable {

        TestUtil.util.doTest(
            subject,
            () -> {
                subject.setThemeList (new ArrayList<> ());
            }
        );
    }

    @Test
    public void testTopicList () throws Throwable {

        TestUtil.util.doTest(
            subject,
            () -> {
                subject.setTopicList (new ArrayList<> ());
            }
        );
    }

    @Test
    public void testTitleInfo () throws Throwable {

        TestUtil.util.doTest(
            subject,
            () -> {
                subject.setTitleInfo (new SubjectTitleInfo ());
            }
        );
    }

    @Test
    public void testGeographicList () throws Throwable {

        TestUtil.util.doTest(
            subject,
            () -> {
                subject.setGeographicList (new ArrayList<> ());
            }
        );
    }

    @Test
    public void testNameList () throws Throwable {

        TestUtil.util.doTest(
            subject,
            () -> {
                subject.setNameList (new ArrayList<> ());
            }
        );
    }

}
