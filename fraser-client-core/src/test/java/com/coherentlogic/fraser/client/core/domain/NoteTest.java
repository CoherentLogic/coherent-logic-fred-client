package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NoteTest {

    private Note note;

    @Before
    public void before () {
        note = new Note ();
    }

    @After
    public void after () {
        note = null;
    }

    @Test
    public void testType () throws Throwable {

        TestUtil.util.doTest(
            note,
            () -> {
                note.setType("type");
            }
        );
    }

    @Test
    public void testValue () throws Throwable {

        TestUtil.util.doTest(
            note,
            () -> {
                note.setValue("value");
            }
        );
    }
}
