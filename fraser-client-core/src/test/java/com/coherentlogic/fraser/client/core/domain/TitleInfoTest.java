package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TitleInfoTest {

    private TitleInfo titleInfo;

    @Before
    public void before () {
        titleInfo = new TitleInfo ();
    }

    @After
    public void after () {
        titleInfo = null;
    }

    @Test
    public void testTitle () throws Throwable {

        TestUtil.util.doTest(
            titleInfo,
            () -> {
                titleInfo.setTitle ("title");
            }
        );
    }
}
