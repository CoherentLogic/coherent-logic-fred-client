package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SubjectNameTest {

    private SubjectName subject;

    @Before
    public void before () {
        subject = new SubjectName ();
    }

    @After
    public void after () {
        subject = null;
    }

    @Test
    public void testName () throws Throwable {

        TestUtil.util.doTest(
            subject,
            () -> {
                subject.setName ("name");
            }
        );
    }

    @Test
    public void testRecordInfo () throws Throwable {

        TestUtil.util.doTest(
            subject,
            () -> {
                subject.setRecordInfo(new RecordInfo ());
            }
        );
    }
}
