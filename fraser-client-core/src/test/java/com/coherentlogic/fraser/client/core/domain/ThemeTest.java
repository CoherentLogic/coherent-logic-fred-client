package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ThemeTest {

    private Theme theme;

    @Before
    public void before () {
        theme = new Theme ();
    }

    @After
    public void after () {
        theme = null;
    }

    @Test
    public void testTheme () throws Throwable {

        TestUtil.util.doTest(
            theme,
            () -> {
                theme.setTheme ("theme");
            }
        );
    }

    @Test
    public void testRecordInfo () throws Throwable {

        TestUtil.util.doTest(
            theme,
            () -> {
                theme.setRecordInfo (new RecordInfo ());
            }
        );
    }
}
