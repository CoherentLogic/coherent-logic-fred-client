package com.coherentlogic.fraser.client.core.domain;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ListMetadataFormatsTest {

    private ListMetadataFormats listMetadataFormats;

    @Before
    public void before () {
        listMetadataFormats = new ListMetadataFormats ();
    }

    @After
    public void after () {
        listMetadataFormats = null;
    }

    @Test
    public void testHeaderList () throws Throwable {

        TestUtil.util.doTest(
            listMetadataFormats,
            () -> {
                listMetadataFormats.setListMetadataFormatList(new ArrayList<MetadataFormat> ());
            }
        );
    }
}
