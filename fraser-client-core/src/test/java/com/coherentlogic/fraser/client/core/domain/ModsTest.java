package com.coherentlogic.fraser.client.core.domain;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ModsTest {

    private Mods mods;

    @Before
    public void before () {
        mods = new Mods ();
    }

    @After
    public void after () {
        mods = null;
    }

    @Test
    public void testNameList () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setNameList (new ArrayList<> ());
            }
        );
    }

    @Test
    public void testGenreList () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setGenreList (new ArrayList<String> ());
            }
        );
    }

    @Test
    public void testLanguage () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setLanguage ("en");
            }
        );
    }

    @Test
    public void testTitleInfoList () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setTitleInfoList (new ArrayList<ModsTitleInfo> ());
            }
        );
    }

    @Test
    public void testIdentifierList () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setIdentifierList (new ArrayList<Identifier> ());
            }
        );
    }

    @Test
    public void testOriginInfo () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setOriginInfo (new OriginInfo ());
            }
        );
    }

    @Test
    public void testRecordInfo () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setRecordInfo (new RecordInfo ());
            }
        );
    }

    @Test
    public void testNoteList () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setNoteList (new ArrayList<> ());
            }
        );
    }

    @Test
    public void testSubject () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setSubject (new Subject ());
            }
        );
    }

    @Test
    public void testAbstract () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setAbstract ("abstract");
            }
        );
    }

    @Test
    public void testRelatedItemList () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setRelatedItemList (new ArrayList<RelatedItem> ());
            }
        );
    }

    @Test
    public void testClassificationList () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setClassificationList (new ArrayList<Classification> ());
            }
        );
    }

    @Test
    public void testTypeOfResource () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setTypeOfResource ("tor");
            }
        );
    }

    @Test
    public void testPhysicalDescription () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setPhysicalDescription (new PhysicalDescription ());
            }
        );
    }

    @Test
    public void testLocation () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setLocation (new Location ());
            }
        );
    }

    @Test
    public void testAccessCondition () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setAccessCondition ("ac");
            }
        );
    }

    @Test
    public void testTableOfContentsList () throws Throwable {

        TestUtil.util.doTest(
            mods,
            () -> {
                mods.setTableOfContentsList (new ArrayList<TableOfContents> ());
            }
        );
    }
}
