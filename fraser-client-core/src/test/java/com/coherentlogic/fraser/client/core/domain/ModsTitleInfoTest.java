package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ModsTitleInfoTest {

    private ModsTitleInfo modsTitleInfo;

    @Before
    public void before () {
        modsTitleInfo = new ModsTitleInfo ();
    }

    @After
    public void after () {
        modsTitleInfo = null;
    }

    @Test
    public void testSetTitle () throws Throwable {

        TestUtil.util.doTest(
            modsTitleInfo,
            () -> {
                modsTitleInfo.setTitle ("title");
            }
        );
    }

    @Test
    public void testSetSubTitle () throws Throwable {

        TestUtil.util.doTest(
            modsTitleInfo,
            () -> {
                modsTitleInfo.setSubTitle("subtitle");
            }
        );
    }
}
