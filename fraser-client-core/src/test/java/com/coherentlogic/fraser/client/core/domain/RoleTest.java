package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RoleTest {

    private Role role;

    @Before
    public void before () {
        role = new Role ();
    }

    @After
    public void after () {
        role = null;
    }

    @Test
    public void testRoleTerm () throws Throwable {

        TestUtil.util.doTest(
            role,
            () -> {
                role.setRoleTerm("roleTerm");
            }
        );
    }
}
