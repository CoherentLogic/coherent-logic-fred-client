package com.coherentlogic.fraser.client.core.domain;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ListRecordsTest {

    private ListRecords listRecords;

    @Before
    public void before () {
        listRecords = new ListRecords ();
    }

    @After
    public void after () {
        listRecords = null;
    }

    @Test
    public void testRecordList () throws Throwable {

        TestUtil.util.doTest(
            listRecords,
            () -> {
                listRecords.setRecordList (new ArrayList<Record> ());
            }
        );
    }

    @Test
    public void testResumptionToken () throws Throwable {

        TestUtil.util.doTest(
            listRecords,
            () -> {
                listRecords.setResumptionToken (new ResumptionToken ());
            }
        );
    }
}
