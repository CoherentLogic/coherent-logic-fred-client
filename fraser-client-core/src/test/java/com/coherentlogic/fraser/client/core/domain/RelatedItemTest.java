package com.coherentlogic.fraser.client.core.domain;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RelatedItemTest {

    private RelatedItem relatedItem;

    @Before
    public void before () {
        relatedItem = new RelatedItem ();
    }

    @After
    public void after () {
        relatedItem = null;
    }

    @Test
    public void testType () throws Throwable {

        TestUtil.util.doTest(
            relatedItem,
            () -> {
                relatedItem.setType ("type");
            }
        );
    }

    @Test
    public void testTitleInfoList () throws Throwable {

        TestUtil.util.doTest(
            relatedItem,
            () -> {
                relatedItem.setTitleInfoList (new ArrayList<ModsTitleInfo> ());
            }
        );
    }

    @Test
    public void testRecordInfo () throws Throwable {

        TestUtil.util.doTest(
            relatedItem,
            () -> {
                relatedItem.setRecordInfo (new RecordInfo ());
            }
        );
    }

    @Test
    public void testTypeOfResource () throws Throwable {

        TestUtil.util.doTest(
            relatedItem,
            () -> {
                relatedItem.setTypeOfResource ("typeOfResource");
            }
        );
    }

    @Test
    public void testSortOrder () throws Throwable {

        TestUtil.util.doTest(
            relatedItem,
            () -> {
                relatedItem.setSortOrder("someSortOrder");
            }
        );
    }
}
