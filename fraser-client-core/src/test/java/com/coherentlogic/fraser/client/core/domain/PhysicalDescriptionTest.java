package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PhysicalDescriptionTest {

    private PhysicalDescription physicalDescription;

    @Before
    public void before () {
        physicalDescription = new PhysicalDescription ();
    }

    @After
    public void after () {
        physicalDescription = null;
    }

    @Test
    public void testForm () throws Throwable {

        TestUtil.util.doTest(
            physicalDescription,
            () -> {
                physicalDescription.setForm ("form");
            }
        );
    }

    @Test
    public void testExtent () throws Throwable {

        TestUtil.util.doTest(
            physicalDescription,
            () -> {
                physicalDescription.setExtent ("extent");
            }
        );
    }

    @Test
    public void testDigitalOrigin () throws Throwable {

        TestUtil.util.doTest(
            physicalDescription,
            () -> {
                physicalDescription.setDigitalOrigin ("digitalOrigin");
            }
        );
    }

    @Test
    public void testInternetMediaType () throws Throwable {

        TestUtil.util.doTest(
            physicalDescription,
            () -> {
                physicalDescription.setInternetMediaType ("internetMediaType");
            }
        );
    }
}
