package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MetaDataTest {

    private MetaData metaData;

    @Before
    public void before () {
        metaData = new MetaData ();
    }

    @After
    public void after () {
        metaData = null;
    }

    @Test
    public void testSetMods () throws Throwable {

        TestUtil.util.doTest(
            metaData,
            () -> {
                metaData.setMods (new Mods ());
            }
        );
    }
}
