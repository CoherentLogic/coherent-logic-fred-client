package com.coherentlogic.fraser.client.core.domain;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class NameTest {

    private Name name;

    @Before
    public void before () {
        name = new Name ();
    }

    @After
    public void after () {
        name = null;
    }

    @Test
    public void testRole () throws Throwable {

        TestUtil.util.doTest(
            name,
            () -> {
                name.setRole (new Role ());
            }
        );
    }

    @Test
    public void testNamePartList () throws Throwable {

        TestUtil.util.doTest(
            name,
            () -> {
                name.setNamePartList (new ArrayList<String> ());
            }
        );
    }

    @Test
    public void testRecordInfo () throws Throwable {

        TestUtil.util.doTest(
            name,
            () -> {
                name.setRecordInfo (new RecordInfo ());
            }
        );
    }

    @Test
    public void testName () throws Throwable {

        TestUtil.util.doTest(
            name,
            () -> {
                name.setName ("name");
            }
        );
    }
}
