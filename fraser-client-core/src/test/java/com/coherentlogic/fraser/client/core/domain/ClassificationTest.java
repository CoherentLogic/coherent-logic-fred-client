package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ClassificationTest {

    private Classification classification;

    @Before
    public void before () {
        classification = new Classification ();
    }

    @After
    public void after () {
        classification = null;
    }

    @Test
    public void testSetAuthority () throws Throwable {

        TestUtil.util.doTest(
            classification,
            () -> {
                classification.setAuthority("test");
            }
        );
    }

    @Test
    public void testSetValue () throws Throwable {

        TestUtil.util.doTest(
            classification,
            () -> {
                classification.setValue("test");
            }
        );
    }
}
