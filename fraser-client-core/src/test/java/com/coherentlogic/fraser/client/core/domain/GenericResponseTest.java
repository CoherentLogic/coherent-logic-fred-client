package com.coherentlogic.fraser.client.core.domain;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GenericResponseTest {

    private GenericResponse genericResponse;

    @Before
    public void before () {
        genericResponse = new GenericResponse ();
    }

    @After
    public void after () {
        genericResponse = null;
    }

    @Test
    public void testSetResponseDate () throws Throwable {

        TestUtil.util.doTest(
            genericResponse,
            () -> {
                genericResponse.setResponseDate(new Date());
            }
        );
    }

    @Test
    public void testSetRequest () throws Throwable {

        TestUtil.util.doTest(
            genericResponse,
            () -> {
                genericResponse.setRequest(new Request ());
            }
        );
    }

    @Test
    public void testSetContent () throws Throwable {

        TestUtil.util.doTest(
            genericResponse,
            () -> {
                genericResponse.setContent(new Mods ());
            }
        );
    }
}
