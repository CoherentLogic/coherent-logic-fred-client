package com.coherentlogic.fraser.client.core.domain;

import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Test;

/**
 * 
 *
 */
public class LocationTest {

    @Test
    public void testSetUrls () throws MalformedURLException {

        final AtomicBoolean flag = new AtomicBoolean (false);

        Location location = new Location ();

        location.addPropertyChangeListener(
            (propertyChangeEvent) -> {
                flag.set(true);
            }
        );

        location.setUrls(Arrays.asList(new URL ("http://coherentlogic.com")));

        assertTrue (flag.get());
    }
}
