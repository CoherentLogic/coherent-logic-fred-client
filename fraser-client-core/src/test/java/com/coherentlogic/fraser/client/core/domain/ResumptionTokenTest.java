package com.coherentlogic.fraser.client.core.domain;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ResumptionTokenTest {

    private ResumptionToken resumptionToken;

    @Before
    public void before () {
        resumptionToken = new ResumptionToken ();
    }

    @After
    public void after () {
        resumptionToken = null;
    }

    @Test
    public void testExpirationDate () throws Throwable {

        TestUtil.util.doTest(
            resumptionToken,
            () -> {
                resumptionToken.setExpirationDate (new Date ());
            }
        );
    }

    @Test
    public void testCompleteListSize () throws Throwable {

        TestUtil.util.doTest(
            resumptionToken,
            () -> {
                resumptionToken.setCompleteListSize (1031);
            }
        );
    }

    @Test
    public void testCursor () throws Throwable {

        TestUtil.util.doTest(
            resumptionToken,
            () -> {
                resumptionToken.setCursor (567);
            }
        );
    }

    @Test
    public void testValue () throws Throwable {

        TestUtil.util.doTest(
            resumptionToken,
            () -> {
                resumptionToken.setValue ("some value");
            }
        );
    }
}
