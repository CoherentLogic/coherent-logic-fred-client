package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ListSetTest {

    private ListSet listSet;

    @Before
    public void before () {
        listSet = new ListSet ();
    }

    @After
    public void after () {
        listSet = null;
    }

    @Test
    public void testSetSpec () throws Throwable {

        TestUtil.util.doTest(
            listSet,
            () -> {
                listSet.setSetSpec ("setSpec");
            }
        );
    }

    @Test
    public void testSetName () throws Throwable {

        TestUtil.util.doTest(
            listSet,
            () -> {
                listSet.setSetName ("name");
            }
        );
    }
}
