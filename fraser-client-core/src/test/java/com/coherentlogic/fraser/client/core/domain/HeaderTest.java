package com.coherentlogic.fraser.client.core.domain;

import java.util.ArrayList;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 */
public class HeaderTest {

    private Header header;

    @Before
    public void before () {
        header = new Header ();
    }

    @After
    public void after () {
        header = null;
    }

    @Test
    public void testSetIdentifier () throws Throwable {

        TestUtil.util.doTest(
            header,
            () -> {
                header.setIdentifier("test");
            }
        );
    }

    @Test
    public void testSetSpec () throws Throwable {

        TestUtil.util.doTest(
            header,
            () -> {
                header.setSetSpec(new ArrayList<String> ());
            }
        );
    }

    @Test
    public void testDatestamp () throws Throwable {

        TestUtil.util.doTest(
            header,
            () -> {
                header.setDatestamp(new Date ());
            }
        );
    }
}
