package com.coherentlogic.fraser.client.core.domain;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ListSetsTest {

    private ListSets listSets;

    @Before
    public void before () {
        listSets = new ListSets ();
    }

    @After
    public void after () {
        listSets = null;
    }

    @Test
    public void testResumptionToken () throws Throwable {

        TestUtil.util.doTest(
            listSets,
            () -> {
                listSets.setResumptionToken (new ResumptionToken());
            }
        );
    }

    @Test
    public void testListSetList () throws Throwable {

        TestUtil.util.doTest(
            listSets,
            () -> {
                listSets.setListSetList (new ArrayList<ListSet> ());
            }
        );
    }
}
