package com.coherentlogic.fraser.client.core.domain;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TableOfContentsTest {

    private TableOfContents tableOfContents;

    @Before
    public void before () {
        tableOfContents = new TableOfContents ();
    }

    @After
    public void after () {
        tableOfContents = null;
    }

    @Test
    public void testExtent () throws Throwable {

        TestUtil.util.doTest(
            tableOfContents,
            () -> {
                tableOfContents.setExtent (new Extent ());
            }
        );
    }

    @Test
    public void testTitleInfo () throws Throwable {

        TestUtil.util.doTest(
            tableOfContents,
            () -> {
                tableOfContents.setTitleInfo (new TitleInfo ());
            }
        );
    }

    @Test
    public void testRecordInfoList () throws Throwable {

        TestUtil.util.doTest(
            tableOfContents,
            () -> {
                tableOfContents.setRecordInfoList (new ArrayList<> ());
            }
        );
    }
}
