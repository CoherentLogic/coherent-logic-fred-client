package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IdentifierTest {

    private Identifier identifier;

    @Before
    public void before () {
        identifier = new Identifier ();
    }

    @After
    public void after () {
        identifier = null;
    }

    @Test
    public void testSetType () throws Throwable {

        TestUtil.util.doTest(
            identifier,
            () -> {
                identifier.setType("type");
            }
        );
    }

    @Test
    public void testSetValue () throws Throwable {

        TestUtil.util.doTest(
            identifier,
            () -> {
                identifier.setValue("test");
            }
        );
    }
}
