package com.coherentlogic.fraser.client.core.domain;

import java.util.ArrayList;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RecordInfoTest {

    private RecordInfo recordInfo;

    @Before
    public void before () {
        recordInfo = new RecordInfo ();
    }

    @After
    public void after () {
        recordInfo = null;
    }

    @Test
    public void testRecordIdentifierList () throws Throwable {

        TestUtil.util.doTest(
            recordInfo,
            () -> {
                recordInfo.setRecordIdentifierList (new ArrayList<Integer> ());
            }
        );
    }

    @Test
    public void testRecordUpdatedDate () throws Throwable {

        TestUtil.util.doTest(
            recordInfo,
            () -> {
                recordInfo.setRecordUpdatedDate (new Date ());
            }
        );
    }

    @Test
    public void testRecordCreationDate () throws Throwable {

        TestUtil.util.doTest(
            recordInfo,
            () -> {
                recordInfo.setRecordCreationDate (new Date ());
            }
        );
    }

    @Test
    public void testRecordContentSource () throws Throwable {

        TestUtil.util.doTest(
            recordInfo,
            () -> {
                recordInfo.setRecordContentSource ("rcs");
            }
        );
    }
}
