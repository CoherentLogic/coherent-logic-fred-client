package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MetadataFormatTest {

    private MetadataFormat metadataFormat;

    @Before
    public void before () {
        metadataFormat = new MetadataFormat ();
    }

    @After
    public void after () {
        metadataFormat = null;
    }

    @Test
    public void testMetadataPrefix () throws Throwable {

        TestUtil.util.doTest(
            metadataFormat,
            () -> {
                metadataFormat.setMetadataPrefix ("prefix");
            }
        );
    }

    @Test
    public void testSchema () throws Throwable {

        TestUtil.util.doTest(
            metadataFormat,
            () -> {
                metadataFormat.setSchema ("schema");
            }
        );
    }

    @Test
    public void testMetadataNamespace () throws Throwable {

        TestUtil.util.doTest(
            metadataFormat,
            () -> {
                metadataFormat.setMetadataNamespace ("namespace");
            }
        );
    }
}
