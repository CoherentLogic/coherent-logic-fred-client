package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ErrorResponseTest {

    private ErrorResponse errorResponse;

    @Before
    public void before() throws Exception {
        errorResponse = new ErrorResponse();
    }

    @After
    public void after() throws Exception {
        errorResponse = null;
    }

    @Test
    public void testSetCode() {
        TestUtil.util.doTest(
            errorResponse,
            () -> {
                errorResponse.setCode("test");
            }
        );
    }

    @Test
    public void testSetValue() {
        TestUtil.util.doTest(
            errorResponse,
            () -> {
                errorResponse.setValue("test");
            }
        );
    }
}
