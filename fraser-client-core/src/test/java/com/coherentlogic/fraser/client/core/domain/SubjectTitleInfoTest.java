package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SubjectTitleInfoTest {

    private SubjectTitleInfo subjectTitleInfo;

    @Before
    public void before () {
        subjectTitleInfo = new SubjectTitleInfo ();
    }

    @After
    public void after () {
        subjectTitleInfo = null;
    }

    @Test
    public void testTitleInfo () throws Throwable {

        TestUtil.util.doTest(
            subjectTitleInfo,
            () -> {
                subjectTitleInfo.setTitleInfo ("titleInfo");
            }
        );
    }

    @Test
    public void testRecordInfo () throws Throwable {

        TestUtil.util.doTest(
            subjectTitleInfo,
            () -> {
                subjectTitleInfo.setRecordInfo(new RecordInfo ());
            }
        );
    }
}
