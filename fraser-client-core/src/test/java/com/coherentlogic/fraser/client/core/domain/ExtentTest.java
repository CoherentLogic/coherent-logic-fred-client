package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExtentTest {

    private Extent extent;

    @Before
    public void before() throws Exception {
        extent = new Extent ();
    }

    @After
    public void after() throws Exception {
        extent = null;
    }

    @Test
    public void testSetStartPage() {
        TestUtil.util.doTest(
            extent,
            () -> {
                extent.setStartPage(111);
            }
        );
    }

    @Test
    public void testSetEndPage() {
        TestUtil.util.doTest(
            extent,
            () -> {
                extent.setEndPage(111);
            }
        );
    }
}
