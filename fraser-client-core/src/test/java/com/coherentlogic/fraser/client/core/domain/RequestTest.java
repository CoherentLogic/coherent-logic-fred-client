package com.coherentlogic.fraser.client.core.domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RequestTest {

    private Request request;

    @Before
    public void before () {
        request = new Request ();
    }

    @After
    public void after () {
        request = null;
    }

    @Test
    public void testVerb () throws Throwable {

        TestUtil.util.doTest(
            request,
            () -> {
                request.setVerb ("verb");
            }
        );
    }

    @Test
    public void testValue () throws Throwable {

        TestUtil.util.doTest(
            request,
            () -> {
                request.setValue ("value");
            }
        );
    }
}
