package com.coherentlogic.fraser.client.core.domain;

import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IdentifyTest {

    private Identify identify;

    @Before
    public void before () {
        identify = new Identify ();
    }

    @After
    public void after () {
        identify = null;
    }

    @Test
    public void testSetAdminEmail () throws Throwable {

        TestUtil.util.doTest(
            identify,
            () -> {
                identify.setAdminEmail("admin");
            }
        );
    }

    @Test
    public void testSetBaseURL () throws Throwable {

        TestUtil.util.doTest(
            identify,
            () -> {
                identify.setBaseURL("url");
            }
        );
    }

    @Test
    public void testSetDeletedRecord () throws Throwable {

        TestUtil.util.doTest(
            identify,
            () -> {
                identify.setDeletedRecord(true);
            }
        );
    }

    @Test
    public void testSetEarliestDatestamp () throws Throwable {

        TestUtil.util.doTest(
            identify,
            () -> {
                identify.setEarliestDatestamp(new Date ());
            }
        );
    }

    @Test
    public void testSetGranularity () throws Throwable {

        TestUtil.util.doTest(
            identify,
            () -> {
                identify.setGranularity("granularity");
            }
        );
    }

    @Test
    public void testSetRepositoryName () throws Throwable {

        TestUtil.util.doTest(
            identify,
            () -> {
                identify.setRepositoryName("name");
            }
        );
    }

    @Test
    public void testProtocolVersion () throws Throwable {

        TestUtil.util.doTest(
            identify,
            () -> {
                identify.setProtocolVersion ("pv");
            }
        );
    }
}
