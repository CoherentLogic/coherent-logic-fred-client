package com.coherentlogic.fraser.client.core.domain;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ListIdentifiersTest {

    private ListIdentifiers listIdentifiers;

    @Before
    public void before () {
        listIdentifiers = new ListIdentifiers ();
    }

    @After
    public void after () {
        listIdentifiers = null;
    }

    @Test
    public void testHeaderList () throws Throwable {

        TestUtil.util.doTest(
            listIdentifiers,
            () -> {
                listIdentifiers.setHeaderList (new ArrayList<Header> ());
            }
        );
    }

    @Test
    public void testResumptionToken () throws Throwable {

        TestUtil.util.doTest(
            listIdentifiers,
            () -> {
                listIdentifiers.setResumptionToken (new ResumptionToken ());
            }
        );
    }
}
