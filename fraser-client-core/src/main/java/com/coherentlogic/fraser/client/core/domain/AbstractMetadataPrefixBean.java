package com.coherentlogic.fraser.client.core.domain;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;

/**
 * @todo Refactor the event notification s.t. method params are changeable.
 *
 */
@Visitable
public abstract class AbstractMetadataPrefixBean extends SerializableBean {

    private static final long serialVersionUID = -2206846126934727801L;

    public static final String METADATA_PREFIX = "metadataPrefix";

    @Visitable
    private String metadataPrefix;

    public String getMetadataPrefix() {
        return metadataPrefix;
    }

    public void setMetadataPrefix(String metadataPrefix) {

        String oldValue = this.metadataPrefix;

        this.metadataPrefix = metadataPrefix;

        firePropertyChange(METADATA_PREFIX, oldValue, metadataPrefix);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((metadataPrefix == null) ? 0 : metadataPrefix.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        AbstractMetadataPrefixBean other = (AbstractMetadataPrefixBean) obj;
        if (metadataPrefix == null) {
            if (other.metadataPrefix != null)
                return false;
        } else if (!metadataPrefix.equals(other.metadataPrefix))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "MetadataPrefixAware [metadataPrefix=" + metadataPrefix + "]";
    }
}
