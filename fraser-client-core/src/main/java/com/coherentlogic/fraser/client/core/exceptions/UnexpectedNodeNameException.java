package com.coherentlogic.fraser.client.core.exceptions;

import org.springframework.core.NestedRuntimeException;

/**
 * Thrown when converting XML to an instance of any domain class while unmarshalling a node is found with a name that
 * the {@link com.thoughtworks.xstream.converters.Converter} is not expecting.
 *
 * @TODO Move this to the CDA xstream module.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class UnexpectedNodeNameException extends NestedRuntimeException {

    private static final long serialVersionUID = 6507698540466996165L;

    public UnexpectedNodeNameException(String msg) {
        super(msg);
    }

    public UnexpectedNodeNameException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
