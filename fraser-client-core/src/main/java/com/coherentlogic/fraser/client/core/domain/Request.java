package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.fraser.client.core.converters.RequestConverter;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * <request verb="ListRecords" metadataPrefix="mods">https://fraser.stlouisfed.org/oai</request>
 *
 */
@Entity
@Table(name=Request.REQUEST_TABLE)
@XStreamConverter(RequestConverter.class)
@Visitable
public class Request extends AbstractMetadataPrefixBean {

    private static final long serialVersionUID = -3615478701991840181L;

    public static final String
        REQUEST = "request",
        REQUEST_TABLE = "identifyRequest",
        VERB = "verb",
        VALUE = "value";

    @Visitable
    private String verb;

    @Visitable
    private String value;

    public String getVerb() {
        return verb;
    }

    public void setVerb(@Changeable (VERB) String verb) {
        this.verb = verb;
    }

    public String getValue() {
        return value;
    }

    public void setValue(@Changeable (VALUE) String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        result = prime * result + ((verb == null) ? 0 : verb.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Request other = (Request) obj;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        if (verb == null) {
            if (other.verb != null)
                return false;
        } else if (!verb.equals(other.verb))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Request [verb=" + verb + ", value=" + value + "]";
    }
}
