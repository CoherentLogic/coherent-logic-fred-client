package com.coherentlogic.fraser.client.core.exceptions;

import org.springframework.core.NestedRuntimeException;

import com.coherentlogic.fraser.client.core.domain.GenericResponse;

/**
 * Thrown when the FRASER web service returns an error, ie what we have below.
 *
 * <?xml version="1.0" encoding="UTF-8"?>
 * <OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/">
 *     <responseDate>2017-07-03T02:19:04Z</responseDate>
 *     <request>https://fraser.stlouisfed.org/oai</request>
 *     <error
 *      code="idDoesNotExist">The value of the identifier argument is unknown or illegal in this repository.</error>
 * </OAI-PMH>
 */
public class QueryFailedException extends NestedRuntimeException {

    private static final long serialVersionUID = 8851757018127140772L;

    private final GenericResponse genericResponse;

    public QueryFailedException (GenericResponse genericResponse) {

        super ("The call to the FRASER web service returned an error; genericResponse: " + genericResponse);

        this.genericResponse = genericResponse;
    }

    public GenericResponse getGenericResponse () {
        return genericResponse;
    }

    @Override
    public String toString() {
        return "QueryFailedException [genericResponse=" + genericResponse + "]";
    }
}
