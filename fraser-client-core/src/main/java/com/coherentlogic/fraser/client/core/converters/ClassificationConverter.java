package com.coherentlogic.fraser.client.core.converters;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.coherentlogic.coherent.data.adapter.core.xstream.exceptions.MarshalMethodNotSupportedException;
import com.coherentlogic.fraser.client.core.domain.Classification;

/**
 * <classification authority="sudocs">Y 4.B 22/1:P 93/4/v.1-2</classification>
 *
 */
public class ClassificationConverter implements Converter {

    @Override
    public boolean canConvert(Class type) {
        return Classification.class.equals(type);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MarshalMethodNotSupportedException ();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        Classification result = new Classification ();

        String authority = reader.getAttribute(Classification.AUTHORITY);

        result.setAuthority(authority);

        String value = reader.getValue();

        result.setValue(value);

        return result;
    }
}
