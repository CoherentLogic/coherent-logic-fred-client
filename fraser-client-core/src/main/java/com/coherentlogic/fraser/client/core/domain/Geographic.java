package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * <geographic>
 *     <geographic>United States</geographic>
 *     <recordInfo>
 *         <recordIdentifier>4293</recordIdentifier>
 *     </recordInfo>
 * </geographic>
 */
@Entity
@Table(name=Geographic.GEOGRAPHIC)
@XStreamAlias(Geographic.GEOGRAPHIC)
@Visitable
public class Geographic extends SerializableBean {

    private static final long serialVersionUID = 2761095668217307770L;

    static final String GEOGRAPHIC = "geographic";

    @XStreamAlias(GEOGRAPHIC)
    @Visitable
    private String geographic;

    @XStreamAlias(RecordInfo.RECORD_INFO)
    @Visitable
    private RecordInfo recordInfo;

    public String getGeographic() {
        return geographic;
    }

    public void setGeographic(@Changeable (GEOGRAPHIC) String geographic) {
        this.geographic = geographic;
    }

    public RecordInfo getRecordInfo() {
        return recordInfo;
    }

    public void setRecordInfo(@Changeable (RecordInfo.RECORD_INFO) RecordInfo recordInfo) {
        this.recordInfo = recordInfo;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((geographic == null) ? 0 : geographic.hashCode());
        result = prime * result + ((recordInfo == null) ? 0 : recordInfo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Geographic other = (Geographic) obj;
        if (geographic == null) {
            if (other.geographic != null)
                return false;
        } else if (!geographic.equals(other.geographic))
            return false;
        if (recordInfo == null) {
            if (other.recordInfo != null)
                return false;
        } else if (!recordInfo.equals(other.recordInfo))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Geographic [geographic=" + geographic + ", recordInfo=" + recordInfo + "]";
    }
}
