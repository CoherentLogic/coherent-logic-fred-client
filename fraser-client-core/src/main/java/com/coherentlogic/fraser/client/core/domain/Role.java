package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 * <role>
 *     <roleTerm>creator</roleTerm>
 * </role>
 *
 */
@Entity
@Table(name=Role.ROLE)
@XStreamAlias(Role.ROLE)
@Visitable
public class Role extends SerializableBean {

    private static final long serialVersionUID = -4892476370915034812L;

    static final String ROLE = "role", ROLE_TERM = "roleTerm";

    @XStreamAlias(ROLE_TERM)
    @Visitable
    private String roleTerm;

    public String getRoleTerm() {
        return roleTerm;
    }

    public void setRoleTerm(@Changeable (ROLE_TERM) String roleTerm) {
        this.roleTerm = roleTerm;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((roleTerm == null) ? 0 : roleTerm.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Role other = (Role) obj;
        if (roleTerm == null) {
            if (other.roleTerm != null)
                return false;
        } else if (!roleTerm.equals(other.roleTerm))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Role [roleTerm=" + roleTerm + "]";
    }
}
