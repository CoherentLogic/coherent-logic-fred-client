package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * 
 * <note type="public">
 *     An account of the history of the Federal Reserve System in general and of the Federal Reserve Bank of New York.
 * </note>
 *
 */
@Entity
@Table(name=Note.NOTE)
@XStreamAlias(Note.NOTE)
@Visitable
public class Note extends SerializableBean implements ValueSpecification<String> {

    private static final long serialVersionUID = -4690129293871357816L;

    static final String
        NOTE = "note",
        TYPE = "type",
        VALUE = "value";

    @XStreamAlias(TYPE)
    @XStreamAsAttribute
    @Visitable
    private String type;

    @XStreamAlias(NOTE)
    @Visitable
    private String value;

    @Override
    public String getValue() {
        return null;
    }

    @Override
    public void setValue(@Changeable (VALUE) String value) {
        this.value = value;
    }

    public String getType() {
        return type;
    }

    public void setType(@Changeable (TYPE) String type) {
        this.type = type;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Note other = (Note) obj;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Note [type=" + type + ", value=" + value + "]";
    }
}
