package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 
 * <set>
 *     <setSpec>author</setSpec>
 *     <setName>Authors</setName>
 * </set>
 *
 */
@Entity
@Table(name=ListSet.SET)
@XStreamAlias(ListSet.SET)
@Visitable
public class ListSet extends SerializableBean {

    private static final long serialVersionUID = -1531212176457942738L;

    public static final String SET = "set", SET_SPEC = "setSpec", SET_NAME = "setName";

    @XStreamAlias(SET_SPEC)
    @Visitable
    private String setSpec;

    @XStreamAlias(SET_NAME)
    @Visitable
    private String setName;

    public String getSetSpec() {
        return setSpec;
    }

    public void setSetSpec(@Changeable (SET_SPEC) String setSpec) {
        this.setSpec = setSpec;
    }

    public String getSetName() {
        return setName;
    }

    public void setSetName(@Changeable (SET_NAME) String setName) {
        this.setName = setName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((setName == null) ? 0 : setName.hashCode());
        result = prime * result + ((setSpec == null) ? 0 : setSpec.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ListSet other = (ListSet) obj;
        if (setName == null) {
            if (other.setName != null)
                return false;
        } else if (!setName.equals(other.setName))
            return false;
        if (setSpec == null) {
            if (other.setSpec != null)
                return false;
        } else if (!setSpec.equals(other.setSpec))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ListSet [setSpec=" + setSpec + ", setName=" + setName + "]";
    }
}
