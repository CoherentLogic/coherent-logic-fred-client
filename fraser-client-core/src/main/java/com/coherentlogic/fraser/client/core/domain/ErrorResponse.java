package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.fraser.client.core.converters.ErrorResponseConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/*
 * Pass the wrong url and this is what's returned:
 *
 * https://fraser.stlouisfed.org/oai?verb=Identify?verb=Identify
 * 
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/">
<responseDate>2017-06-14T03:13:18Z</responseDate>
<request>https://fraser.stlouisfed.org/oai</request>
<error code="badVerb">
Value of the verb argument is not a legal OAI-PMH verb or the verb argument is missing.
</error>
</OAI-PMH>
 */
@Entity
@Table(name=ErrorResponse.FRASER_WEB_SERVICE_ERROR_TABLE)
@XStreamAlias(ErrorResponse.ERROR)
@XStreamConverter(ErrorResponseConverter.class)
@Visitable
public class ErrorResponse extends SerializableBean {

    private static final long serialVersionUID = 825609712074143743L;

    public static final String
        FRASER_WEB_SERVICE_ERROR_TABLE = "FRASER_WEB_SERVICE_ERROR_TABLE",
        ERROR = "error",
        CODE = "code",
        VALUE = "value";

    @Visitable
    private String code;

    @Visitable
    private String value;

    public String getCode() {
        return code;
    }

    public void setCode(@Changeable (CODE) String code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(@Changeable (VALUE) String errorValue) {
        this.value = errorValue;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ErrorResponse other = (ErrorResponse) obj;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ErrorResponse [code=" + code + ", value=" + value + "]";
    }
}
