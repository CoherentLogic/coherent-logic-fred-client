package com.coherentlogic.fraser.client.core.converters;

import com.coherentlogic.coherent.data.adapter.core.xstream.exceptions.MarshalMethodNotSupportedException;
import com.coherentlogic.fraser.client.core.domain.Identifier;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * 
 * @author thospfuller
 */
public class IdentifierConverter implements Converter {

    @Override
    public boolean canConvert(Class type) {
        return Identifier.class.equals(type);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MarshalMethodNotSupportedException ();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        Identifier result = new Identifier ();

        String type = reader.getAttribute(Identifier.TYPE);

        result.setType(type);

        String value = reader.getValue();

        result.setValue(value);

        return result;
    }
}
