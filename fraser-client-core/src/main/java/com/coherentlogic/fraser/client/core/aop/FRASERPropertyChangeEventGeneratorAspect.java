package com.coherentlogic.fraser.client.core.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import com.coherentlogic.coherent.data.adapter.aop.aspects.AbstractPropertyChangeGenerator;

/**
 * @see http://janistoolbox.typepad.com/blog/2011/11/aspectjload-time-weavingabstract-aspect.html
 * http://www.aspectprogrammer.org/blogs/adrian/2004/05/integrating_asp.html
 * 
 * <T extends SerializableBean>
 * 
 * 
 *
 * @param <T>
 */
@Aspect
public class FRASERPropertyChangeEventGeneratorAspect extends AbstractPropertyChangeGenerator {

    @Around("execution(* com.coherentlogic.fraser.client.core.domain.*.set*(..))")
    @Override
    public void onSetterMethodCalled(ProceedingJoinPoint joinPoint) {
        super.onSetterMethodCalled(joinPoint);
    }
}
