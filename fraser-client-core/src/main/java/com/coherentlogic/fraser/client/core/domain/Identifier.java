package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.fraser.client.core.converters.IdentifierConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * <identifier type="oclc">20860421</identifier>
 *
 */
@Entity
@Table(name=Identifier.IDENTIFIER)
@XStreamAlias(Identifier.IDENTIFIER)
@XStreamConverter(IdentifierConverter.class)
@Visitable
public class Identifier extends SerializableBean implements ValueSpecification<String> {

    private static final long serialVersionUID = -1835503077190828304L;

    public static final String
        IDENTIFIER = "identifier",
        TYPE = "type";

    @XStreamAlias(TYPE)
    @XStreamAsAttribute
    @Visitable
    private String type;

    @XStreamAlias(TYPE)
    @Visitable
    private String value;

    public String getType() {
        return type;
    }

    public void setType(@Changeable (TYPE) String type) {
        this.type = type;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(@Changeable (VALUE) String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Identifier other = (Identifier) obj;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Identifier [type=" + type + ", value=" + value + "]";
    }
}
