package com.coherentlogic.fraser.client.core.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * <relatedItem type="series">
 *   <sortOrder>0772</sortOrder>
 *   <titleInfo>
 *     <title>Bulletin of the United States Bureau of Labor Statistics</title>
 *   </titleInfo>
 *   <titleInfo type="alternate">
 *     <title>BLS Bulletin</title>
 *   </titleInfo>
 *   <titleInfo type="alternate">
 *     <title>Bulletin of the United States Bureau of Labor</title>
 *   </titleInfo>
 *   <recordInfo>
 *     <recordIdentifier>1486</recordIdentifier>
 *   </recordInfo>
 * </relatedItem>
 */
@Entity
@Table(name=RelatedItem.RELATED_ITEM)
@XStreamAlias(RelatedItem.RELATED_ITEM)
@Visitable
public class RelatedItem extends SerializableBean {

    private static final long serialVersionUID = 7462085356277580626L;

    static final String
        RELATED_ITEM = "relatedItem",
        TYPE = "type",
        SORT_ORDER = "sortOrder",
        TITLE_INFO = "titleInfo",
        TITLE_INFO_LIST = "titleInfoList",
        TITLE = "title",
        RECORD_INFO = "recordInfo",
        TYPE_OF_RESOURCE = "typeOfResource";

    @XStreamAlias(TYPE)
    @XStreamAsAttribute
    @Visitable
    private String type;

    @XStreamAlias(SORT_ORDER)
    @Visitable
    private String sortOrder;

    @XStreamAlias(ModsTitleInfo.TITLE_INFO)
    @XStreamImplicit
    @Visitable
    private List<ModsTitleInfo> titleInfoList;

    @XStreamAlias(RecordInfo.RECORD_INFO)
    @Visitable
    private RecordInfo recordInfo;

    @XStreamAlias(TYPE_OF_RESOURCE)
    @Visitable
    private String typeOfResource;

    public String getType() {
        return type;
    }

    public void setType(@Changeable (TYPE) String type) {
        this.type = type;
    }

    public List<ModsTitleInfo> getTitleInfoList() {
        return titleInfoList;
    }

    public void setTitleInfoList(@Changeable (TITLE_INFO_LIST) List<ModsTitleInfo> titleInfoList) {
        this.titleInfoList = titleInfoList;
    }

    public RecordInfo getRecordInfo() {
        return recordInfo;
    }

    public void setRecordInfo(@Changeable (RECORD_INFO) RecordInfo recordInfo) {
        this.recordInfo = recordInfo;
    }

    public String getTypeOfResource() {
        return typeOfResource;
    }

    public void setTypeOfResource(@Changeable (TYPE_OF_RESOURCE) String typeOfResource) {
        this.typeOfResource = typeOfResource;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(@Changeable (SORT_ORDER) String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((recordInfo == null) ? 0 : recordInfo.hashCode());
        result = prime * result + ((sortOrder == null) ? 0 : sortOrder.hashCode());
        result = prime * result + ((titleInfoList == null) ? 0 : titleInfoList.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        result = prime * result + ((typeOfResource == null) ? 0 : typeOfResource.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        RelatedItem other = (RelatedItem) obj;
        if (recordInfo == null) {
            if (other.recordInfo != null)
                return false;
        } else if (!recordInfo.equals(other.recordInfo))
            return false;
        if (sortOrder == null) {
            if (other.sortOrder != null)
                return false;
        } else if (!sortOrder.equals(other.sortOrder))
            return false;
        if (titleInfoList == null) {
            if (other.titleInfoList != null)
                return false;
        } else if (!titleInfoList.equals(other.titleInfoList))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        if (typeOfResource == null) {
            if (other.typeOfResource != null)
                return false;
        } else if (!typeOfResource.equals(other.typeOfResource))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "RelatedItem [type=" + type + ", sortOrder=" + sortOrder + ", titleInfoList=" + titleInfoList
            + ", recordInfo=" + recordInfo + ", typeOfResource=" + typeOfResource + "]";
    }
}
