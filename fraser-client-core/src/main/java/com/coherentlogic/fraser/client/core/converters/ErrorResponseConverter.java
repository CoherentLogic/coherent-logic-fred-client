package com.coherentlogic.fraser.client.core.converters;

import java.time.Instant;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.xstream.exceptions.MarshalMethodNotSupportedException;
import com.coherentlogic.fraser.client.core.domain.ErrorResponse;
import com.coherentlogic.fraser.client.core.exceptions.QueryFailedException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Converts an error response from a call to a FRASER web service into an instance of {@link QueryFailedException}.
 *
 * <?xml version="1.0" encoding="UTF-8"?>
 * <OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/">
 *     <responseDate>2017-07-03T02:19:04Z</responseDate>
 *     <request>https://fraser.stlouisfed.org/oai</request>
 *     <error
 *      code="idDoesNotExist">The value of the identifier argument is unknown or illegal in this repository.</error>
 * </OAI-PMH>
 *
 * @see {@link com.coherentlogic.fraser.client.core.exceptions.QueryFailedException}
 */
public class ErrorResponseConverter implements Converter {

    private static final Logger log = LoggerFactory.getLogger(ErrorResponseConverter.class);

    @Override
    public boolean canConvert(Class type) {
        return ErrorResponse.class.equals(type);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MarshalMethodNotSupportedException ();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

    	log.info("unmarshal: method begins; reader: " + reader + ", context: " + context);

        ErrorResponse result = new ErrorResponse ();

        String errorCode = reader.getAttribute(ErrorResponse.CODE);

        String errorValue = reader.getValue();

        result.setCode(errorCode);

        result.setValue(errorValue);

        log.info("unmarshal: method ends; result: " + result);

        return result;
    }
}
