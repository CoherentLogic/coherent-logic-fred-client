package com.coherentlogic.fraser.client.core.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * 
 * <recordInfo>
 *     <recordIdentifier>770</recordIdentifier>
 *     <recordUpdatedDate>2017-02-15 15:28:11</recordUpdatedDate>
 *     <recordCreationDate>2017-02-14 15:37:34</recordCreationDate>
 *     <recordContentSource>frs</recordContentSource>
 * </recordInfo>
 */
@Entity
@Table(name=RecordInfo.RECORD_INFO)
@XStreamAlias(RecordInfo.RECORD_INFO)
@Visitable
public class RecordInfo extends SerializableBean {

    private static final long serialVersionUID = -7290479278435718925L;

    static final String
        RECORD_INFO = "recordInfo",
        RECORD_IDENTIFIER = "recordIdentifier",
        RECORD_IDENTIFIER_LIST = "recordIdentifierList",
        RECORD_UPDATED_DATE ="recordUpdatedDate",
        RECORD_CREATION_DATE = "recordCreationDate",
        RECORD_CONTENT_SOURCE="recordContentSource";

    @XStreamAlias(RECORD_IDENTIFIER)
    @XStreamImplicit
    @Visitable
    private List<Integer> recordIdentifierList;

    @XStreamAlias(RECORD_UPDATED_DATE)
    @Visitable
    private Date recordUpdatedDate;

    @XStreamAlias(RECORD_CREATION_DATE)
    @Visitable
    private Date recordCreationDate;

    @XStreamAlias(RECORD_CONTENT_SOURCE)
    @Visitable
    private String recordContentSource;

    public List<Integer> getRecordIdentifierList() {
        return recordIdentifierList;
    }

    public void setRecordIdentifierList(List<Integer> recordIdentifierList) {

        List<Integer> oldValue = this.recordIdentifierList;

        this.recordIdentifierList = recordIdentifierList;

        firePropertyChange(RECORD_IDENTIFIER_LIST, oldValue, recordIdentifierList);
    }

    public Date getRecordUpdatedDate() {
        return recordUpdatedDate;
    }

    public void setRecordUpdatedDate(Date recordUpdatedDate) {

        Date oldValue = this.recordUpdatedDate;

        this.recordUpdatedDate = recordUpdatedDate;

        firePropertyChange(RECORD_UPDATED_DATE, oldValue, recordUpdatedDate);
    }

    public Date getRecordCreationDate() {
        return recordCreationDate;
    }

    public void setRecordCreationDate(Date recordCreationDate) {

        Date oldValue = this.recordCreationDate;

        this.recordCreationDate = recordCreationDate;

        firePropertyChange(RECORD_CREATION_DATE, oldValue, recordCreationDate);
    }

    public String getRecordContentSource() {
        return recordContentSource;
    }

    public void setRecordContentSource(String recordContentSource) {

        String oldValue = this.recordContentSource;

        this.recordContentSource = recordContentSource;

        firePropertyChange(RECORD_CONTENT_SOURCE, oldValue, recordContentSource);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((recordContentSource == null) ? 0 : recordContentSource.hashCode());
        result = prime * result + ((recordCreationDate == null) ? 0 : recordCreationDate.hashCode());
        result = prime * result + ((recordIdentifierList == null) ? 0 : recordIdentifierList.hashCode());
        result = prime * result + ((recordUpdatedDate == null) ? 0 : recordUpdatedDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        RecordInfo other = (RecordInfo) obj;
        if (recordContentSource == null) {
            if (other.recordContentSource != null)
                return false;
        } else if (!recordContentSource.equals(other.recordContentSource))
            return false;
        if (recordCreationDate == null) {
            if (other.recordCreationDate != null)
                return false;
        } else if (!recordCreationDate.equals(other.recordCreationDate))
            return false;
        if (recordIdentifierList == null) {
            if (other.recordIdentifierList != null)
                return false;
        } else if (!recordIdentifierList.equals(other.recordIdentifierList))
            return false;
        if (recordUpdatedDate == null) {
            if (other.recordUpdatedDate != null)
                return false;
        } else if (!recordUpdatedDate.equals(other.recordUpdatedDate))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "RecordInfo [recordIdentifierList=" + recordIdentifierList + ", recordUpdatedDate=" + recordUpdatedDate
            + ", recordCreationDate=" + recordCreationDate + ", recordContentSource=" + recordContentSource + "]";
    }
}
