package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * <titleInfo>
 *    <title>Preface</title>
 * </titleInfo>
 */
@Entity
@Table(name=TitleInfo.TITLE_INFO)
@XStreamAlias(TitleInfo.TITLE_INFO)
@Visitable
public class TitleInfo extends SerializableBean {

    private static final long serialVersionUID = -8158243280541311702L;

    static final String TITLE_INFO = "titleInfo",
        TITLE = "title";

    @XStreamAlias(TITLE)
    @Visitable
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(@Changeable (TITLE) String title) {
        this.title = title;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        TitleInfo other = (TitleInfo) obj;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TitleInfo [title=" + title + "]";
    }
}
