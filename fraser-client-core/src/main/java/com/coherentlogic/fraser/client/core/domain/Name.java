package com.coherentlogic.fraser.client.core.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
<name>
  <role>
    <roleTerm>creator</roleTerm>
  </role>
  <namePart>United States. Women's Bureau</namePart>
  <recordInfo>
    <recordIdentifier>770</recordIdentifier>
  </recordInfo>
</name>
 *
 */
@Entity
@Table(name=Name.NAME)
@XStreamAlias(Name.NAME)
@Visitable
public class Name extends SerializableBean {

    private static final long serialVersionUID = 2114397773439479335L;

    static final String
        NAME = "name",
        ROLE = "role",
        NAME_PART = "namePart",
        NAME_PART_LIST = "namePartList",
        RECORD_INFO = "recordInfo";

    @XStreamAlias(ROLE)
    @Visitable
    private Role role;

    @XStreamAlias(NAME_PART)
    @XStreamImplicit
    @Visitable
    private List<String> namePartList;

    @XStreamAlias(RECORD_INFO)
    @Visitable
    private RecordInfo recordInfo;

    @XStreamAlias(NAME)
    @Visitable
    private String name;

    public Role getRole() {
        return role;
    }

    public void setRole(@Changeable (ROLE) Role role) {
        this.role = role;
    }

    public List<String> getNamePartList() {
        return namePartList;
    }

    public void setNamePartList(@Changeable (NAME_PART_LIST) List<String> namePartList) {
        this.namePartList = namePartList;
    }

    public RecordInfo getRecordInfo() {
        return recordInfo;
    }

    public void setRecordInfo(@Changeable (RECORD_INFO) RecordInfo recordInfo) {
        this.recordInfo = recordInfo;
    }

    public String getName() {
        return name;
    }

    public void setName(@Changeable (NAME) String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((namePartList == null) ? 0 : namePartList.hashCode());
        result = prime * result + ((recordInfo == null) ? 0 : recordInfo.hashCode());
        result = prime * result + ((role == null) ? 0 : role.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Name other = (Name) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (namePartList == null) {
            if (other.namePartList != null)
                return false;
        } else if (!namePartList.equals(other.namePartList))
            return false;
        if (recordInfo == null) {
            if (other.recordInfo != null)
                return false;
        } else if (!recordInfo.equals(other.recordInfo))
            return false;
        if (role == null) {
            if (other.role != null)
                return false;
        } else if (!role.equals(other.role))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Name [role=" + role + ", namePartList=" + namePartList + ", recordInfo=" + recordInfo + ", name=" + name
            + "]";
    }
}
