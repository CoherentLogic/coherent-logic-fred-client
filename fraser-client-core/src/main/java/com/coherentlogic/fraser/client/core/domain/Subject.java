package com.coherentlogic.fraser.client.core.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * <subject>
 *     <theme>
 *         <theme>Federal Reserve Bank of New York</theme>
 *         <recordInfo>
 *             <recordIdentifier>26</recordIdentifier>
 *         </recordInfo>
 *     </theme>
 *     <topic>
 *         <topic>Federal Reserve banks</topic>
 *         <recordInfo>
 *             <recordIdentifier>4173</recordIdentifier>
 *         </recordInfo>
 *     </topic>
 * </subject>
 *
 * <subject>
 *     <name>
 *         <name>Federal Home Loan Bank of Seattle</name>
 *         <recordInfo>
 *             <recordIdentifier>4801</recordIdentifier>
 *         </recordInfo>
 *     </name>
 *     <theme>
 *         <theme>Financial Crisis of 2007-2009</theme>
 *         <recordInfo>
 *             <recordIdentifier>103</recordIdentifier>
 *         </recordInfo>
 *     </theme>
 * </subject>
 *
 * <subject>
 *     <topic>
 *         <topic>Finance</topic>
 *         <recordInfo>
 *             <recordIdentifier>4187</recordIdentifier>
 *         </recordInfo>
 *     </topic>
 *     <topic>
 *         <topic>History</topic>
 *         <recordInfo>
 *             <recordIdentifier>4215</recordIdentifier>
 *         </recordInfo>
 *     </topic>
 *     <geographic>
 *         <geographic>United States</geographic>
 *         <recordInfo>
 *             <recordIdentifier>4293</recordIdentifier>
 *         </recordInfo>
 *     </geographic>
 * </subject>
 *
 * <subject>
 * <name>
 * <name>Troubled Asset Relief Program (U.S.)</name>
 * <recordInfo>
 * <recordIdentifier>5915</recordIdentifier>
 * </recordInfo>
 * </name>
 * <theme>
 * <theme>Financial Crisis of 2007-2009</theme>
 * <recordInfo>
 * <recordIdentifier>103</recordIdentifier>
 * </recordInfo>
 * </theme>
 * <topic>
 * <topic>Bank failures</topic>
 * <recordInfo>
 * <recordIdentifier>4078</recordIdentifier>
 * </recordInfo>
 * </topic>
 * <topic>
 * <topic>Economic assistance, Domestic</topic>
 * <recordInfo>
 * <recordIdentifier>4900</recordIdentifier>
 * </recordInfo>
 * </topic>
 * <topic>
 * <topic>Financial crises</topic>
 * <recordInfo>
 * <recordIdentifier>4189</recordIdentifier>
 * </recordInfo>
 * </topic>
 * <topic>
 * <topic>Government policy</topic>
 * <recordInfo>
 * <recordIdentifier>4207</recordIdentifier>
 * </recordInfo>
 * </topic>
 * <topic>
 * <topic>Management</topic>
 * <recordInfo>
 * <recordIdentifier>5521</recordIdentifier>
 * </recordInfo>
 * </topic>
 * <geographic>
 * <geographic>United States</geographic>
 * <recordInfo>
 * <recordIdentifier>4293</recordIdentifier>
 * </recordInfo>
 * </geographic>
 * </subject>
 */
@Entity
@Table(name=Subject.SUBJECT)
@XStreamAlias(Subject.SUBJECT)
@Visitable
public class Subject extends SerializableBean {

    private static final long serialVersionUID = 7174389820275009745L;

    static final String
        SUBJECT = "subject",
        THEME_LIST = "themeList",
        TOPIC_LIST = "topicList",
        GEOGRAPHIC_LIST = "geographicList",
        NAME_LIST = "nameList";

    @XStreamAlias(Theme.THEME)
    @XStreamImplicit
    @Visitable
    private List<Theme> themeList;

    @XStreamAlias(Topic.TOPIC)
    @XStreamImplicit
    @Visitable
    private List<Topic> topicList;

    @XStreamAlias(SubjectTitleInfo.TITLE_INFO)
    @Visitable
    private SubjectTitleInfo titleInfo;

    @XStreamAlias(Geographic.GEOGRAPHIC)
    @XStreamImplicit
    private List<Geographic> geographicList;

    @XStreamAlias(Name.NAME)
    @XStreamImplicit
    private List<Name> nameList;

    public List<Theme> getThemeList() {
        return themeList;
    }

    public void setThemeList(@Changeable (THEME_LIST) List<Theme> themeList) {
        this.themeList = themeList;
    }

    public List<Topic> getTopicList() {
        return topicList;
    }

    public void setTopicList(@Changeable (TOPIC_LIST) List<Topic> topicList) {
        this.topicList = topicList;
    }

    public SubjectTitleInfo getTitleInfo() {
        return titleInfo;
    }

    public void setTitleInfo(@Changeable (SubjectTitleInfo.TITLE_INFO) SubjectTitleInfo titleInfo) {
        this.titleInfo = titleInfo;
    }

    public List<Geographic> getGeographicList() {
        return geographicList;
    }

    public void setGeographicList(@Changeable (GEOGRAPHIC_LIST) List<Geographic> geographicList) {
        this.geographicList = geographicList;
    }

    public List<Name> getNameList() {
        return nameList;
    }

    public void setNameList(@Changeable (NAME_LIST) List<Name> nameList) {
        this.nameList = nameList;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((geographicList == null) ? 0 : geographicList.hashCode());
        result = prime * result + ((nameList == null) ? 0 : nameList.hashCode());
        result = prime * result + ((themeList == null) ? 0 : themeList.hashCode());
        result = prime * result + ((titleInfo == null) ? 0 : titleInfo.hashCode());
        result = prime * result + ((topicList == null) ? 0 : topicList.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Subject other = (Subject) obj;
        if (geographicList == null) {
            if (other.geographicList != null)
                return false;
        } else if (!geographicList.equals(other.geographicList))
            return false;
        if (nameList == null) {
            if (other.nameList != null)
                return false;
        } else if (!nameList.equals(other.nameList))
            return false;
        if (themeList == null) {
            if (other.themeList != null)
                return false;
        } else if (!themeList.equals(other.themeList))
            return false;
        if (titleInfo == null) {
            if (other.titleInfo != null)
                return false;
        } else if (!titleInfo.equals(other.titleInfo))
            return false;
        if (topicList == null) {
            if (other.topicList != null)
                return false;
        } else if (!topicList.equals(other.topicList))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Subject [themeList=" + themeList + ", topicList=" + topicList + ", titleInfo=" + titleInfo
            + ", geographicList=" + geographicList + ", nameList=" + nameList + "]";
    }
}
