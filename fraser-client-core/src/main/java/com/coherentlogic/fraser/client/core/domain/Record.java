package com.coherentlogic.fraser.client.core.domain;

import java.util.Collection;
import java.util.function.Consumer;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@Entity
@Table(name=Record.RECORD)
@XStreamAlias(Record.RECORD)
@Visitable
public class Record extends SerializableBean {

    private static final long serialVersionUID = 6187415891868789164L;

    static final String RECORD = "record", HEADER = "header", METADATA = "metadata";

    @XStreamAlias(HEADER)
    @Visitable
    private Header header;

    @XStreamAlias(METADATA)
    @Visitable
    private MetaData metaData;

    public Header getHeader() {
        return header;
    }

    public void setHeader(@Changeable (HEADER) Header header) {
        this.header = header;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(@Changeable (MetaData.META_DATA_ALIAS) MetaData metaData) {
        this.metaData = metaData;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((header == null) ? 0 : header.hashCode());
        result = prime * result + ((metaData == null) ? 0 : metaData.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Record other = (Record) obj;
        if (header == null) {
            if (other.header != null)
                return false;
        } else if (!header.equals(other.header))
            return false;
        if (metaData == null) {
            if (other.metaData != null)
                return false;
        } else if (!metaData.equals(other.metaData))
            return false;
        return true;
    }

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        header.accept(visitors);
        metaData.accept(visitors);
    }

    @Override
    public String toString() {
        return "Record [header=" + header + ", metaData=" + metaData + "]";
    }
}

/*

<?xml version="1.0" encoding="UTF-8"?>
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/">
  <responseDate>2017-06-19T02:41:41Z</responseDate>
  <request verb="ListRecords" metadataPrefix="mods">https://fraser.stlouisfed.org/oai</request>
  <ListRecords>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5549</identifier>
        <datestamp>2017-02-15T15:26:47Z</datestamp>
        <setSpec>author:770</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Women's Bureau</namePart>
            <recordInfo>
              <recordIdentifier>770</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <language>eng</language>
          <titleInfo>
            <title>15 Years After College: A Study of Alumnae of the Class of 1945</title>
            <subTitle>Women's Bureau Bulletin, No. 283</subTitle>
          </titleInfo>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1962-01-01</sortDate>
            <publisher>Govt. Print. Off.</publisher>
            <dateIssued>1962</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5549</recordIdentifier>
            <recordUpdatedDate>2017-02-15 15:28:11</recordUpdatedDate>
            <recordCreationDate>2017-02-14 15:37:34</recordCreationDate>
            <recordContentSource>frs</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the Women's Bureau</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the Women's Bureau</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Women's Bureau Bulletin</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>243</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>32 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=5549&amp;amp;filepath=/files/docs/publications/women/b0283_dolwb_1962.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=5549&amp;amp;filepath=/files/docs/publications/women/b0283_dolwb_1962.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:210</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:128</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Reserve Bank of New York</namePart>
            <recordInfo>
              <recordIdentifier>128</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">An account of the history of the Federal Reserve System in general and of the Federal Reserve Bank of New York.</note>
          <subject>
            <theme>
              <theme>Federal Reserve Bank of New York</theme>
              <recordInfo>
                <recordIdentifier>26</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Federal Reserve banks</topic>
              <recordInfo>
                <recordIdentifier>4173</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <abstract>This pamphlet, published in 1964 on the occasion of the 50th Anniversary of the Federal Reserve Banks, briefly documents the early history of the 2nd District Bank of the Federal Reserve System.</abstract>
          <language>eng</language>
          <titleInfo>
            <title>1914/1964</title>
            <subTitle>50th Anniversary Publication</subTitle>
          </titleInfo>
          <identifier type="oclc">20860421</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1964-12-01</sortDate>
            <dateIssued>December 1964</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>210</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:20:23</recordUpdatedDate>
            <recordCreationDate>2011-04-20 15:50:18.903742</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>24 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=210&amp;amp;filepath=/files/docs/publications/frbny/frbny_50anniversary.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=210&amp;amp;filepath=/files/docs/publications/frbny/frbny_50anniversary.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:405</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:16</setSpec>
        <setSpec>author:1107</setSpec>
        <setSpec>author:8498</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. House. Committee on Banking and Currency</namePart>
            <namePart type="date">1865-1974</namePart>
            <recordInfo>
              <recordIdentifier>16</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Spence, Brent</namePart>
            <namePart type="date">1874-1967</namePart>
            <recordInfo>
              <recordIdentifier>1107</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Seventy-Ninth Congress</namePart>
            <namePart type="date">1945-1947</namePart>
            <recordInfo>
              <recordIdentifier>8498</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Meltzer's History of the Federal Reserve - Primary Sources</theme>
              <recordInfo>
                <recordIdentifier>97</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Standard of living</topic>
              <recordInfo>
                <recordIdentifier>4283</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
            <titleInfo>
              <titleInfo>Stabilization Act of 1942</titleInfo>
              <recordInfo>
                <recordIdentifier>7399</recordIdentifier>
              </recordInfo>
            </titleInfo>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>1946 Extension of the Emergency Price Control and Stabilization Acts of 1942, as Amended</title>
            <subTitle>Hearings Before the Committee on Banking and Currency, House of Representatives</subTitle>
            <titlePartNumber>Seventy-Ninth Congress, Second Session, on H.R. 5270 (1946)</titlePartNumber>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Emergency Price Control and Stabilization Acts</title>
          </titleInfo>
          <identifier type="oclc">1162846</identifier>
          <originInfo>
            <issuance>multipart</issuance>
            <dateIssued point="start">1946-02-18</dateIssued>
            <dateIssued point="end">1946-03-20</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>405</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:35:18</recordUpdatedDate>
            <recordCreationDate>2012-02-07 11:21:27.930849</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>1946 Extension of the Emergency Price Control and Stabilization Acts of 1942, as Amended</title>
              <subTitle>Hearings Before the Committee on Banking and Currency, United States Senate</subTitle>
              <titlePartNumber>Seventy-Ninth Congress, Second Session, on S. 2028 (1946)</titlePartNumber>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Emergency Price Control and Stabilization Acts</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>406</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Public Law 79-548, 79th Congress, H.J. Res. 371</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Joint Resolution Extending the Effective Period of the Emergency Price Control Act of 1942, as Amended, and the Stabilization Act of 1942, as Amended</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>404</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4.B 22/1:P 93/4/v.1-2</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/405</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:406</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:27</setSpec>
        <setSpec>author:8498</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. Senate. Committee on Banking and Currency</namePart>
            <namePart type="date">1913-1970</namePart>
            <recordInfo>
              <recordIdentifier>27</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Seventy-Ninth Congress</namePart>
            <namePart type="date">1945-1947</namePart>
            <recordInfo>
              <recordIdentifier>8498</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Standard of living</topic>
              <recordInfo>
                <recordIdentifier>4283</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
            <titleInfo>
              <titleInfo>Stabilization Act of 1942</titleInfo>
              <recordInfo>
                <recordIdentifier>7399</recordIdentifier>
              </recordInfo>
            </titleInfo>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>1946 Extension of the Emergency Price Control and Stabilization Acts of 1942, as Amended</title>
            <subTitle>Hearings Before the Committee on Banking and Currency, United States Senate</subTitle>
            <titlePartNumber>Seventy-Ninth Congress, Second Session, on S. 2028 (1946)</titlePartNumber>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Emergency Price Control and Stabilization Acts</title>
          </titleInfo>
          <identifier type="oclc">9055923</identifier>
          <originInfo>
            <issuance>multipart</issuance>
            <dateIssued point="start">1946-04-15</dateIssued>
            <dateIssued point="end">1946-05-02</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>406</recordIdentifier>
            <recordUpdatedDate>2016-08-23 16:39:06</recordUpdatedDate>
            <recordCreationDate>2012-02-07 11:22:50.729255</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>1946 Extension of the Emergency Price Control and Stabilization Acts of 1942, as Amended</title>
              <subTitle>Hearings Before the Committee on Banking and Currency, House of Representatives</subTitle>
              <titlePartNumber>Seventy-Ninth Congress, Second Session, on H.R. 5270 (1946)</titlePartNumber>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Emergency Price Control and Stabilization Acts</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>405</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Public Law 79-548, 79th Congress, H.J. Res. 371</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Joint Resolution Extending the Effective Period of the Emergency Price Control Act of 1942, as Amended, and the Stabilization Act of 1942, as Amended</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>404</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4. B 22/3:P 93/4/v.1-2</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/406</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1211</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:350</setSpec>
        <setSpec>author:356</setSpec>
        <setSpec>author:8467</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. Joint Economic Committee</namePart>
            <namePart type="date">1956-</namePart>
            <recordInfo>
              <recordIdentifier>350</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>United States. President</namePart>
            <recordInfo>
              <recordIdentifier>356</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Ninetieth Congress</namePart>
            <namePart type="date">1967-1969</namePart>
            <recordInfo>
              <recordIdentifier>8467</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Meltzer's History of the Federal Reserve - Primary Sources</theme>
              <recordInfo>
                <recordIdentifier>97</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Budget</topic>
              <recordInfo>
                <recordIdentifier>4102</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Economic policy</topic>
              <recordInfo>
                <recordIdentifier>4150</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <abstract>Focuses on issues of unemployment, tax policies, capital formation, and Federal programs and budget priorities.</abstract>
          <language>eng</language>
          <titleInfo>
            <title>1967 Economic Report of the President</title>
            <subTitle>Hearings Before the Joint Economic Committee, Congress of the United States</subTitle>
            <titlePartNumber>Ninetieth Congress, First Session (1967)</titlePartNumber>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Economic Report of the President</title>
          </titleInfo>
          <identifier type="oclc">21749322</identifier>
          <originInfo>
            <issuance>multipart</issuance>
            <dateIssued point="start">1967-02-02</dateIssued>
            <dateIssued point="end">1967-02-28</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1211</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:35:17</recordUpdatedDate>
            <recordCreationDate>2014-02-11 12:02:40.95016</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Economic Report of the President</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Economic Report of the President</title>
            </titleInfo>
            <titleInfo type="uniform">
              <title>Economic Report of the President Transmitted to the Congress (Dept. Ed.)</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>45</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/1211</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:757</identifier>
        <datestamp>2017-03-30T10:22:07Z</datestamp>
        <setSpec>author:350</setSpec>
        <setSpec>author:268</setSpec>
        <setSpec>author:8469</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. Joint Economic Committee</namePart>
            <namePart type="date">1956-</namePart>
            <recordInfo>
              <recordIdentifier>350</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Proxmire, William</namePart>
            <recordInfo>
              <recordIdentifier>268</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Ninety-Second Congress</namePart>
            <namePart type="date">1971-1973</namePart>
            <recordInfo>
              <recordIdentifier>8469</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">92d Congress, 1st Session, Joint Committee Print</note>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Economic conditions</topic>
              <recordInfo>
                <recordIdentifier>4145</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional reports</topic>
              <recordInfo>
                <recordIdentifier>7381</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>1971 Midyear Review of the Economy</title>
            <subTitle>Report of the Joint Economic Committee, Congress of the United States</subTitle>
          </titleInfo>
          <identifier type="oclc">206925</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1971-08-16</sortDate>
            <dateIssued>August 16, 1971</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>757</recordIdentifier>
            <recordUpdatedDate>2017-03-30 10:30:30</recordUpdatedDate>
            <recordCreationDate>2012-05-16 13:04:13.345328</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Economic Report of the President</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Economic Report of the President</title>
            </titleInfo>
            <titleInfo type="uniform">
              <title>Economic Report of the President Transmitted to the Congress (Dept. Ed.)</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>45</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4.Ec7:Ec7/28/971</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>35 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=757&amp;amp;filepath=/files/docs/historical/jec/1971jec_midrev.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5332</identifier>
        <datestamp>2017-01-24T10:58:57Z</datestamp>
        <setSpec>author:350</setSpec>
        <setSpec>author:8472</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. Joint Economic Committee</namePart>
            <namePart type="date">1956-</namePart>
            <recordInfo>
              <recordIdentifier>350</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Ninety-Sixth Congress</namePart>
            <namePart type="date">1979-1981</namePart>
            <recordInfo>
              <recordIdentifier>8472</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>The 1979 Economic Report of the President</title>
            <subTitle>Hearings Before the Joint Economic Committee, Congress of the United States</subTitle>
            <titlePartNumber>Ninety-Sixth Congress, First Session</titlePartNumber>
          </titleInfo>
          <originInfo>
            <place>Washington</place>
            <issuance>multipart</issuance>
            <publisher>U. S. Government Printing Office</publisher>
            <dateIssued point="start">1979-01-23</dateIssued>
            <dateIssued point="end">1979-03-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5332</recordIdentifier>
            <recordUpdatedDate>2017-01-24 10:20:57</recordUpdatedDate>
            <recordCreationDate>2017-01-11 15:58:09</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>The Joint Economic Report</title>
              <subTitle>Report of the Joint Economic Committee, Congress of the United States, on the Economic Report of the President</subTitle>
            </titleInfo>
            <titleInfo type="uniform">
              <title>Joint Economic Report (Senate Report)</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1250</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4.Ec 7: Ec 7/2/979/</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/5332</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/5332</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:764</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:524</setSpec>
        <setSpec>author:8480</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. Senate. Committee on Finance</namePart>
            <namePart type="date">1815-</namePart>
            <recordInfo>
              <recordIdentifier>524</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Ninety-Seventh Congress</namePart>
            <namePart type="date">1981-1983</namePart>
            <recordInfo>
              <recordIdentifier>8480</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Debts, Public</topic>
              <recordInfo>
                <recordIdentifier>4136</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Debt limit</topic>
              <recordInfo>
                <recordIdentifier>7393</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>1981 Public Debt Limit II</title>
            <subTitle>Hearing Before the Committee on Finance, United States Senate</subTitle>
            <titlePartNumber>Ninety-Seventh Congress, First Session, September 11, 1981</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">07930068</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1981-09-11</sortDate>
            <publisher>U.S. Government Printing Office</publisher>
            <dateIssued>September 11, 1981</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>764</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:10:50</recordUpdatedDate>
            <recordCreationDate>2012-05-16 13:04:13.345328</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4.F 49:D 35/9/981-2</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>39 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=764&amp;amp;filepath=/files/docs/historical/congressional/19810911sen_pubdebtlimit.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5036</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:4801</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Home Loan Bank of Seattle</namePart>
            <recordInfo>
              <recordIdentifier>4801</recordIdentifier>
            </recordInfo>
          </name>
          <subject>
            <name>
              <name>Federal Home Loan Bank of Seattle</name>
              <recordInfo>
                <recordIdentifier>4801</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Financial Crisis of 2007-2009</theme>
              <recordInfo>
                <recordIdentifier>103</recordIdentifier>
              </recordInfo>
            </theme>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>2009 Member News</title>
            <subTitle>Important Information About the Seattle Bank's Risk-Based Capital Requirement</subTitle>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>2009-01-13</sortDate>
            <dateIssued>January 13, 2009</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5036</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:33:56</recordUpdatedDate>
            <recordCreationDate>2015-12-22 16:36:53</recordCreationDate>
            <recordContentSource>frs</recordContentSource>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>electronic</form>
            <extent>2 pages</extent>
            <digitalOrigin>born digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=5036&amp;amp;filepath=/files/docs/historical/fct/fhlbseattle_news_20090113.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=5036&amp;amp;filepath=/files/docs/historical/fct/fhlbseattle_news_20090113.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:797</identifier>
        <datestamp>2017-05-18T16:09:43Z</datestamp>
        <setSpec>author:1699</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Board of Governors of the Federal Reserve System (U.S.). Committee on Branch, Group, and Chain Banking</namePart>
            <namePart type="date">1935-</namePart>
            <recordInfo>
              <recordIdentifier>1699</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Great Depression</theme>
              <recordInfo>
                <recordIdentifier>8</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Bank failures</topic>
              <recordInfo>
                <recordIdentifier>4078</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Bank examination</topic>
              <recordInfo>
                <recordIdentifier>4077</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>225 Bank Suspensions</title>
            <subTitle>Case Histories from Examiners' Reports</subTitle>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Two Hundred Twenty-five Bank Suspensions</title>
          </titleInfo>
          <identifier type="oclc">10700666</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1932-10-01</sortDate>
            <dateIssued>Circa 1932</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>797</recordIdentifier>
            <recordUpdatedDate>2017-05-18 16:09:46</recordUpdatedDate>
            <recordCreationDate>2012-05-16 13:04:13.345328</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Material Prepared for the Information of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1488</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>264 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=797&amp;amp;filepath=/files/docs/historical/federal reserve history/frcom_br_gp_ch_banking/225_bank_suspensions.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=797&amp;amp;filepath=/files/docs/historical/federal reserve history/frcom_br_gp_ch_banking/225_bank_suspensions.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:162</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:515</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Hostetler, L. Merle</namePart>
            <recordInfo>
              <recordIdentifier>515</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">A running financial history of the United States.</note>
          <note type="public">Strip printed on one side only and folded to form 84 leaves. Last two leaves blank.</note>
          <note type="public">For a presentation friendly view, visit &lt;a target="_blank" rel="nofollow" href="https://fraser.stlouisfed.org/75years/presentation.php"&gt;https://fraser.stlouisfed.org/75years/presentation.php&lt;/a&gt;</note>
          <genre>graphic</genre>
          <genre>picture</genre>
          <subject>
            <topic>
              <topic>Finance</topic>
              <recordInfo>
                <recordIdentifier>4187</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>History</topic>
              <recordInfo>
                <recordIdentifier>4215</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <abstract>A graphic presentation of American financial history from 1861 through 1938. It was published as a continuous timeline over 85 feet long.</abstract>
          <language>eng</language>
          <titleInfo>
            <title>75 Yrs. of American Finance</title>
            <subTitle>A Graphic Presentation, 1861 to 1935</subTitle>
          </titleInfo>
          <titleInfo type="alternate">
            <title>75 Years of American Finance</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Seventy-five Years of American Finance</title>
          </titleInfo>
          <identifier type="oclc">670438970</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <dateIssued point="start">1936-01-01</dateIssued>
            <dateIssued point="end">1936-01-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>162</recordIdentifier>
            <recordUpdatedDate>2016-04-01 11:37:07</recordUpdatedDate>
            <recordCreationDate>2010-05-24 12:17:25.471847</recordCreationDate>
          </recordInfo>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Business Booms and Depressions Since 1775</title>
              <subTitle>An Accurate Charting of the Past and Present Trend of Prices, National Income, Federal Debt, Business, Stock Averages and Commodities with a Special Study of Wages and Postwar Industry</subTitle>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>145</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>mixed media</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/162</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/162</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:201</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:437</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Kemmerer, Edwin Walter</namePart>
            <namePart type="date">1875-1945</namePart>
            <recordInfo>
              <recordIdentifier>437</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Preface by Benjamin Strong</note>
          <subject>
            <topic>
              <topic>Federal Reserve banks</topic>
              <recordInfo>
                <recordIdentifier>4173</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>The ABC of the Federal Reserve System</title>
            <subTitle>Why the Federal Reserve System Was Called into Being, the Main Features of Its Organization, and How It Works</subTitle>
          </titleInfo>
          <identifier type="oclc">5577076</identifier>
          <originInfo>
            <edition>Fifth edition</edition>
            <issuance>monographic</issuance>
            <sortDate>1922-01-01</sortDate>
            <dateIssued>January 1922</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>201</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:20:02</recordUpdatedDate>
            <recordCreationDate>2011-04-01 08:53:32.677614</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>228 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=201&amp;amp;filepath=/files/docs/publications/books/abcfrs_kemmerer_1922.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=201&amp;amp;filepath=/files/docs/publications/books/abcfrs_kemmerer_1922.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4225</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:5320</setSpec>
        <setSpec>author:4933</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Kennedy, Eleanor Virginia</namePart>
            <recordInfo>
              <recordIdentifier>5320</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics. Division of Industrial Relations</namePart>
            <recordInfo>
              <recordIdentifier>4933</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Reprinted from the Monthly labor review, February 1943, with additional data.</note>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Absenteeism (Labor)</topic>
              <recordInfo>
                <recordIdentifier>5683</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Shipbuilding</topic>
              <recordInfo>
                <recordIdentifier>5638</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Absenteeism in Commercial Shipyards</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 734</subTitle>
          </titleInfo>
          <identifier type="oclc">15580427</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1943-01-01</sortDate>
            <publisher>G.P.O.</publisher>
            <dateIssued>1943</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4225</recordIdentifier>
            <recordUpdatedDate>2016-05-19 11:40:23</recordUpdatedDate>
            <recordCreationDate>2015-05-20 15:52:09</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD8051 .A62 no. 734</classification>
          <classification authority="ddc">331.06173 s 331.81838</classification>
          <classification authority="sudocs">L 2.3:734</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>16 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=4225&amp;amp;filepath=/files/docs/publications/bls/bls_0734_1943.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4252</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:5109</setSpec>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Kossoris, Max D. (Max Davis)</namePart>
            <recordInfo>
              <recordIdentifier>5109</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Cover title.</note>
          <note type="public">"Prepared ... by Max D. Kossoris."--Letter of transmittal.</note>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Industrial accidents</topic>
              <recordInfo>
                <recordIdentifier>5539</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Statistics</topic>
              <recordInfo>
                <recordIdentifier>4285</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Accident-Record Manual for Industrial Plants</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 772</subTitle>
          </titleInfo>
          <identifier type="oclc">24367644</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1944-01-01</sortDate>
            <publisher>G.P.O.</publisher>
            <dateIssued>1944</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4252</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:11:58</recordUpdatedDate>
            <recordCreationDate>2015-05-20 15:52:10</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD8051 .A62 no. 772</classification>
          <classification authority="ddc">331.823</classification>
          <classification authority="sudocs">L 2.3:772</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>22 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=4252&amp;amp;filepath=/files/docs/publications/bls/bls_0772_1944.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:3855</identifier>
        <datestamp>2017-03-02T14:54:13Z</datestamp>
        <setSpec>author:4075</setSpec>
        <setSpec>author:4087</setSpec>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Chaney, Lucian W. (Lucian West)</namePart>
            <namePart type="date">1857-1935</namePart>
            <recordInfo>
              <recordIdentifier>4075</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Hanna, Hugh S. (Hugh Sisson)</namePart>
            <recordInfo>
              <recordIdentifier>4087</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">At head of title: U.S. Department of Labor. Bureau of Labor Statistics. Royal Meeker, Commissioner ...</note>
          <note type="public">Part of the plates printed on both sides.</note>
          <note type="public">Issued also as House doc. 57, U.S., 65th Cong., 1st sess.</note>
          <genre>government publication</genre>
          <genre>statistics</genre>
          <language>eng</language>
          <titleInfo>
            <title>Accidents and Accident Prevention in Machine Building</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 216</subTitle>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Accidents and Accident Prevention in Machine Building</title>
            <subTitle>Industrial Accidents and Hygiene Series</subTitle>
            <titlePartNumber>No. 13</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">24201108</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1917-08-01</sortDate>
            <publisher>Government Printing Office</publisher>
            <dateIssued>August 1917</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>3855</recordIdentifier>
            <recordUpdatedDate>2017-03-02 14:54:39</recordUpdatedDate>
            <recordCreationDate>2015-01-14 17:56:51</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="series">
            <titleInfo>
              <title>Industrial Accidents and Hygiene Series</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3782</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD8051 .A62 no. 216</classification>
          <classification authority="sudocs">L 2.3:216</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>136 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=3855&amp;amp;filepath=/files/docs/publications/bls/bls_0216_1917.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:3882</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:4075</setSpec>
        <setSpec>author:4087</setSpec>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Chaney, Lucian W. (Lucian West)</namePart>
            <namePart type="date">1857-1935</namePart>
            <recordInfo>
              <recordIdentifier>4075</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Hanna, Hugh S. (Hugh Sisson)</namePart>
            <recordInfo>
              <recordIdentifier>4087</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">At head of title: U.S. Department of Labor. Bureau of Labor Statistics. Royal Meeker, Commissioner.</note>
          <note type="public">Part of the plates printed on both sides.</note>
          <note type="public">Issued also as House doc. 90, U.S., 66th Cong., 1st sess.</note>
          <genre>government publication</genre>
          <genre>statistics</genre>
          <subject>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Accidents and Accident Prevention in Machine Building. Revision of Bulletin 216</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 256</subTitle>
          </titleInfo>
          <identifier type="oclc">24206428</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1919-11-01</sortDate>
            <dateOther>1920</dateOther>
            <publisher>Government Printing Office</publisher>
            <dateIssued>November 1919</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>3882</recordIdentifier>
            <recordUpdatedDate>2015-10-07 09:51:35</recordUpdatedDate>
            <recordCreationDate>2015-01-14 17:56:51</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="series">
            <titleInfo>
              <title>Industrial Accidents and Hygiene Series</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3782</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD8051 .A62 no.256</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>142 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=3882&amp;amp;filepath=/files/docs/publications/bls/bls_0256_1920.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5007</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:5923</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congressional Oversight Panel</namePart>
            <recordInfo>
              <recordIdentifier>5923</recordIdentifier>
            </recordInfo>
          </name>
          <subject>
            <name>
              <name>Troubled Asset Relief Program (U.S.)</name>
              <recordInfo>
                <recordIdentifier>5915</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Financial Crisis of 2007-2009</theme>
              <recordInfo>
                <recordIdentifier>103</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Bank failures</topic>
              <recordInfo>
                <recordIdentifier>4078</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Economic assistance, Domestic</topic>
              <recordInfo>
                <recordIdentifier>4900</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Financial crises</topic>
              <recordInfo>
                <recordIdentifier>4189</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Government policy</topic>
              <recordInfo>
                <recordIdentifier>4207</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Management</topic>
              <recordInfo>
                <recordIdentifier>5521</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Accountability for the Troubled Asset Relief Program</title>
            <subTitle>The Second Report of the Congressional Oversight Panel</subTitle>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Second Report of the Congressional Oversight Panel</title>
          </titleInfo>
          <identifier type="oclc">318643396</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>2009-01-09</sortDate>
            <publisher>U.S. G.P.O.</publisher>
            <dateIssued>January 9, 2009</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5007</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:34:03</recordUpdatedDate>
            <recordCreationDate>2015-12-22 16:36:48</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Oversight Panel Reports</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>4963</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HG2461 .U53 2009eb</classification>
          <classification authority="sudocs">Y 3.2:C 76/3/T 75</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>electronic</form>
            <extent>67 pages</extent>
            <digitalOrigin>born digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=5007&amp;amp;filepath=/files/docs/historical/fct/cop_report_20090109.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=5007&amp;amp;filepath=/files/docs/historical/fct/cop_report_20090109.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4059</identifier>
        <datestamp>2017-03-02T16:04:02Z</datestamp>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Labor bureaus</topic>
              <recordInfo>
                <recordIdentifier>5565</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States -- New York (State)</geographic>
              <recordInfo>
                <recordIdentifier>4386</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Activities and Functions of a State Department of Labor</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 479</subTitle>
          </titleInfo>
          <identifier type="oclc">04031621</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1928-09-01</sortDate>
            <publisher>G.P.O.</publisher>
            <dateIssued>September 1928</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4059</recordIdentifier>
            <recordUpdatedDate>2017-03-02 16:04:39</recordUpdatedDate>
            <recordCreationDate>2015-04-21 17:34:55</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
              <subTitle>Miscellaneous Series</subTitle>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3779</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD4835.U4 U5x 1928</classification>
          <classification authority="sudocs">L 2.3:479</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>165 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=4059&amp;amp;filepath=/files/docs/publications/bls/bls_0479_1928.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:203</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:851</setSpec>
        <setSpec>author:8480</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. House. Committee on Banking, Finance, and Urban Affairs. Subcommittee on Domestic Monetary Policy</namePart>
            <namePart type="date">1977-1995</namePart>
            <recordInfo>
              <recordIdentifier>851</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Ninety-Seventh Congress</namePart>
            <namePart type="date">1981-1983</namePart>
            <recordInfo>
              <recordIdentifier>8480</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Monetary Policy</theme>
              <recordInfo>
                <recordIdentifier>11</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Monetary policy</topic>
              <recordInfo>
                <recordIdentifier>4250</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Federal Reserve banks</topic>
              <recordInfo>
                <recordIdentifier>4173</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Activities and Policies of District Banks and Their Implications for Monetary Policy</title>
            <subTitle>Hearing Before the Subcommittee on Domestic Monetary Policy of the Committee on Banking, Finance and Urban Affairs, House of Representatives</subTitle>
            <titlePartNumber>Ninety-Seventh Congress, Second Session, September 23, 1982</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">9287508</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1982-09-23</sortDate>
            <publisher>U.S. Government Printing Office</publisher>
            <dateIssued>September 23, 1982</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>203</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:20:36</recordUpdatedDate>
            <recordCreationDate>2011-04-01 16:54:50.752146</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>206 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=203&amp;amp;filepath=/files/docs/historical/federal reserve history/198209hr_actpoldb.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4242</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Reprinted from the Monthly labor review, Oct. 1943 with additional data.</note>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Consumer cooperatives</topic>
              <recordInfo>
                <recordIdentifier>5559</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Activities of Consumers' Cooperatives in 1942</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 757</subTitle>
          </titleInfo>
          <identifier type="oclc">21415493</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1943-01-01</sortDate>
            <publisher>G.P.O.</publisher>
            <dateIssued>1943</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4242</recordIdentifier>
            <recordUpdatedDate>2016-05-19 11:40:24</recordUpdatedDate>
            <recordCreationDate>2015-05-20 15:52:10</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD8051 .L123 v.10</classification>
          <classification authority="sudocs">L 2.3:757</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>12 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=4242&amp;amp;filepath=/files/docs/publications/bls/bls_0757_1943.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4289</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Prepared by: Editorial and Research Division, 1944; Labor Economics Staff, 1945.</note>
          <note type="public">Reprinted from the Monthly Labor Review, with additional data.</note>
          <genre>statistics</genre>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Credit unions</topic>
              <recordInfo>
                <recordIdentifier>4132</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Periodicals</topic>
              <recordInfo>
                <recordIdentifier>5485</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Activities of Credit Unions In</title>
          </titleInfo>
          <identifier type="oclc">39547168</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>periodical</issuance>
            <sortDate>1943-01-01</sortDate>
            <frequency>annual</frequency>
            <publisher>G.P.O.</publisher>
            <dateIssued point="start">1944-01-01</dateIssued>
            <dateIssued point="end">1947-01-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4289</recordIdentifier>
            <recordUpdatedDate>2016-05-19 11:40:29</recordUpdatedDate>
            <recordCreationDate>2015-05-20 15:52:11</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD8051 .A62 no. 797 [etc.]</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/4289</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5394</identifier>
        <datestamp>2017-03-02T14:17:53Z</datestamp>
        <setSpec>author:770</setSpec>
        <setSpec>author:9169</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Women's Bureau</namePart>
            <recordInfo>
              <recordIdentifier>770</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Peterson, Agnes Lydia</namePart>
            <recordInfo>
              <recordIdentifier>9169</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Cover title.</note>
          <note type="public">At head of title: United States Department of Labor. W.N. Doak, Secretary. Women's Bureau. Mary Anderson, Director.</note>
          <note type="public">"Prepared by Miss Agnes L. Peterson, assistant director of the bureau, for presentation to the Pan Pacific Women's Conference, in session in Honolulu August 9 to 22, 1930"--Letter of transmittal.</note>
          <genre>government publication</genre>
          <subject>
            <name>
              <name>United States. Women's Bureau</name>
              <recordInfo>
                <recordIdentifier>770</recordIdentifier>
              </recordInfo>
            </name>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Activities of the Women's Bureau of the United States</title>
            <subTitle>Women's Bureau Bulletin, No. 86</subTitle>
          </titleInfo>
          <identifier type="oclc">ocm07266840</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1931-01-01</sortDate>
            <publisher>Govt. Print. Off.</publisher>
            <dateIssued>1931</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5394</recordIdentifier>
            <recordUpdatedDate>2017-03-02 14:18:13</recordUpdatedDate>
            <recordCreationDate>2017-02-14 15:36:54</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the Women's Bureau</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin (United States. Women's Bureau)</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Women's Bureau Bulletin</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>243</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD6093 .A3 no. 86</classification>
          <classification authority="sudocs">L 13.3:86</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>19 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=5394&amp;amp;filepath=/files/docs/publications/women/b0086_dolwb_1931.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=5394&amp;amp;filepath=/files/docs/publications/women/b0086_dolwb_1931.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1116</identifier>
        <datestamp>2017-05-08T10:24:29Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">13 Stat 498. 38th Congress, 2d Session, Ch. 82.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Money</topic>
              <recordInfo>
                <recordIdentifier>4251</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Act to Amend an Act Entitled "An Act to Provide a National Currency, Secured by a Pledge of United States Bonds, and to Provide for the Circulation and Redemption Thereof"</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Act of March 3, 1865</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1865-03-03</sortDate>
            <dateIssued>March 3, 1865</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1116</recordIdentifier>
            <recordUpdatedDate>2017-05-08 10:24:29</recordUpdatedDate>
            <recordCreationDate>2013-11-26 10:48:47.612387</recordCreationDate>
          </recordInfo>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>National Bank Act</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>An Act to Provide a National Currency, Secured by a Pledge of United States Bonds, and to Provide for the Circulation and Redemption Thereof</title>
            </titleInfo>
            <titleInfo type="abbreviated">
              <title>Act of June 3, 1864</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>National Banking Act of 1864</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1113</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>1 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1116&amp;amp;filepath=/files/docs/historical/congressional/national-bank-act-amendment-1865.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:356</identifier>
        <datestamp>2017-05-08T08:38:59Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">48 Stat. 969.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <titleInfo>
              <titleInfo>Federal Reserve Act</titleInfo>
              <recordInfo>
                <recordIdentifier>4172</recordIdentifier>
              </recordInfo>
            </titleInfo>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Act to Amend Section 12B of the Federal Reserve Act so as to Extend for One Year the Temporary Plan for Deposit Insurance, and for Other Purposes</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Public Law 73-362, 73d Congress, S. 3025</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1934-06-16</sortDate>
            <dateIssued>June 16, 1934</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>356</recordIdentifier>
            <recordUpdatedDate>2017-05-08 08:38:59</recordUpdatedDate>
            <recordCreationDate>2011-12-07 16:52:16.59022</recordCreationDate>
          </recordInfo>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>S. 3025, An Act to Amend Section 12B of the Federal Reserve Act so as to Extend for One Year the Temporary Plan for Deposit Insurance, and for Other Purposes</title>
              <subTitle>73d Congress, 2d Session</subTitle>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>382</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Extension for 1 Year of the Temporary Plan for Deposit Insurance and Other Amendments to the Federal Reserve Act</title>
              <subTitle>Report (To Accompany S. 3025)</subTitle>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Extension for One Year on the Temporary Plan for Deposit Insurance and Other Amendments to the Federal Reserve Act</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>379</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>To Amend the Federal Reserve Act</title>
              <subTitle>Conference Report (To Accompany S. 3025)</subTitle>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>378</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>4 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=356&amp;amp;filepath=/files/docs/historical/fr_act/pa362_s3025_cong_19340616.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=356&amp;amp;filepath=/files/docs/historical/fr_act/pa362_s3025_cong_19340616.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:353</identifier>
        <datestamp>2017-05-08T08:38:17Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">59 Stat. 237.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Banks and banking</topic>
              <recordInfo>
                <recordIdentifier>4093</recordIdentifier>
              </recordInfo>
            </topic>
            <titleInfo>
              <titleInfo>Federal Reserve Act</titleInfo>
              <recordInfo>
                <recordIdentifier>4172</recordIdentifier>
              </recordInfo>
            </titleInfo>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Act to Amend Sections 11 (c) and 16 of the Federal Reserve Act, as Amended, and for Other Purposes</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Public Law 79-84, 79th Congress, S. 510</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1945-06-12</sortDate>
            <dateIssued>June 12, 1945</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>353</recordIdentifier>
            <recordUpdatedDate>2017-05-08 08:38:17</recordUpdatedDate>
            <recordCreationDate>2011-12-07 16:49:13.97084</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>2 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=353&amp;amp;filepath=/files/docs/historical/fr_act/pl84_s510_cong_19450612.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=353&amp;amp;filepath=/files/docs/historical/fr_act/pl84_s510_cong_19450612.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5564</identifier>
        <datestamp>2017-05-08T14:14:34Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">41 Stat. 987</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <name>
              <name>United States. Women's Bureau</name>
              <recordInfo>
                <recordIdentifier>770</recordIdentifier>
              </recordInfo>
            </name>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Act to Establish in the Department of Labor a Bureau to be Known as the Women's Bureau</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Public Law 66-259, 66th Congress, H.R. 13229</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1920-06-05</sortDate>
            <dateIssued>June 5, 1920</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5564</recordIdentifier>
            <recordUpdatedDate>2017-05-08 14:14:34</recordUpdatedDate>
            <recordCreationDate>2017-05-08 12:58:17</recordCreationDate>
            <recordContentSource>frs</recordContentSource>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>1 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=5564&amp;amp;filepath=/files/docs/historical/congressional/act-establish-womens-bureau.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=5564&amp;amp;filepath=/files/docs/historical/congressional/act-establish-womens-bureau.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1121</identifier>
        <datestamp>2017-05-08T10:27:47Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">1 Stat. 65. 1st Congress, 1st Session, Ch. 12.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <name>
              <name>United States. Department of the Treasury</name>
              <recordInfo>
                <recordIdentifier>33</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Money</topic>
              <recordInfo>
                <recordIdentifier>4251</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Act to Establish the Treasury Department</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Act of September 2, 1789</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Treasury Act</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1789-09-02</sortDate>
            <dateIssued>September 2, 1789</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1121</recordIdentifier>
            <recordUpdatedDate>2017-05-08 10:27:47</recordUpdatedDate>
            <recordCreationDate>2013-11-26 11:18:44.96258</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>3 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1121&amp;amp;filepath=/files/docs/historical/congressional/treasury-act.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=1121&amp;amp;filepath=/files/docs/historical/congressional/treasury-act.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:348</identifier>
        <datestamp>2017-05-08T08:24:23Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">44 Stat. 1234.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Act to Improve the Facilities of the Federal Reserve System for the Service of Commerce, Industry, &amp;amp; Agriculture, to Provide Means for Meeting the Needs of Member Banks in Exceptional Circumstances, &amp;amp; for Other Purposes</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Public Law 72-44, 72d Congress, H.R. 9203</title>
          </titleInfo>
          <identifier type="oclc">255375931</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1932-02-27</sortDate>
            <dateIssued>February 27, 1932</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>348</recordIdentifier>
            <recordUpdatedDate>2017-05-08 08:24:23</recordUpdatedDate>
            <recordCreationDate>2011-12-07 11:11:29.057707</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>2 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=348&amp;amp;filepath=/files/docs/historical/fr_act/pa44_hr9203_cong_19320227_orig.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=348&amp;amp;filepath=/files/docs/historical/fr_act/pa44_hr9203_cong_19320227_orig.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1124</identifier>
        <datestamp>2017-05-08T14:50:10Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">1 Stat. 191. 1st Congress, 3d Session, Ch. 10.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <name>
              <name>Bank of the United States, 1791-1811</name>
              <recordInfo>
                <recordIdentifier>570</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Act to Incorporate the Subscribers to the Bank of the United States (1791)</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Act of February 25, 1791</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1791-02-25</sortDate>
            <dateIssued>February 25, 1791</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1124</recordIdentifier>
            <recordUpdatedDate>2017-05-08 14:50:10</recordUpdatedDate>
            <recordCreationDate>2013-11-27 14:49:51.046524</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>7 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1124&amp;amp;filepath=/files/docs/historical/congressional/first-bank-united-states.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=1124&amp;amp;filepath=/files/docs/historical/congressional/first-bank-united-states.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1118</identifier>
        <datestamp>2017-05-08T14:50:38Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">3 Stat. 266. 14th Congress, 1st Session, Ch. 44.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <name>
              <name>Bank of the United States, 1816-1836</name>
              <recordInfo>
                <recordIdentifier>458</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Act to Incorporate the Subscribers to the Bank of the United States (1816)</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Act of April 10, 1816</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1816-04-10</sortDate>
            <dateIssued>April 10, 1816</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1118</recordIdentifier>
            <recordUpdatedDate>2017-05-08 14:50:38</recordUpdatedDate>
            <recordCreationDate>2013-11-26 10:57:20.090945</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>12 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1118&amp;amp;filepath=/files/docs/historical/congressional/second-bank-united-states.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1126</identifier>
        <datestamp>2017-05-08T09:56:20Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">64 Stat. 463. Chapter 754, 2d Session.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Act to Permit National Banks to Give Security in the Form Required by State Law for Deposits of Funds by Local Public Agencies and Officers</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Public Law 81-715, 81st Congress, H.R. 8597</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1950-08-18</sortDate>
            <dateIssued>August 18, 1950</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1126</recordIdentifier>
            <recordUpdatedDate>2017-05-08 09:56:20</recordUpdatedDate>
            <recordCreationDate>2013-11-27 14:56:00.388201</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>1 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1126&amp;amp;filepath=/files/docs/historical/congressional/public-law-81-715.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=1126&amp;amp;filepath=/files/docs/historical/congressional/public-law-81-715.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:349</identifier>
        <datestamp>2017-05-08T11:31:22Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">48 Stat. 20.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Banks and banking</topic>
              <recordInfo>
                <recordIdentifier>4093</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Act to Provide for Direct Loans by Federal Reserve Banks to State Banks and Trust Companies in Certain Cases, and for Other Purposes</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Public Law 73-4, 73d Congress, H.R. 3757</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1933-03-24</sortDate>
            <dateIssued>March 24, 1933</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>349</recordIdentifier>
            <recordUpdatedDate>2017-05-08 11:31:22</recordUpdatedDate>
            <recordCreationDate>2011-12-07 16:45:24.426618</recordCreationDate>
          </recordInfo>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Draft: Public Act, 73d Congress (H.R. 3757) An Act to Provide for Direct Loans by Federal Reserve Banks to State Banks and Trust Companies in Certain Cases, and for Other Purposes</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>351</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Emergency Banking Relief Act</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>An Act to Provide Relief in the Existing National Emergency in Banking, and for Other Purposes</title>
            </titleInfo>
            <titleInfo type="abbreviated">
              <title>Public Law 73-1, 73d Congress, H.R. 1491</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Emergency Banking Act</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Emergency Banking Act of 1933</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1098</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>2 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=349&amp;amp;filepath=/files/docs/historical/fr_act/pa4_hr3757_cong_19330324.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=349&amp;amp;filepath=/files/docs/historical/fr_act/pa4_hr3757_cong_19330324.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1125</identifier>
        <datestamp>2017-05-08T09:54:10Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">64 Stat. 455. Chapter 729, 2d Session.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Act to Provide for the Conversion of National Banking Associations Into and Their Merger or Consolidation with State Banks, and for Other Purposes</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Public Law 81-706, 81st Congress, H.R. 1161</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1950-08-17</sortDate>
            <dateIssued>August 17, 1950</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1125</recordIdentifier>
            <recordUpdatedDate>2017-05-08 09:54:10</recordUpdatedDate>
            <recordCreationDate>2013-11-27 14:53:55.633485</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>4 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1125&amp;amp;filepath=/files/docs/historical/congressional/public-law-81-706.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=1125&amp;amp;filepath=/files/docs/historical/congressional/public-law-81-706.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:3620</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:486</setSpec>
        <setSpec>author:547</setSpec>
        <setSpec>author:554</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Southard, Samuel L. (Samuel Lewis)</namePart>
            <namePart type="date">1787-1842</namePart>
            <recordInfo>
              <recordIdentifier>486</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Pennsylvania. General Assembly. House of Representatives</namePart>
            <namePart>House of Representatives</namePart>
            <recordInfo>
              <recordIdentifier>547</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Fenn, Theophilus</namePart>
            <namePart type="date">1800-1883</namePart>
            <recordInfo>
              <recordIdentifier>554</recordIdentifier>
            </recordInfo>
          </name>
          <genre>legislation</genre>
          <subject>
            <name>
              <name>Bank of the United States (Pennsylvania), 1836-1841</name>
              <recordInfo>
                <recordIdentifier>550</recordIdentifier>
              </recordInfo>
            </name>
            <topic>
              <topic>Taxation</topic>
              <recordInfo>
                <recordIdentifier>4290</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States -- Pennsylvania</geographic>
              <recordInfo>
                <recordIdentifier>4404</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Act to Repeal the State Tax on Real and Personal Property, and to Continue and Extend the Improvements of the State by Railroads and Canals, and to Charter a State Bank to be Called the United States Bank</title>
          </titleInfo>
          <identifier type="oclc">54743234</identifier>
          <originInfo>
            <place>Harrisburg</place>
            <issuance>monographic</issuance>
            <sortDate>1836-02-24</sortDate>
            <publisher>Theo. Fenn</publisher>
            <dateIssued>February 24, 1836</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>3620</recordIdentifier>
            <recordUpdatedDate>2015-10-09 11:07:34</recordUpdatedDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>24 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=3620&amp;amp;filepath=/files/docs/bankunitedstates/firstsecondbank_pennhouse_charter_18360224.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5096</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:5929</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Office of the Special Inspector General for the Troubled Asset Relief Program</namePart>
            <recordInfo>
              <recordIdentifier>5929</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <name>
              <name>Troubled Asset Relief Program (U.S.)</name>
              <recordInfo>
                <recordIdentifier>5915</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Financial Crisis of 2007-2009</theme>
              <recordInfo>
                <recordIdentifier>103</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Economic assistance, Domestic</topic>
              <recordInfo>
                <recordIdentifier>4900</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Additional Insight on Use of Troubled Asset Relief Program Funds</title>
            <titlePartNumber>SIGTARP-10-004</titlePartNumber>
          </titleInfo>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>2009-12-10</sortDate>
            <publisher>U.S. G.P.O.</publisher>
            <dateIssued>December 10, 2009</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5096</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:33:59</recordUpdatedDate>
            <recordCreationDate>2015-12-22 16:37:04</recordCreationDate>
            <recordContentSource>frs</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Audits and Other Reports from the Office of the Special Inspector General for the Troubled Asset Relief Program (SIGTARP)</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>4965</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>electronic</form>
            <extent>40 pages</extent>
            <digitalOrigin>born digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=5096&amp;amp;filepath=/files/docs/historical/fct/sigtarp_report_additionalinsighttarp_20091210.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=5096&amp;amp;filepath=/files/docs/historical/fct/sigtarp_report_additionalinsighttarp_20091210.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:140</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:4048</setSpec>
        <setSpec>author:4059</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Beal, James H.</namePart>
            <recordInfo>
              <recordIdentifier>4048</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Wills, David C.</namePart>
            <recordInfo>
              <recordIdentifier>4059</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Addresses delivered by James H. Beal, Mr. D.C. Wills, and Dr. J.T. Holdsworth at a luncheon given by the Subcommittee on Capital Issues, William Penn Hotel, Pittsburgh, Pennsylvania.</note>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Finance</topic>
              <recordInfo>
                <recordIdentifier>4187</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Addresses At a Luncheon Given By the Subcommittee on Capital Issues : June 28, 1918</title>
          </titleInfo>
          <identifier type="oclc">742050982</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1918-06-28</sortDate>
            <dateIssued>June 28, 1918</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>140</recordIdentifier>
            <recordUpdatedDate>2015-04-06 15:57:11</recordUpdatedDate>
            <recordCreationDate>2010-02-12 10:30:10.488008</recordCreationDate>
          </recordInfo>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Capital Issues Committee (Created By the War Finance Corporation Act) Rules and Regulations</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>139</recordIdentifier>
            </recordInfo>
            <typeOfResource>text</typeOfResource>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>20 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=140&amp;amp;filepath=/files/docs/publications/wfca/cic_beal19180628.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=140&amp;amp;filepath=/files/docs/publications/wfca/cic_beal19180628.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4552</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Includes bibliographical references (pages 80-86).</note>
          <genre>bibliography</genre>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Automation</topic>
              <recordInfo>
                <recordIdentifier>5595</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Adjustments to the Introduction of Office Automation : A Study of Some Implications of the Installation of Electronic Data Processing in 20 Offices in Private Industry, with Special Reference to Older Workers</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 1276</subTitle>
          </titleInfo>
          <identifier type="oclc">07502980</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1960-01-01</sortDate>
            <publisher>G.P.O.</publisher>
            <dateIssued>1960</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4552</recordIdentifier>
            <recordUpdatedDate>2016-05-19 10:51:07</recordUpdatedDate>
            <recordCreationDate>2015-07-14 14:24:48</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD45 .W37</classification>
          <classification authority="ddc">658 U59</classification>
          <classification authority="sudocs">L 2.3:1276</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>97 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=4552&amp;amp;filepath=/files/docs/publications/bls/bls_1276_1960.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:3808</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:4115</setSpec>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Price, George Moses</namePart>
            <namePart type="date">1864-1942</namePart>
            <recordInfo>
              <recordIdentifier>4115</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">At head of title: U.S. Department of Labor, Bureau of Labor Statistics.</note>
          <note type="public">Includes bibliographical references and index.</note>
          <note type="public">Also issued online.</note>
          <genre>government publication</genre>
          <genre>statistics</genre>
          <subject>
            <topic>
              <topic>Labor</topic>
              <recordInfo>
                <recordIdentifier>4237</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>Europe</geographic>
              <recordInfo>
                <recordIdentifier>4458</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Administration of Labor Laws and Factory Inspection in Certain European Countries</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 142</subTitle>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Administration of Labor Laws and Factory Inspection in Certain European Countries</title>
            <subTitle>Foreign Labor Laws Series</subTitle>
            <titlePartNumber>No. 1</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">08116837</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1914-02-27</sortDate>
            <publisher>Government Printing Office</publisher>
            <dateIssued>February 27, 1914</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>3808</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:13:13</recordUpdatedDate>
            <recordCreationDate>2015-01-14 17:56:50</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="series">
            <titleInfo>
              <title>Foreign Labor Laws Series</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3781</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">KJC2855 .P74 1914</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>310 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=3808&amp;amp;filepath=/files/docs/publications/bls/bls_0142_1914.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:3887</identifier>
        <datestamp>2017-03-02T14:54:13Z</datestamp>
        <setSpec>author:4123</setSpec>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Sweeney, Charles Patrick</namePart>
            <recordInfo>
              <recordIdentifier>4123</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">At head of title: U.S. Department of Labor. W.B. Wilson, secretary. Bureau of Labor Statistics. Ethelbert Stewart, commissioner.</note>
          <note type="public">Issued also as House doc. 836, 66th Cong., 3d sess.</note>
          <genre>government publication</genre>
          <genre>statistics</genre>
          <subject>
            <geographic>
              <geographic>Great Britain</geographic>
              <recordInfo>
                <recordIdentifier>4211</recordIdentifier>
              </recordInfo>
            </geographic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Adult Working-Class Education in Great Britain and the United States: (A Study of Recent Developments)</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 271</subTitle>
          </titleInfo>
          <identifier type="oclc">04393583</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1920-08-01</sortDate>
            <publisher>Government Printing Office</publisher>
            <dateIssued>August 1920</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>3887</recordIdentifier>
            <recordUpdatedDate>2017-03-02 14:54:40</recordUpdatedDate>
            <recordCreationDate>2015-01-14 17:56:51</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
              <subTitle>Miscellaneous Series</subTitle>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3779</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD8051 .A5 no.271</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>107 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=3887&amp;amp;filepath=/files/docs/publications/bls/bls_0271_1920.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:765</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:524</setSpec>
        <setSpec>author:8475</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. Senate. Committee on Finance</namePart>
            <namePart type="date">1815-</namePart>
            <recordInfo>
              <recordIdentifier>524</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Eighty-Seventh Congress</namePart>
            <namePart type="date">1961-1963</namePart>
            <recordInfo>
              <recordIdentifier>8475</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Debt limit</topic>
              <recordInfo>
                <recordIdentifier>7393</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Debts, Public</topic>
              <recordInfo>
                <recordIdentifier>4136</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Advance Refunding and Debt Management</title>
            <subTitle>Hearings Before the Committee on Finance, United States Senate</subTitle>
            <titlePartNumber>Eighty-Seventh Congress, Second Session, March 14 and 16, 1962</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">6884457</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1962-03-14</sortDate>
            <publisher>U.S. Government Printing Office</publisher>
            <dateIssued>March 14 and 16, 1962</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>765</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:10:56</recordUpdatedDate>
            <recordCreationDate>2012-05-16 13:04:13.345328</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4.F 49:D 35/6</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>89 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=765&amp;amp;filepath=/files/docs/historical/congressional/19620316_sen_refunddebtmgt.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1144</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:129</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Reserve Bank of St. Louis</namePart>
            <recordInfo>
              <recordIdentifier>129</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">A brochure from the Federal Reserve Bank of St. Louis.</note>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Federal Reserve banks</topic>
              <recordInfo>
                <recordIdentifier>4173</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Advantages of Membership in the Federal Reserve System</title>
          </titleInfo>
          <identifier type="oclc">21192527</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1922-01-25</sortDate>
            <dateIssued>January 25, 1922</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1144</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:19:55</recordUpdatedDate>
            <recordCreationDate>2013-12-03 09:13:17.66825</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>8 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1144&amp;amp;filepath=/files/docs/historical/frbsl_history/general_advantages_membership_1922.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=1144&amp;amp;filepath=/files/docs/historical/frbsl_history/general_advantages_membership_1922.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5307</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:151</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Glass, Carter</namePart>
            <namePart type="date">1858-1946</namePart>
            <recordInfo>
              <recordIdentifier>151</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">An account of the Federal Reserve System.</note>
          <note type="public">Copy inscribed by author</note>
          <note type="public">Annotated by Robert L. Owen</note>
          <subject>
            <topic>
              <topic>Federal Reserve banks</topic>
              <recordInfo>
                <recordIdentifier>4173</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Adventure in Constructive Finance</title>
            <titlePartNumber>Box 1, Folder 7, Item 1</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">00742526</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <publisher>Doubleday, Page</publisher>
            <dateIssued>1927</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5307</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:20:05</recordUpdatedDate>
            <recordCreationDate>2016-12-16 09:19:30</recordCreationDate>
          </recordInfo>
          <relatedItem type="itemParent">
            <titleInfo>
              <title>Miscellaneous Clippings</title>
              <titlePartNumber>Box 1, Folder 7</titlePartNumber>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>535323</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HG2563 .G45</classification>
          <classification authority="ddc">332.11 G54a</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>436 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=5307&amp;amp;filepath=/files/docs/historical/owen/owen_01_07_01.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=5307&amp;amp;filepath=/files/docs/historical/owen/owen_01_07_01.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1316</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:3164</setSpec>
        <setSpec>author:3163</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Weidenbaum Center on the Economy, Government, and Public Policy</namePart>
            <recordInfo>
              <recordIdentifier>3164</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Weidenbaum, Murray L.</namePart>
            <recordInfo>
              <recordIdentifier>3163</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">At head of title: Murray Weidenbaum Center on the Economy, Government, and Public Policy.</note>
          <note type="public"> Murray Weidenbaum Center on the Economy, Government, and Public Policy</note>
          <subject>
            <name>
              <name>Reagan, Ronald</name>
              <recordInfo>
                <recordIdentifier>658</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Meltzer's History of the Federal Reserve - Primary Sources</theme>
              <recordInfo>
                <recordIdentifier>97</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Economic policy</topic>
              <recordInfo>
                <recordIdentifier>4150</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Advising Reagan</title>
            <subTitle>Making Economic Policy, 1981-82 : a Memoir</subTitle>
          </titleInfo>
          <identifier type="oclc">62355314</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>2005-06-01</sortDate>
            <dateIssued>June 2005</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1316</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:34:45</recordUpdatedDate>
            <recordCreationDate>2014-04-17 14:51:20.54208</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>54 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1316&amp;amp;filepath=/files/docs/meltzer/weiadv2005.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=1316&amp;amp;filepath=/files/docs/meltzer/weiadv2005.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1492</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:4612</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>American Enterprise Institute for Public Policy Research</namePart>
            <recordInfo>
              <recordIdentifier>4612</recordIdentifier>
            </recordInfo>
          </name>
          <genre>series</genre>
          <titleInfo>
            <title>AEI Studies</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>American Enterprise Institute Studies</title>
          </titleInfo>
          <originInfo>
            <issuance>multipart</issuance>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1492</recordIdentifier>
            <recordUpdatedDate>2015-08-31 15:38:14</recordUpdatedDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <location>
            <url>https://fraser.stlouisfed.org/series/1492</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/series/1492</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5415</identifier>
        <datestamp>2017-02-15T15:26:47Z</datestamp>
        <setSpec>author:770</setSpec>
        <setSpec>author:9116</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Women's Bureau</namePart>
            <recordInfo>
              <recordIdentifier>770</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Byrne, Harriet A. (Harriet Anne)</namePart>
            <namePart type="date">1892-</namePart>
            <recordInfo>
              <recordIdentifier>9116</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <language>eng</language>
          <titleInfo>
            <title>The Age Factor as it Relates to Women in Business and the Professions</title>
            <subTitle>Women's Bureau Bulletin, No. 117</subTitle>
          </titleInfo>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1934-01-01</sortDate>
            <publisher>Govt. Print. Off.</publisher>
            <dateIssued>1934</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5415</recordIdentifier>
            <recordUpdatedDate>2017-02-15 15:27:21</recordUpdatedDate>
            <recordCreationDate>2017-02-14 15:37:00</recordCreationDate>
            <recordContentSource>frs</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the Women's Bureau</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the Women's Bureau</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Women's Bureau Bulletin</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>243</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>72 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=5415&amp;amp;filepath=/files/docs/publications/women/b0117_dolwb_1934.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=5415&amp;amp;filepath=/files/docs/publications/women/b0117_dolwb_1934.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5148</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:6300</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Department of the Treasury. Office of Financial Stability</namePart>
            <recordInfo>
              <recordIdentifier>6300</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <genre>periodical</genre>
          <subject>
            <name>
              <name>Troubled Asset Relief Program (U.S.)</name>
              <recordInfo>
                <recordIdentifier>5915</recordIdentifier>
              </recordInfo>
            </name>
            <name>
              <name>United States. Department of the Treasury. Office of Financial Stability</name>
              <recordInfo>
                <recordIdentifier>6300</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Financial Crisis of 2007-2009</theme>
              <recordInfo>
                <recordIdentifier>103</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Periodicals</topic>
              <recordInfo>
                <recordIdentifier>5485</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Agency Financial Report</title>
          </titleInfo>
          <identifier type="oclc">769222378</identifier>
          <originInfo>
            <place>Washington, D.C.</place>
            <issuance>periodical</issuance>
            <sortDate>2009-09-30</sortDate>
            <frequency>annual</frequency>
            <publisher>U.S. Dept. of the Treasury, Office of Financial Stability</publisher>
            <dateIssued point="start">2009-09-30</dateIssued>
            <dateIssued point="end">2016-11-30</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5148</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:33:48</recordUpdatedDate>
            <recordCreationDate>2015-12-31 11:55:12</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <classification authority="ddc">332</classification>
          <classification authority="sudocs">T 1.72/6:</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>electronic</form>
            <digitalOrigin>born digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/5148</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/5148</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:874</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:401</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congressional Budget Office</namePart>
            <recordInfo>
              <recordIdentifier>401</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Social security</topic>
              <recordInfo>
                <recordIdentifier>4282</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
            <geographic>
              <geographic>United States -- Texas</geographic>
              <recordInfo>
                <recordIdentifier>4417</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Aggregate Economic Effects of Changes in Social Security Taxes</title>
          </titleInfo>
          <identifier type="oclc">8475240</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1978-08-01</sortDate>
            <dateIssued>August 1978</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>874</recordIdentifier>
            <recordUpdatedDate>2015-10-07 09:49:40</recordUpdatedDate>
            <recordCreationDate>2012-11-23 11:33:06.573455</recordCreationDate>
          </recordInfo>
          <classification authority="sudocs">Y 10.11:Ec 7</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>69 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=874&amp;amp;filepath=/files/docs/publications/cbo/aggecon_cbo_197808.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=874&amp;amp;filepath=/files/docs/publications/cbo/aggecon_cbo_197808.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4978</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:4799</setSpec>
        <setSpec>author:5600</setSpec>
        <setSpec>author:5630</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>European Central Bank</namePart>
            <recordInfo>
              <recordIdentifier>4799</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Committee of European Banking Supervisors</namePart>
            <recordInfo>
              <recordIdentifier>5600</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>European Commission</namePart>
            <recordInfo>
              <recordIdentifier>5630</recordIdentifier>
            </recordInfo>
          </name>
          <subject>
            <theme>
              <theme>Financial Crisis of 2007-2009</theme>
              <recordInfo>
                <recordIdentifier>103</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Banks and banking</topic>
              <recordInfo>
                <recordIdentifier>4093</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Bank supervision</topic>
              <recordInfo>
                <recordIdentifier>4089</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>European Union</geographic>
              <recordInfo>
                <recordIdentifier>7099</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Aggregate Outcome of the 2010 EU Wide Stress Test Exercise Coordinated by CEBS in Cooperation with the ECB</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>2010-07-23</sortDate>
            <dateIssued>July 23, 2010</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4978</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:33:50</recordUpdatedDate>
            <recordCreationDate>2015-12-22 16:36:42</recordCreationDate>
            <recordContentSource>frs</recordContentSource>
          </recordInfo>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Joint Press Release - Publication of the Results of the EU-Wide Stress-Testing Exercise</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>4977</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>electronic</form>
            <extent>55 pages</extent>
            <digitalOrigin>born digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=4978&amp;amp;filepath=/files/docs/historical/fct/cebs_stresstest_20100723.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=4978&amp;amp;filepath=/files/docs/historical/fct/cebs_stresstest_20100723.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5286</identifier>
        <datestamp>2017-03-03T16:37:38Z</datestamp>
        <setSpec>author:516</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Reserve Bank of Chicago</namePart>
            <recordInfo>
              <recordIdentifier>516</recordIdentifier>
            </recordInfo>
          </name>
          <subject>
            <theme>
              <theme>Federal Reserve Bank of Chicago</theme>
              <recordInfo>
                <recordIdentifier>44</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Agriculture</topic>
              <recordInfo>
                <recordIdentifier>4065</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Periodicals</topic>
              <recordInfo>
                <recordIdentifier>5485</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <abstract>Newsletter reporting on agricultural developments in the United States and the Seventh Federal Reserve District.</abstract>
          <titleInfo>
            <title>Agletter</title>
            <subTitle>The Agricultural Newsletter From the Federal Reserve Bank of Chicago</subTitle>
          </titleInfo>
          <titleInfo type="alternate">
            <title>AgLetter (Chic. Ill.)</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Ag letter</title>
          </titleInfo>
          <identifier type="oclc">31915112</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <sortDate>1979-07-01</sortDate>
            <frequency>monthly</frequency>
            <publisher>Federal Reserve Bank of Chicago</publisher>
            <dateIssued point="start">1979-07-01</dateIssued>
            <dateIssued point="end">2016-05-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5286</recordIdentifier>
            <recordUpdatedDate>2017-03-03 16:37:48</recordUpdatedDate>
            <recordCreationDate>2016-08-16 13:19:41</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <classification authority="lcc">S21 .A19</classification>
          <classification authority="ddc">338</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/5286</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/5286</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1091</identifier>
        <datestamp>2017-05-08T09:35:39Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">62 Stat. 1247.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Banks and banking</topic>
              <recordInfo>
                <recordIdentifier>4093</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Agricultural Act of 1948</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>An Act to Authorize the Secretary of Agriculture to Stabilize Prices of Agricultural Commodities; to Amend Section 22 of the Agricultural Adjustment Act, Reenacted by the Agricultural Marketing Agreement Act of 1937; and for Other Purposes</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Public Law 80-897, 80th Congress, H.R. 6248</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1948-07-03</sortDate>
            <dateIssued>July 3, 1948</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1091</recordIdentifier>
            <recordUpdatedDate>2017-05-08 09:35:39</recordUpdatedDate>
            <recordCreationDate>2013-11-25 10:30:50.391735</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>13 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1091&amp;amp;filepath=/files/docs/historical/congressional/agricultural-act-1948.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=1091&amp;amp;filepath=/files/docs/historical/congressional/agricultural-act-1948.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:699</identifier>
        <datestamp>2017-02-24T16:26:04Z</datestamp>
        <setSpec>author:405</setSpec>
        <setSpec>author:3</setSpec>
        <setSpec>author:406</setSpec>
        <setSpec>author:407</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Bunce, Arthur C. (Arthur Cyril)</namePart>
            <namePart type="date">1901-1953</namePart>
            <recordInfo>
              <recordIdentifier>405</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Board of Governors of the Federal Reserve System (U.S.)</namePart>
            <namePart type="date">1935-</namePart>
            <recordInfo>
              <recordIdentifier>3</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Fisher, W. Halder (William Halder)</namePart>
            <namePart type="date">1914-</namePart>
            <recordInfo>
              <recordIdentifier>406</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Rauber, Earle L. (Earle Leroy)</namePart>
            <recordInfo>
              <recordIdentifier>407</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Meltzer's History of the Federal Reserve - Primary Sources</theme>
              <recordInfo>
                <recordIdentifier>97</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Agriculture</topic>
              <recordInfo>
                <recordIdentifier>4065</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
            <geographic>
              <geographic>Europe</geographic>
              <recordInfo>
                <recordIdentifier>4458</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Agricultural Adjustment and Income</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Postwar Economic Studies</title>
            <titlePartNumber>No. 2, October 1945</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">826911</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1945-10-01</sortDate>
            <dateIssued>October 1945</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>699</recordIdentifier>
            <recordUpdatedDate>2017-02-24 16:26:04</recordUpdatedDate>
            <recordCreationDate>2012-05-16 13:04:13.345328</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Postwar Economic Studies</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1485</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">FR 1.27:2</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>70 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=699&amp;amp;filepath=/files/docs/historical/federal reserve history/bog_publications/pes_2_1945.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=699&amp;amp;filepath=/files/docs/historical/federal reserve history/bog_publications/pes_2_1945.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1103</identifier>
        <datestamp>2017-05-08T09:45:11Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">42 Stat. 1461.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Agricultural Credits Act of 1923</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>An Act to Provide Additional Credit Facilities for the Agricultural and Live-stock Industries of the United States; to Amend the Federal Farm Loan Act; to Amend the Federal Reserve Act; and for Other Purposes</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Public Law 67-503, 67th Congress, S. 4280</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1923-03-04</sortDate>
            <dateIssued>March 4, 1923</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1103</recordIdentifier>
            <recordUpdatedDate>2017-05-08 09:45:12</recordUpdatedDate>
            <recordCreationDate>2013-11-26 10:08:35.208454</recordCreationDate>
          </recordInfo>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Federal Reserve Act</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>An Act to Provide for the Establishment of Federal Reserve Banks, to Furnish an Elastic Currency, to Afford Means of Rediscounting Commercial Paper, to Establish a More Effective Supervision of Banking in the United States, and for Other Purposes</title>
            </titleInfo>
            <titleInfo type="abbreviated">
              <title>Public Law 63-43, 63d Congress, H.R. 7837</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>975</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>The Federal Farm Loan Act</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>An Act to Provide Capital for Agricultural Development, to Create Standard Forms of Investment Based Upon Farm Mortgage, to Equalize Rates of Interest Upon Farm Loans, to Furnish a Market for United States Bonds, to Create Government Depositaries and Financial Agents for the United States, and for Other Purposes</title>
            </titleInfo>
            <titleInfo type="abbreviated">
              <title>Public Law 64-158, 64th Congress, S. 2986</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1102</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>32 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1103&amp;amp;filepath=/files/docs/historical/congressional/federal-farm-loan-amendments-1923.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=1103&amp;amp;filepath=/files/docs/historical/congressional/federal-farm-loan-amendments-1923.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:959</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:348</setSpec>
        <setSpec>author:8455</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. Joint Commission of Agricultural Inquiry</namePart>
            <namePart type="date">1921-[1922]</namePart>
            <recordInfo>
              <recordIdentifier>348</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Sixty-Seventh Congress</namePart>
            <namePart type="date">1921-1923</namePart>
            <recordInfo>
              <recordIdentifier>8455</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Meltzer's History of the Federal Reserve - Primary Sources</theme>
              <recordInfo>
                <recordIdentifier>97</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Agriculture</topic>
              <recordInfo>
                <recordIdentifier>4065</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Agricultural Inquiry</title>
            <subTitle>Hearings Before the Joint Commission of Agricultural Inquiry</subTitle>
            <titlePartNumber>Sixty-Seventh Congress, First Session, Under Senate Concurrent Resolution 4 (1921)</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">6257375</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>multipart</issuance>
            <publisher>Government Printing Office</publisher>
            <dateIssued point="start">1921-07-11</dateIssued>
            <dateIssued point="end">1921-08-04</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>959</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:35:19</recordUpdatedDate>
            <recordCreationDate>2013-03-11 15:51:35.102046</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4.AG 8/2:H 35</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/959</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5139</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:5923</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congressional Oversight Panel</namePart>
            <recordInfo>
              <recordIdentifier>5923</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <name>
              <name>American International Group, Inc.</name>
              <recordInfo>
                <recordIdentifier>5528</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Financial Crisis of 2007-2009</theme>
              <recordInfo>
                <recordIdentifier>103</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Insurance</topic>
              <recordInfo>
                <recordIdentifier>4886</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Subsidies</topic>
              <recordInfo>
                <recordIdentifier>6604</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Financial services industry</topic>
              <recordInfo>
                <recordIdentifier>6614</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Economic assistance, Domestic</topic>
              <recordInfo>
                <recordIdentifier>4900</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>The AIG Rescue, Its Impact on Markets, and the Government's Exit Strategy</title>
            <subTitle>Congressional Oversight Panel June Oversight Report</subTitle>
          </titleInfo>
          <titleInfo type="alternate">
            <title>June Oversight Report</title>
          </titleInfo>
          <identifier type="oclc">652546388</identifier>
          <originInfo>
            <place>Washington, DC</place>
            <issuance>monographic</issuance>
            <sortDate>2010-06-10</sortDate>
            <publisher>U.S. G.P.O.</publisher>
            <dateIssued>June 10, 2010</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5139</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:34:08</recordUpdatedDate>
            <recordCreationDate>2015-12-28 12:17:00</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Oversight Panel Reports</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>4963</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 3.2:C 76/3/AI 3</classification>
          <classification authority="lcc">HG8535.U35 2010eb </classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>electronic</form>
            <extent>291 pages</extent>
            <digitalOrigin>born digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=5139&amp;amp;filepath=/files/docs/historical/fct/cop_report_20100610.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4652</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:5231</setSpec>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Cimini, Michael H.</namePart>
            <recordInfo>
              <recordIdentifier>5231</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Includes bibliographical references (pages 47-48).</note>
          <genre>bibliography</genre>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Airlines</topic>
              <recordInfo>
                <recordIdentifier>5719</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Employees</topic>
              <recordInfo>
                <recordIdentifier>5478</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Collective labor agreements</topic>
              <recordInfo>
                <recordIdentifier>5533</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Aeronautics</topic>
              <recordInfo>
                <recordIdentifier>5788</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Airline Experience Under the Railway Labor Act</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 1683</subTitle>
          </titleInfo>
          <identifier type="oclc">00144791</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1971-01-01</sortDate>
            <publisher>U.S. Dept. of Labor Bureau of Labor Statistics</publisher>
            <dateIssued>1971</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4652</recordIdentifier>
            <recordUpdatedDate>2016-05-19 10:51:10</recordUpdatedDate>
            <recordCreationDate>2015-07-30 11:39:45</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD8051 .A62 no. 1683</classification>
          <classification authority="ddc">331.89/041/38770973</classification>
          <classification authority="sudocs">L 2.3:1683</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>60 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=4652&amp;amp;filepath=/files/docs/publications/bls/bls_1683_1971.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:39</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:3</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Board of Governors of the Federal Reserve System (U.S.)</namePart>
            <namePart type="date">1935-</namePart>
            <recordInfo>
              <recordIdentifier>3</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <genre>statistics</genre>
          <subject>
            <theme>
              <theme>Economic Data</theme>
              <recordInfo>
                <recordIdentifier>21</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Banks and banking</topic>
              <recordInfo>
                <recordIdentifier>4093</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Statistics</topic>
              <recordInfo>
                <recordIdentifier>4285</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>All-Bank Statistics, United States, 1896-1955</title>
          </titleInfo>
          <identifier type="oclc">742533</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1959-04-01</sortDate>
            <dateIssued>1896 - 1955</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>39</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:11:55</recordUpdatedDate>
            <recordCreationDate>2004-03-29 10:54:18.144209</recordCreationDate>
          </recordInfo>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Banking and Monetary Statistics, 1914-1941</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>38</recordIdentifier>
            </recordInfo>
            <typeOfResource>text</typeOfResource>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Banking and Monetary Statistics, 1941-1970</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>41</recordIdentifier>
            </recordInfo>
            <typeOfResource>text</typeOfResource>
          </relatedItem>
          <classification authority="sudocs">FR 1.2:B 22/6</classification>
          <typeOfResource>text</typeOfResource>
          <tableOfContents>
            <extent>
              <endPage>4</endPage>
              <startPage>3</startPage>
            </extent>
            <titleInfo>
              <title>Preface</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48794</recordIdentifier>
              <recordIdentifier>48794</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>7</endPage>
              <startPage>5</startPage>
            </extent>
            <titleInfo>
              <title>Contents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48795</recordIdentifier>
              <recordIdentifier>48795</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>92</endPage>
              <startPage>9</startPage>
            </extent>
            <titleInfo>
              <title>United States Summary</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48796</recordIdentifier>
              <recordIdentifier>48796</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>55</endPage>
              <startPage>35</startPage>
            </extent>
            <titleInfo>
              <title>United States Summary, Appendix A: All Banks - Continental United States</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48797</recordIdentifier>
              <recordIdentifier>48797</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>67</endPage>
              <startPage>56</startPage>
            </extent>
            <titleInfo>
              <title>United States Summary, Appendix B: Revised Series and Former Series</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48798</recordIdentifier>
              <recordIdentifier>48798</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>69</endPage>
              <startPage>68</startPage>
            </extent>
            <titleInfo>
              <title>United States Summary, Appendix C: Reports of State Banking Authorities</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48799</recordIdentifier>
              <recordIdentifier>48799</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>83</endPage>
              <startPage>70</startPage>
            </extent>
            <titleInfo>
              <title>United States Summary, Appendix D: Nonlicensed Banks, 1933 and 1934</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48800</recordIdentifier>
              <recordIdentifier>48800</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>88</endPage>
              <startPage>84</startPage>
            </extent>
            <titleInfo>
              <title>United States Summary, Appendix E: Composition of Asset and Liability Items</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48801</recordIdentifier>
              <recordIdentifier>48801</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>92</endPage>
              <startPage>89</startPage>
            </extent>
            <titleInfo>
              <title>United States Summary, Appendix F: Interpolating Technique No. 2</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48802</recordIdentifier>
              <recordIdentifier>48802</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>109</endPage>
              <startPage>94</startPage>
            </extent>
            <titleInfo>
              <title>Alabama</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48803</recordIdentifier>
              <recordIdentifier>48803</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>125</endPage>
              <startPage>110</startPage>
            </extent>
            <titleInfo>
              <title>Arizona</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48804</recordIdentifier>
              <recordIdentifier>48804</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>141</endPage>
              <startPage>126</startPage>
            </extent>
            <titleInfo>
              <title>Arkansas</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48805</recordIdentifier>
              <recordIdentifier>48805</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>157</endPage>
              <startPage>142</startPage>
            </extent>
            <titleInfo>
              <title>California</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48806</recordIdentifier>
              <recordIdentifier>48806</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>178</endPage>
              <startPage>158</startPage>
            </extent>
            <titleInfo>
              <title>Colorado</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48807</recordIdentifier>
              <recordIdentifier>48807</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>207</endPage>
              <startPage>179</startPage>
            </extent>
            <titleInfo>
              <title>Connecticut</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48808</recordIdentifier>
              <recordIdentifier>48808</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>232</endPage>
              <startPage>208</startPage>
            </extent>
            <titleInfo>
              <title>Delaware</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48809</recordIdentifier>
              <recordIdentifier>48809</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>246</endPage>
              <startPage>233</startPage>
            </extent>
            <titleInfo>
              <title>District of Columbia</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48810</recordIdentifier>
              <recordIdentifier>48810</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>262</endPage>
              <startPage>247</startPage>
            </extent>
            <titleInfo>
              <title>Florida</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48811</recordIdentifier>
              <recordIdentifier>48811</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>284</endPage>
              <startPage>263</startPage>
            </extent>
            <titleInfo>
              <title>Georgia</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48812</recordIdentifier>
              <recordIdentifier>48812</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>299</endPage>
              <startPage>285</startPage>
            </extent>
            <titleInfo>
              <title>Idaho</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48813</recordIdentifier>
              <recordIdentifier>48813</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>319</endPage>
              <startPage>300</startPage>
            </extent>
            <titleInfo>
              <title>Illinois</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48814</recordIdentifier>
              <recordIdentifier>48814</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>350</endPage>
              <startPage>320</startPage>
            </extent>
            <titleInfo>
              <title>Indiana</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48815</recordIdentifier>
              <recordIdentifier>48815</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>372</endPage>
              <startPage>351</startPage>
            </extent>
            <titleInfo>
              <title>Iowa</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48816</recordIdentifier>
              <recordIdentifier>48816</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>388</endPage>
              <startPage>373</startPage>
            </extent>
            <titleInfo>
              <title>Kansas</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48817</recordIdentifier>
              <recordIdentifier>48817</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>404</endPage>
              <startPage>389</startPage>
            </extent>
            <titleInfo>
              <title>Kentucky</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48818</recordIdentifier>
              <recordIdentifier>48818</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>419</endPage>
              <startPage>405</startPage>
            </extent>
            <titleInfo>
              <title>Louisiana</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48819</recordIdentifier>
              <recordIdentifier>48819</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>443</endPage>
              <startPage>420</startPage>
            </extent>
            <titleInfo>
              <title>Maine</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48820</recordIdentifier>
              <recordIdentifier>48820</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>473</endPage>
              <startPage>444</startPage>
            </extent>
            <titleInfo>
              <title>Maryland</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48821</recordIdentifier>
              <recordIdentifier>48821</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>502</endPage>
              <startPage>474</startPage>
            </extent>
            <titleInfo>
              <title>Massachusetts</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48822</recordIdentifier>
              <recordIdentifier>48822</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>524</endPage>
              <startPage>503</startPage>
            </extent>
            <titleInfo>
              <title>Michigan</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48823</recordIdentifier>
              <recordIdentifier>48823</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>555</endPage>
              <startPage>525</startPage>
            </extent>
            <titleInfo>
              <title>Minnesota</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48824</recordIdentifier>
              <recordIdentifier>48824</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>571</endPage>
              <startPage>556</startPage>
            </extent>
            <titleInfo>
              <title>Mississippi</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48825</recordIdentifier>
              <recordIdentifier>48825</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>587</endPage>
              <startPage>572</startPage>
            </extent>
            <titleInfo>
              <title>Missouri</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48826</recordIdentifier>
              <recordIdentifier>48826</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>607</endPage>
              <startPage>588</startPage>
            </extent>
            <titleInfo>
              <title>Montana</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48827</recordIdentifier>
              <recordIdentifier>48827</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>623</endPage>
              <startPage>608</startPage>
            </extent>
            <titleInfo>
              <title>Nebraska</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48828</recordIdentifier>
              <recordIdentifier>48828</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>638</endPage>
              <startPage>624</startPage>
            </extent>
            <titleInfo>
              <title>Nevada</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48829</recordIdentifier>
              <recordIdentifier>48829</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>661</endPage>
              <startPage>639</startPage>
            </extent>
            <titleInfo>
              <title>New Hampshire</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48830</recordIdentifier>
              <recordIdentifier>48830</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>691</endPage>
              <startPage>662</startPage>
            </extent>
            <titleInfo>
              <title>New Jersey</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48831</recordIdentifier>
              <recordIdentifier>48831</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>706</endPage>
              <startPage>692</startPage>
            </extent>
            <titleInfo>
              <title>New Mexico</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48832</recordIdentifier>
              <recordIdentifier>48832</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>740</endPage>
              <startPage>707</startPage>
            </extent>
            <titleInfo>
              <title>New York</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48833</recordIdentifier>
              <recordIdentifier>48833</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>755</endPage>
              <startPage>741</startPage>
            </extent>
            <titleInfo>
              <title>North Carolina</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48834</recordIdentifier>
              <recordIdentifier>48834</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>771</endPage>
              <startPage>756</startPage>
            </extent>
            <titleInfo>
              <title>North Dakota</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48835</recordIdentifier>
              <recordIdentifier>48835</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>802</endPage>
              <startPage>772</startPage>
            </extent>
            <titleInfo>
              <title>Ohio</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48836</recordIdentifier>
              <recordIdentifier>48836</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>819</endPage>
              <startPage>803</startPage>
            </extent>
            <titleInfo>
              <title>Oklahoma</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48837</recordIdentifier>
              <recordIdentifier>48837</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>843</endPage>
              <startPage>820</startPage>
            </extent>
            <titleInfo>
              <title>Oregon</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48838</recordIdentifier>
              <recordIdentifier>48838</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>874</endPage>
              <startPage>844</startPage>
            </extent>
            <titleInfo>
              <title>Pennsylvania</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48839</recordIdentifier>
              <recordIdentifier>48839</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>898</endPage>
              <startPage>875</startPage>
            </extent>
            <titleInfo>
              <title>Rhode Island</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48840</recordIdentifier>
              <recordIdentifier>48840</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>914</endPage>
              <startPage>899</startPage>
            </extent>
            <titleInfo>
              <title>South Carolina</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48841</recordIdentifier>
              <recordIdentifier>48841</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>929</endPage>
              <startPage>915</startPage>
            </extent>
            <titleInfo>
              <title>South Dakota</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48842</recordIdentifier>
              <recordIdentifier>48842</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>945</endPage>
              <startPage>930</startPage>
            </extent>
            <titleInfo>
              <title>Tennessee</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48843</recordIdentifier>
              <recordIdentifier>48843</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>967</endPage>
              <startPage>946</startPage>
            </extent>
            <titleInfo>
              <title>Texas</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48844</recordIdentifier>
              <recordIdentifier>48844</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>982</endPage>
              <startPage>968</startPage>
            </extent>
            <titleInfo>
              <title>Utah</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48845</recordIdentifier>
              <recordIdentifier>48845</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>1005</endPage>
              <startPage>983</startPage>
            </extent>
            <titleInfo>
              <title>Vermont</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48846</recordIdentifier>
              <recordIdentifier>48846</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>1025</endPage>
              <startPage>1006</startPage>
            </extent>
            <titleInfo>
              <title>Virginia</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48847</recordIdentifier>
              <recordIdentifier>48847</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>1054</endPage>
              <startPage>1026</startPage>
            </extent>
            <titleInfo>
              <title>Washington</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48848</recordIdentifier>
              <recordIdentifier>48848</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>1078</endPage>
              <startPage>1055</startPage>
            </extent>
            <titleInfo>
              <title>West Virginia</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48849</recordIdentifier>
              <recordIdentifier>48849</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>1103</endPage>
              <startPage>1079</startPage>
            </extent>
            <titleInfo>
              <title>Wisconsin</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48850</recordIdentifier>
              <recordIdentifier>48850</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>1118</endPage>
              <startPage>1104</startPage>
            </extent>
            <titleInfo>
              <title>Wyoming</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48851</recordIdentifier>
              <recordIdentifier>48851</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <tableOfContents>
            <extent>
              <endPage>1206</endPage>
              <startPage>1119</startPage>
            </extent>
            <titleInfo>
              <title>Other Areas</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>48852</recordIdentifier>
              <recordIdentifier>48852</recordIdentifier>
            </recordInfo>
          </tableOfContents>
          <physicalDescription>
            <form>print</form>
            <extent>1206 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=39&amp;amp;filepath=/files/docs/publications/allbkstat/1896-1955/allbankstats_complete.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=39&amp;amp;filepath=/files/docs/publications/allbkstat/1896-1955/allbankstats_complete.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:397</identifier>
        <datestamp>2017-03-30T10:22:07Z</datestamp>
        <setSpec>author:78</setSpec>
        <setSpec>author:233</setSpec>
        <setSpec>author:852</setSpec>
        <setSpec>author:8465</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Brunner, Karl</namePart>
            <namePart type="date">1916-1989</namePart>
            <recordInfo>
              <recordIdentifier>78</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Meltzer, Allan H.</namePart>
            <recordInfo>
              <recordIdentifier>233</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>United States. Congress. House. Committee on Banking and Currency. Subcommittee on Domestic Finance</namePart>
            <namePart type="date">1964-1975</namePart>
            <recordInfo>
              <recordIdentifier>852</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Eighty-Eighth Congress</namePart>
            <namePart type="date">1963-1965</namePart>
            <recordInfo>
              <recordIdentifier>8465</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Monetary Policy</theme>
              <recordInfo>
                <recordIdentifier>11</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Monetary policy</topic>
              <recordInfo>
                <recordIdentifier>4250</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional reports</topic>
              <recordInfo>
                <recordIdentifier>7381</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Alternative Approach to the Monetary Mechanism</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Analysis of Federal Reserve Monetary Policy Making</title>
          </titleInfo>
          <identifier type="oclc">21641890</identifier>
          <identifier type="oclc"> 2995983</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1964-08-17</sortDate>
            <publisher>U.S. Government Printing Office</publisher>
            <dateIssued>August 17, 1964</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>397</recordIdentifier>
            <recordUpdatedDate>2017-03-30 10:30:50</recordUpdatedDate>
            <recordCreationDate>2012-01-12 12:55:32.683481</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>An Analysis of Federal Reserve Monetary Policy Making</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1484</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4.B 22/1:M 74/17</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>148 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=397&amp;amp;filepath=/files/docs/historical/congressional/19640817hr_aamm.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:410</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:14</setSpec>
        <setSpec>author:8472</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. Senate. Committee on Banking, Housing, and Urban Affairs</namePart>
            <namePart type="date">1970-</namePart>
            <recordInfo>
              <recordIdentifier>14</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Ninety-Sixth Congress</namePart>
            <namePart type="date">1979-1981</namePart>
            <recordInfo>
              <recordIdentifier>8472</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">96th Congress, 1st Session, Senate Report No. 96-423</note>
          <genre>government publication</genre>
          <genre>legislation</genre>
          <subject>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <titleInfo>
              <titleInfo>Federal Reserve Act</titleInfo>
              <recordInfo>
                <recordIdentifier>4172</recordIdentifier>
              </recordInfo>
            </titleInfo>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Amending the Federal Reserve Act</title>
            <subTitle>Report (To Accompany H.R. 4998)</subTitle>
            <titlePartNumber>Calendar No. 453</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">6181488</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1979-11-20</sortDate>
            <dateIssued>November 20, 1979</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>410</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:06:01</recordUpdatedDate>
            <recordCreationDate>2012-03-02 11:05:31.340678</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">X 96-1:S.rp.423</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>4 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=410&amp;amp;filepath=/files/docs/historical/congressional/1979sen_amendfract.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:372</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:849</setSpec>
        <setSpec>author:3157</setSpec>
        <setSpec>author:8475</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. House. Committee on Banking and Currency. Subcommittee No. 1</namePart>
            <recordInfo>
              <recordIdentifier>849</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Roosa, Robert V.</namePart>
            <recordInfo>
              <recordIdentifier>3157</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Eighty-Seventh Congress</namePart>
            <namePart type="date">1961-1963</namePart>
            <recordInfo>
              <recordIdentifier>8475</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Federal Reserve Act</topic>
              <recordInfo>
                <recordIdentifier>4172</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Debts, Public</topic>
              <recordInfo>
                <recordIdentifier>4136</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Bonds</topic>
              <recordInfo>
                <recordIdentifier>4098</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
            <titleInfo>
              <titleInfo>Federal Reserve Act</titleInfo>
              <recordInfo>
                <recordIdentifier>4172</recordIdentifier>
              </recordInfo>
            </titleInfo>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Amendment of Section 14(b) of the Federal Reserve Act</title>
            <subTitle>Hearing Before Subcommittee No. 1 of the Committee on Banking and Currency, House of Representatives</subTitle>
            <titlePartNumber>Eighty-Seventh Congress, Second Session, on H. R. 11654, June 19, 1962</titlePartNumber>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Federal Reserve Act</title>
          </titleInfo>
          <identifier type="oclc">12379092</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1962-06-19</sortDate>
            <publisher>Government Printing Office</publisher>
            <dateIssued>June 19, 1962</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>372</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:10:45</recordUpdatedDate>
            <recordCreationDate>2011-12-07 17:13:27.375483</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Amendment of Section 14(b) of the Federal Reserve Act</title>
              <subTitle>Hearing Before the Committee on Banking and Currency, House of Representatives</subTitle>
              <titlePartNumber>Eighty-Fifth Congress, Second Session, on H.R. 12586, June 12, 1958</titlePartNumber>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>370</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4.B 22/1:F 31/29/962-2</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>15 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=372&amp;amp;filepath=/files/docs/historical/fr_act/hearing_hr11654_hr_19620619.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:370</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:16</setSpec>
        <setSpec>author:8463</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. House. Committee on Banking and Currency</namePart>
            <namePart type="date">1865-1974</namePart>
            <recordInfo>
              <recordIdentifier>16</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Eighty-Fifth Congress</namePart>
            <namePart type="date">1957-1959</namePart>
            <recordInfo>
              <recordIdentifier>8463</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Debts, Public</topic>
              <recordInfo>
                <recordIdentifier>4136</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Federal Reserve Act</topic>
              <recordInfo>
                <recordIdentifier>4172</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Bonds</topic>
              <recordInfo>
                <recordIdentifier>4098</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
            <titleInfo>
              <titleInfo>Federal Reserve Act</titleInfo>
              <recordInfo>
                <recordIdentifier>4172</recordIdentifier>
              </recordInfo>
            </titleInfo>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Amendment of Section 14(b) of the Federal Reserve Act</title>
            <subTitle>Hearing Before the Committee on Banking and Currency, House of Representatives</subTitle>
            <titlePartNumber>Eighty-Fifth Congress, Second Session, on H.R. 12586, June 12, 1958</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">12307657</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1958-06-12</sortDate>
            <publisher>Government Printing Office</publisher>
            <dateIssued>June 12, 1958</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>370</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:10:49</recordUpdatedDate>
            <recordCreationDate>2011-12-07 17:11:08.626829</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Amendment of Section 14(b) of the Federal Reserve Act</title>
              <subTitle>Hearing Before Subcommittee No. 1 of the Committee on Banking and Currency, House of Representatives</subTitle>
              <titlePartNumber>Eighty-Seventh Congress, Second Session, on H. R. 11654, June 19, 1962</titlePartNumber>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Federal Reserve Act</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>372</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4.B 22/1:F 31/29/958</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>28 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=370&amp;amp;filepath=/files/docs/historical/fr_act/hearing_hr12586_hr_19580612.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:197</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:14</setSpec>
        <setSpec>author:8470</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. Senate. Committee on Banking, Housing, and Urban Affairs</namePart>
            <namePart type="date">1970-</namePart>
            <recordInfo>
              <recordIdentifier>14</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Ninety-Fourth Congress</namePart>
            <namePart type="date">1975-1977</namePart>
            <recordInfo>
              <recordIdentifier>8470</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
            <titleInfo>
              <titleInfo>Bretton Woods Agreement Act</titleInfo>
              <recordInfo>
                <recordIdentifier>4880</recordIdentifier>
              </recordInfo>
            </titleInfo>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Amendments of the Bretton Woods Agreements Act</title>
            <subTitle>Hearing Before the Subcommittee on International Finance of the Committee on Banking, Housing, and Urban Affairs, United States Senate</subTitle>
            <titlePartNumber>Ninety-Fourth Congress, Second Session, on H.R. 13955, August 27, 1976</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">2610750</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1976-08-27</sortDate>
            <publisher>U.S. Government Printing Office</publisher>
            <dateIssued>August 27, 1976</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>197</recordIdentifier>
            <recordUpdatedDate>2016-08-23 16:39:37</recordUpdatedDate>
            <recordCreationDate>2011-03-29 13:15:22.39584</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4.B22/3:B 75/2</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>167 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=197&amp;amp;filepath=/files/docs/historical/senate/19760827hr_abwoods.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:990</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:344</setSpec>
        <setSpec>author:8456</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Sixty-Ninth Congress</namePart>
            <namePart type="date">1925-1927</namePart>
            <recordInfo>
              <recordIdentifier>8456</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">69th Congress, 2d Session, House of Representatives Report No. 1796</note>
          <genre>government publication</genre>
          <genre>legislation</genre>
          <subject>
            <topic>
              <topic>Banks and banking</topic>
              <recordInfo>
                <recordIdentifier>4093</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Amendments to Banking Laws</title>
            <subTitle>Conference Report (To Accompany H.R. 2)</subTitle>
          </titleInfo>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1927-01-19</sortDate>
            <publisher>Government Printing Office</publisher>
            <dateIssued>January 19, 1927</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>990</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:06:57</recordUpdatedDate>
            <recordCreationDate>2013-08-05 14:29:25.30763</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Banking Act of 1935</title>
              <subTitle>Hearings Before a Subcommittee of the Committee on Banking and Currency, United States Senate</subTitle>
              <titlePartNumber>Seventy-Fourth Congress, First Session, on S. 1715 and H.R. 7617, April 19 to June 3, 1935</titlePartNumber>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>778</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>2 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=990&amp;amp;filepath=/files/docs/historical/congressional/1926_mcfaddenact_confrep1796.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:988</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:344</setSpec>
        <setSpec>author:8456</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Sixty-Ninth Congress</namePart>
            <namePart type="date">1925-1927</namePart>
            <recordInfo>
              <recordIdentifier>8456</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">69th Congress, 1st Session, House of Representatives Report No. 1481</note>
          <genre>government publication</genre>
          <genre>legislation</genre>
          <subject>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Amendments to Banking Laws</title>
            <subTitle>Conference Report (To Accompany H.R. 2)</subTitle>
          </titleInfo>
          <identifier type="oclc">22235829</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1926-06-15</sortDate>
            <publisher>Government Printing Office</publisher>
            <dateIssued>June 15, 1926</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>988</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:06:01</recordUpdatedDate>
            <recordCreationDate>2013-08-05 14:14:19.46746</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Banking Act of 1935</title>
              <subTitle>Hearings Before a Subcommittee of the Committee on Banking and Currency, United States Senate</subTitle>
              <titlePartNumber>Seventy-Fourth Congress, First Session, on S. 1715 and H.R. 7617, April 19 to June 3, 1935</titlePartNumber>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>778</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>6 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=988&amp;amp;filepath=/files/docs/historical/congressional/1926_mcfaddenact_confrep1481.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:715</identifier>
        <datestamp>2017-03-30T10:22:07Z</datestamp>
        <setSpec>author:16</setSpec>
        <setSpec>author:536</setSpec>
        <setSpec>author:8455</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. House. Committee on Banking and Currency</namePart>
            <namePart type="date">1865-1974</namePart>
            <recordInfo>
              <recordIdentifier>16</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Harding, W. P. G. (William Proctor Gould)</namePart>
            <namePart type="date">1864-1930</namePart>
            <recordInfo>
              <recordIdentifier>536</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Sixty-Seventh Congress</namePart>
            <namePart type="date">1921-1923</namePart>
            <recordInfo>
              <recordIdentifier>8455</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <name>
              <name>United States. Office of the Comptroller of the Currency</name>
              <recordInfo>
                <recordIdentifier>19</recordIdentifier>
              </recordInfo>
            </name>
            <topic>
              <topic>Federal Reserve Act</topic>
              <recordInfo>
                <recordIdentifier>4172</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Amendment to Abolish Office of Comptroller of the Currency, Etc.</title>
            <subTitle>Hearing Before the Committee on Banking and Currency of the House of Representatives</subTitle>
            <titlePartNumber>Sixty-Seventh Congress, June 1, 1921</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">8964891</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1921-06-01</sortDate>
            <publisher>Government Printing Office</publisher>
            <dateIssued>June 1, 1921</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>715</recordIdentifier>
            <recordUpdatedDate>2017-03-30 10:24:04</recordUpdatedDate>
            <recordCreationDate>2012-05-16 13:04:13.345328</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>48 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=715&amp;amp;filepath=/files/docs/historical/congressional/19210601_house_compcurr.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:367</identifier>
        <datestamp>2017-03-30T10:22:07Z</datestamp>
        <setSpec>author:16</setSpec>
        <setSpec>author:13</setSpec>
        <setSpec>author:8476</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. House. Committee on Banking and Currency</namePart>
            <namePart type="date">1865-1974</namePart>
            <recordInfo>
              <recordIdentifier>16</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Martin, William McChesney</namePart>
            <recordInfo>
              <recordIdentifier>13</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Eighty-Third Congress</namePart>
            <namePart type="date">1953-1955</namePart>
            <recordInfo>
              <recordIdentifier>8476</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Federal Reserve Act</topic>
              <recordInfo>
                <recordIdentifier>4172</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
            <titleInfo>
              <titleInfo>Federal Reserve Act</titleInfo>
              <recordInfo>
                <recordIdentifier>4172</recordIdentifier>
              </recordInfo>
            </titleInfo>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Amendment to Section 10 of Federal Reserve Act</title>
            <subTitle>Hearing Before the Committee on Banking and Currency, House of Representatives</subTitle>
            <titlePartNumber>Eighty-Third Congress, First Session, on H.R. 4605, May 5, 1953</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">6838138</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1953-05-05</sortDate>
            <publisher>Government Printing Office</publisher>
            <dateIssued>May 5, 1953</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>367</recordIdentifier>
            <recordUpdatedDate>2017-03-30 10:23:58</recordUpdatedDate>
            <recordCreationDate>2011-12-07 17:06:32.477765</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4.B 22/1:F 31/28</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>50 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=367&amp;amp;filepath=/files/docs/historical/fr_act/hearing_hr4605_hr_19530505.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:374</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:16</setSpec>
        <setSpec>author:8465</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. House. Committee on Banking and Currency</namePart>
            <namePart type="date">1865-1974</namePart>
            <recordInfo>
              <recordIdentifier>16</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Eighty-Eighth Congress</namePart>
            <namePart type="date">1963-1965</namePart>
            <recordInfo>
              <recordIdentifier>8465</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Followed by House Report no. 1473, 88th Congress, 2d session</note>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Federal Reserve Act</topic>
              <recordInfo>
                <recordIdentifier>4172</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Debts, Public</topic>
              <recordInfo>
                <recordIdentifier>4136</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Bonds</topic>
              <recordInfo>
                <recordIdentifier>4098</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional hearings</topic>
              <recordInfo>
                <recordIdentifier>6824</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Amend Section 14(b) of the Federal Reserve Act</title>
            <subTitle>Hearing Before the Committee on Banking and Currency, House of Representatives</subTitle>
            <titlePartNumber>Eighty-Eighth Congress, Second Session, on H.R. 11499, June 11, 1964</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">2954542</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1964-06-11</sortDate>
            <publisher>Government Printing Office</publisher>
            <dateIssued>June 11, 1964</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>374</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:10:53</recordUpdatedDate>
            <recordCreationDate>2011-12-07 17:15:40.013325</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">Y 4.B 22/1:F 31/29/964</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>11 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=374&amp;amp;filepath=/files/docs/historical/fr_act/hearing_hr11499_hr_19640611.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1056</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:1798</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>American Bankers Association</namePart>
            <recordInfo>
              <recordIdentifier>1798</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Unpaged.</note>
          <note type="public">Cover title reads: Banking systems of the world.</note>
          <subject>
            <topic>
              <topic>Banks and banking</topic>
              <recordInfo>
                <recordIdentifier>4093</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>American Bankers Convention Year Book, Containing the Banking Systems of the World</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Banking Systems of the World</title>
          </titleInfo>
          <identifier type="oclc">609907492</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1908-09-28</sortDate>
            <dateIssued>September 28, 1908</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1056</recordIdentifier>
            <recordUpdatedDate>2015-08-31 15:38:13</recordUpdatedDate>
            <recordCreationDate>2013-10-01 11:04:15.2171</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>128 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1056&amp;amp;filepath=/files/docs/publications/books/banking-systems-world-1908.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=1056&amp;amp;filepath=/files/docs/publications/books/banking-systems-world-1908.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1161</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <note type="public">Selected articles published in the American Economic Review.  American Economic Association. Reproduced with the permission of the American Economic Association.</note>
          <genre>periodical</genre>
          <subject>
            <theme>
              <theme>Meltzer's History of the Federal Reserve - Primary Sources</theme>
              <recordInfo>
                <recordIdentifier>97</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Economics</topic>
              <recordInfo>
                <recordIdentifier>4151</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>American Economic Review</title>
          </titleInfo>
          <identifier type="issn">0002-8282</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>monthly</frequency>
            <dateIssued point="start">1921-03-01</dateIssued>
            <dateIssued point="end">2002-05-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1161</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:09:58</recordUpdatedDate>
            <recordCreationDate>2013-12-26 16:37:49.58526</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Limited Use</accessCondition>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/1161</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1022</identifier>
        <datestamp>2017-05-08T08:41:31Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">114 Stat. 2944. Title I, the Housing Affordability Barrier Removal Act of 2000, is on pages 4-6. Title IV, the Private Mortgage Insurance Technical Corrections and Clarification Act, is on pages 14-17.</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Finance</topic>
              <recordInfo>
                <recordIdentifier>4187</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Housing</topic>
              <recordInfo>
                <recordIdentifier>4217</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>American Homeownership and Economic Opportunity Act of 2000</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>An Act to Expand Homeownership in the United States, and for Other Purposes</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Public Law 106-569, 106th Congress, H.R. 5640</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Housing Affordability Barrier Removal Act of 2000</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Private Mortgage Insurance Technical Corrections and Clarification Act</title>
          </titleInfo>
          <identifier type="oclc">46851505</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>2000-12-27</sortDate>
            <dateIssued>December 27, 2000</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1022</recordIdentifier>
            <recordUpdatedDate>2017-05-08 08:41:31</recordUpdatedDate>
            <recordCreationDate>2013-09-27 10:41:45.513088</recordCreationDate>
          </recordInfo>
          <classification authority="sudocs">AE 2.110:106-569</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>electronic</form>
            <extent>95 pages</extent>
            <digitalOrigin>born digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1022&amp;amp;filepath=/files/docs/historical/congressional/american-homeownership-economic-opportunity-2000.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5126</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:5528</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>American International Group, Inc.</namePart>
            <recordInfo>
              <recordIdentifier>5528</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">These documents were captured from publicly available websites during and after the financial crisis. Some were retrieved from mirror sites or cached pages and as a result may have identifying marks from third-party sites or recent time stamps. Where possible, links to the original sites or further information have been added.</note>
          <subject>
            <theme>
              <theme>Financial Crisis of 2007-2009</theme>
              <recordInfo>
                <recordIdentifier>103</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Global Financial Crisis, 2008-2009</topic>
              <recordInfo>
                <recordIdentifier>4898</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>American International Group, Inc.</title>
            <subTitle>Press Releases Relating to the Financial Crisis of 2007-2009</subTitle>
          </titleInfo>
          <originInfo>
            <issuance>multipart</issuance>
            <sortDate>2009-03-02</sortDate>
            <dateIssued point="start">2009-03-02</dateIssued>
            <dateIssued point="end">2009-12-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5126</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:14:25</recordUpdatedDate>
            <recordCreationDate>2015-12-22 16:37:08</recordCreationDate>
            <recordContentSource>frs</recordContentSource>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>electronic</form>
            <digitalOrigin>born digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/5126</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/5126</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4463</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:4619</setSpec>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Bowden, Witt</namePart>
            <namePart type="date">1886-1979</namePart>
            <recordInfo>
              <recordIdentifier>4619</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <genre>bibliography</genre>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Labor unions</topic>
              <recordInfo>
                <recordIdentifier>4469</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Industrial relations</topic>
              <recordInfo>
                <recordIdentifier>5562</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Labor productivity</topic>
              <recordInfo>
                <recordIdentifier>4240</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Labor laws and legislation</topic>
              <recordInfo>
                <recordIdentifier>5497</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>American Labor and the American Spirit: Unions, Labor-Management Relations, and Productivity</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 1145</subTitle>
          </titleInfo>
          <identifier type="oclc">883755692</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1954-01-01</sortDate>
            <publisher>U.S. Govt. Print. Off.</publisher>
            <dateIssued>1954</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4463</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:15:56</recordUpdatedDate>
            <recordCreationDate>2015-05-22 12:27:10</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD8051 .A5 no.1141-1151</classification>
          <classification authority="sudocs">L 2.3:1145</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>72 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=4463&amp;amp;filepath=/files/docs/publications/bls/bls_1145_1954.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4971</identifier>
        <datestamp>2017-05-08T10:09:51Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">123 Stat. 115.</note>
          <genre>legislation</genre>
          <subject>
            <theme>
              <theme>Financial Crisis of 2007-2009</theme>
              <recordInfo>
                <recordIdentifier>103</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Appropriations and expenditures</topic>
              <recordInfo>
                <recordIdentifier>6755</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Economic stabilization</topic>
              <recordInfo>
                <recordIdentifier>6773</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Finance</topic>
              <recordInfo>
                <recordIdentifier>4187</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Health insurance</topic>
              <recordInfo>
                <recordIdentifier>5609</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Medicaid</topic>
              <recordInfo>
                <recordIdentifier>6802</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Medical informatics</topic>
              <recordInfo>
                <recordIdentifier>6803</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Taxation</topic>
              <recordInfo>
                <recordIdentifier>4290</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Trade adjustment assistance</topic>
              <recordInfo>
                <recordIdentifier>6820</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Unemployment insurance</topic>
              <recordInfo>
                <recordIdentifier>5599</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>American Recovery and Reinvestment Act of 2009</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>An Act Making Supplemental Appropriations for Job Preservation and Creation, Infrastructure Investment, Energy Efficiency and Science, Assistance to the Unemployed, and State and Local Fiscal Stabilization, for the Fiscal Year Ending September 30, 2009, and for Other Purposes</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Public Law 111-5, H.R. 1</title>
          </titleInfo>
          <identifier type="oclc">319062579</identifier>
          <originInfo>
            <place>Washington, D.C.</place>
            <issuance>monographic</issuance>
            <sortDate>2009-02-17</sortDate>
            <publisher>U.S. G.P.O.</publisher>
            <dateIssued>February 17, 2009</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4971</recordIdentifier>
            <recordUpdatedDate>2017-05-08 10:09:51</recordUpdatedDate>
            <recordCreationDate>2015-12-22 16:36:41</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <classification authority="sudocs">AE 2.110:111-5</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>electronic</form>
            <extent>407 pages</extent>
            <digitalOrigin>born digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=4971&amp;amp;filepath=/files/docs/historical/fct/american_recovery_reinvestment_act_20090217.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=4971&amp;amp;filepath=/files/docs/historical/fct/american_recovery_reinvestment_act_20090217.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5565</identifier>
        <datestamp>2017-05-08T14:12:10Z</datestamp>
        <setSpec>author:344</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress</namePart>
            <recordInfo>
              <recordIdentifier>344</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">128 Stat. 2888</note>
          <genre>legislation</genre>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Banking Legislation</theme>
              <recordInfo>
                <recordIdentifier>40</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Law and legislation</topic>
              <recordInfo>
                <recordIdentifier>4241</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>American Savings Promotion Act</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>An Act to Provide for the Use of Savings Promotion Raffle Products by Financial Institutions to Encourage Savings, and for Other Purposes</title>
          </titleInfo>
          <titleInfo type="abbreviated">
            <title>Public Law 113-251, 113th Congress, H.R. 3374</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>2014-12-18</sortDate>
            <publisher>GPO</publisher>
            <dateIssued>December 18, 2014</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5565</recordIdentifier>
            <recordUpdatedDate>2017-05-08 14:12:11</recordUpdatedDate>
            <recordCreationDate>2017-05-08 13:04:01</recordCreationDate>
            <recordContentSource>frs</recordContentSource>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>6 pages</extent>
            <digitalOrigin>born digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=5565&amp;amp;filepath=/files/docs/historical/congressional/american-savings-promotion-act.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1221</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:2717</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. President (1981-1989 : Reagan)</namePart>
            <recordInfo>
              <recordIdentifier>2717</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Spine title: A Program for economic recovery.</note>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Meltzer's History of the Federal Reserve - Primary Sources</theme>
              <recordInfo>
                <recordIdentifier>97</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Economic policy</topic>
              <recordInfo>
                <recordIdentifier>4150</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Taxation</topic>
              <recordInfo>
                <recordIdentifier>4290</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Budget</topic>
              <recordInfo>
                <recordIdentifier>4102</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>America's New Beginning</title>
            <subTitle>A Program for Economic Recovery</subTitle>
          </titleInfo>
          <identifier type="oclc">7158423</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1981-02-18</sortDate>
            <dateIssued>February 18, 1981</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1221</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:35:03</recordUpdatedDate>
            <recordCreationDate>2014-02-11 13:56:19.388836</recordCreationDate>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>360 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=1221&amp;amp;filepath=/files/docs/meltzer/reagan_americas_new_beginning_19810218.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=1221&amp;amp;filepath=/files/docs/meltzer/reagan_americas_new_beginning_19810218.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:680</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:3</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Board of Governors of the Federal Reserve System (U.S.)</namePart>
            <namePart type="date">1935-</namePart>
            <recordInfo>
              <recordIdentifier>3</recordIdentifier>
            </recordInfo>
          </name>
          <genre>book</genre>
          <subject>
            <topic>
              <topic>Banks and banking</topic>
              <recordInfo>
                <recordIdentifier>4093</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Analyses of the Banking Structure</title>
          </titleInfo>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1937-08-01</sortDate>
            <dateIssued>August 1937</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>680</recordIdentifier>
            <recordUpdatedDate>2015-09-02 10:39:49</recordUpdatedDate>
            <recordCreationDate>2012-05-16 13:04:13.345328</recordCreationDate>
          </recordInfo>
          <relatedItem type="parent">
            <titleInfo>
              <title>Federal Reserve Bulletin, August 1937</title>
            </titleInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>34 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=680&amp;amp;filepath=/files/docs/historical/banksusp/frbull_analbs_193708.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=680&amp;amp;filepath=/files/docs/historical/banksusp/frbull_analbs_193708.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5555</identifier>
        <datestamp>2017-02-15T15:26:47Z</datestamp>
        <setSpec>author:770</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Women's Bureau</namePart>
            <recordInfo>
              <recordIdentifier>770</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <language>eng</language>
          <titleInfo>
            <title>Analysis of Coverage and Wage Rates of State Minimum Wage Laws and Orders, August 1, 1965</title>
            <subTitle>Women's Bureau Bulletin, No. 291</subTitle>
          </titleInfo>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1965-01-01</sortDate>
            <publisher>Govt. Print. Off.</publisher>
            <dateIssued>1965</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5555</recordIdentifier>
            <recordUpdatedDate>2017-02-15 15:28:13</recordUpdatedDate>
            <recordCreationDate>2017-02-14 15:37:35</recordCreationDate>
            <recordContentSource>frs</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the Women's Bureau</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the Women's Bureau</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Women's Bureau Bulletin</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>243</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>138 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=5555&amp;amp;filepath=/files/docs/publications/women/b0291_dolwb_1965.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=5555&amp;amp;filepath=/files/docs/publications/women/b0291_dolwb_1965.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:716</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:15</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Congress. House. Committee on Banking, Finance, and Urban Affairs</namePart>
            <namePart type="date">1977-1995</namePart>
            <recordInfo>
              <recordIdentifier>15</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Bank failures</topic>
              <recordInfo>
                <recordIdentifier>4078</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Discount window</topic>
              <recordInfo>
                <recordIdentifier>4141</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional reports</topic>
              <recordInfo>
                <recordIdentifier>7381</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>An Analysis of Federal Reserve Discount Window Loans to Failed Institutions</title>
          </titleInfo>
          <identifier type="oclc">24146036</identifier>
          <originInfo>
            <issuance>monographic</issuance>
            <sortDate>1991-06-11</sortDate>
            <dateIssued>June 11, 1991</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>716</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:18:38</recordUpdatedDate>
            <recordCreationDate>2012-05-16 13:04:13.345328</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>12 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=716&amp;amp;filepath=/files/docs/historical/house/analysisfedresdis_1991.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:1484</identifier>
        <datestamp>2017-03-30T10:22:07Z</datestamp>
        <setSpec>author:8465</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Eighty-Eighth Congress</namePart>
            <namePart type="date">1963-1965</namePart>
            <recordInfo>
              <recordIdentifier>8465</recordIdentifier>
            </recordInfo>
          </name>
          <genre>series</genre>
          <genre>government publication</genre>
          <subject>
            <name>
              <name>United States. Federal Open Market Committee</name>
              <recordInfo>
                <recordIdentifier>10</recordIdentifier>
              </recordInfo>
            </name>
            <topic>
              <topic>Monetary policy</topic>
              <recordInfo>
                <recordIdentifier>4250</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Congressional reports</topic>
              <recordInfo>
                <recordIdentifier>7381</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <abstract>A three part staff analysis of the Federal Reserve System's policy action pertaining to the System's determination of the volume of money and credit.</abstract>
          <titleInfo>
            <title>An Analysis of Federal Reserve Monetary Policy Making</title>
          </titleInfo>
          <originInfo>
            <issuance>multipart</issuance>
          </originInfo>
          <recordInfo>
            <recordIdentifier>1484</recordIdentifier>
            <recordUpdatedDate>2017-03-30 10:30:38</recordUpdatedDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Congressional Documents</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>5292</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <location>
            <url>https://fraser.stlouisfed.org/series/1484</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4501</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:1014</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics. Division of Occupational Outlook</namePart>
            <namePart>Division of Occupational Outlook</namePart>
            <recordInfo>
              <recordIdentifier>1014</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Health insurance</topic>
              <recordInfo>
                <recordIdentifier>5609</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Hospitalization insurance</topic>
              <recordInfo>
                <recordIdentifier>5732</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Accident insurance</topic>
              <recordInfo>
                <recordIdentifier>5887</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Life insurance</topic>
              <recordInfo>
                <recordIdentifier>5610</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Employee fringe benefits</topic>
              <recordInfo>
                <recordIdentifier>5498</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Collective labor agreements</topic>
              <recordInfo>
                <recordIdentifier>5533</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Analysis of Health and Insurance Plans Under Collective Bargaining, Late 1955: Life Insurance, Accidental Death and Dismemberment, Accident and Sickness, Hospitalization, Surgical, Medical, Maternity</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 1221</subTitle>
          </titleInfo>
          <identifier type="oclc">09811312</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1957-01-01</sortDate>
            <dateIssued>1957</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4501</recordIdentifier>
            <recordUpdatedDate>2016-05-19 10:50:59</recordUpdatedDate>
            <recordCreationDate>2015-05-22 17:04:43</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD8051 .A5 no.1221</classification>
          <classification authority="ddc">368.4100973 Un3a</classification>
          <classification authority="sudocs">L 2.3:1221</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>100 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=4501&amp;amp;filepath=/files/docs/publications/bls/bls_1221_1957.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4499</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:1014</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics. Division of Occupational Outlook</namePart>
            <namePart>Division of Occupational Outlook</namePart>
            <recordInfo>
              <recordIdentifier>1014</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">"From the Monthly labor review December 1956 and January, February, and March 1957 issues, with additional tables."</note>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Layoff systems</topic>
              <recordInfo>
                <recordIdentifier>5750</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Collective labor agreements</topic>
              <recordInfo>
                <recordIdentifier>5533</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Analysis of Layoff, Recall, and Work-Sharing Procedures in Union Contracts</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 1209</subTitle>
          </titleInfo>
          <identifier type="oclc">06706575</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>monographic</issuance>
            <sortDate>1957-01-01</sortDate>
            <publisher>U.S. Govt. Print. Off.</publisher>
            <dateIssued>1957</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4499</recordIdentifier>
            <recordUpdatedDate>2016-05-19 11:40:57</recordUpdatedDate>
            <recordCreationDate>2015-05-22 17:04:43</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD8051 .A62 no. 1209</classification>
          <classification authority="sudocs">L 2.3:1209</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>40 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=4499&amp;amp;filepath=/files/docs/publications/bls/bls_1209_1957.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:3965</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <genre>statistics</genre>
          <subject>
            <topic>
              <topic>Strikes and lockouts</topic>
              <recordInfo>
                <recordIdentifier>5523</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Statistics</topic>
              <recordInfo>
                <recordIdentifier>4285</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Analysis of Work Stoppages</title>
          </titleInfo>
          <identifier type="oclc">01126708</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>periodical</issuance>
            <sortDate>1949-01-01</sortDate>
            <frequency>annual</frequency>
            <publisher>U.S. Dept. of Labor, Bureau of Labor Statistics</publisher>
            <dateIssued point="start">1943-01-01</dateIssued>
            <dateIssued point="end">1982-03-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>3965</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:13:01</recordUpdatedDate>
            <recordCreationDate>2015-04-07 09:18:24</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD5323 .A26</classification>
          <classification authority="sudocs">L 2.3:</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/3965</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5216</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Includes bibliographical references.</note>
          <subject>
            <topic>
              <topic>Wages</topic>
              <recordInfo>
                <recordIdentifier>4448</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Analyzing 1981 Earnings Data from the Current Population Survey</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 2149</subTitle>
          </titleInfo>
          <identifier type="oclc">09157109</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1982-09-01</sortDate>
            <publisher>U.S. Dept. of Labor, Bureau of Labor Statistics</publisher>
            <dateIssued>September 1982</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5216</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:17:56</recordUpdatedDate>
            <recordCreationDate>2016-05-24 10:55:55</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD4975 .A76 1982</classification>
          <classification authority="ddc">331.2/973</classification>
          <classification authority="sudocs">L 2.3:2149</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>33 pages</extent>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=5216&amp;amp;filepath=/files/docs/publications/bls/bls_2149_1982.pdf</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=5216&amp;amp;filepath=/files/docs/publications/bls/bls_2149_1982.pdf</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4302</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:5135</setSpec>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Mohn, Kermit B.</namePart>
            <recordInfo>
              <recordIdentifier>5135</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Reprinted from the Monthly labor review, May 1945, with additional data.--Page 1.</note>
          <genre>statistics</genre>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Clothing workers</topic>
              <recordInfo>
                <recordIdentifier>5786</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Knit goods industry</topic>
              <recordInfo>
                <recordIdentifier>5787</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Statistics</topic>
              <recordInfo>
                <recordIdentifier>4285</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Wages</topic>
              <recordInfo>
                <recordIdentifier>4448</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States -- Pennsylvania</geographic>
              <recordInfo>
                <recordIdentifier>4404</recordIdentifier>
              </recordInfo>
            </geographic>
            <geographic>
              <geographic>United States -- Pennsylvania -- Philadelphia</geographic>
              <recordInfo>
                <recordIdentifier>4406</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual and Hourly Earnings Philadelphia Knitted-Outerwear Industry, 1943</title>
            <subTitle>Bulletin of the United States Bureau of Labor Statistics, No. 830</subTitle>
          </titleInfo>
          <identifier type="oclc">26795524</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1945-01-01</sortDate>
            <publisher>For sale by the Superintendent of Documents, U.S. Government Printing Office</publisher>
            <dateIssued>1945</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4302</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:18:03</recordUpdatedDate>
            <recordCreationDate>2015-05-21 11:27:54</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD8051 .A62 no.830</classification>
          <classification authority="ddc">331 Un3Ls, No.830</classification>
          <classification authority="sudocs">L 2.3:830</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <extent>11 pages</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/scribd/?title_id=4302&amp;amp;filepath=/files/docs/publications/bls/bls_0830_1945.pdf</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:4653</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:7</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Bureau of Labor Statistics</namePart>
            <recordInfo>
              <recordIdentifier>7</recordIdentifier>
            </recordInfo>
          </name>
          <genre>statistics</genre>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Wages</topic>
              <recordInfo>
                <recordIdentifier>4448</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Statistics</topic>
              <recordInfo>
                <recordIdentifier>4285</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Periodicals</topic>
              <recordInfo>
                <recordIdentifier>5485</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Earnings and Employment Patterns, Private Nonagricultural Employment</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Annual Earnings and Employment Patterns of Private Nonagricultural Employees</title>
          </titleInfo>
          <identifier type="oclc">01956741</identifier>
          <originInfo>
            <place>Washington, D.C</place>
            <issuance>periodical</issuance>
            <sortDate>1964-01-01</sortDate>
            <frequency>completely irregular</frequency>
            <publisher>U.S. Dept. of Labor, Bureau of Labor Statistics</publisher>
            <dateIssued point="start">1970-01-01</dateIssued>
            <dateIssued point="end">1979-01-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>4653</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:17:11</recordUpdatedDate>
            <recordCreationDate>2015-07-30 11:39:45</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Bulletin of the United States Bureau of Labor Statistics</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>BLS Bulletin</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Bulletin of the United States Bureau of Labor</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1486</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="lcc">HD4906 .A5</classification>
          <classification authority="sudocs">L 2.3:</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public Domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/4653</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:849</identifier>
        <datestamp>2016-12-08T12:35:55Z</datestamp>
        <setSpec>author:2268</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Resolution Trust Corporation (U.S.)</namePart>
            <recordInfo>
              <recordIdentifier>2268</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Loans</topic>
              <recordInfo>
                <recordIdentifier>4246</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>RTC Annual Report</title>
          </titleInfo>
          <identifier type="oclc">27472348</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1989-01-01</dateIssued>
            <dateIssued point="end">1995-01-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>849</recordIdentifier>
            <recordUpdatedDate>2015-08-31 15:45:52</recordUpdatedDate>
            <recordCreationDate>2012-08-03 09:55:47.669286</recordCreationDate>
          </recordInfo>
          <classification authority="sudocs">Y 3.R 31/2:1/</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/849</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/849</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:235</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:3</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Board of Governors of the Federal Reserve System (U.S.)</namePart>
            <namePart type="date">1935-</namePart>
            <recordInfo>
              <recordIdentifier>3</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Companion to the Board's Annual report.</note>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Federal Reserve banks</topic>
              <recordInfo>
                <recordIdentifier>4173</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Banks and banking</topic>
              <recordInfo>
                <recordIdentifier>4093</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <abstract>This companion to the Annual Report of the Board of Governors of the Federal Reserve System has been prepared annually since 1986, with the purpose of bringing together information about the Federal Reserve System's spending and budgetary processes.</abstract>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report</title>
            <subTitle>Budget Review</subTitle>
          </titleInfo>
          <identifier type="oclc">20162909</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1986-01-01</dateIssued>
            <dateIssued point="end">2013-01-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>235</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:20:11</recordUpdatedDate>
            <recordCreationDate>2011-07-20 08:40:12.511168</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Annual Reports of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3758</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Annual Report of the Board of Governors of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>117</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">FR 1.1/5X:</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/235</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:5149</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:5926</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Financial Stability Oversight Council</namePart>
            <recordInfo>
              <recordIdentifier>5926</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <name>
              <name>United States. Financial Stability Oversight Council</name>
              <recordInfo>
                <recordIdentifier>5926</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Financial Crisis of 2007-2009</theme>
              <recordInfo>
                <recordIdentifier>103</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Financial crises</topic>
              <recordInfo>
                <recordIdentifier>4189</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Financial institutions</topic>
              <recordInfo>
                <recordIdentifier>4884</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Periodicals</topic>
              <recordInfo>
                <recordIdentifier>5485</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report (Financial Stability Oversight Council)</title>
          </titleInfo>
          <identifier type="oclc">769121803</identifier>
          <originInfo>
            <place>Washington, D.C.</place>
            <issuance>periodical</issuance>
            <sortDate>2011-08-01</sortDate>
            <frequency>annual</frequency>
            <publisher>Financial Stability Oversight Council</publisher>
            <dateIssued point="start">2011-08-01</dateIssued>
            <dateIssued point="end">2016-06-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>5149</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:33:47</recordUpdatedDate>
            <recordCreationDate>2015-12-31 11:55:12</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <classification authority="sudocs">T 1.77</classification>
          <classification authority="ddc">353</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>electronic</form>
            <digitalOrigin>born digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/5149</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/5149</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:117</identifier>
        <datestamp>2017-06-15T17:33:29Z</datestamp>
        <setSpec>author:3</setSpec>
        <setSpec>author:3379</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Board of Governors of the Federal Reserve System (U.S.)</namePart>
            <namePart type="date">1935-</namePart>
            <recordInfo>
              <recordIdentifier>3</recordIdentifier>
            </recordInfo>
          </name>
          <name>
            <role>
              <roleTerm>contributor</roleTerm>
            </role>
            <namePart>Federal Reserve Board</namePart>
            <namePart type="date">1914-1935</namePart>
            <recordInfo>
              <recordIdentifier>3379</recordIdentifier>
            </recordInfo>
          </name>
          <genre>government publication</genre>
          <subject>
            <name>
              <name>Board of Governors of the Federal Reserve System (U.S.), 1935-</name>
              <recordInfo>
                <recordIdentifier>3</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Economic Data</theme>
              <recordInfo>
                <recordIdentifier>21</recordIdentifier>
              </recordInfo>
            </theme>
            <theme>
              <theme>Federal Reserve History</theme>
              <recordInfo>
                <recordIdentifier>25</recordIdentifier>
              </recordInfo>
            </theme>
            <theme>
              <theme>Board of Governors of the Federal Reserve System, Federal Reserve Board</theme>
              <recordInfo>
                <recordIdentifier>28</recordIdentifier>
              </recordInfo>
            </theme>
            <theme>
              <theme>Meltzer's History of the Federal Reserve - Primary Sources</theme>
              <recordInfo>
                <recordIdentifier>97</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Federal Reserve banks</topic>
              <recordInfo>
                <recordIdentifier>4173</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <abstract>Report on operations of the Board during the year. Provides minutes of Federal Open Market Committee meetings, financial statements of the Board and combined financial statements of the Reserve Banks, financial statements for Federal Reserve priced services, information on other services provided by the Reserve Banks, directories of Federal Reserve officials and advisory committees, statistical tables, and maps showing the System's District and Branch boundaries.</abstract>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Board of Governors of the Federal Reserve System</title>
          </titleInfo>
          <identifier type="oclc">5209774</identifier>
          <identifier type="oclc"> 1768174</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1914-12-31</dateIssued>
            <dateIssued point="end">2016-01-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>117</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:19:52</recordUpdatedDate>
            <recordCreationDate>2009-10-20 16:52:16.104492</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Annual Reports of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3758</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Federal Reserve Bulletin</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>62</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Annual Report</title>
              <subTitle>Budget Review</subTitle>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>235</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Prelude to the Annual Report</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>U.S. Economy in Transition</title>
              <titlePartNumber>1970</titlePartNumber>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Monetary Policy and the U.S. Economy in ...</title>
              <titlePartNumber>1971-1974</titlePartNumber>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>236</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">FR 1.1:</classification>
          <typeOfResource>text</typeOfResource>
          <accessCondition>Public domain</accessCondition>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/117</url>
          </location>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:56</identifier>
        <datestamp>2017-03-31T10:45:34Z</datestamp>
        <setSpec>author:19</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Office of the Comptroller of the Currency</namePart>
            <recordInfo>
              <recordIdentifier>19</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">For the years 1923-1941 and 1949, the Individual Statements of Condition of National Banks were published as a separate supplement.</note>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Economic Data</theme>
              <recordInfo>
                <recordIdentifier>21</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Banks and banking</topic>
              <recordInfo>
                <recordIdentifier>4093</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Statistics</topic>
              <recordInfo>
                <recordIdentifier>4285</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Comptroller of the Currency</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Annual Report</title>
          </titleInfo>
          <identifier type="oclc">1589176</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1863-01-01</dateIssued>
            <dateIssued point="end">1980-01-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>56</recordIdentifier>
            <recordUpdatedDate>2017-03-31 11:12:36</recordUpdatedDate>
            <recordCreationDate>2006-10-18 12:55:57.546756</recordCreationDate>
          </recordInfo>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>All-Bank Statistics, United States, 1896-1955</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>39</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Annual Report of the Secretary of the Treasury on the State of the Finances</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Report of the Secretary of the Treasury</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>194</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Individual Statements of Condition of National Banks</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Individual Statements of Condition of National Banks (and Non-national Banks in District of Columbia) at Close of Business ...1939-&lt;1940&gt;</title>
            </titleInfo>
            <titleInfo type="alternate">
              <title>Individual Statements of Condition of National Banks (and Private Banks Not Under State Supervision) at the Close of Business ...1934</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>85</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">T 12.1</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/56</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/56</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:312</identifier>
        <datestamp>2017-05-18T16:07:25Z</datestamp>
        <setSpec>author:770</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>United States. Women's Bureau</namePart>
            <recordInfo>
              <recordIdentifier>770</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Title varies slightly.</note>
          <note type="public">The predecessor to the Women's Bureau, the Women in Industry Service, produced a report for 1919. In 1932, the report was included in the Annual Report of the Secretary of Labor.</note>
          <genre>government publication</genre>
          <subject>
            <theme>
              <theme>Women in the Economy</theme>
              <recordInfo>
                <recordIdentifier>73</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Women</topic>
              <recordInfo>
                <recordIdentifier>4450</recordIdentifier>
              </recordInfo>
            </topic>
            <topic>
              <topic>Employment</topic>
              <recordInfo>
                <recordIdentifier>4158</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>United States</geographic>
              <recordInfo>
                <recordIdentifier>4293</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Director of the Women's Bureau for the Fiscal Year Ended June 30</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Annual Report of the Director of the Woman in Industry Service for the Fiscal Year Ended June 30</title>
          </titleInfo>
          <identifier type="oclc">10080670</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1919-09-30</dateIssued>
            <dateIssued point="end">1932-07-15</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>312</recordIdentifier>
            <recordUpdatedDate>2017-05-18 16:07:26</recordUpdatedDate>
            <recordCreationDate>2011-10-03 16:11:17.614238</recordCreationDate>
          </recordInfo>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Annual Report of the Director of the Woman in Industry Service for the Fiscal Year Ended June 30</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>313</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <classification authority="sudocs">L 13.1:</classification>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/312</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/312</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:3768</identifier>
        <datestamp>2017-02-28T15:46:01Z</datestamp>
        <setSpec>author:455</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Deposit Insurance Corporation</namePart>
            <recordInfo>
              <recordIdentifier>455</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Title varies slightly.</note>
          <genre>periodical</genre>
          <genre>government publication</genre>
          <subject>
            <topic>
              <topic>Banks and banking</topic>
              <recordInfo>
                <recordIdentifier>4093</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Federal Deposit Insurance Corporation</title>
          </titleInfo>
          <identifier type="oclc">1296007</identifier>
          <originInfo>
            <place>Washington</place>
            <issuance>periodical</issuance>
            <sortDate>1934-03-30</sortDate>
            <frequency>annual</frequency>
            <publisher>Federal Deposit Insurance Corporation</publisher>
            <dateIssued point="start">1934-01-01</dateIssued>
            <dateIssued point="end">2016-01-01</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>3768</recordIdentifier>
            <recordUpdatedDate>2016-05-19 12:56:08</recordUpdatedDate>
            <recordCreationDate>2015-01-07 12:36:55</recordCreationDate>
            <recordContentSource>oclc</recordContentSource>
          </recordInfo>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <extent>83 vol.</extent>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/3768</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/3768</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:148</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:508</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Reserve Bank of Atlanta</namePart>
            <recordInfo>
              <recordIdentifier>508</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Title varies slightly.</note>
          <note type="public">Annual reports for the years 1988-current are available at &lt;a href="http://www.frbatlanta.org/pubs/annualreport/"&gt;http://www.frbatlanta.org/pubs/annualreport/&lt;/a&gt;.</note>
          <genre>periodical</genre>
          <subject>
            <theme>
              <theme>Federal Reserve Bank of Atlanta</theme>
              <recordInfo>
                <recordIdentifier>38</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Banks and banking</topic>
              <recordInfo>
                <recordIdentifier>4093</recordIdentifier>
              </recordInfo>
            </topic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Federal Reserve Bank of Atlanta</title>
          </titleInfo>
          <identifier type="oclc">1350436</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1915-12-31</dateIssued>
            <dateIssued point="end">2000-12-31</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>148</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:32:51</recordUpdatedDate>
            <recordCreationDate>2012-05-10 10:43:06.217304</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Annual Reports of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3758</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/148</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/148</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:465</identifier>
        <datestamp>2017-03-30T10:22:07Z</datestamp>
        <setSpec>author:127</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Reserve Bank of Boston</namePart>
            <recordInfo>
              <recordIdentifier>127</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Title varies slightly.</note>
          <note type="public">Annual reports were not published for the years 1969-1988.</note>
          <note type="public">Annual reports for the years 1989-current are available at &lt;a href="http://www.bostonfed.org/about/ar/index.htm"&gt;http://www.bostonfed.org/about/ar/index.htm&lt;/a&gt;.</note>
          <genre>periodical</genre>
          <subject>
            <theme>
              <theme>Federal Reserve Bank of Boston</theme>
              <recordInfo>
                <recordIdentifier>43</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Economic conditions</topic>
              <recordInfo>
                <recordIdentifier>4145</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>New England</geographic>
              <recordInfo>
                <recordIdentifier>4454</recordIdentifier>
              </recordInfo>
            </geographic>
            <geographic>
              <geographic>Federal Reserve District, 1st</geographic>
              <recordInfo>
                <recordIdentifier>4177</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Federal Reserve Bank of Boston</title>
          </titleInfo>
          <identifier type="oclc">1346285</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1915-12-31</dateIssued>
            <dateIssued point="end">1967-12-31</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>465</recordIdentifier>
            <recordUpdatedDate>2017-03-30 10:25:00</recordUpdatedDate>
            <recordCreationDate>2012-05-10 10:43:06.217304</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Annual Reports of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3758</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/465</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/465</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:472</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:516</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Reserve Bank of Chicago</namePart>
            <recordInfo>
              <recordIdentifier>516</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Annual reports for the years 1998-current are available at &lt;a href="http://www.chicagofed.org/webpages/publications/annual_report/"&gt;http://www.chicagofed.org/webpages/publications/annual_report/&lt;/a&gt;.</note>
          <genre>periodical</genre>
          <subject>
            <name>
              <name>Federal Reserve Bank of Chicago</name>
              <recordInfo>
                <recordIdentifier>516</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Federal Reserve Bank of Chicago</theme>
              <recordInfo>
                <recordIdentifier>44</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Economic conditions</topic>
              <recordInfo>
                <recordIdentifier>4145</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>Federal Reserve District, 7th</geographic>
              <recordInfo>
                <recordIdentifier>4183</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Federal Reserve Bank of Chicago</title>
          </titleInfo>
          <identifier type="oclc">4107517</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1919-12-31</dateIssued>
            <dateIssued point="end">1997-12-31</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>472</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:32:53</recordUpdatedDate>
            <recordCreationDate>2012-05-10 10:43:06.217304</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Annual Reports of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3758</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/472</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/472</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:470</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:523</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Reserve Bank of Cleveland</namePart>
            <recordInfo>
              <recordIdentifier>523</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Issued to the public to provide current information about the structure and functions of the 4th Federal Reserve District. Annual reports for the years 1980-current are available at &lt;a href="http://www.clevelandfed.org/about_us/annual_report/"&gt;http://www.clevelandfed.org/about_us/annual_report/&lt;/a&gt;.</note>
          <genre>periodical</genre>
          <subject>
            <name>
              <name>Federal Reserve Bank of Cleveland</name>
              <recordInfo>
                <recordIdentifier>523</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Federal Reserve Bank of Cleveland</theme>
              <recordInfo>
                <recordIdentifier>50</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Economic conditions</topic>
              <recordInfo>
                <recordIdentifier>4145</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>Federal Reserve District, 4th</geographic>
              <recordInfo>
                <recordIdentifier>4180</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Federal Reserve Bank of Cleveland</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Federal Reserve Bank of Cleveland ... Annual Report</title>
          </titleInfo>
          <identifier type="oclc">1332214</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1916-12-31</dateIssued>
            <dateIssued point="end">1979-12-31</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>470</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:32:55</recordUpdatedDate>
            <recordCreationDate>2012-05-10 10:43:06.217304</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Annual Reports of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3758</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/470</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/470</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:475</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:517</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Reserve Bank of Dallas</namePart>
            <recordInfo>
              <recordIdentifier>517</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Each report has a specific title.</note>
          <note type="public">Annual reports for the years 1915-current are available at &lt;a href="http://www.dallasfed.org/fed/annual/"&gt;http://www.dallasfed.org/fed/annual/&lt;/a&gt;.</note>
          <genre>periodical</genre>
          <subject>
            <name>
              <name>Federal Reserve Bank of Dallas</name>
              <recordInfo>
                <recordIdentifier>517</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Federal Reserve Bank of Dallas</theme>
              <recordInfo>
                <recordIdentifier>51</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Economic conditions</topic>
              <recordInfo>
                <recordIdentifier>4145</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>Federal Reserve District, 11th</geographic>
              <recordInfo>
                <recordIdentifier>4175</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Federal Reserve Bank of Dallas</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Federal Reserve Bank of Dallas ... Annual Report</title>
            <titlePartNumber>1998</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">1332233</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1919-12-31</dateIssued>
            <dateIssued point="end">1935-12-31</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>475</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:32:56</recordUpdatedDate>
            <recordCreationDate>2012-05-10 10:43:06.217304</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Annual Reports of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3758</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/475</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/475</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:474</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:518</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Reserve Bank of Kansas City</namePart>
            <recordInfo>
              <recordIdentifier>518</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Annual reports for the years 1995-current are available at &lt;a href="http://www.kc.frb.org/publications/aboutthefed/annualreport/"&gt;http://www.kc.frb.org/publications/aboutthefed/annualreport/&lt;/a&gt;.</note>
          <genre>periodical</genre>
          <subject>
            <name>
              <name>Federal Reserve Bank of Kansas City</name>
              <recordInfo>
                <recordIdentifier>518</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Federal Reserve Bank of Kansas City</theme>
              <recordInfo>
                <recordIdentifier>52</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Economic conditions</topic>
              <recordInfo>
                <recordIdentifier>4145</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>Federal Reserve District, 10th</geographic>
              <recordInfo>
                <recordIdentifier>4174</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Federal Reserve Bank of Kansas City</title>
          </titleInfo>
          <titleInfo type="uniform">
            <title>Annual Report</title>
          </titleInfo>
          <identifier type="oclc">24563413</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1919-12-31</dateIssued>
            <dateIssued point="end">1935-12-31</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>474</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:32:57</recordUpdatedDate>
            <recordCreationDate>2012-05-10 10:43:06.217304</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Annual Reports of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3758</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/474</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/474</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:473</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:519</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Reserve Bank of Minneapolis</namePart>
            <recordInfo>
              <recordIdentifier>519</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Some issues called Annual report or have other slight variations in title.</note>
          <note type="public">Issues for 1937-1944 lack title.</note>
          <note type="public">Report for 1996- issued as number of: Region (Minneapolis, Minn.).</note>
          <note type="public">Annual reports for the years 2000-current are available at &lt;a href="http://www.minneapolisfed.org/publications_papers/ar/index_issues.cfm"&gt;http://www.minneapolisfed.org/publications_papers/ar/index_issues.cfm&lt;/a&gt;.</note>
          <genre>periodical</genre>
          <subject>
            <name>
              <name>Federal Reserve Bank of Minneapolis</name>
              <recordInfo>
                <recordIdentifier>519</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Federal Reserve Bank of Minneapolis</theme>
              <recordInfo>
                <recordIdentifier>48</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Economic conditions</topic>
              <recordInfo>
                <recordIdentifier>4145</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>Federal Reserve District, 9th</geographic>
              <recordInfo>
                <recordIdentifier>4185</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Federal Reserve Bank of Minneapolis</title>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Annual Report of the Federal Reserve Agent of the Ninth Federal Reserve District to the Federal Reserve Board</title>
            <titlePartNumber>2010</titlePartNumber>
          </titleInfo>
          <titleInfo type="alternate">
            <title>Annual Statement</title>
            <titlePartNumber>2010</titlePartNumber>
          </titleInfo>
          <identifier type="oclc">1569019</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1915-12-31</dateIssued>
            <dateIssued point="end">1998-12-31</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>473</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:32:57</recordUpdatedDate>
            <recordCreationDate>2012-05-10 10:43:06.217304</recordCreationDate>
          </recordInfo>
          <relatedItem type="similarTo">
            <titleInfo>
              <title>Annual Report to the Directors of the Federal Reserve Bank of Minneapolis</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>1159</recordIdentifier>
            </recordInfo>
            <typeOfResource>text</typeOfResource>
          </relatedItem>
          <relatedItem type="series">
            <titleInfo>
              <title>Annual Reports of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3758</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/473</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/473</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:467</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:128</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Reserve Bank of New York</namePart>
            <recordInfo>
              <recordIdentifier>128</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Title varies slightly.</note>
          <note type="public">Annual reports for the years 1995-current are available at &lt;a href="http://www.newyorkfed.org/aboutthefed/annualreports.html"&gt;http://www.newyorkfed.org/aboutthefed/annualreports.html&lt;/a&gt;.</note>
          <genre>periodical</genre>
          <subject>
            <theme>
              <theme>Federal Reserve Bank of New York</theme>
              <recordInfo>
                <recordIdentifier>26</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Economic conditions</topic>
              <recordInfo>
                <recordIdentifier>4145</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>Federal Reserve District, 2nd</geographic>
              <recordInfo>
                <recordIdentifier>4178</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Federal Reserve Bank of New York</title>
          </titleInfo>
          <identifier type="oclc">1569026</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1915-12-31</dateIssued>
            <dateIssued point="end">1994-12-31</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>467</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:32:59</recordUpdatedDate>
            <recordCreationDate>2012-05-10 10:43:06.217304</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Annual Reports of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3758</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/467</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/467</accessCondition>
        </mods>
      </metadata>
    </record>
    <record>
      <header>
        <identifier>oai:fraser.stlouisfed.org:title:469</identifier>
        <datestamp>2017-02-23T07:31:04Z</datestamp>
        <setSpec>author:520</setSpec>
      </header>
      <metadata>
        <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
          <name>
            <role>
              <roleTerm>creator</roleTerm>
            </role>
            <namePart>Federal Reserve Bank of Philadelphia</namePart>
            <recordInfo>
              <recordIdentifier>520</recordIdentifier>
            </recordInfo>
          </name>
          <note type="public">Annual reports for the years 1998-current are available at &lt;a href="http://www.philadelphiafed.org/publications/annual-report/"&gt;http://www.philadelphiafed.org/publications/annual-report/&lt;/a&gt;.</note>
          <genre>periodical</genre>
          <subject>
            <name>
              <name>Federal Reserve Bank of Philadelphia</name>
              <recordInfo>
                <recordIdentifier>520</recordIdentifier>
              </recordInfo>
            </name>
            <theme>
              <theme>Federal Reserve Bank of Philadelphia</theme>
              <recordInfo>
                <recordIdentifier>47</recordIdentifier>
              </recordInfo>
            </theme>
            <topic>
              <topic>Economic conditions</topic>
              <recordInfo>
                <recordIdentifier>4145</recordIdentifier>
              </recordInfo>
            </topic>
            <geographic>
              <geographic>Federal Reserve District, 3rd</geographic>
              <recordInfo>
                <recordIdentifier>4179</recordIdentifier>
              </recordInfo>
            </geographic>
          </subject>
          <language>eng</language>
          <titleInfo>
            <title>Annual Report of the Federal Reserve Bank of Philadelphia</title>
          </titleInfo>
          <identifier type="oclc">706051687</identifier>
          <originInfo>
            <issuance>periodical</issuance>
            <frequency>annual</frequency>
            <dateIssued point="start">1919-12-31</dateIssued>
            <dateIssued point="end">1935-12-31</dateIssued>
          </originInfo>
          <recordInfo>
            <recordIdentifier>469</recordIdentifier>
            <recordUpdatedDate>2017-02-23 07:33:00</recordUpdatedDate>
            <recordCreationDate>2012-05-10 10:43:06.217304</recordCreationDate>
          </recordInfo>
          <relatedItem type="series">
            <titleInfo>
              <title>Annual Reports of the Federal Reserve System</title>
            </titleInfo>
            <recordInfo>
              <recordIdentifier>3758</recordIdentifier>
            </recordInfo>
          </relatedItem>
          <typeOfResource>text</typeOfResource>
          <physicalDescription>
            <form>print</form>
            <digitalOrigin>reformatted digital</digitalOrigin>
            <internetMediaType>application/pdf</internetMediaType>
          </physicalDescription>
          <location>
            <url>https://fraser.stlouisfed.org/title/469</url>
          </location>
          <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/469</accessCondition>
        </mods>
      </metadata>
    </record>
    <resumptionToken expirationDate="2017-06-20T02:41:41Z" completeListSize="2996" cursor="0">1497926501:0</resumptionToken>
  </ListRecords>
</OAI-PMH>

*/