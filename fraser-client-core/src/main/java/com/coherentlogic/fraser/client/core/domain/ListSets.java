package com.coherentlogic.fraser.client.core.domain;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * 
 *
 */
@Entity
@Table(name=ListSets.LIST_SETS)
@XStreamAlias(ListSets.LIST_SETS)
@Visitable
public class ListSets extends SerializableBean {

    private static final long serialVersionUID = -6306791136338274881L;

    public static final String LIST_SETS = "ListSets", LIST_SET_LIST = "listSetList";

    @XStreamAlias(ListSet.SET)
    @XStreamImplicit
    @Visitable
    private List<ListSet> listSetList;

    @XStreamAlias(ResumptionToken.RESUMPTION_TOKEN)
    private ResumptionToken resumptionToken;

    public List<ListSet> getListSetList() {
        return listSetList;
    }

    public void setListSetList(@Changeable (LIST_SET_LIST) List<ListSet> listSetList) {
        this.listSetList = listSetList;
    }

    public ResumptionToken getResumptionToken() {
        return resumptionToken;
    }

    public void setResumptionToken(
        @Changeable (ResumptionToken.RESUMPTION_TOKEN) ResumptionToken resumptionToken
    ) {
        this.resumptionToken = resumptionToken;
    }

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        listSetList.forEach( listSet -> { listSet.accept(visitors); } );

        resumptionToken.accept(visitors);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((listSetList == null) ? 0 : listSetList.hashCode());
        result = prime * result + ((resumptionToken == null) ? 0 : resumptionToken.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ListSets other = (ListSets) obj;
        if (listSetList == null) {
            if (other.listSetList != null)
                return false;
        } else if (!listSetList.equals(other.listSetList))
            return false;
        if (resumptionToken == null) {
            if (other.resumptionToken != null)
                return false;
        } else if (!resumptionToken.equals(other.resumptionToken))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ListSets [listSetList=" + listSetList + ", resumptionToken=" + resumptionToken + "]";
    }
}
/*
<ListSets>
<set>
<setSpec>author</setSpec>
<setName>Authors</setName>
</set>
<set>
<setSpec>author:1</setSpec>
<setName>Council of Economic Advisers (U.S.)</setName>
</set>
<set>
<setSpec>author:10</setSpec>
<setName>United States. Federal Open Market Committee</setName>
</set>
<set>
<setSpec>author:1010</setSpec>
<setName>United States. Department of Agriculture</setName>
</set>
<set>
<setSpec>author:1014</setSpec>
<setName>
United States. Bureau of Labor Statistics. Division of Occupational Outlook
</setName>
</set>
<set>
<setSpec>author:1023</setSpec>
<setName>Federal Savings and Loan Insurance Corporation</setName>
</set>
<set>
<setSpec>author:1031</setSpec>
<setName>
Federal Deposit Insurance Corporation. Division of Research and Statistics
</setName>
</set>
<set>
<setSpec>author:1065</setSpec>
<setName>Robertson, A. Willis (Absalom Willis), 1887-1971</setName>
</set>
<set>
<setSpec>author:1072</setSpec>
<setName>Perkins, Frances, 1880-1965</setName>
</set>
<set>
<setSpec>author:109</setSpec>
<setName>D'Arista, Jane W.</setName>
</set>
<set>
<setSpec>author:1092</setSpec>
<setName>Bricker, John W. (John William), 1893-1986</setName>
</set>
<set>
<setSpec>author:1100</setSpec>
<setName>Powell, Oliver S., 1896-1963</setName>
</set>
<set>
<setSpec>author:1107</setSpec>
<setName>Spence, Brent, 1874-1967</setName>
</set>
<set>
<setSpec>author:1119</setSpec>
<setName>Flanders, Ralph E. (Ralph Edward), 1880-1970</setName>
</set>
<set>
<setSpec>author:1133</setSpec>
<setName>
United States. Congress. Senate. Committee on Banking and Currency. Subcommittee on Monetary Policy, Banking, and Deposit Insurance
</setName>
</set>
<set>
<setSpec>author:114</setSpec>
<setName>Douglas, Paul H. (Paul Howard), 1892-1976</setName>
</set>
<set>
<setSpec>author:1145</setSpec>
<setName>
Board of Governors of the Federal Reserve System (U.S.). Research Library
</setName>
</set>
<set>
<setSpec>author:1152</setSpec>
<setName>Robinson, R. I.</setName>
</set>
<set>
<setSpec>author:116</setSpec>
<setName>Dunne, Gerald T.</setName>
</set>
<set>
<setSpec>author:118</setSpec>
<setName>Eccles, Marriner S. (Marriner Stoddard), 1890-1977</setName>
</set>
<set>
<setSpec>author:1184</setSpec>
<setName>Holdsworth, John Thom, 1873-1965</setName>
</set>
<set>
<setSpec>author:1185</setSpec>
<setName>Havenstein, Rudolf</setName>
</set>
<set>
<setSpec>author:1186</setSpec>
<setName>Germany. Bankenquete-kommission</setName>
</set>
<set>
<setSpec>author:1187</setSpec>
<setName>Huntington, A. T. (Andrew Tyler), 1848-1915</setName>
</set>
<set>
<setSpec>author:1188</setSpec>
<setName>Mawhinney, Robert J.</setName>
</set>
<set>
<setSpec>author:1189</setSpec>
<setName>United States. Congress. Conference Committees</setName>
</set>
<set>
<setSpec>author:1244</setSpec>
<setName>
United States. Department of the Treasury. Bureau of Government Financial Operations
</setName>
</set>
<set>
<setSpec>author:1245</setSpec>
<setName>
United States. Congress. Senate. Committee on the Judiciary
</setName>
</set>
<set>
<setSpec>author:127</setSpec>
<setName>Federal Reserve Bank of Boston</setName>
</set>
<set>
<setSpec>author:128</setSpec>
<setName>Federal Reserve Bank of New York</setName>
</set>
<set>
<setSpec>author:1289</setSpec>
<setName>Shade, C. S.</setName>
</set>
<set>
<setSpec>author:129</setSpec>
<setName>Federal Reserve Bank of St. Louis</setName>
</set>
<set>
<setSpec>author:13</setSpec>
<setName>Martin, William McChesney</setName>
</set>
<set>
<setSpec>author:1331</setSpec>
<setName>
United States. Federal Home Loan Bank Administration
</setName>
</set>
<set>
<setSpec>author:134</setSpec>
<setName>Fisher, Irving, 1867-1947</setName>
</set>
<set>
<setSpec>author:1370</setSpec>
<setName>Black, Eugene R. (Eugene Robert), 1898-1992</setName>
</set>
<set>
<setSpec>author:1372</setSpec>
<setName>United States. International Trade Administration</setName>
</set>
<set>
<setSpec>author:1375</setSpec>
<setName>Tamagna, Frank M.</setName>
</set>
<set>
<setSpec>author:139</setSpec>
<setName>Friedman, Milton, 1912-2006</setName>
</set>
<set>
<setSpec>author:1393</setSpec>
<setName>United States. National Resources Committee</setName>
</set>
<set>
<setSpec>author:14</setSpec>
<setName>
United States. Congress. Senate. Committee on Banking, Housing, and Urban Affairs, 1970-
</setName>
</set>
<set>
<setSpec>author:146</setSpec>
<setName>Gans, A.R.</setName>
</set>
<set>
<setSpec>author:1491</setSpec>
<setName>Davis, James J. (James John), 1873-1947</setName>
</set>
<set>
<setSpec>author:15</setSpec>
<setName>
United States. Congress. House. Committee on Banking, Finance, and Urban Affairs, 1977-1995
</setName>
</set>
<set>
<setSpec>author:151</setSpec>
<setName>Glass, Carter, 1858-1946</setName>
</set>
<set>
<setSpec>author:1514</setSpec>
<setName>Litterer, Oscar F.</setName>
</set>
<set>
<setSpec>author:152</setSpec>
<setName>
Goldenweiser, E. A. (Emanuel Alexandrovich), 1883-1953
</setName>
</set>
<set>
<setSpec>author:153</setSpec>
<setName>Golembe, Carter Harry</setName>
</set>
<set>
<setSpec>author:1539</setSpec>
<setName>Draper, Ernest Gallaudet, 1885-1954</setName>
</set>
<set>
<setSpec>author:1547</setSpec>
<setName>United States. Farm Credit Administration</setName>
</set>
<set>
<setSpec>author:16</setSpec>
<setName>
United States. Congress. House. Committee on Banking and Currency, 1865-1974
</setName>
</set>
<set>
<setSpec>author:161</setSpec>
<setName>Hackley, Howard H.</setName>
</set>
<set>
<setSpec>author:164</setSpec>
<setName>Hansen, Alvin H. (Alvin Harvey), 1887-1975</setName>
</set>
<set>
<setSpec>author:165</setSpec>
<setName>Hardy, Charles O. (Charles Oscar), 1884-1948</setName>
</set>
<set>
<setSpec>author:1659</setSpec>
<setName>Steagall, Henry Bascom</setName>
</set>
<set>
<setSpec>author:168</setSpec>
<setName>Harris, Seymour Edwin, 1897-1974</setName>
</set>
<set>
<setSpec>author:169</setSpec>
<setName>Harrison, George Leslie, 1887-1958</setName>
</set>
<set>
<setSpec>author:1698</setSpec>
<setName>
Board of Governors of the Federal Reserve System (U.S.). Steering Committee for the Fundamental Reappraisal of the Discount Mechanism, 1935-
</setName>
</set>
<set>
<setSpec>author:1699</setSpec>
<setName>
Board of Governors of the Federal Reserve System (U.S.). Committee on Branch, Group, and Chain Banking, 1935-
</setName>
</set>
<set>
<setSpec>author:17</setSpec>
<setName>
United States. Congress. House. Committee on Financial Services, 2001-
</setName>
</set>
<set>
<setSpec>author:1729</setSpec>
<setName>Nugent, Rolf, 1901-1946</setName>
</set>
<set>
<setSpec>author:179</setSpec>
<setName>Holland, Robert C.</setName>
</set>
<set>
<setSpec>author:1798</setSpec>
<setName>American Bankers Association</setName>
</set>
<set>
<setSpec>author:186</setSpec>
<setName>International Monetary Fund</setName>
</set>
<set>
<setSpec>author:19</setSpec>
<setName>
United States. Office of the Comptroller of the Currency
</setName>
</set>
<set>
<setSpec>author:190</setSpec>
<setName>Johnson, Richard N.</setName>
</set>
<set>
<setSpec>author:1936</setSpec>
<setName>Fisher, Clyde Olin, 1891-</setName>
</set>
<set>
<setSpec>author:2</setSpec>
<setName>
United States. Congress. Joint Committee on the Economic Report, 1946-1956
</setName>
</set>
<set>
<setSpec>author:20</setSpec>
<setName>Burns, Arthur F. (Arthur Frank), 1904-1987</setName>
</set>
<set>
<setSpec>author:209</setSpec>
<setName>Laidler, David E. W.</setName>
</set>
<set>
<setSpec>author:21</setSpec>
<setName>Greenspan, Alan, 1926-</setName>
</set>
<set>
<setSpec>author:212</setSpec>
<setName>Leach, Hugh</setName>
</set>
<set>
<setSpec>author:22</setSpec>
<setName>
United States. Office of Commissioner of Internal Revenue
</setName>
</set>
<set>
<setSpec>author:2258</setSpec>
<setName>Ritter, Lawrence S.</setName>
</set>
<set>
<setSpec>author:2261</setSpec>
<setName>Murphy, Philip G.</setName>
</set>
<set>
<setSpec>author:2262</setSpec>
<setName>
United States. Congress. House. Committee on Banking, Finance, and Urban Affairs. Subcommittee on Economic Stabilization, 1977-[1995]
</setName>
</set>
<set>
<setSpec>author:2263</setSpec>
<setName>Clifford, Edward</setName>
</set>
<set>
<setSpec>author:2264</setSpec>
<setName>National Woman's Liberty Loan Committee (U.S.)</setName>
</set>
<set>
<setSpec>author:2265</setSpec>
<setName>
Federal Reserve Bank of New York. Liberty Loan Committee
</setName>
</set>
<set>
<setSpec>author:2267</setSpec>
<setName>
United States. Commission on Economy and Efficiency in the Government Service
</setName>
</set>
<set>
<setSpec>author:2268</setSpec>
<setName>Resolution Trust Corporation (U.S.)</setName>
</set>
<set>
<setSpec>author:2269</setSpec>
<setName>
Resolution Trust Corporation (U.S.). Office of Planning, Research, and Statistics
</setName>
</set>
<set>
<setSpec>author:2272</setSpec>
<setName>Wall, Norman J.</setName>
</set>
<set>
<setSpec>author:2273</setSpec>
<setName>Engquist, E. J., Jr.</setName>
</set>
<set>
<setSpec>author:2274</setSpec>
<setName>United States. Federal Farm Loan Board</setName>
</set>
<set>
<setSpec>author:2275</setSpec>
<setName>Home Builders Association of Mississippi</setName>
</set>
<set>
<setSpec>author:2276</setSpec>
<setName>O'Neill, June</setName>
</set>
<set>
<setSpec>author:2277</setSpec>
<setName>Chaikind, Stephen</setName>
</set>
<set>
<setSpec>author:2278</setSpec>
<setName>Sanders, Hyman</setName>
</set>
<set>
<setSpec>author:2279</setSpec>
<setName>
United States. Congress. Joint Economic Committee. Subcommittee on Economic Statistics
</setName>
</set>
<set>
<setSpec>author:228</setSpec>
<setName>Martin, William McChesney, 1874-1955</setName>
</set>
<set>
<setSpec>author:2280</setSpec>
<setName>
United States. Congress. Joint Economic Committee. Subcommittee on Economic Goals and Intergovernmental Policy
</setName>
</set>
<set>
<setSpec>author:2281</setSpec>
<setName>
United States. Congress. Joint Economic Committee. Subcommittee on Economic Growth
</setName>
</set>
<set>
<setSpec>author:2283</setSpec>
<setName>
United States. Congress. Joint Economic Committee. Subcommittee on International Exchange and Payments
</setName>
</set>
<set>
<setSpec>author:2285</setSpec>
<setName>
United States. Congress. Joint Economic Committee. Subcommittee on International Economics
</setName>
</set>
<set>
<setSpec>author:2286</setSpec>
<setName>
United States. Congress. Senate. Committee on the Budget
</setName>
</set>
<set>
<setSpec>author:2287</setSpec>
<setName>
United States. Congress. Joint Economic Committee. Subcommittee on Energy
</setName>
</set>
<set>
<setSpec>author:2288</setSpec>
<setName>
United States. Congress. Joint Economic Committee. Subcommittee on Consumer Economics
</setName>
</set>
<set>
<setSpec>author:2289</setSpec>
<setName>
United States. Congress. Joint Economic Committee. Subcommittee on Economic Progress
</setName>
</set>
<set>
<setSpec>author:23</setSpec>
<setName>Volcker, Paul A.</setName>
</set>
<resumptionToken expirationDate="2017-06-30T21:08:36Z" completeListSize="1819" cursor="0">1498856916:0</resumptionToken>
</ListSets>
*/