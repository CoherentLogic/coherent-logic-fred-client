package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * <topic>
 *     <topic>Federal Reserve banks</topic>
 *     <recordInfo>
 *         <recordIdentifier>4173</recordIdentifier>
 *     </recordInfo>
 * </topic>
 *
 * @TODO Combine this class with Theme and put the recordInfo in the base class.
 */
@Entity
@Table(name=Topic.TOPIC)
@XStreamAlias(Topic.TOPIC)
@Visitable
public class Topic extends SerializableBean {

    private static final long serialVersionUID = 3726725597414598500L;

    static final String TOPIC = "topic";

    @XStreamAlias(TOPIC)
    @Visitable
    private String topic;

    @XStreamAlias(RecordInfo.RECORD_INFO)
    @Visitable
    private RecordInfo recordInfo;

    public String getTopic() {
        return topic;
    }

    public void setTopic(@Changeable (TOPIC) String topic) {
        this.topic = topic;
    }

    public RecordInfo getRecordInfo() {
        return recordInfo;
    }

    public void setRecordInfo(@Changeable (RecordInfo.RECORD_INFO) RecordInfo recordInfo) {
        this.recordInfo = recordInfo;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((recordInfo == null) ? 0 : recordInfo.hashCode());
        result = prime * result + ((topic == null) ? 0 : topic.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Topic other = (Topic) obj;
        if (recordInfo == null) {
            if (other.recordInfo != null)
                return false;
        } else if (!recordInfo.equals(other.recordInfo))
            return false;
        if (topic == null) {
            if (other.topic != null)
                return false;
        } else if (!topic.equals(other.topic))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Topic [topic=" + topic + ", recordInfo=" + recordInfo + "]";
    }
}
