package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 
 * <name>
 *     <name>Federal Home Loan Bank of Seattle</name>
 *     <recordInfo>
 *         <recordIdentifier>4801</recordIdentifier>
 *     </recordInfo>
 * </name>
 *
 */
@Entity
@Table(name=SubjectName.NAME)
@Visitable
public class SubjectName extends SerializableBean {

    private static final long serialVersionUID = 5643582066415532872L;

    static final String NAME = "name";

    @Visitable
    private String name;

    @XStreamAlias(RecordInfo.RECORD_INFO)
    @Visitable
    private RecordInfo recordInfo;

    public String getName() {
        return name;
    }

    public void setName(@Changeable (NAME) String name) {
        this.name = name;
    }

    public RecordInfo getRecordInfo() {
        return recordInfo;
    }

    public void setRecordInfo(@Changeable (RecordInfo.RECORD_INFO) RecordInfo recordInfo) {
        this.recordInfo = recordInfo;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((recordInfo == null) ? 0 : recordInfo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SubjectName other = (SubjectName) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        if (recordInfo == null) {
            if (other.recordInfo != null)
                return false;
        } else if (!recordInfo.equals(other.recordInfo))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SubjectName [name=" + name + ", recordInfo=" + recordInfo + "]";
    }
}
