package com.coherentlogic.fraser.client.core.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * 
 * <tableOfContents>
 *     <extent>
 *         <endPage>4</endPage>
 *         <startPage>3</startPage>
 *     </extent>
 *     <titleInfo>
 *         <title>Preface</title>
 *     </titleInfo>
 *     <recordInfo>
 *         <recordIdentifier>48794</recordIdentifier>
 *         <recordIdentifier>48794</recordIdentifier>
 *     </recordInfo>
 * </tableOfContents>
 */
@Entity
@Table(name=TableOfContents.TABLE_OF_CONTENTS)
@XStreamAlias(TableOfContents.TABLE_OF_CONTENTS)
@Visitable
public class TableOfContents extends SerializableBean {

    private static final long serialVersionUID = -6461022792956617306L;

    static final String TABLE_OF_CONTENTS = "tableOfContents", RECORD_INFO_LIST = "recordInfoList";

    @XStreamAlias(Extent.EXTENT)
    @Visitable
    private Extent extent;

    @XStreamAlias(TitleInfo.TITLE_INFO)
    @Visitable
    private TitleInfo titleInfo;

    @XStreamAlias(RecordInfo.RECORD_INFO)
    @XStreamImplicit
    @Visitable
    private List<RecordInfo> recordInfoList;

    public Extent getExtent() {
        return extent;
    }

    public void setExtent(@Changeable (Extent.EXTENT) Extent extent) {
        this.extent = extent;
    }

    public TitleInfo getTitleInfo() {
        return titleInfo;
    }

    public void setTitleInfo(@Changeable (TitleInfo.TITLE_INFO) TitleInfo titleInfo) {
        this.titleInfo = titleInfo;
    }

    public List<RecordInfo> getRecordInfoList() {
        return recordInfoList;
    }

    public void setRecordInfoList(@Changeable (RECORD_INFO_LIST) List<RecordInfo> recordInfoList) {
        this.recordInfoList = recordInfoList;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((extent == null) ? 0 : extent.hashCode());
        result = prime * result + ((recordInfoList == null) ? 0 : recordInfoList.hashCode());
        result = prime * result + ((titleInfo == null) ? 0 : titleInfo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        TableOfContents other = (TableOfContents) obj;
        if (extent == null) {
            if (other.extent != null)
                return false;
        } else if (!extent.equals(other.extent))
            return false;
        if (recordInfoList == null) {
            if (other.recordInfoList != null)
                return false;
        } else if (!recordInfoList.equals(other.recordInfoList))
            return false;
        if (titleInfo == null) {
            if (other.titleInfo != null)
                return false;
        } else if (!titleInfo.equals(other.titleInfo))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TableOfContents [extent=" + extent + ", titleInfo=" + titleInfo + ", recordInfoList=" +
            recordInfoList + "]";
    }
}
