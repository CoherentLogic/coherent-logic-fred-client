package com.coherentlogic.fraser.client.core.converters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.exceptions.MethodNotSupportedException;
import com.coherentlogic.coherent.data.adapter.core.xstream.exceptions.MarshalMethodNotSupportedException;
import com.coherentlogic.fraser.client.core.domain.AbstractMetadataPrefixBean;
import com.coherentlogic.fraser.client.core.domain.Request;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Used to convert XML in the form below into an instance of {@link Request}.
 *
 * <request verb="Identify">https://fraser.stlouisfed.org/oai</request>
 *
 * The reason we need a custom converter is that when *both* the {@link Request} class and the Request.value property
 * are annotated with @XStreamAlias the {@link Request#getValue()} method will end up returning null.
 *
 * @TODO determine if something regarding the use of annotations has been overlooked, specifically with respect to the
 *  value property.
 */
public class RequestConverter implements Converter {

    private static final Logger log = LoggerFactory.getLogger(GenericResponseConverter.class);

    @Override
    public boolean canConvert(Class type) {
        return Request.class.equals(type);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MarshalMethodNotSupportedException ();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        log.debug("unmarshal: method begins; reader: " + reader);

        Request result = new Request ();

        String verb = reader.getAttribute(Request.VERB);

        log.debug("verb: " + verb);

        result.setVerb(verb);

        String metadataPrefix = reader.getAttribute(AbstractMetadataPrefixBean.METADATA_PREFIX);

        log.debug("metadataPrefix: " + metadataPrefix);

        result.setMetadataPrefix(metadataPrefix);

        String value = reader.getValue();

        log.debug("value: " + value);

        result.setValue(value);

        log.debug("unmarshal: method ends; result: " + result);

        return result;
    }

}
