package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * https://fraser.stlouisfed.org/oai/?verb=ListMetadataFormats
 *
 * <OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/">
 *     <responseDate>2017-06-30T01:19:13Z</responseDate>
 *     <request verb="ListMetadataFormats">https://fraser.stlouisfed.org/oai</request>
 *     <ListMetadataFormats>
 *         <metadataFormat>
 *             <metadataPrefix>mods</metadataPrefix>
 *             <schema>http://www.loc.gov/standards/mods/v3/mods-3-5.xsd</schema>
 *             <metadataNamespace>http://www.loc.gov/mods/v3</metadataNamespace>
 *         </metadataFormat>
 *     </ListMetadataFormats>
 * </OAI-PMH>
 */
@Entity
@Table(name=MetadataFormat.METADATA_FORMAT)
@XStreamAlias(MetadataFormat.METADATA_FORMAT)
@Visitable
public class MetadataFormat extends SerializableBean {

    public static final String
        METADATA_FORMAT = "metadataFormat",
        METADATA_PREFIX = "metadataPrefix",
        SCHEMA = "schema",
        METADATA_NAMESPACE = "metadataNamespace";

    @XStreamAlias(METADATA_PREFIX)
    @Visitable
    private String metadataPrefix;

    @XStreamAlias(SCHEMA)
    @Visitable
    private String schema;

    @XStreamAlias(METADATA_NAMESPACE)
    @Visitable
    private String metadataNamespace;

    public String getMetadataPrefix() {
        return metadataPrefix;
    }

    public void setMetadataPrefix(@Changeable(METADATA_PREFIX) String metadataPrefix) {
        this.metadataPrefix = metadataPrefix;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(@Changeable(SCHEMA) String schema) {
        this.schema = schema;
    }

    public String getMetadataNamespace() {
        return metadataNamespace;
    }

    public void setMetadataNamespace(@Changeable(METADATA_NAMESPACE) String metadataNamespace) {
        this.metadataNamespace = metadataNamespace;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((metadataNamespace == null) ? 0 : metadataNamespace.hashCode());
        result = prime * result + ((metadataPrefix == null) ? 0 : metadataPrefix.hashCode());
        result = prime * result + ((schema == null) ? 0 : schema.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        MetadataFormat other = (MetadataFormat) obj;
        if (metadataNamespace == null) {
            if (other.metadataNamespace != null)
                return false;
        } else if (!metadataNamespace.equals(other.metadataNamespace))
            return false;
        if (metadataPrefix == null) {
            if (other.metadataPrefix != null)
                return false;
        } else if (!metadataPrefix.equals(other.metadataPrefix))
            return false;
        if (schema == null) {
            if (other.schema != null)
                return false;
        } else if (!schema.equals(other.schema))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "MetadataFormat [metadataPrefix=" + metadataPrefix + ", schema=" + schema + ", metadataNamespace="
            + metadataNamespace + "]";
    }
}
