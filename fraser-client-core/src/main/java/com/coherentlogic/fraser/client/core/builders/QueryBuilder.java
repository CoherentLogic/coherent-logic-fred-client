package com.coherentlogic.fraser.client.core.builders;

import java.util.function.Function;

import javax.ws.rs.core.UriBuilder;

import org.springframework.web.client.RestTemplate;

import com.coherentlogic.coherent.data.adapter.core.builders.WithCommandSpecification;
import com.coherentlogic.coherent.data.adapter.core.builders.rest.AbstractRESTQueryBuilder;
import com.coherentlogic.coherent.data.adapter.core.cache.CacheServiceProviderSpecification;
import com.coherentlogic.coherent.data.adapter.core.util.WelcomeMessage;
import com.coherentlogic.coherent.data.model.core.command.CommandSpecification;
import com.coherentlogic.fraser.client.core.domain.GenericResponse;
import com.coherentlogic.fraser.client.core.domain.GetRecord;
import com.coherentlogic.fraser.client.core.domain.ListIdentifiers;
import com.coherentlogic.fraser.client.core.domain.ListMetadataFormats;
import com.coherentlogic.fraser.client.core.domain.ListRecords;
import com.coherentlogic.fraser.client.core.domain.ListSets;

/**
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class QueryBuilder extends AbstractRESTQueryBuilder<String> implements WithCommandSpecification<QueryBuilder> {

    public static final String
        FRASER_API_ENTRY_POINT = "https://fraser.stlouisfed.org/oai",
        VERB = "verb",
        IDENTIFY = "Identify",
        IDENTIFIER = "identifier",
        LIST_METADATA_FORMATS = "ListMetadataFormats",
        LIST_RECORDS = "ListRecords",
        LIST_IDENTIFIERS = "ListIdentifiers",
        LIST_SETS = "ListSets",
        GET_RECORD = "GetRecord",
        METADATA_PREFIX = "metadataPrefix",
        MODS = "MODS",
        SET = "set",
        RESUMPTION_TOKEN = "resumptionToken";

    static final String[] WELCOME_MESSAGE = {
        " FRASER Client version 2.0.4-RELEASE"
    };

    static {

        WelcomeMessage welcomeMessage = new WelcomeMessage();

        for (String next : WELCOME_MESSAGE) {
            welcomeMessage.addText(next);
        }

        welcomeMessage.display();
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        String uri,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uri, cache);
    }

    public QueryBuilder (RestTemplate restTemplate) {
        super (restTemplate, FRASER_API_ENTRY_POINT);
    }

    public QueryBuilder(RestTemplate restTemplate, String uri) {
        super(restTemplate, uri);
    }

    public QueryBuilder(
        RestTemplate restTemplate,
        UriBuilder uriBuilder,
        CacheServiceProviderSpecification<String> cache
    ) {
        super(restTemplate, uriBuilder, cache);
    }

    public QueryBuilder(RestTemplate restTemplate, UriBuilder uriBuilder) {
        super(restTemplate, uriBuilder);
    }

    @Override
    public String getCacheKey() {
        return getEscapedURI();
    }

    @Override
    protected <T> T doExecute(Class<T> type) {
        return (T) getRestTemplate ().getForObject(getEscapedURI (), type);
    }

    /**
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/">FRASER API</a>
     */
    protected QueryBuilder withVerb (String verb) {
        return (QueryBuilder) addParameter(VERB, verb);
    }

    @Override
    public QueryBuilder withCommand(CommandSpecification command) {

        throw new RuntimeException ("The withCommand method is not yet implemented.");
//        getCommandExecutor().addCommand(command);
//
//        return this;
    }

    /**
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/">FRASER API</a>
     */
    public QueryBuilder withVerbAsIdentify () {
        return (QueryBuilder) addParameter(VERB, IDENTIFY);
    }

    /**
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/">FRASER API</a>
     */
    public QueryBuilder withVerbAsListMetadataFormats () {
        return (QueryBuilder) addParameter(VERB, LIST_METADATA_FORMATS);
    }

    /**
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/">FRASER API</a>
     */
    public QueryBuilder withVerbAsListRecords () {
        return (QueryBuilder) addParameter(VERB, LIST_RECORDS);
    }

    /**
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/">FRASER API</a>
     */
    public QueryBuilder withVerbAsListIdentifiers () {
        return (QueryBuilder) addParameter(VERB, LIST_IDENTIFIERS);
    }

    /**
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/">FRASER API</a>
     */
    public QueryBuilder withVerbAsListSets () {
        return (QueryBuilder) addParameter(VERB, LIST_SETS);
    }

    /**
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/">FRASER API</a>
     */
    public QueryBuilder withVerbAsGetRecord () {
        return (QueryBuilder) addParameter(VERB, GET_RECORD);
    }

    /**
     * This parameter specifies the metadata format that should be used in the response.
     *
     * The supported metadata formats can be retrieved using the
     * <a href="https://research.stlouisfed.org/docs/api/fraser/listMetadataFormats.html">ListMetadataFormats</a>
     * request.
     *
     * The FRASER repository currently supports only the MODS metadata format.
     *
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/listRecords.html">listRecords</a>
     */
    public QueryBuilder withMetadataPrefix (String metadataPrefix) {
        return (QueryBuilder) addParameter(METADATA_PREFIX, metadataPrefix);
    }

    /**
     * @see {@link #withMetadataPrefix(String)}
     */
    public QueryBuilder withMetadataPrefixAsMODS () {
        return (QueryBuilder) addParameter(METADATA_PREFIX, MODS);
    }

    /**
     * This parameter specifies the setSpec value and limits the records that are retrieved to only those in the
     * specified set.
     *
     * Ignore this parameter to return all records.
     *
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/listRecords.html">listRecords</a>
     */
    public QueryBuilder withSet (String set) {
        return (QueryBuilder) addParameter(SET, set);
    }

    /**
     * A unique identifier for the record you are requesting. Identifiers can be found using the ListRecords and
     * ListIdentifiers requests.
     *
     * The identifier parameter is required when calling GetRecord.
     *
     * For example:
     *
     * https://fraser.stlouisfed.org/oai/?verb=GetRecord&identifier=oai:fraser.stlouisfed.org:title:176
     *
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/getRecord.html">GetRecord</a>
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/listRecords.html">ListRecords</a>
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/listIdentifiers.html">ListIdentifiers</a>
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/listMetadataFormats.html">ListMetadataFormats</a>
     */
    public QueryBuilder withIdentifier (String identifier) {
        return (QueryBuilder) addParameter(IDENTIFIER, identifier);
    }

    /**
     * A flow control token is returned by a previous request that issued an incomplete list.
     *
     * Use this parameter to retrieve the next set of 100 results.
     *
     * A resumptionToken is not needed for the first request.
     *
     * @see <a href="https://research.stlouisfed.org/docs/api/fraser/listRecords.html">listRecords</a>
     *
     * @deprecated Not deprecated but we need to sort out what a valid resumptionToken format looks like and then check
     *  the value matches that pattern when this method is called -- right now we're simply setting this string.
     */
    public QueryBuilder withResumptionToken (String resumptionToken) {
        return (QueryBuilder) addParameter(RESUMPTION_TOKEN, resumptionToken);
    }

    /**
     * Do get as {@link GenericResponse} and then return that result.
     */
    public GenericResponse doGetAsIdentify () {
        return doGetAsIdentify(data -> { return data; });
    }

    /**
     * Do get as {@link GenericResponse}, execute the given function, and then return an instance of type {@link GenericResponse}.
     */
    public GenericResponse doGetAsIdentify (Function<GenericResponse, GenericResponse> function) {
        return doGetAsIdentify(GenericResponse.class, function);
    }

    /**
     * Do get as {@link GenericResponse}, execute the given function, and then return an instance of type resultType.
     */
    public <R> R doGetAsIdentify (Class<R> resultType, Function<GenericResponse, R> function) {

        GenericResponse identify = doGet(GenericResponse.class);

        R result = function.apply(identify);

        return result;
    }

    /**
     * Do get as {@link ListMetadataFormats} and then return that result.
     */
    public GenericResponse doGetAsListMetadataFormats () {
        return doGetAsListMetadataFormats(data -> { return data; });
    }

    /**
     * Do get as {@link ListMetadataFormats}, execute the given function, and then return an instance of type
     * {@link ListMetadataFormats}.
     */
    public GenericResponse doGetAsListMetadataFormats (Function<GenericResponse, GenericResponse> function) {
        return doGetAsListMetadataFormats(GenericResponse.class, function);
    }

    /**
     * Do get as {@link ListMetadataFormats}, execute the given function, and then return an instance of type
     * resultType.
     */
    public <R> R doGetAsListMetadataFormats (Class<R> resultType, Function<GenericResponse, R> function) {

        GenericResponse listMetadataFormats = doGet(GenericResponse.class);

        R result = function.apply(listMetadataFormats);

        return result;
    }

    /**
     * Do get as {@link ListRecords} and then return that result.
     */
    public GenericResponse doGetAsListRecords () {
        return doGetAsListRecords(data -> { return data; });
    }

    /**
     * Do get as {@link ListRecords}, execute the given function, and then return an instance of type
     * {@link ListRecords}.
     */
    public GenericResponse doGetAsListRecords (Function<GenericResponse, GenericResponse> function) {
        return doGetAsListRecords(GenericResponse.class, function);
    }

    /**
     * Do get as {@link ListRecords}, execute the given function, and then return an instance of type resultType.
     */
    public <R> R doGetAsListRecords (Class<R> resultType, Function<GenericResponse, R> function) {

        GenericResponse listRecords = doGet(GenericResponse.class);

        R result = function.apply(listRecords);

        return result;
    }

    /**
     * Do get as {@link ListIdentifiers} and then return that result.
     */
    public GenericResponse doGetAsListIdentifiers () {
        return doGetAsListIdentifiers(data -> { return data; });
    }

    /**
     * Do get as {@link ListIdentifiers}, execute the given function, and then return an instance of type
     * {@link ListIdentifiers}.
     */
    public GenericResponse doGetAsListIdentifiers (Function<GenericResponse, GenericResponse> function) {
        return doGetAsListIdentifiers(GenericResponse.class, function);
    }

    /**
     * Do get as {@link ListIdentifiers}, execute the given function, and then return an instance of type resultType.
     */
    public <R> R doGetAsListIdentifiers (Class<R> resultType, Function<GenericResponse, R> function) {

        GenericResponse listIdentifiers = doGet(GenericResponse.class);

        R result = function.apply(listIdentifiers);

        return result;
    }

    /**
     * Do get as {@link ListSets} and then return that result.
     */
    public GenericResponse doGetAsListSets () {
        return doGetAsListSets(data -> { return data; });
    }

    /**
     * Do get as {@link ListSets}, execute the given function, and then return an instance of type {@link ListSets}.
     */
    public GenericResponse doGetAsListSets (Function<GenericResponse, GenericResponse> function) {
        return doGetAsListSets(GenericResponse.class, function);
    }

    /**
     * Do get as {@link ListSets}, execute the given function, and then return an instance of type resultType.
     */
    public <R> R doGetAsListSets (Class<R> resultType, Function<GenericResponse, R> function) {

        GenericResponse listSets = doGet(GenericResponse.class);

        R result = function.apply(listSets);

        return result;
    }

    /**
     * Do get as {@link GetRecord} and then return that result.
     */
    public GenericResponse doGetAsGetRecord () {
        return doGetAsGetRecord(data -> { return data; });
    }

    /**
     * Do get as {@link GetRecord}, execute the given function, and then return an instance of type {@link GetRecord}.
     */
    public GenericResponse doGetAsGetRecord (Function<GenericResponse, GenericResponse> function) {
        return doGetAsGetRecord(GenericResponse.class, function);
    }

    /**
     * Do get as {@link GetRecord}, execute the given function, and then return an instance of type resultType.
     */
    public <R> R doGetAsGetRecord (Class<R> resultType, Function<GenericResponse, R> function) {

        GenericResponse getRecord = doGet(GenericResponse.class);

        R result = function.apply(getRecord);

        return result;
    }
}
