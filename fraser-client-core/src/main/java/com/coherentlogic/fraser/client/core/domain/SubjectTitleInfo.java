package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * <titleInfo>
 *     <titleInfo>Stabilization Act of 1942</titleInfo>
 *     <recordInfo>
 *         <recordIdentifier>7399</recordIdentifier>
 *     </recordInfo>
 * </titleInfo>
 */
@Entity
@Table(name=SubjectTitleInfo.TITLE_INFO)
@XStreamAlias(SubjectTitleInfo.TITLE_INFO)
@Visitable
public class SubjectTitleInfo extends SerializableBean {

    private static final long serialVersionUID = -2465728648621661295L;

    static final String TITLE_INFO = "titleInfo";

    @XStreamAlias(TITLE_INFO)
    @Visitable
    private String titleInfo;

    @XStreamAlias(RecordInfo.RECORD_INFO)
    @Visitable
    private RecordInfo recordInfo;

    public String getTitleInfo() {
        return titleInfo;
    }

    public void setTitleInfo(@Changeable (TITLE_INFO) String titleInfo) {
        this.titleInfo = titleInfo;
    }

    public RecordInfo getRecordInfo() {
        return recordInfo;
    }

    public void setRecordInfo(@Changeable (RecordInfo.RECORD_INFO) RecordInfo recordInfo) {
        this.recordInfo = recordInfo;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((recordInfo == null) ? 0 : recordInfo.hashCode());
        result = prime * result + ((titleInfo == null) ? 0 : titleInfo.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SubjectTitleInfo other = (SubjectTitleInfo) obj;
        if (recordInfo == null) {
            if (other.recordInfo != null)
                return false;
        } else if (!recordInfo.equals(other.recordInfo))
            return false;
        if (titleInfo == null) {
            if (other.titleInfo != null)
                return false;
        } else if (!titleInfo.equals(other.titleInfo))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SubjectTitleInfo [titleInfo=" + titleInfo + ", recordInfo=" + recordInfo + "]";
    }
}
