package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.fraser.client.core.converters.ClassificationConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * <classification authority="sudocs">Y 4.B 22/1:P 93/4/v.1-2</classification>
 *
 */
@Entity
@Table(name=Classification.CLASSIFICATION)
@XStreamAlias(Classification.CLASSIFICATION)
@XStreamConverter(ClassificationConverter.class)
@Visitable
public class Classification extends SerializableBean implements ValueSpecification<String> {

    private static final long serialVersionUID = 3914745106792858063L;

    public static final String
        CLASSIFICATION = "classification",
        AUTHORITY = "authority",
        VALUE = "value";

    @XStreamAlias(AUTHORITY)
    @XStreamAsAttribute
    @Visitable
    private String authority;

    @Visitable
    private String value;

    public String getAuthority() {
        return authority;
    }

    public void setAuthority(@Changeable (AUTHORITY) String authority) {
        this.authority = authority;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(@Changeable (VALUE) String value) {
        this.value = value;
    }
}
