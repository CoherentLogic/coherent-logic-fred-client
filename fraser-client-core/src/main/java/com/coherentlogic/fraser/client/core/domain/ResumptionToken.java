package com.coherentlogic.fraser.client.core.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.fraser.client.core.converters.ResumptionTokenConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * 
 *
 * <resumptionToken
 *  expirationDate="2016-07-23T19:02:19Z"
 *  completeListSize="2698"
 *  cursor="100"
 * >1469300539:100</resumptionToken>
 *
 * @See <a href="http://article.gmane.org/gmane.comp.java.xstream.user/7898">Re: Using converters with xstream when an
 *  element has a value and child elements</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=ResumptionToken.RESUMPTION_TOKEN)
@XStreamAlias(ResumptionToken.RESUMPTION_TOKEN)
@XStreamConverter(ResumptionTokenConverter.class)
@Visitable
public class ResumptionToken extends SerializableBean implements ValueSpecification<String> {

    private static final long serialVersionUID = 1574438677994081469L;

    public static final String
        RESUMPTION_TOKEN = "resumptionToken",
        EXPIRATION_DATE = "expirationDate",
        COMPLETE_LIST_SIZE = "completeListSize",
        CURSOR = "cursor",
        VALUE = "value";

    @XStreamAlias(ResumptionToken.EXPIRATION_DATE)
    @XStreamAsAttribute
    @Visitable
    private Date expirationDate;

    @XStreamAlias(ResumptionToken.COMPLETE_LIST_SIZE)
    @XStreamAsAttribute
    @Visitable
    private Integer completeListSize;

    @XStreamAlias(ResumptionToken.CURSOR)
    @XStreamAsAttribute
    @Visitable
    private Integer cursor;

    @XStreamAlias(ResumptionToken.RESUMPTION_TOKEN)
    @Visitable
    private String value;

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(@Changeable (EXPIRATION_DATE) Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Integer getCompleteListSize() {
        return completeListSize;
    }

    public void setCompleteListSize(@Changeable (COMPLETE_LIST_SIZE) Integer completeListSize) {
        this.completeListSize = completeListSize;
    }

    public Integer getCursor() {
        return cursor;
    }

    public void setCursor(@Changeable (CURSOR) Integer cursor) {
        this.cursor = cursor;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(@Changeable (VALUE) String value) {
        this.value = value;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((completeListSize == null) ? 0 : completeListSize.hashCode());
        result = prime * result + ((cursor == null) ? 0 : cursor.hashCode());
        result = prime * result + ((expirationDate == null) ? 0 : expirationDate.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ResumptionToken other = (ResumptionToken) obj;
        if (completeListSize == null) {
            if (other.completeListSize != null)
                return false;
        } else if (!completeListSize.equals(other.completeListSize))
            return false;
        if (cursor == null) {
            if (other.cursor != null)
                return false;
        } else if (!cursor.equals(other.cursor))
            return false;
        if (expirationDate == null) {
            if (other.expirationDate != null)
                return false;
        } else if (!expirationDate.equals(other.expirationDate))
            return false;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ResumptionToken [expirationDate=" + expirationDate + ", completeListSize=" + completeListSize
            + ", cursor=" + cursor + ", value=" + value + "]";
    }
}
