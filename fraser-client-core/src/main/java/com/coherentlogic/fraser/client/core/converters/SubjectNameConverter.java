package com.coherentlogic.fraser.client.core.converters;

import com.coherentlogic.coherent.data.adapter.core.xstream.exceptions.MarshalMethodNotSupportedException;
import com.coherentlogic.fraser.client.core.domain.SubjectName;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class SubjectNameConverter implements Converter {

    @Override
    public boolean canConvert(Class type) {
        return SubjectName.class.equals(type);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MarshalMethodNotSupportedException ();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        SubjectName result = new SubjectName ();

        return result; //context.convertAnother(result, SubjectName.class);
    }
}
