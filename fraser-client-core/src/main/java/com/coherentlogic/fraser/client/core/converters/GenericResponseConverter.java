package com.coherentlogic.fraser.client.core.converters;

import java.time.Instant;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.xstream.exceptions.MarshalMethodNotSupportedException;
import com.coherentlogic.fraser.client.core.domain.ErrorResponse;
import com.coherentlogic.fraser.client.core.domain.GenericResponse;
import com.coherentlogic.fraser.client.core.domain.Identify;
import com.coherentlogic.fraser.client.core.domain.ListIdentifiers;
import com.coherentlogic.fraser.client.core.domain.ListMetadataFormats;
import com.coherentlogic.fraser.client.core.domain.ListRecords;
import com.coherentlogic.fraser.client.core.domain.ListSets;
import com.coherentlogic.fraser.client.core.domain.Request;
import com.coherentlogic.fraser.client.core.exceptions.UnexpectedNodeNameException;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class GenericResponseConverter implements Converter {

    private static final Logger log = LoggerFactory.getLogger(GenericResponseConverter.class);

    @Override
    public boolean canConvert(Class clazz) {
        return GenericResponse.class.equals(clazz);
    }

    @Override
    public void marshal(Object object, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MarshalMethodNotSupportedException ();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        GenericResponse genericResponse = new GenericResponse();

        return unmarshal (reader, context, genericResponse);
    }

    static final String OAI_PMH = "OAI-PMH", RESPONSE_DATE = "responseDate", IDENTIFY = "Identify",
        LIST_RECORDS = "ListRecords", LIST_SETS = "ListSets";

    Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context, GenericResponse genericResponse) {

        if (reader.hasMoreChildren()) {

            reader.moveDown();

            String nodeName = reader.getNodeName();

            log.debug ("nodeName: " + nodeName);

            if (OAI_PMH.equals(nodeName)) {
                // Ignored.
            } else if (RESPONSE_DATE.equals(nodeName)) {

                String responseDateText = reader.getValue();

                log.debug("responseDateText: " + responseDateText);

                Instant responseDateInstant = Instant.parse(responseDateText);

                Date responseDate = Date.from(responseDateInstant);

                log.debug("responseDate: " + responseDate);

                genericResponse.setResponseDate(responseDate);

                unmarshal(reader, context, genericResponse);

            } else if ("request".equals(nodeName)) {

                Request request = (Request) context.convertAnother(genericResponse , Request.class);

                log.debug("request: " + request);

                genericResponse.setRequest(request);

                unmarshal(reader, context, genericResponse);

            } else if (IDENTIFY.equals(nodeName)) {

                Identify identify = (Identify) context.convertAnother(genericResponse, Identify.class);

                genericResponse.setContent(identify);

            } else if (LIST_RECORDS.equals(nodeName)) {

                ListRecords listRecords = (ListRecords) context.convertAnother(genericResponse, ListRecords.class);

                genericResponse.setContent(listRecords);

            } else if (ListIdentifiers.LIST_IDENTIFIERS.equals(nodeName)) {

                ListIdentifiers listIdentifiers = (ListIdentifiers) context.convertAnother(genericResponse, ListIdentifiers.class);

                genericResponse.setContent(listIdentifiers);

            } else if (LIST_SETS.equals(nodeName)) {

                ListSets listSets = (ListSets) context.convertAnother(genericResponse, ListSets.class);

                genericResponse.setContent(listSets);

            } else if (ListMetadataFormats.LIST_METADATA_FORMATS.equals(nodeName)) {

                ListMetadataFormats listMetadataFormats = (ListMetadataFormats)
                    context.convertAnother(genericResponse, ListMetadataFormats.class);

                genericResponse.setContent(listMetadataFormats);

            } else if (ErrorResponse.ERROR.equals(nodeName)) {

                ErrorResponse errorResponse = (ErrorResponse)
                    context.convertAnother(genericResponse, ErrorResponse.class);

                genericResponse.setContent(errorResponse);

            } else throw new UnexpectedNodeNameException ("The nodeName " + nodeName + " is not expected!");

        } else {

            reader.moveUp();

            unmarshal(reader, context, genericResponse);
        }

        return genericResponse;
    }
}

/*
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/">
    <responseDate>2016-07-22T19:02:19Z</responseDate>
    <request verb="ListIdentifiers">https://fraser.stlouisfed.org/oai</request>
    <ListIdentifiers>
        <header>
            <identifier>oai:fraser.stlouisfed.org:title:176</identifier>
            <datestamp>2016-05-31T10:36:09Z</datestamp>
        </header>
        <header>
            <identifier>oai:fraser.stlouisfed.org:title:177</identifier>
            <datestamp>2016-06-24T12:02:03Z</datestamp>
        </header>
        <header>
            <identifier>oai:fraser.stlouisfed.org:title:178</identifier>
            <datestamp>2016-06-24T12:02:03Z</datestamp>
        </header>
        <header>
            <identifier>oai:fraser.stlouisfed.org:title:180</identifier>
            <datestamp>2016-03-31T09:51:02Z</datestamp>
        </header>
        <resumptionToken expirationDate="2016-07-23T19:02:19Z" completeListSize="2698" cursor="100">1469300539:100</resumptionToken>
    </ListIdentifiers>
</OAI-PMH>
*/
