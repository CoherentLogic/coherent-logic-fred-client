package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * <theme>
 *     <theme>Federal Reserve Bank of New York</theme>
 *     <recordInfo>
 *         <recordIdentifier>26</recordIdentifier>
 *     </recordInfo>
 * </theme>
 */
@Entity
@Table(name=Theme.THEME)
@XStreamAlias(Theme.THEME)
@Visitable
public class Theme extends SerializableBean {

    private static final long serialVersionUID = 4835707015263028181L;

    static final String THEME = "theme";

    @XStreamAlias(THEME)
    @Visitable
    private String theme;

    @XStreamAlias(RecordInfo.RECORD_INFO)
    @Visitable
    private RecordInfo recordInfo;

    public String getTheme() {
        return theme;
    }

    public void setTheme(@Changeable (THEME) String theme) {
        this.theme = theme;
    }

    public RecordInfo getRecordInfo() {
        return recordInfo;
    }

    public void setRecordInfo(@Changeable (RecordInfo.RECORD_INFO) RecordInfo recordInfo) {
        this.recordInfo = recordInfo;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((recordInfo == null) ? 0 : recordInfo.hashCode());
        result = prime * result + ((theme == null) ? 0 : theme.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Theme other = (Theme) obj;
        if (recordInfo == null) {
            if (other.recordInfo != null)
                return false;
        } else if (!recordInfo.equals(other.recordInfo))
            return false;
        if (theme == null) {
            if (other.theme != null)
                return false;
        } else if (!theme.equals(other.theme))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Theme [theme=" + theme + ", recordInfo=" + recordInfo + "]";
    }
}
