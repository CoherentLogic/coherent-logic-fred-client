package com.coherentlogic.fraser.client.core.oxm.xstream;

import java.net.URL;

import com.coherentlogic.fraser.client.core.domain.Classification;
import com.coherentlogic.fraser.client.core.domain.GenericResponse;
import com.coherentlogic.fraser.client.core.domain.Identifier;
import com.coherentlogic.fraser.client.core.domain.Identify;
import com.coherentlogic.fraser.client.core.domain.ListIdentifiers;
import com.coherentlogic.fraser.client.core.domain.ListMetadataFormats;
import com.coherentlogic.fraser.client.core.domain.ListRecords;
import com.coherentlogic.fraser.client.core.domain.ListSet;
import com.coherentlogic.fraser.client.core.domain.ListSets;
import com.coherentlogic.fraser.client.core.domain.Location;
import com.coherentlogic.fraser.client.core.domain.MetaData;
import com.coherentlogic.fraser.client.core.domain.Mods;
import com.coherentlogic.fraser.client.core.domain.ModsTitleInfo;
import com.coherentlogic.fraser.client.core.domain.Name;
import com.coherentlogic.fraser.client.core.domain.Record;
import com.coherentlogic.fraser.client.core.domain.RecordInfo;
import com.coherentlogic.fraser.client.core.domain.Request;
import com.coherentlogic.fraser.client.core.domain.SubjectName;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.NoTypePermission;

/**
 * 
 */
public class XStreamMarshaller extends org.springframework.oxm.xstream.XStreamMarshaller {

    /**
     * @see https://groups.google.com/forum/#!topic/xstream-user/wiKfdJPL8aY
     * 
     * @throws com.thoughtworks.xstream.security.ForbiddenClassException
     */
    @Override
    protected void customizeXStream(XStream xstream) {

        super.customizeXStream(xstream);

        xstream.addPermission(NoTypePermission.NONE);

        xstream.allowTypes(
            new Class[] {
                URL.class,
                GenericResponse.class,
                Identify.class,
                Identifier.class,
                Request.class,
                Record.class,
                ModsTitleInfo.class,
                Name.class,
                SubjectName.class,
                RecordInfo.class,
                ListIdentifiers.class,
                ListSets.class,
                ListSet.class,
                ListMetadataFormats.class,
                Location.class,
                Mods.class,
                MetaData.class,
                Record.class,
                ListRecords.class,
                Classification.class
            }
        );
    }
}
