package com.coherentlogic.fraser.client.core.domain;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@Entity
@Table(name=ListRecords.LIST_RECORDS)
@XStreamAlias(ListRecords.LIST_RECORDS)
@Visitable
public class ListRecords extends SerializableBean {

    private static final long serialVersionUID = 2709429245771282378L;

    static final String LIST_RECORDS = "ListRecords", RECORD_LIST = "recordList";

    @XStreamImplicit
    @Visitable
    private List<Record> recordList;

    @Visitable
    private ResumptionToken resumptionToken;

    public ResumptionToken getResumptionToken() {
        return resumptionToken;
    }

    public void setResumptionToken(
        @Changeable(ResumptionToken.RESUMPTION_TOKEN) ResumptionToken resumptionToken
    ) {
        this.resumptionToken = resumptionToken;
    }

    public List<Record> getRecordList() {
        return recordList;
    }

    public void setRecordList(@Changeable(RECORD_LIST) List<Record> recordList) {
        this.recordList = recordList;
    }

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        recordList.forEach(
            record -> {
                record.accept(visitors);
            }
        );
    }

    @Override
    public String toString() {
        return "ListRecords [recordList=" + recordList + ", resumptionToken=" + resumptionToken + "]";
    }
}
