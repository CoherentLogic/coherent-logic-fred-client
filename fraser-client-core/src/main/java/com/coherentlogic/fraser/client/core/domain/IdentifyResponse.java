package com.coherentlogic.fraser.client.core.domain;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * 
 * @author thospfuller
 */
public class IdentifyResponse extends GenericResponse {

    private static final long serialVersionUID = 768225621304100084L;

    public static final String IDENTIFY = "identify";

    @XStreamAlias(Identify.IDENTIFY_ALIAS)
    @XStreamAsAttribute
    @Visitable
    private Identify identify;

    public Identify getIdentify() {
        return identify;
    }

    public void setIdentify(@Changeable (IDENTIFY) Identify identify) {
        this.identify = identify;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((identify == null) ? 0 : identify.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        IdentifyResponse other = (IdentifyResponse) obj;
        if (identify == null) {
            if (other.identify != null)
                return false;
        } else if (!identify.equals(other.identify))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "IdentifyResponse [identify=" + identify + "]";
    }
}
