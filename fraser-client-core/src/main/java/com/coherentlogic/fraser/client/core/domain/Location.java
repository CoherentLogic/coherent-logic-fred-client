package com.coherentlogic.fraser.client.core.domain;

import java.net.URL;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * 
 * <location>
 *     <url>
 *     https://fraser.stlouisfed.org/scribd/?title_id=5549&amp;filepath=/files/docs/publications/women/b0283_dolwb_1962.pdf
 *     </url>
 * </location>
 *
 */
@Entity
@Table(name=Location.LOCATION)
@XStreamAlias(Location.LOCATION)
@Visitable
public class Location extends SerializableBean {

    private static final long serialVersionUID = 5121617522365447133L;

    static final String LOCATION = "location", URLS = "urls";

    @XStreamAlias(URLS)
    @XStreamImplicit
    @Visitable
    private List<URL> urls;

    public List<URL> getUrls() {
        return urls;
    }

    public void setUrls(@Changeable (URLS) List<URL> urls) {
        this.urls = urls;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((urls == null) ? 0 : urls.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Location other = (Location) obj;
        if (urls == null) {
            if (other.urls != null)
                return false;
        } else if (!urls.equals(other.urls))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Location [urls=" + urls + "]";
    }
}
