package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * <physicalDescription>
 *     <form>print</form>
 *     <extent>32 pages</extent>
 *     <digitalOrigin>reformatted digital</digitalOrigin>
 *     <internetMediaType>application/pdf</internetMediaType>
 * </physicalDescription>
 *
 */
@Entity
@Table(name=PhysicalDescription.PHYSICAL_DESCRIPTION)
@XStreamAlias(PhysicalDescription.PHYSICAL_DESCRIPTION)
@Visitable
public class PhysicalDescription extends SerializableBean {

    private static final long serialVersionUID = 8479125324935682325L;

    static final String
        PHYSICAL_DESCRIPTION = "physicalDescription",
        FORM = "form",
        EXTENT = "extent",
        DIGITAL_ORIGIN = "digitalOrigin",
        INTERNET_MEDIA_TYPE = "internetMediaType";

    @XStreamAlias(FORM)
    @Visitable
    private String form;

    @XStreamAlias(EXTENT)
    @Visitable
    private String extent;

    @XStreamAlias(DIGITAL_ORIGIN)
    @Visitable
    private String digitalOrigin;

    @XStreamAlias(INTERNET_MEDIA_TYPE)
    @Visitable
    private String internetMediaType;

    public String getForm() {
        return form;
    }

    public void setForm(@Changeable (FORM) String form) {
        this.form = form;
    }

    public String getExtent() {
        return extent;
    }

    public void setExtent(@Changeable (EXTENT) String extent) {
        this.extent = extent;
    }

    public String getDigitalOrigin() {
        return digitalOrigin;
    }

    public void setDigitalOrigin(@Changeable (DIGITAL_ORIGIN) String digitalOrigin) {
        this.digitalOrigin = digitalOrigin;
    }

    public String getInternetMediaType() {
        return internetMediaType;
    }

    public void setInternetMediaType(@Changeable (INTERNET_MEDIA_TYPE) String internetMediaType) {
        this.internetMediaType = internetMediaType;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((digitalOrigin == null) ? 0 : digitalOrigin.hashCode());
        result = prime * result + ((extent == null) ? 0 : extent.hashCode());
        result = prime * result + ((form == null) ? 0 : form.hashCode());
        result = prime * result + ((internetMediaType == null) ? 0 : internetMediaType.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        PhysicalDescription other = (PhysicalDescription) obj;
        if (digitalOrigin == null) {
            if (other.digitalOrigin != null)
                return false;
        } else if (!digitalOrigin.equals(other.digitalOrigin))
            return false;
        if (extent == null) {
            if (other.extent != null)
                return false;
        } else if (!extent.equals(other.extent))
            return false;
        if (form == null) {
            if (other.form != null)
                return false;
        } else if (!form.equals(other.form))
            return false;
        if (internetMediaType == null) {
            if (other.internetMediaType != null)
                return false;
        } else if (!internetMediaType.equals(other.internetMediaType))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "PhysicalDescription [form=" + form + ", extent=" + extent + ", digitalOrigin=" +
            digitalOrigin + ", internetMediaType=" + internetMediaType + "]";
    }
}
