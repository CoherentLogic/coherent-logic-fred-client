package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * <sortOrder>0772</sortOrder>
 *
 *@see RelatedItem
 */
@Entity
@Table(name=SortOrder.SORT_ORDER)
@XStreamAlias(SortOrder.SORT_ORDER)
@Visitable
public class SortOrder extends SerializableBean {

    private static final long serialVersionUID = -6468571641900519897L;

    static final String SORT_ORDER = "sortOrder";

    @XStreamAlias(SORT_ORDER)
    @Visitable
    private String sortOrder;

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(@Changeable (SORT_ORDER) String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((sortOrder == null) ? 0 : sortOrder.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        SortOrder other = (SortOrder) obj;
        if (sortOrder == null) {
            if (other.sortOrder != null)
                return false;
        } else if (!sortOrder.equals(other.sortOrder))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "SortOrder [sortOrder=" + sortOrder + "]";
    }
}
