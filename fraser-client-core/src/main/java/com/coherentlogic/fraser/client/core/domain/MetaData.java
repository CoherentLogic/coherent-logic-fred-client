package com.coherentlogic.fraser.client.core.domain;

import java.util.Collection;
import java.util.function.Consumer;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @TODO Rename to Metadata.
 *
 */
@Entity
@Table(name=MetaData.METADATA)
@XStreamAlias(MetaData.METADATA)
@Visitable
public class MetaData extends SerializableBean {

    private static final long serialVersionUID = 2199507332061001779L;

    static final String METADATA = "metadata", META_DATA_ALIAS = "metaData";

    @XStreamAlias(Mods.MODS)
    @Visitable
    private Mods mods;

    public Mods getMods() {
        return mods;
    }

    public void setMods(@Changeable (Mods.MODS) Mods mods) {
        this.mods = mods;
    }

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        mods.accept(visitors);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((mods == null) ? 0 : mods.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        MetaData other = (MetaData) obj;
        if (mods == null) {
            if (other.mods != null)
                return false;
        } else if (!mods.equals(other.mods))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "MetaData [mods=" + mods + "]";
    }
}

/*
<metadata>
<mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="">
<name>
  <role>
    <roleTerm>creator</roleTerm>
  </role>
  <namePart>United States. Women's Bureau</namePart>
  <recordInfo>
    <recordIdentifier>770</recordIdentifier>
  </recordInfo>
</name>
<genre>government publication</genre>
<language>eng</language>
<titleInfo>
  <title>15 Years After College: A Study of Alumnae of the Class of 1945</title>
  <subTitle>Women's Bureau Bulletin, No. 283</subTitle>
</titleInfo>
<originInfo>
  <place>Washington</place>
  <issuance>monographic</issuance>
  <sortDate>1962-01-01</sortDate>
  <publisher>Govt. Print. Off.</publisher>
  <dateIssued>1962</dateIssued>
</originInfo>
<recordInfo>
  <recordIdentifier>5549</recordIdentifier>
  <recordUpdatedDate>2017-02-15 15:28:11</recordUpdatedDate>
  <recordCreationDate>2017-02-14 15:37:34</recordCreationDate>
  <recordContentSource>frs</recordContentSource>
</recordInfo>
<relatedItem type="series">
  <titleInfo>
    <title>Bulletin of the Women's Bureau</title>
  </titleInfo>
  <titleInfo type="alternate">
    <title>Bulletin of the Women's Bureau</title>
  </titleInfo>
  <titleInfo type="alternate">
    <title>Women's Bureau Bulletin</title>
  </titleInfo>
  <recordInfo>
    <recordIdentifier>243</recordIdentifier>
  </recordInfo>
</relatedItem>
<typeOfResource>text</typeOfResource>
<physicalDescription>
  <form>print</form>
  <extent>32 pages</extent>
  <digitalOrigin>reformatted digital</digitalOrigin>
  <internetMediaType>application/pdf</internetMediaType>
</physicalDescription>
<location>
  <url>https://fraser.stlouisfed.org/scribd/?title_id=5549&amp;amp;filepath=/files/docs/publications/women/b0283_dolwb_1962.pdf</url>
</location>
<accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/scribd/?title_id=5549&amp;amp;filepath=/files/docs/publications/women/b0283_dolwb_1962.pdf</accessCondition>
</mods>
</metadata>
*/