package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * <extent>
 *     <endPage>4</endPage>
 *     <startPage>3</startPage>
 * </extent>
 */
@Entity
@Table(name=Extent.EXTENT)
@XStreamAlias(Extent.EXTENT)
@Visitable
public class Extent extends SerializableBean {

    private static final long serialVersionUID = 947229338263621326L;

    static final String EXTENT = "extent",
        START_PAGE = "startPage",
        END_PAGE = "endPage";

    @XStreamAlias(START_PAGE)
    @Visitable
    private Integer startPage;

    @XStreamAlias(END_PAGE)
    @Visitable
    private Integer endPage;

    public Integer getStartPage() {
        return startPage;
    }

    public void setStartPage(@Changeable (START_PAGE) Integer startPage) {
        this.startPage = startPage;
    }

    public Integer getEndPage() {
        return endPage;
    }

    public void setEndPage(@Changeable (END_PAGE) Integer endPage) {
        this.endPage = endPage;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((endPage == null) ? 0 : endPage.hashCode());
        result = prime * result + ((startPage == null) ? 0 : startPage.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Extent other = (Extent) obj;
        if (endPage == null) {
            if (other.endPage != null)
                return false;
        } else if (!endPage.equals(other.endPage))
            return false;
        if (startPage == null) {
            if (other.startPage != null)
                return false;
        } else if (!startPage.equals(other.startPage))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Extent [startPage=" + startPage + ", endPage=" + endPage + "]";
    }
}
