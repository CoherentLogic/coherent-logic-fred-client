package com.coherentlogic.fraser.client.core.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
<repositoryName>FRASER</repositoryName>
    <baseURL>https://fraser.stlouisfed.org/oai</baseURL>
    <protocolVersion>2.0</protocolVersion>
    <adminEmail>historical@fraser.stlouisfed.org</adminEmail>
    <earliestDatestamp>1781-05-17T00:00:00Z</earliestDatestamp>
    <deletedRecord>no</deletedRecord>
    <granularity>YYYY-MM-DD</granularity>
 */

/**
 * 
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 */
@Entity
@Table(name=Identify.IDENTIFY_TABLE)
//@XStreamAlias(Identify.IDENTIFY_ALIAS)
@Visitable
public class Identify extends SerializableBean {

    private static final long serialVersionUID = 128100543592404119L;

    public static final String
        IDENTIFY = "identify",
        IDENTIFY_TABLE = "identify_table",
        IDENTIFY_ALIAS = "Identify",
        REPOSITORY_NAME = "repositoryName",
        BASE_URL = "baseURL",
        PROTOCOL_VERSION = "protocolVersion",
        ADMIN_EMAIL = "adminEmail",
        EARLIEST_DATETIMESTAMP = "earliestDatestamp",
        DELETED_RECORD = "deletedRecord",
        GRANULARITY = "granularity";

    @XStreamAlias(REPOSITORY_NAME)
    @XStreamAsAttribute
    @Visitable
    private String repositoryName;

    @XStreamAlias(BASE_URL)
    @XStreamAsAttribute
    @Visitable
    private String baseURL;

    @XStreamAlias(PROTOCOL_VERSION)
    @XStreamAsAttribute
    @Visitable
    private String protocolVersion;

    @XStreamAlias(ADMIN_EMAIL)
    @XStreamAsAttribute
    @Visitable
    private String adminEmail;

    @XStreamAlias(EARLIEST_DATETIMESTAMP)
    @XStreamAsAttribute
    @Visitable
    private Date earliestDatestamp;

    @XStreamAlias(DELETED_RECORD)
    @XStreamAsAttribute
    @Visitable
    private Boolean deletedRecord;

    @XStreamAlias(GRANULARITY)
    @XStreamAsAttribute
    @Visitable
    private String granularity;

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(@Changeable (REPOSITORY_NAME) String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public void setBaseURL(@Changeable (BASE_URL) String baseURL) {
        this.baseURL = baseURL;
    }

    public String getProtocolVersion() {
        return protocolVersion;
    }

    public void setProtocolVersion(@Changeable (PROTOCOL_VERSION) String protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(@Changeable (ADMIN_EMAIL) String adminEmail) {
        this.adminEmail = adminEmail;
    }

    public Date getEarliestDatestamp() {
        return earliestDatestamp;
    }

    public void setEarliestDatestamp(@Changeable (EARLIEST_DATETIMESTAMP) Date earliestDatestamp) {
        this.earliestDatestamp = earliestDatestamp;
    }

    public Boolean getDeletedRecord() {
        return deletedRecord;
    }

    public void setDeletedRecord(@Changeable (DELETED_RECORD) Boolean deletedRecord) {
        this.deletedRecord = deletedRecord;
    }

    public String getGranularity() {
        return granularity;
    }

    public void setGranularity(@Changeable (GRANULARITY) String granularity) {
        this.granularity = granularity;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((adminEmail == null) ? 0 : adminEmail.hashCode());
        result = prime * result + ((baseURL == null) ? 0 : baseURL.hashCode());
        result = prime * result + ((deletedRecord == null) ? 0 : deletedRecord.hashCode());
        result = prime * result + ((earliestDatestamp == null) ? 0 : earliestDatestamp.hashCode());
        result = prime * result + ((granularity == null) ? 0 : granularity.hashCode());
        result = prime * result + ((protocolVersion == null) ? 0 : protocolVersion.hashCode());
        result = prime * result + ((repositoryName == null) ? 0 : repositoryName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Identify other = (Identify) obj;
        if (adminEmail == null) {
            if (other.adminEmail != null)
                return false;
        } else if (!adminEmail.equals(other.adminEmail))
            return false;
        if (baseURL == null) {
            if (other.baseURL != null)
                return false;
        } else if (!baseURL.equals(other.baseURL))
            return false;
        if (deletedRecord == null) {
            if (other.deletedRecord != null)
                return false;
        } else if (!deletedRecord.equals(other.deletedRecord))
            return false;
        if (earliestDatestamp == null) {
            if (other.earliestDatestamp != null)
                return false;
        } else if (!earliestDatestamp.equals(other.earliestDatestamp))
            return false;
        if (granularity == null) {
            if (other.granularity != null)
                return false;
        } else if (!granularity.equals(other.granularity))
            return false;
        if (protocolVersion == null) {
            if (other.protocolVersion != null)
                return false;
        } else if (!protocolVersion.equals(other.protocolVersion))
            return false;
        if (repositoryName == null) {
            if (other.repositoryName != null)
                return false;
        } else if (!repositoryName.equals(other.repositoryName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Identify [repositoryName=" + repositoryName + ", baseURL=" + baseURL + ", protocolVersion="
            + protocolVersion + ", adminEmail=" + adminEmail + ", earliestDatestamp=" + earliestDatestamp
            + ", deletedRecord=" + deletedRecord + ", granularity=" + granularity + "]";
    }
}
