package com.coherentlogic.fraser.client.core.domain;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * https://fraser.stlouisfed.org/oai/?verb=ListIdentifiers
 *
 */
@Entity
@Table(name=ListIdentifiers.LIST_IDENTIFIERS)
@XStreamAlias(ListIdentifiers.LIST_IDENTIFIERS)
@Visitable
public class ListIdentifiers extends SerializableBean {

    private static final long serialVersionUID = 6586799238062880766L;

    public static final String
        LIST_IDENTIFIERS = "ListIdentifiers",
        HEADER_LIST = "headerList",
        RESUMPTION_TOKEN = "resumptionToken";

    @XStreamAlias(Header.HEADER)
    @XStreamImplicit
    @Visitable
    private List<Header> headerList;

    @Visitable
    private ResumptionToken resumptionToken;

    public List<Header> getHeaderList() {
        return headerList;
    }

    public void setHeaderList(@Changeable (HEADER_LIST) List<Header> headerList) {
        this.headerList = headerList;
    }

    public ResumptionToken getResumptionToken() {
        return resumptionToken;
    }

    public void setResumptionToken(@Changeable (RESUMPTION_TOKEN) ResumptionToken resumptionToken) {
        this.resumptionToken = resumptionToken;
    }

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        headerList.forEach(
            header -> {
                header.accept(visitors);
            }
        );
    }

    @Override
    public String toString() {
        return "ListIdentifiers [headerList=" + headerList + ", resumptionToken=" + resumptionToken + "]";
    }
}
