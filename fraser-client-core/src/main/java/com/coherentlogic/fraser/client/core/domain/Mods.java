package com.coherentlogic.fraser.client.core.domain;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * <mods xmlns="http://www.loc.gov/mods/v3" xmlns:default="http://www.loc.gov/mods/v3" default:xsi="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd" default:schemaLocation="http://www.loc.gov/standards/mods/v3/mods-3-5.xsd">
 *   <name>
 *     <role>
 *       <roleTerm>creator</roleTerm>
 *     </role>
 *     <namePart>United States. Women's Bureau</namePart>
 *     <recordInfo>
 *       <recordIdentifier>770</recordIdentifier>
 *     </recordInfo>
 *   </name>
 *   <genre>government publication</genre>
 *   <language>eng</language>
 *   <titleInfo>
 *     <title>15 Years After College: A Study of Alumnae of the Class of 1945</title>
 *     <subTitle>Women's Bureau Bulletin, No. 283</subTitle>
 *   </titleInfo>
 *   <originInfo>
 *     <place>Washington</place>
 *     <issuance>monographic</issuance>
 *     <sortDate>1962-01-01</sortDate>
 *     <publisher>Govt. Print. Off.</publisher>
 *     <dateIssued>1962</dateIssued>
 *   </originInfo>
 *   <relatedItem type="series">
 *     <sortOrder>b0283</sortOrder>
 *     <titleInfo>
 *       <title>Bulletin of the Women's Bureau</title>
 *     </titleInfo>
 *     <titleInfo type="alternate">
 *       <title>Bulletin of the Women's Bureau</title>
 *     </titleInfo>
 *     <titleInfo type="alternate">
 *       <title>Women's Bureau Bulletin</title>
 *     </titleInfo>
 *     <recordInfo>
 *       <recordIdentifier>243</recordIdentifier>
 *     </recordInfo>
 *   </relatedItem>
 *   <typeOfResource>text</typeOfResource>
 *   <physicalDescription>
 *   <form>print</form>
 *   <extent>32 pages</extent>
 *   <digitalOrigin>reformatted digital</digitalOrigin>
 *     <internetMediaType>application/pdf</internetMediaType>
 *   </physicalDescription>
 *   <contentType>title</contentType>
 *   <location>
 *     <url>https://fraser.stlouisfed.org/title/15-years-college-a-study-alumnae-class-1945-5549</url>
 *     <url access="preview">https://fraser.stlouisfed.org/images/record-thumbnail.jpg</url>
 *   </location>
 *   <accessCondition>For more information on rights relating to this item, please see: https://fraser.stlouisfed.org/title/15-years-college-a-study-alumnae-class-1945-5549</accessCondition>
 * </mods>
 *
 */
@Entity
@Table(name=Mods.MODS)
@XStreamAlias(Mods.MODS)
@Visitable
public class Mods extends SerializableBean {

    private static final long serialVersionUID = -5669083646765528362L;

    static final String
        MODS = "mods",
        GENRE = "genre",
        GENRE_LIST = "genreList",
        NAME_LIST = "nameList",
        TITLE_INFO_LIST = "titleInfoList",
        IDENTIFIER_LIST = "identifierList",
        NOTE_LIST = "noteList",
        LANGUAGE = "language",
        TYPE_OF_RESOURCE = "typeOfResource",
        ABSTRACT = "abstract",
        _ABSTRACT = "_abstract",
        RELATED_ITEM_LIST = "relatedItemList",
        CLASSIFICATION_LIST = "classificationList",
        PHYSICAL_DESCRIPTION = "physicalDescription",
        LOCATION = "location",
        ACCESS_CONDITION = "accessCondition",
        TABLE_OF_CONTENTS = "tableOfContents",
        TABLE_OF_CONTENTS_LIST = "tableOfContentsList",
        CONTENT_TYPE = "contentType";

    @XStreamAlias(Name.NAME)
    @XStreamImplicit
    @Visitable
    private List<Name> nameList;

    @XStreamAlias(GENRE)
    @XStreamImplicit
    @Visitable
    private List<String> genreList;

    @XStreamAlias(LANGUAGE)
    @Visitable
    private String language;

    @XStreamAlias(ModsTitleInfo.TITLE_INFO)
    @XStreamImplicit
    @Visitable
    private List<ModsTitleInfo> titleInfoList;

    @XStreamAlias(Identifier.IDENTIFIER)
    @XStreamImplicit
    @Visitable
    private List<Identifier> identifierList;

    @XStreamAlias(OriginInfo.ORIGIN_INFO)
    @Visitable
    private OriginInfo originInfo;

    @XStreamAlias(RecordInfo.RECORD_INFO)
    @Visitable
    private RecordInfo recordInfo;

    @XStreamAlias(Note.NOTE)
    @XStreamImplicit
    @Visitable
    private List<Note> noteList;

    @XStreamAlias(Subject.SUBJECT)
    @Visitable
    private Subject subject;

    @XStreamAlias(ABSTRACT)
    @Visitable
    private String _abstract;

    @XStreamAlias(RelatedItem.RELATED_ITEM)
    @XStreamImplicit
    @Visitable
    private List<RelatedItem> relatedItemList;

    @XStreamAlias(Classification.CLASSIFICATION)
    @XStreamImplicit
    @Visitable
    private List<Classification> classificationList;

    @XStreamAlias(TYPE_OF_RESOURCE)
    @Visitable
    private String typeOfResource;

    @XStreamAlias(PHYSICAL_DESCRIPTION)
    @Visitable
    private PhysicalDescription physicalDescription;

    @XStreamAlias(LOCATION)
    @Visitable
    private Location location;

    @XStreamAlias(ACCESS_CONDITION)
    @Visitable
    private String accessCondition;

    @XStreamAlias(TABLE_OF_CONTENTS)
    @XStreamImplicit
    @Visitable
    private List<TableOfContents> tableOfContentsList;

    @XStreamAlias(CONTENT_TYPE)
    @Visitable
    private String contentType;

    public List<Name> getNameList() {
        return nameList;
    }

    public void setNameList(@Changeable (NAME_LIST) List<Name> nameList) {
        this.nameList = nameList;
    }

    public List<String> getGenreList() {
        return genreList;
    }

    public void setGenreList(@Changeable (GENRE_LIST) List<String> genreList) {
        this.genreList = genreList;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(@Changeable (LANGUAGE) String language) {
        this.language = language;
    }

    public List<ModsTitleInfo> getTitleInfoList() {
        return titleInfoList;
    }

    public void setTitleInfoList(@Changeable (TITLE_INFO_LIST) List<ModsTitleInfo> titleInfoList) {
        this.titleInfoList = titleInfoList;
    }

    public List<Identifier> getIdentifierList() {
        return identifierList;
    }

    public void setIdentifierList(@Changeable (IDENTIFIER_LIST) List<Identifier> identifierList) {
        this.identifierList = identifierList;
    }

    public OriginInfo getOriginInfo() {
        return originInfo;
    }

    public void setOriginInfo(@Changeable (OriginInfo.ORIGIN_INFO) OriginInfo originInfo) {
        this.originInfo = originInfo;
    }

    public RecordInfo getRecordInfo() {
        return recordInfo;
    }

    public void setRecordInfo(@Changeable (RecordInfo.RECORD_INFO) RecordInfo recordInfo) {
        this.recordInfo = recordInfo;
    }

    public List<Note> getNoteList() {
        return noteList;
    }

    public void setNoteList(@Changeable (NOTE_LIST) List<Note> noteList) {
        this.noteList = noteList;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(@Changeable (Subject.SUBJECT) Subject subject) {
        this.subject = subject;
    }

    public String getAbstract() {
        return _abstract;
    }

    public void setAbstract(@Changeable (_ABSTRACT) String _abstract) {
        this._abstract = _abstract;
    }

    public List<RelatedItem> getRelatedItemList() {
        return relatedItemList;
    }

    public void setRelatedItemList(@Changeable (RELATED_ITEM_LIST) List<RelatedItem> relatedItemList) {
        this.relatedItemList = relatedItemList;
    }

    public List<Classification> getClassificationList() {
        return classificationList;
    }

    public void setClassificationList(@Changeable (CLASSIFICATION_LIST) List<Classification> classificationList) {
        this.classificationList = classificationList;
    }

    public String getTypeOfResource() {
        return typeOfResource;
    }

    public void setTypeOfResource(@Changeable (TYPE_OF_RESOURCE) String typeOfResource) {
        this.typeOfResource = typeOfResource;
    }

    public PhysicalDescription getPhysicalDescription() {
        return physicalDescription;
    }

    public void setPhysicalDescription(
        @Changeable (PhysicalDescription.PHYSICAL_DESCRIPTION) PhysicalDescription physicalDescription
    ) {
        this.physicalDescription = physicalDescription;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(@Changeable (Location.LOCATION) Location location) {
        this.location = location;
    }

    public String getAccessCondition() {
        return accessCondition;
    }

    public void setAccessCondition(@Changeable (ACCESS_CONDITION) String accessCondition) {
        this.accessCondition = accessCondition;
    }

    public List<TableOfContents> getTableOfContentsList() {
        return tableOfContentsList;
    }

    public void setTableOfContentsList(
        @Changeable (TABLE_OF_CONTENTS_LIST) List<TableOfContents> tableOfContentsList
    ) {
        this.tableOfContentsList = tableOfContentsList;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(@Changeable (CONTENT_TYPE) String contentType) {
        this.contentType = contentType;
    }

    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        if (getClassificationList() != null)
            getClassificationList().forEach(classification -> { classification.accept(visitors); });

        if (getIdentifierList() != null)
            getIdentifierList().forEach(identifier -> { identifier.accept(visitors); });

        if (getNameList() != null)
            getNameList().forEach(name -> { name.accept(visitors); });

        if (getNoteList() != null)
            getNoteList().forEach(note -> { note.accept(visitors); });

        if (getRelatedItemList() != null)
            getRelatedItemList().forEach(relatedItem -> { relatedItem.accept(visitors); });

        if (getTableOfContentsList() != null)
            getTableOfContentsList().forEach(tableOfContents -> { tableOfContents.accept(visitors); });

        if (getTitleInfoList() != null)
            getTitleInfoList().forEach(titleInfo -> { titleInfo.accept(visitors); });
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((_abstract == null) ? 0 : _abstract.hashCode());
        result = prime * result + ((accessCondition == null) ? 0 : accessCondition.hashCode());
        result = prime * result + ((classificationList == null) ? 0 : classificationList.hashCode());
        result = prime * result + ((contentType == null) ? 0 : contentType.hashCode());
        result = prime * result + ((genreList == null) ? 0 : genreList.hashCode());
        result = prime * result + ((identifierList == null) ? 0 : identifierList.hashCode());
        result = prime * result + ((language == null) ? 0 : language.hashCode());
        result = prime * result + ((location == null) ? 0 : location.hashCode());
        result = prime * result + ((nameList == null) ? 0 : nameList.hashCode());
        result = prime * result + ((noteList == null) ? 0 : noteList.hashCode());
        result = prime * result + ((originInfo == null) ? 0 : originInfo.hashCode());
        result = prime * result + ((physicalDescription == null) ? 0 : physicalDescription.hashCode());
        result = prime * result + ((recordInfo == null) ? 0 : recordInfo.hashCode());
        result = prime * result + ((relatedItemList == null) ? 0 : relatedItemList.hashCode());
        result = prime * result + ((subject == null) ? 0 : subject.hashCode());
        result = prime * result + ((tableOfContentsList == null) ? 0 : tableOfContentsList.hashCode());
        result = prime * result + ((titleInfoList == null) ? 0 : titleInfoList.hashCode());
        result = prime * result + ((typeOfResource == null) ? 0 : typeOfResource.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Mods other = (Mods) obj;
        if (_abstract == null) {
            if (other._abstract != null)
                return false;
        } else if (!_abstract.equals(other._abstract))
            return false;
        if (accessCondition == null) {
            if (other.accessCondition != null)
                return false;
        } else if (!accessCondition.equals(other.accessCondition))
            return false;
        if (classificationList == null) {
            if (other.classificationList != null)
                return false;
        } else if (!classificationList.equals(other.classificationList))
            return false;
        if (contentType == null) {
            if (other.contentType != null)
                return false;
        } else if (!contentType.equals(other.contentType))
            return false;
        if (genreList == null) {
            if (other.genreList != null)
                return false;
        } else if (!genreList.equals(other.genreList))
            return false;
        if (identifierList == null) {
            if (other.identifierList != null)
                return false;
        } else if (!identifierList.equals(other.identifierList))
            return false;
        if (language == null) {
            if (other.language != null)
                return false;
        } else if (!language.equals(other.language))
            return false;
        if (location == null) {
            if (other.location != null)
                return false;
        } else if (!location.equals(other.location))
            return false;
        if (nameList == null) {
            if (other.nameList != null)
                return false;
        } else if (!nameList.equals(other.nameList))
            return false;
        if (noteList == null) {
            if (other.noteList != null)
                return false;
        } else if (!noteList.equals(other.noteList))
            return false;
        if (originInfo == null) {
            if (other.originInfo != null)
                return false;
        } else if (!originInfo.equals(other.originInfo))
            return false;
        if (physicalDescription == null) {
            if (other.physicalDescription != null)
                return false;
        } else if (!physicalDescription.equals(other.physicalDescription))
            return false;
        if (recordInfo == null) {
            if (other.recordInfo != null)
                return false;
        } else if (!recordInfo.equals(other.recordInfo))
            return false;
        if (relatedItemList == null) {
            if (other.relatedItemList != null)
                return false;
        } else if (!relatedItemList.equals(other.relatedItemList))
            return false;
        if (subject == null) {
            if (other.subject != null)
                return false;
        } else if (!subject.equals(other.subject))
            return false;
        if (tableOfContentsList == null) {
            if (other.tableOfContentsList != null)
                return false;
        } else if (!tableOfContentsList.equals(other.tableOfContentsList))
            return false;
        if (titleInfoList == null) {
            if (other.titleInfoList != null)
                return false;
        } else if (!titleInfoList.equals(other.titleInfoList))
            return false;
        if (typeOfResource == null) {
            if (other.typeOfResource != null)
                return false;
        } else if (!typeOfResource.equals(other.typeOfResource))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Mods [nameList=" + nameList + ", genreList=" + genreList + ", language=" + language + ", titleInfoList="
            + titleInfoList + ", identifierList=" + identifierList + ", originInfo=" + originInfo + ", recordInfo="
            + recordInfo + ", noteList=" + noteList + ", subject=" + subject + ", _abstract=" + _abstract
            + ", relatedItemList=" + relatedItemList + ", classificationList=" + classificationList
            + ", typeOfResource=" + typeOfResource + ", physicalDescription=" + physicalDescription + ", location="
            + location + ", accessCondition=" + accessCondition + ", tableOfContentsList=" + tableOfContentsList
            + ", contentType=" + contentType + "]";
    }
}
