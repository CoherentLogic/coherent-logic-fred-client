package com.coherentlogic.fraser.client.core.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * https://fraser.stlouisfed.org/oai/?verb=ListMetadataFormats
 *
 * <OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/">
 *     <responseDate>2017-06-30T01:19:13Z</responseDate>
 *     <request verb="ListMetadataFormats">https://fraser.stlouisfed.org/oai</request>
 *     <ListMetadataFormats>
 *         <metadataFormat>
 *             <metadataPrefix>mods</metadataPrefix>
 *             <schema>http://www.loc.gov/standards/mods/v3/mods-3-5.xsd</schema>
 *             <metadataNamespace>http://www.loc.gov/mods/v3</metadataNamespace>
 *         </metadataFormat>
 *     </ListMetadataFormats>
 * </OAI-PMH>
 */
@Entity
@Table(name=ListMetadataFormats.LIST_METADATA_FORMATS)
@XStreamAlias(ListMetadataFormats.LIST_METADATA_FORMATS)
@Visitable
public class ListMetadataFormats extends SerializableBean {

    private static final long serialVersionUID = 4233646409649767352L;

    public static final String
        LIST_METADATA_FORMATS = "ListMetadataFormats",
        LIST_METADATA_FORMAT_LIST = "listMetadataFormatList";

    @XStreamAlias(MetadataFormat.METADATA_FORMAT)
    @XStreamImplicit
    private List<MetadataFormat> listMetadataFormatList;

    public List<MetadataFormat> getListMetadataFormatList() {
        return listMetadataFormatList;
    }

    public void setListMetadataFormatList(
        @Changeable (LIST_METADATA_FORMAT_LIST) List<MetadataFormat> listMetadataFormatList
    ) {
        this.listMetadataFormatList = listMetadataFormatList;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((listMetadataFormatList == null) ? 0 : listMetadataFormatList.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ListMetadataFormats other = (ListMetadataFormats) obj;
        if (listMetadataFormatList == null) {
            if (other.listMetadataFormatList != null)
                return false;
        } else if (!listMetadataFormatList.equals(other.listMetadataFormatList))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ListMetadataFormats [listMetadataFormatList=" + listMetadataFormatList + "]";
    }
}
