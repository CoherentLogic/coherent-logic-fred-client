package com.coherentlogic.fraser.client.core.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
<originInfo>
  <place>Washington</place>
  <issuance>monographic</issuance>
  <sortDate>1962-01-01</sortDate>
  <publisher>Govt. Print. Off.</publisher>
  <dateIssued>1962</dateIssued>
</originInfo>

<originInfo>
            <edition>Fifth edition</edition>
            <issuance>monographic</issuance>
            <sortDate>1922-01-01</sortDate>
            <dateIssued>January 1922</dateIssued>
          </originInfo>
          
<originInfo>
            <place>Washington, D.C</place>
            <issuance>monographic</issuance>
            <sortDate>1919-11-01</sortDate>
            <dateOther>1920</dateOther>
            <publisher>Government Printing Office</publisher>
            <dateIssued>November 1919</dateIssued>
          </originInfo>

<originInfo>
            <place>Washington, D.C</place>
            <issuance>periodical</issuance>
            <sortDate>1943-01-01</sortDate>
            <frequency>annual</frequency>
            <publisher>G.P.O.</publisher>
            <dateIssued point="start">1944-01-01</dateIssued>
            <dateIssued point="end">1947-01-01</dateIssued>
          </originInfo>
 */
@Entity
@Table(name=OriginInfo.ORIGIN_INFO)
@XStreamAlias(OriginInfo.ORIGIN_INFO)
@Visitable
public class OriginInfo extends SerializableBean {

    private static final long serialVersionUID = 3298785014602558983L;

    static final String
        EDITION = "edition",
        ORIGIN_INFO = "originInfo",
        PLACE = "place",
        ISSUANCE = "issuance",
        SORT_DATE = "sortDate",
        FREQUENCY = "frequency",
        DATE_OTHER = "dateOther",
        PUBLISHER = "publisher",
        DATE_ISSUED = "dateIssued",
        DATE_ISSUED_LIST = "dateIssuedList";

    @XStreamAlias(EDITION)
    @Visitable
    private String edition;

    @XStreamAlias(PLACE)
    @Visitable
    private String place;

    @XStreamAlias(ISSUANCE)
    @Visitable
    private String issuance;

    @XStreamAlias(DATE_OTHER)
    @Visitable
    private String dateOther;

    @XStreamAlias(SORT_DATE)
    @Visitable
    private Date sortDate;

    @XStreamAlias(FREQUENCY)
    @Visitable
    private String frequency;

    @XStreamAlias(PUBLISHER)
    @Visitable
    private String publisher;

    /**
     * 1962, "December 1964"
     */
    @XStreamAlias(DATE_ISSUED)
    @XStreamImplicit
    @Visitable
    private List<String> dateIssuedList;

    public String getEdition() {
        return edition;
    }

    public void setEdition(@Changeable (EDITION) String edition) {
        this.edition = edition;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(@Changeable (PLACE) String place) {
        this.place = place;
    }

    public String getIssuance() {
        return issuance;
    }

    public void setIssuance(@Changeable (ISSUANCE) String issuance) {
        this.issuance = issuance;
    }

    public String getDateOther() {
        return dateOther;
    }

    /**
     * @deprecated Should this be a date?
     */
    public void setDateOther(@Changeable (DATE_OTHER) String dateOther) {
        this.dateOther = dateOther;
    }

    public Date getSortDate() {
        return sortDate;
    }

    public void setSortDate(@Changeable (SORT_DATE) Date sortDate) {
        this.sortDate = sortDate;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(@Changeable (FREQUENCY) String frequency) {
        this.frequency = frequency;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(@Changeable (PUBLISHER) String publisher) {
        this.publisher = publisher;
    }

    public List<String> getDateIssuedList() {
        return dateIssuedList;
    }

    public void setDateIssuedList(@Changeable (DATE_ISSUED_LIST) List<String> dateIssuedList) {
        this.dateIssuedList = dateIssuedList;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((dateIssuedList == null) ? 0 : dateIssuedList.hashCode());
        result = prime * result + ((dateOther == null) ? 0 : dateOther.hashCode());
        result = prime * result + ((edition == null) ? 0 : edition.hashCode());
        result = prime * result + ((frequency == null) ? 0 : frequency.hashCode());
        result = prime * result + ((issuance == null) ? 0 : issuance.hashCode());
        result = prime * result + ((place == null) ? 0 : place.hashCode());
        result = prime * result + ((publisher == null) ? 0 : publisher.hashCode());
        result = prime * result + ((sortDate == null) ? 0 : sortDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        OriginInfo other = (OriginInfo) obj;
        if (dateIssuedList == null) {
            if (other.dateIssuedList != null)
                return false;
        } else if (!dateIssuedList.equals(other.dateIssuedList))
            return false;
        if (dateOther == null) {
            if (other.dateOther != null)
                return false;
        } else if (!dateOther.equals(other.dateOther))
            return false;
        if (edition == null) {
            if (other.edition != null)
                return false;
        } else if (!edition.equals(other.edition))
            return false;
        if (frequency == null) {
            if (other.frequency != null)
                return false;
        } else if (!frequency.equals(other.frequency))
            return false;
        if (issuance == null) {
            if (other.issuance != null)
                return false;
        } else if (!issuance.equals(other.issuance))
            return false;
        if (place == null) {
            if (other.place != null)
                return false;
        } else if (!place.equals(other.place))
            return false;
        if (publisher == null) {
            if (other.publisher != null)
                return false;
        } else if (!publisher.equals(other.publisher))
            return false;
        if (sortDate == null) {
            if (other.sortDate != null)
                return false;
        } else if (!sortDate.equals(other.sortDate))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "OriginInfo [edition=" + edition + ", place=" + place + ", issuance=" + issuance +
            ", dateOther=" + dateOther + ", sortDate=" + sortDate + ", frequency=" + frequency +
            ", publisher=" + publisher + ", dateIssuedList=" + dateIssuedList + "]";
    }
}
