package com.coherentlogic.fraser.client.core.converters;

import java.time.Instant;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.adapter.core.xstream.exceptions.MarshalMethodNotSupportedException;
import com.coherentlogic.fraser.client.core.domain.ResumptionToken;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * 
 * <resumptionToken
 *  expirationDate="2016-07-23T19:02:19Z"
 *  completeListSize="2698"
 *  cursor="100"
 * >1469300539:100</resumptionToken>
 */
public class ResumptionTokenConverter implements Converter {

    private static final Logger log = LoggerFactory.getLogger(ResumptionTokenConverter.class);

    @Override
    public boolean canConvert(Class type) {
        return ResumptionToken.class.equals(type);
    }

    @Override
    public void marshal(Object source, HierarchicalStreamWriter writer, MarshallingContext context) {
        throw new MarshalMethodNotSupportedException ();
    }

    @Override
    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        if (!ResumptionToken.RESUMPTION_TOKEN.equals (reader.getNodeName()))
            throw new RuntimeException ("Node not found: " + ResumptionToken.RESUMPTION_TOKEN);

        ResumptionToken result = (ResumptionToken) unmarshal (reader, context, new ResumptionToken ());

        log.info("result: " + result);

        return result;
    }

    Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context, ResumptionToken result) {

        String expirationDateText = reader.getAttribute(ResumptionToken.EXPIRATION_DATE);

        log.info("expirationDateText: " + expirationDateText);

        Instant expirationDateInstant = Instant.parse(expirationDateText);

        Date expirationDate = Date.from(expirationDateInstant);

        result.setExpirationDate(expirationDate);

        String completeListSizeText = reader.getAttribute(ResumptionToken.COMPLETE_LIST_SIZE);

        log.info("completeListSizeText: " + completeListSizeText);

        Integer completeListSize = Integer.parseInt(completeListSizeText);

        result.setCompleteListSize(completeListSize);

        String cursorText = reader.getAttribute(ResumptionToken.CURSOR);

        log.info("cursorText: " + cursorText);

        Integer cursor = Integer.parseInt(cursorText);

        result.setCursor(cursor);

        String value = reader.getValue();

        result.setValue(value);

        return result;
    }
}
