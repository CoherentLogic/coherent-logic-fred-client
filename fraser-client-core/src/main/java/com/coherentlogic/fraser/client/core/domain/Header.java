package com.coherentlogic.fraser.client.core.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 *
 * <header>
 *     <identifier>oai:fraser.stlouisfed.org:title:5549</identifier>
 *     <datestamp>2017-02-15T15:26:47Z</datestamp>
 *     <setSpec>author:770</setSpec>
 * </header>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=Header.HEADER)
@XStreamAlias(Header.HEADER)
@Visitable
public class Header extends SerializableBean {

    private static final long serialVersionUID = 5632192221692019398L;

    static final String
        HEADER = "header",
        IDENTIFIER = "identifier",
        DATE_STAMP_ALIAS = "datestamp",
        DATE_STAMP = "dateStamp",
        SET_SPEC = "setSpec",
        SET_SPEC_LIST = "setSpecList";

    @XStreamAlias(IDENTIFIER)
    @Visitable
    private String identifier;

    @XStreamAlias(DATE_STAMP_ALIAS)
    @Visitable
    private Date datestamp;

    @XStreamAlias(SET_SPEC)
    @XStreamImplicit
    @Visitable
    private List<String> setSpecList;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(@Changeable (IDENTIFIER) String identifier) {
        this.identifier = identifier;
    }

    public Date getDatestamp() {
        return datestamp;
    }

    public void setDatestamp(@Changeable (DATE_STAMP_ALIAS) Date datestamp) {
        this.datestamp = datestamp;
    }

    public List<String> getSetSpec() {
        return setSpecList;
    }

    public void setSetSpec(@Changeable (SET_SPEC_LIST) List<String> setSpecList) {
        this.setSpecList = setSpecList;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((datestamp == null) ? 0 : datestamp.hashCode());
        result = prime * result + ((identifier == null) ? 0 : identifier.hashCode());
        result = prime * result + ((setSpecList == null) ? 0 : setSpecList.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Header other = (Header) obj;
        if (datestamp == null) {
            if (other.datestamp != null)
                return false;
        } else if (!datestamp.equals(other.datestamp))
            return false;
        if (identifier == null) {
            if (other.identifier != null)
                return false;
        } else if (!identifier.equals(other.identifier))
            return false;
        if (setSpecList == null) {
            if (other.setSpecList != null)
                return false;
        } else if (!setSpecList.equals(other.setSpecList))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Header [identifier=" + identifier + ", datestamp=" + datestamp + ", setSpecList=" + setSpecList + "]";
    }
}
