package com.coherentlogic.fraser.client.core.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.fraser.client.core.converters.GenericResponseConverter;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;

/**
 * 
 * @see <a href="https://www.openarchives.org/">Open Archives Initiative Protocol for Metadata Harvesting</a>
 *
 * @todo Consider splitting this out into another client altogether.
 */
@Entity
@Table(name=GenericResponse.OAI_PMH_TABLE)
@XStreamAlias(GenericResponse.OAI_PMH_ALIAS)
@XStreamConverter(GenericResponseConverter.class)
@Visitable
public class GenericResponse extends SerializableBean {

    private static final long serialVersionUID = -4121494151245628220L;

    public static final String
        OAI_PMH_ALIAS = "OAI-PMH",
        OAI_PMH_TABLE = "OAI_PMH_TABLE",
        RESPONSE_DATE = "responseDate",
        CONTENT = "content";

//    @XStreamAlias(RESPONSE_DATE)
//    @XStreamAsAttribute
    @Visitable
    private Date responseDate;

    @XStreamAlias(Request.REQUEST)
    @XStreamAsAttribute
    @Visitable
    private Request request;

    private SerializableBean content;

    public Date getResponseDate() {
        return responseDate;
    }

    public void setResponseDate(@Changeable(RESPONSE_DATE) Date responseDate) {
        this.responseDate = clone (responseDate);
    }

    public Request getRequest() {
        return request == null ? null : request.clone(Request.class);
    }

    public void setRequest(@Changeable(Request.REQUEST) Request request) {
        this.request = request;
    }

    public void setContent(@Changeable(CONTENT) SerializableBean content) {
        this.content = content;
    }

    public SerializableBean getContent () {
        return content;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((content == null) ? 0 : content.hashCode());
        result = prime * result + ((request == null) ? 0 : request.hashCode());
        result = prime * result + ((responseDate == null) ? 0 : responseDate.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        GenericResponse other = (GenericResponse) obj;
        if (content == null) {
            if (other.content != null)
                return false;
        } else if (!content.equals(other.content))
            return false;
        if (request == null) {
            if (other.request != null)
                return false;
        } else if (!request.equals(other.request))
            return false;
        if (responseDate == null) {
            if (other.responseDate != null)
                return false;
        } else if (!responseDate.equals(other.responseDate))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "GenericResponse [responseDate=" + responseDate + ", request=" + request + ", content=" + content + "]";
    }
}
