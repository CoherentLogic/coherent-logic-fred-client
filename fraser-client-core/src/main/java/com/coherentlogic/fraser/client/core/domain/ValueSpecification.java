package com.coherentlogic.fraser.client.core.domain;

import java.io.Serializable;

/**
 * 
 * @author thospfuller
 *
 * @param <T> The value type.
 *
 * @TODO Move to the EDA
 */
public interface ValueSpecification<T> extends Serializable {

	public static final String VALUE = "value";

	T getValue ();

	void setValue (T value);
}
