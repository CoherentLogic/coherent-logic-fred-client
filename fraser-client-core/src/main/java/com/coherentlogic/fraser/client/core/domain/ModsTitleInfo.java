package com.coherentlogic.fraser.client.core.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Changeable;
import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 
<titleInfo>
  <title>15 Years After College: A Study of Alumnae of the Class of 1945</title>
  <subTitle>Women's Bureau Bulletin, No. 283</subTitle>
</titleInfo>

Might want to create a SubjectTitleInfo because this is very different than the above, which is a ModsTitleInfo,
effectively.

<titleInfo>
<titleInfo>Stabilization Act of 1942</titleInfo>
<recordInfo>
<recordIdentifier>7399</recordIdentifier>
</recordInfo>
</titleInfo>
 */
@Entity
@Table(name=ModsTitleInfo.TITLE_INFO)
@XStreamAlias(ModsTitleInfo.TITLE_INFO)
@Visitable
public class ModsTitleInfo extends SerializableBean {

    private static final long serialVersionUID = 2060820451034546494L;

    static final String
        TITLE_INFO = "titleInfo",
        TITLE = "title",
        SUB_TITLE = "subTitle",
        TITLE_PART_NUMBER = "titlePartNumber";

    @XStreamAlias(TITLE)
    @Visitable
    private String title;

    @XStreamAlias(SUB_TITLE)
    @Visitable
    private String subTitle;

    /**
     * <titlePartNumber>
     *     Seventy-Ninth Congress, Second Session, on S. 2028 (1946)
     * </titlePartNumber>
     */
    @XStreamAlias(TITLE_PART_NUMBER)
    @Visitable
    private String titlePartNumber;

    public String getTitle() {
        return title;
    }

    public void setTitle(@Changeable (TITLE) String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(@Changeable (SUB_TITLE) String subTitle) {
        this.subTitle = subTitle;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        result = prime * result + ((subTitle == null) ? 0 : subTitle.hashCode());
        result = prime * result + ((title == null) ? 0 : title.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        ModsTitleInfo other = (ModsTitleInfo) obj;
        if (subTitle == null) {
            if (other.subTitle != null)
                return false;
        } else if (!subTitle.equals(other.subTitle))
            return false;
        if (title == null) {
            if (other.title != null)
                return false;
        } else if (!title.equals(other.title))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "TitleInfo [title=" + title + ", subTitle=" + subTitle + "]";
    }
}
