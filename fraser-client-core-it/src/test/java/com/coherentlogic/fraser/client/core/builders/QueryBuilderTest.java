package com.coherentlogic.fraser.client.core.builders;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.web.client.RestTemplate;

import com.coherentlogic.fraser.client.core.domain.Classification;
import com.coherentlogic.fraser.client.core.domain.GenericResponse;
import com.coherentlogic.fraser.client.core.domain.Identifier;
import com.coherentlogic.fraser.client.core.domain.ListRecords;
import com.coherentlogic.fraser.client.core.domain.Request;
import com.coherentlogic.fraser.client.core.exceptions.QueryFailedException;

/**
 * Integration test for the QueryBuilder class.
 *
 * Note that if you want to run this test, you'll need to set your own
 * FRED_API_KEY as a key/value pair in you operating system's environment
 * variables, otherwise the call to <code>System.getenv(FRED_API_KEY)</code>,
 * below, will return null, and all tests will fail.
 *
 * WARNING: Some of these numbers change over time, which is why we've changed
 *          the unit tests such that they simply check for null, otherwise we
 *          have tests which will pass for a few days/weeks and then they fail
 *          because, for example 20 is now 19.
 *
 * WARNING: The dates may appear to be off by one day however this is due to the
 *          time zone -- ie. in winter the time is off by one hour.
 *
 * @see http://osdir.com/ml/java.xstream.user/2007-01/msg00058.html
 * @see http://www.timeanddate.com/library/abbreviations/timezones/na/cst.html
 * @see http://en.wikipedia.org/wiki/Daylight_saving_time
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class QueryBuilderTest {

    private static final Logger log = LoggerFactory.getLogger(QueryBuilderTest.class);

    static final String FRED_API_KEY = "FRED_API_KEY";

    /**
     * This value should be set in the environment properties of the operating
     * system. Make sure to restart your IDE and/or OS shell once this has been
     * set.
     */
    static final String API_KEY = System.getenv(FRED_API_KEY),
        FRASER_REST_TEMPLATE_ID = "fraserRestTemplate";

    private final ApplicationContext context
        = new FileSystemXmlApplicationContext ("src/test/resources/spring/application-context.xml");

    private RestTemplate restTemplate = null;

    private QueryBuilder builder = null;

    /**
     * @see https://groups.google.com/forum/#!msg/xstream-user/wiKfdJPL8aY/oiZx6R05CAAJ
     *  Security framework of XStream not initialized, XStream is probably vulnerable.
     */
    @Before
    public void setUp () throws Exception {

        restTemplate = (RestTemplate) context.getBean (FRASER_REST_TEMPLATE_ID);

        builder = new QueryBuilder (restTemplate);
    }

    @After
    public void tearDown () throws Exception {
        restTemplate = null;
        builder = null;
    }

    @Test
    public void getIdentify () {

        GenericResponse result = builder.withVerbAsIdentify().doGetAsIdentify ();

        log.info("result: " + result);

        assertNotNull (result);
        // Previously passing with -- is this a valid format?: 2016-07-22T14:58:13Z
        assertNotNull(result.getResponseDate());
        assertNotEquals("", result.getResponseDate()); // "Sat Jun 10 23:30:43 EDT 2017",

        Request request = new Request ();

        request.setVerb("Identify");
        request.setValue("https://fraser.stlouisfed.org/oai");

        assertEquals(request, result.getRequest());
    }

    /**
     * https://fraser.stlouisfed.org/oai/?verb=ListRecords&metadataPrefix=mods&resumptionToken=1469299598:0
     */
    @Test
    public void getListRecords () {

        GenericResponse result = builder.withVerbAsListRecords().withMetadataPrefixAsMODS().doGetAsListRecords();

        ListRecords listRecords = (ListRecords) result.getContent();

        assertNotNull(listRecords.getResumptionToken().getValue());

        listRecords.accept(
            next -> {
                if (next instanceof Identifier) {

                    Identifier identifier = (Identifier) next;

                    assertNotNull(identifier.getType());
                    assertNotNull(identifier.getValue());

                } else if (next instanceof Classification) {

                    Classification classification = (Classification) next;

                    assertNotNull(classification.getAuthority());
                    assertNotNull(classification.getValue());

                } else {
//                    System.out.println("next: " + next);
                }
            }
        );
    }

    /**
     * https://fraser.stlouisfed.org/oai/?verb=ListIdentifiers
     */
    @Test
    public void getListIdentifiers () {

        GenericResponse result = builder.withVerbAsListIdentifiers().doGetAsListIdentifiers();

        result
            .getContent()
            .accept(
                item -> {
//                    System.out.println("item: " + item);
                }
            );
        
    }

    /**
     * https://fraser.stlouisfed.org/oai/?verb=ListSets
     */
    @Test
    public void getListSets () {

        GenericResponse result = builder.withVerbAsListSets().doGetAsListSets();

        result
            .getContent()
            .accept(
                item -> {
//                    System.out.println("content.item: " + item);
                }
            );

        result
            .getRequest()
            .accept(
                item -> {
//                    System.out.println("request.item: " + item);
                }
            );

    }

    /**
     * https://fraser.stlouisfed.org/oai/?verb=ListMetadataFormats
     */
    @Test
    public void getListMetadataFormats () {

        GenericResponse result = builder.withVerbAsListMetadataFormats().doGetAsListMetadataFormats();

        result
        .getContent()
        .accept(
            item -> {
//                System.out.println("content.item: " + item);
            }
        );

    result
        .getRequest()
        .accept(
            item -> {
//                System.out.println("request.item: " + item);
            }
        );
    }

    /**
     * https://fraser.stlouisfed.org/oai/?verb=GetRecord&identifier=oai:fraser.stlouisfed.org:title:176
     */
    @Test
    public void getRecord () {

        GenericResponse result =
            builder
                .withVerbAsGetRecord()
                .withMetadataPrefixAsMODS()
                .withIdentifier("oai:fraser.stlouisfed.org:title:176")
                .doGetAsGetRecord ();

        result
        .getContent()
        .accept(
            item -> {
//                System.out.println("content.item: " + item);
            }
        );

    result
        .getRequest()
        .accept(
            item -> {
//                System.out.println("request.item: " + item);
            }
        );
    }

// Not sure this test makes sense anymore so I'm disabling it.
//    @Test(expected=QueryFailedException.class)
//    public void causeError () {
//        // https://fraser.stlouisfed.org/oai/?verb=GetRecord&metadataPrefix=MODS&identifier=4237
//        GenericResponse result =
//            builder
//                .withVerbAsGetRecord()
//                .withMetadataPrefixAsMODS()
//                .withIdentifier("4237")
//                .doGetAsGetRecord ();
////                .withCommand(
////                    serializableBean -> {
////
////                        if (! (serializableBean instanceof GenericResponse))
////                            throw new RuntimeException ("Expected an instance of GenericResponse but instead got this: " + serializableBean);
////
////                        GenericResponse genericResponse = (GenericResponse) serializableBean;
////
////                        throw new QueryFailedException (genericResponse);
////                    }
////                ).doGetAsGetRecord ();
//    }
}

/*
<OAI-PMH xmlns="http://www.openarchives.org/OAI/2.0/">
<responseDate>2017-06-15T19:20:12Z</responseDate>
<request verb="Identify">https://fraser.stlouisfed.org/oai</request>
<Identify>
  <repositoryName>FRASER</repositoryName>
  <baseURL>https://fraser.stlouisfed.org/oai</baseURL>
  <protocolVersion>2.0</protocolVersion>
  <adminEmail>historical@fraser.stlouisfed.org</adminEmail>
  <earliestDatestamp>1781-05-17T00:00:00Z</earliestDatestamp>
  <deletedRecord>no</deletedRecord>
  <granularity>YYYY-MM-DD</granularity>
</Identify>
</OAI-PMH>
*/