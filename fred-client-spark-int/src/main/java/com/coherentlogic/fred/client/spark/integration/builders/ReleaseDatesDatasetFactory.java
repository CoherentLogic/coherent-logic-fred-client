package com.coherentlogic.fred.client.spark.integration.builders;

import java.util.List;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import com.coherentlogic.fred.client.core.domain.ReleaseDatesSparkBeanSpecification;

/**
 * A Dataset builder for {@link ReleaseDatesSparkBeanSpecification} objects.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class ReleaseDatesDatasetFactory extends AbstractDatasetFactory<ReleaseDatesSparkBeanSpecification> {

    static final Encoder<ReleaseDatesSparkBeanSpecification> encoder = Encoders.bean(ReleaseDatesSparkBeanSpecification.class);

    public ReleaseDatesDatasetFactory(SparkSession sparkSession, List<ReleaseDatesSparkBeanSpecification> beans) {
        super(sparkSession, encoder, beans);
    }

    public ReleaseDatesDatasetFactory(SparkSession sparkSession) {
        super(sparkSession, encoder);
    }
}
