package com.coherentlogic.fred.client.spark.integration.builders;

import java.util.List;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import com.coherentlogic.fred.client.core.domain.SeriessSparkBeanSpecification;

/**
 * A Dataset builder for {@link SeriessSparkBeanSpecification} objects.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class SeriessDatasetFactory extends AbstractDatasetFactory<SeriessSparkBeanSpecification> {

    static final Encoder<SeriessSparkBeanSpecification> encoder = Encoders.bean(SeriessSparkBeanSpecification.class);

    protected SeriessDatasetFactory(
        SparkSession sparkSession,
        Encoder<SeriessSparkBeanSpecification> encoder,
        List<SeriessSparkBeanSpecification> beans
    ) {
        super(sparkSession, encoder, beans);
    }

    public SeriessDatasetFactory(SparkSession sparkSession, List<SeriessSparkBeanSpecification> beans) {
        super(sparkSession, encoder, beans);
    }

    public SeriessDatasetFactory(SparkSession sparkSession) {
        super(sparkSession, encoder);
    }
}
