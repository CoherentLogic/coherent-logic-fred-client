package com.coherentlogic.fred.client.spark.integration.builders;

import java.util.List;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import com.coherentlogic.fred.client.core.domain.ObservationsSparkBeanSpecification;

/**
 * A Dataset builder for {@link ObservationsSparkBeanSpecification} objects.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class ObservationsDatasetFactory extends AbstractDatasetFactory<ObservationsSparkBeanSpecification> {

    static final Encoder<ObservationsSparkBeanSpecification> encoder =
        Encoders.bean(ObservationsSparkBeanSpecification.class);

    protected ObservationsDatasetFactory(
        SparkSession sparkSession,
        Encoder<ObservationsSparkBeanSpecification> encoder,
        List<ObservationsSparkBeanSpecification> beans
    ) {
        super(sparkSession, encoder, beans);
    }

    public ObservationsDatasetFactory(SparkSession sparkSession, List<ObservationsSparkBeanSpecification> beans) {
        super(sparkSession, encoder, beans);
    }

    public ObservationsDatasetFactory(SparkSession sparkSession) {
        super(sparkSession, encoder);
    }
}
