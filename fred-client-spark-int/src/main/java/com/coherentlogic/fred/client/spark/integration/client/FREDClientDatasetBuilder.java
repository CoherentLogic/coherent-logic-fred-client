package com.coherentlogic.fred.client.spark.integration.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.PreDestroy;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ImportResource;

import com.coherentlogic.fred.client.core.builders.QueryBuilder;
import com.coherentlogic.fred.client.core.domain.Categories;
import com.coherentlogic.fred.client.core.domain.Category;
import com.coherentlogic.fred.client.core.domain.Observation;
import com.coherentlogic.fred.client.core.domain.Observations;
import com.coherentlogic.fred.client.core.domain.ObservationsSparkBeanSpecification;
import com.coherentlogic.fred.client.core.domain.ReleaseDates;
import com.coherentlogic.fred.client.core.domain.ReleaseDatesSparkBeanSpecification;
import com.coherentlogic.fred.client.core.domain.ReleasesSparkBeanSpecification;
import com.coherentlogic.fred.client.core.domain.SeriessSparkBeanSpecification;
import com.coherentlogic.fred.client.core.domain.SourcesSparkBeanSpecification;
import com.coherentlogic.fred.client.core.domain.TagsSparkBeanSpecification;
import com.coherentlogic.fred.client.core.domain.VintageDatesSparkBeanSpecification;
import com.coherentlogic.fred.client.core.factories.QueryBuilderFactory;
import com.coherentlogic.fred.client.spark.integration.builders.CategoriesDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.CategoryDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.ObservationDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.ObservationsDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.ReleaseDatesDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.ReleasesDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.SeriessDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.SourcesDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.TagsDatasetFactory;
import com.coherentlogic.fred.client.spark.integration.builders.VintageDatesDatasetFactory;

/**
 * 
 */
@SpringBootApplication(
    exclude = {
        DataSourceAutoConfiguration.class,
        DataSourceTransactionManagerAutoConfiguration.class,
        HibernateJpaAutoConfiguration.class
    }
)
@EnableAutoConfiguration(
//    exclude = {
//        DataSourceAutoConfiguration.class,
//        DataSourceTransactionManagerAutoConfiguration.class,
//        HibernateJpaAutoConfiguration.class
//    }
)
//@ComponentScan(basePackages="com.coherentlogic.fred.client.core")
@ImportResource({
    "classpath*:spring/api-key-beans.xml",
    "classpath*:spring/application-context.xml"
})
public class FREDClientDatasetBuilder {

    private static final Logger log = LoggerFactory.getLogger(FREDClientDatasetBuilder.class);

    @Autowired
    private ConfigurableApplicationContext applicationContext;

    @Autowired
    private QueryBuilderFactory queryBuilderFactory;

    public Date getDate (int year, int month, int day) {

        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        return calendar.getTime();
    }

    public QueryBuilder newQueryBuilder () {
        return queryBuilderFactory.getInstance();
    }

//    /**
//     * Method returns a new instance of {@link CategoriesDatasetFactory} using the Spark session provided.
//     */
//    public CategoriesDatasetFactory newCategoriesDatasetFactory(SparkSession sparkSession) {
//        return newCategoriesDatasetFactory(sparkSession, new ArrayList<Categories> ());
//    }
//
//    /**
//     * Method returns a new instance of {@link CategoriesDatasetFactory} using the Spark session provided and
//     * containing the specified categories.
//     */
//    public CategoriesDatasetFactory newCategoriesDatasetFactory(SparkSession sparkSession, Categories... categories) {
//        return newCategoriesDatasetFactory(sparkSession, Arrays.asList(categories));
//    }
//
//    /**
//     * Method returns a new instance of {@link CategoriesDatasetFactory} using the Spark session provided and
//     * containing the specified categories.
//     */
//    public CategoriesDatasetFactory newCategoriesDatasetFactory(
//        SparkSession sparkSession,
//        List<Categories> categories
//    ) {
//        CategoriesDatasetFactory result = new CategoriesDatasetFactory(sparkSession);
//
//        result.addBeans(categories);
//
//        return result;
//    }

//    /**
//     * Method returns a new instance of {@link Dataset} using the Spark session provided.
//     */
//    public Dataset<Categories> newCategoriesDataset(SparkSession sparkSession) {
//        return newCategoriesDataset(sparkSession, new ArrayList<Categories> ());
//    }
//
//    /**
//     * Method returns a new instance of {@link Dataset} using the Spark session provided and
//     * containing the specified categories.
//     */
//    public Dataset<Categories> newCategoriesDataset(SparkSession sparkSession, Categories... categories) {
//        return newCategoriesDataset(sparkSession, Arrays.asList(categories));
//    }
//
//    /**
//     * Method returns a new instance of {@link Dataset} using the Spark session provided and
//     * containing the specified categories.
//     */
//    public Dataset<Categories> newCategoriesDataset(SparkSession sparkSession, List<Categories> categories) {
//
//        CategoriesDatasetFactory result = new CategoriesDatasetFactory(sparkSession);
//
//        result.addBeans(categories);
//
//        return result.getInstance();
//    }

    /**
     * Method returns a new instance of {@link CategoriesDatasetFactory} using the Spark session provided.
     */
    public CategoriesDatasetFactory newCategoriesDatasetFactory(SparkSession sparkSession) {
        return newCategoriesDatasetFactory(sparkSession, new ArrayList<Categories> ());
    }

    /**
     * Method returns a new instance of {@link CategoriesDatasetFactory} using the Spark session provided and
     * containing the specified categories.
     */
    public CategoriesDatasetFactory newCategoriesDatasetFactory(SparkSession sparkSession, Categories... categories) {
        return newCategoriesDatasetFactory(sparkSession, Arrays.asList(categories));
    }

    /**
     * Method returns a new instance of {@link CategoriesDatasetFactory} using the Spark session provided and
     * containing the specified categories.
     */
    public CategoriesDatasetFactory newCategoriesDatasetFactory(
        SparkSession sparkSession,
        List<Categories> categories
    ) {
        CategoriesDatasetFactory result = new CategoriesDatasetFactory(sparkSession);

        result.addBeans(categories);

        return result;
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided.
     */
    public Dataset<Categories> newCategoriesDataset(SparkSession sparkSession) {
        return newCategoriesDataset(sparkSession, new ArrayList<Categories> ());
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified categories.
     */
    public Dataset<Categories> newCategoriesDataset(SparkSession sparkSession, Categories... categories) {
        return newCategoriesDataset(sparkSession, Arrays.asList(categories));
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified categories.
     */
    public Dataset<Categories> newCategoriesDataset(SparkSession sparkSession, List<Categories> categories) {

        CategoriesDatasetFactory result = new CategoriesDatasetFactory(sparkSession);

        result.addBeans(categories);

        return result.getInstance();
    }

    /**
     * Method returns a new instance of {@link ObservationsDatasetFactory} using the Spark session provided.
     */
    public ObservationsDatasetFactory newObservationsDatasetFactory(SparkSession sparkSession) {
        return newObservationsDatasetFactory(sparkSession, new ArrayList<ObservationsSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link ObservationsDatasetFactory} using the Spark session provided and
     * containing the specified observations.
     */
    public ObservationsDatasetFactory newObservationsDatasetFactory(
        SparkSession sparkSession,
        Observations... observations
    ) {
        return newObservationsDatasetFactory(sparkSession, Arrays.asList(observations));
    }

    /**
     * Method returns a new instance of {@link ObservationsDatasetFactory} using the Spark session provided and
     * containing the specified observations.
     */
    public ObservationsDatasetFactory newObservationsDatasetFactory(
        SparkSession sparkSession,
        List<ObservationsSparkBeanSpecification> observations
    ) {
        ObservationsDatasetFactory result = new ObservationsDatasetFactory(sparkSession);

        result.addBeans(observations);

        return result;
    }

    // -----------------------------------------------------------------------------------------------------------------
    /**
     * Method returns a new instance of {@link CategoryDatasetFactory} using the Spark session provided.
     */
    public CategoryDatasetFactory newCategoryDatasetFactory(SparkSession sparkSession) {
        return newCategoryDatasetFactory(sparkSession, new ArrayList<Category> ());
    }

    /**
     * Method returns a new instance of {@link CategoryDatasetFactory} using the Spark session provided and
     * containing the specified category.
     */
    public CategoryDatasetFactory newCategoryDatasetFactory(SparkSession sparkSession, Category... category) {
        return newCategoryDatasetFactory(sparkSession, Arrays.asList(category));
    }

    /**
     * Method returns a new instance of {@link CategoryDatasetFactory} using the Spark session provided and
     * containing the specified category.
     */
    public CategoryDatasetFactory newCategoryDatasetFactory(
        SparkSession sparkSession,
        List<Category> category
    ) {
        CategoryDatasetFactory result = new CategoryDatasetFactory(sparkSession);

        result.addBeans(category);

        return result;
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided.
     */
    public Dataset<Category> newCategoryDataset(SparkSession sparkSession) {
        return newCategoryDataset(sparkSession, new ArrayList<Category> ());
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified category.
     */
    public Dataset<Category> newCategoryDataset(SparkSession sparkSession, Category... category) {
        return newCategoryDataset(sparkSession, Arrays.asList(category));
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified category.
     */
    public Dataset<Category> newCategoryDataset(SparkSession sparkSession, List<Category> category) {

        CategoryDatasetFactory result = new CategoryDatasetFactory(sparkSession);

        result.addBeans(category);

        return result.getInstance();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided.
     */
    public Dataset<ObservationsSparkBeanSpecification> newObservationsDataset(SparkSession sparkSession) {
        return newObservationsDataset(sparkSession, new ArrayList<ObservationsSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified observations.
     */
    public Dataset<ObservationsSparkBeanSpecification> newObservationsDataset(
        SparkSession sparkSession,
        ObservationsSparkBeanSpecification... observations
    ) {
        return newObservationsDataset(sparkSession, Arrays.asList(observations));
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified observations.
     */
    public Dataset<ObservationsSparkBeanSpecification> newObservationsDataset(
        SparkSession sparkSession,
        List<ObservationsSparkBeanSpecification> observations
    ) {
        ObservationsDatasetFactory result = new ObservationsDatasetFactory(sparkSession);

        result.addBeans(observations);

        return result.getInstance();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Method returns a new instance of {@link ObservationDatasetFactory} using the Spark session provided.
     */
    public ObservationDatasetFactory newObservationDatasetFactory(SparkSession sparkSession) {
        return newObservationDatasetFactory(sparkSession, new ArrayList<Observation> ());
    }

    /**
     * Method returns a new instance of {@link ObservationDatasetFactory} using the Spark session provided and
     * containing the specified observation.
     */
    public ObservationDatasetFactory newObservationDatasetFactory(SparkSession sparkSession, Observation... observation) {
        return newObservationDatasetFactory(sparkSession, Arrays.asList(observation));
    }

    /**
     * Method returns a new instance of {@link ObservationDatasetFactory} using the Spark session provided and
     * containing the specified observation.
     */
    public ObservationDatasetFactory newObservationDatasetFactory(
        SparkSession sparkSession,
        List<Observation> observation
    ) {
        ObservationDatasetFactory result = new ObservationDatasetFactory(sparkSession);

        result.addBeans(observation);

        return result;
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided.
     */
    public Dataset<Observation> newObservationDataset(SparkSession sparkSession) {
        return newObservationDataset(sparkSession, new ArrayList<Observation> ());
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified observation.
     */
    public Dataset<Observation> newObservationDataset(SparkSession sparkSession, Observation... observation) {
        return newObservationDataset(sparkSession, Arrays.asList(observation));
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified observation.
     */
    public Dataset<Observation> newObservationDataset(SparkSession sparkSession, List<Observation> observation) {

        ObservationDatasetFactory result = new ObservationDatasetFactory(sparkSession);

        result.addBeans(observation);

        return result.getInstance();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Method returns a new instance of {@link ReleaseDatesDatasetFactory} using the Spark session provided.
     */
    public ReleaseDatesDatasetFactory newReleaseDatesDatasetFactory(SparkSession sparkSession) {
        return newReleaseDatesDatasetFactory(sparkSession, new ArrayList<ReleaseDatesSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link ReleaseDatesDatasetFactory} using the Spark session provided and
     * containing the specified releaseDates.
     */
    public ReleaseDatesDatasetFactory newReleaseDatesDatasetFactory(
        SparkSession sparkSession,
        ReleaseDates... releaseDates
    ) {
        return newReleaseDatesDatasetFactory(sparkSession, Arrays.asList(releaseDates));
    }

    /**
     * Method returns a new instance of {@link ReleaseDatesDatasetFactory} using the Spark session provided and
     * containing the specified releaseDates.
     */
    public ReleaseDatesDatasetFactory newReleaseDatesDatasetFactory(
        SparkSession sparkSession,
        List<ReleaseDatesSparkBeanSpecification> releaseDates
    ) {
        ReleaseDatesDatasetFactory result = new ReleaseDatesDatasetFactory(sparkSession);

        result.addBeans(releaseDates);

        return result;
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided.
     */
    public Dataset<ReleaseDatesSparkBeanSpecification> newReleaseDatesDataset(SparkSession sparkSession) {
        return newReleaseDatesDataset(sparkSession, new ArrayList<ReleaseDatesSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified releaseDates.
     */
    public Dataset<ReleaseDatesSparkBeanSpecification> newReleaseDatesDataset(
        SparkSession sparkSession,
        ReleaseDatesSparkBeanSpecification... releaseDates
    ) {
        return newReleaseDatesDataset(sparkSession, Arrays.asList(releaseDates));
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified releaseDates.
     */
    public Dataset<ReleaseDatesSparkBeanSpecification> newReleaseDatesDataset(
        SparkSession sparkSession,
        List<ReleaseDatesSparkBeanSpecification> releaseDates
    ) {

        ReleaseDatesDatasetFactory result = new ReleaseDatesDatasetFactory(sparkSession);

        result.addBeans(releaseDates);

        return result.getInstance();
    }
    /**
     * Method returns a new instance of {@link ReleasesDatasetFactory} using the Spark session provided.
     */
    public ReleasesDatasetFactory newReleasesDatasetFactory(SparkSession sparkSession) {
        return newReleasesDatasetFactory(sparkSession, new ArrayList<ReleasesSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link ReleasesDatasetFactory} using the Spark session provided and
     * containing the specified releases.
     */
    public ReleasesDatasetFactory newReleasesDatasetFactory(SparkSession sparkSession, ReleasesSparkBeanSpecification... releases) {
        return newReleasesDatasetFactory(sparkSession, Arrays.asList(releases));
    }

    /**
     * Method returns a new instance of {@link ReleasesDatasetFactory} using the Spark session provided and
     * containing the specified releases.
     */
    public ReleasesDatasetFactory newReleasesDatasetFactory(
        SparkSession sparkSession,
        List<ReleasesSparkBeanSpecification> releases
    ) {
        ReleasesDatasetFactory result = new ReleasesDatasetFactory(sparkSession);

        result.addBeans(releases);

        return result;
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided.
     */
    public Dataset<ReleasesSparkBeanSpecification> newReleasesDataset(SparkSession sparkSession) {
        return newReleasesDataset(sparkSession, new ArrayList<ReleasesSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified releases.
     */
    public Dataset<ReleasesSparkBeanSpecification> newReleasesDataset(
        SparkSession sparkSession,
        ReleasesSparkBeanSpecification... releases
    ) {
        return newReleasesDataset(sparkSession, Arrays.asList(releases));
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified releases.
     */
    public Dataset<ReleasesSparkBeanSpecification> newReleasesDataset(
        SparkSession sparkSession,
        List<ReleasesSparkBeanSpecification> releases
    ) {
        ReleasesDatasetFactory result = new ReleasesDatasetFactory(sparkSession);

        result.addBeans(releases);

        return result.getInstance();
    }

    /**
     * Method returns a new instance of {@link SeriessDatasetFactory} using the Spark session provided.
     */
    public SeriessDatasetFactory newSeriessDatasetFactory(SparkSession sparkSession) {
        return newSeriessDatasetFactory(sparkSession, new ArrayList<SeriessSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link SeriessDatasetFactory} using the Spark session provided and
     * containing the specified seriess.
     */
    public SeriessDatasetFactory newSeriessDatasetFactory(
        SparkSession sparkSession,
        SeriessSparkBeanSpecification... seriess
    ) {
        return newSeriessDatasetFactory(sparkSession, Arrays.asList(seriess));
    }

    /**
     * Method returns a new instance of {@link SeriessDatasetFactory} using the Spark session provided and
     * containing the specified seriess.
     */
    public SeriessDatasetFactory newSeriessDatasetFactory(
        SparkSession sparkSession,
        List<SeriessSparkBeanSpecification> seriess
    ) {
        SeriessDatasetFactory result = new SeriessDatasetFactory(sparkSession);

        result.addBeans(seriess);

        return result;
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided.
     */
    public Dataset<SeriessSparkBeanSpecification> newSeriessDataset(SparkSession sparkSession) {
        return newSeriessDataset(sparkSession, new ArrayList<SeriessSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified seriess.
     */
    public Dataset<SeriessSparkBeanSpecification> newSeriessDataset(
        SparkSession sparkSession,
        SeriessSparkBeanSpecification... seriess
    ) {
        return newSeriessDataset(sparkSession, Arrays.asList(seriess));
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified seriess.
     */
    public Dataset<SeriessSparkBeanSpecification> newSeriessDataset(
        SparkSession sparkSession,
        List<SeriessSparkBeanSpecification> seriess
    ) {
        SeriessDatasetFactory result = new SeriessDatasetFactory(sparkSession);

        result.addBeans(seriess);

        return result.getInstance();
    }

    /**
     * Method returns a new instance of {@link SourcesDatasetFactory} using the Spark session provided.
     */
    public SourcesDatasetFactory newSourcesDatasetFactory(SparkSession sparkSession) {
        return newSourcesDatasetFactory(sparkSession, new ArrayList<SourcesSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link SourcesDatasetFactory} using the Spark session provided and
     * containing the specified sources.
     */
    public SourcesDatasetFactory newSourcesDatasetFactory(
        SparkSession sparkSession,
        SourcesSparkBeanSpecification... sources
    ) {
        return newSourcesDatasetFactory(sparkSession, Arrays.asList(sources));
    }

    /**
     * Method returns a new instance of {@link SourcesDatasetFactory} using the Spark session provided and
     * containing the specified sources.
     */
    public SourcesDatasetFactory newSourcesDatasetFactory(
        SparkSession sparkSession,
        List<SourcesSparkBeanSpecification> sources
    ) {
        SourcesDatasetFactory result = new SourcesDatasetFactory(sparkSession);

        result.addBeans(sources);

        return result;
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided.
     */
    public Dataset<SourcesSparkBeanSpecification> newSourcesDataset(SparkSession sparkSession) {
        return newSourcesDataset(sparkSession, new ArrayList<SourcesSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified sources.
     */
    public Dataset<SourcesSparkBeanSpecification> newSourcesDataset(
        SparkSession sparkSession,
        SourcesSparkBeanSpecification... sources
    ) {
        return newSourcesDataset(sparkSession, Arrays.asList(sources));
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified sources.
     */
    public Dataset<SourcesSparkBeanSpecification> newSourcesDataset(
        SparkSession sparkSession,
        List<SourcesSparkBeanSpecification> sources
    ) {
        SourcesDatasetFactory result = new SourcesDatasetFactory(sparkSession);

        result.addBeans(sources);

        return result.getInstance();
    }

    /**
     * Method returns a new instance of {@link TagsDatasetFactory} using the Spark session provided.
     */
    public TagsDatasetFactory newTagsDatasetFactory(SparkSession sparkSession) {
        return newTagsDatasetFactory(sparkSession, new ArrayList<TagsSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link TagsDatasetFactory} using the Spark session provided and
     * containing the specified tags.
     */
    public TagsDatasetFactory newTagsDatasetFactory(SparkSession sparkSession, TagsSparkBeanSpecification... tags) {
        return newTagsDatasetFactory(sparkSession, Arrays.asList(tags));
    }

    /**
     * Method returns a new instance of {@link TagsDatasetFactory} using the Spark session provided and
     * containing the specified tags.
     */
    public TagsDatasetFactory newTagsDatasetFactory(
        SparkSession sparkSession,
        List<TagsSparkBeanSpecification> tags
    ) {
        TagsDatasetFactory result = new TagsDatasetFactory(sparkSession);

        result.addBeans(tags);

        return result;
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided.
     */
    public Dataset<TagsSparkBeanSpecification> newTagsDataset(SparkSession sparkSession) {
        return newTagsDataset(sparkSession, new ArrayList<TagsSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified tags.
     */
    public Dataset<TagsSparkBeanSpecification> newTagsDataset(
        SparkSession sparkSession,
        TagsSparkBeanSpecification... tags
    ) {
        return newTagsDataset(sparkSession, Arrays.asList(tags));
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified tags.
     */
    public Dataset<TagsSparkBeanSpecification> newTagsDataset(
        SparkSession sparkSession,
        List<TagsSparkBeanSpecification> tags
    ) {
        TagsDatasetFactory result = new TagsDatasetFactory(sparkSession);

        result.addBeans(tags);

        return result.getInstance();
    }

    /**
     * Method returns a new instance of {@link VintageDatesDatasetFactory} using the Spark session provided.
     */
    public VintageDatesDatasetFactory newVintageDatesDatasetFactory(SparkSession sparkSession) {
        return newVintageDatesDatasetFactory(sparkSession, new ArrayList<VintageDatesSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link VintageDatesDatasetFactory} using the Spark session provided and
     * containing the specified vintageDates.
     */
    public VintageDatesDatasetFactory newVintageDatesDatasetFactory(
        SparkSession sparkSession,
        VintageDatesSparkBeanSpecification... vintageDates
    ) {
        return newVintageDatesDatasetFactory(sparkSession, Arrays.asList(vintageDates));
    }

    /**
     * Method returns a new instance of {@link VintageDatesDatasetFactory} using the Spark session provided and
     * containing the specified vintageDates.
     */
    public VintageDatesDatasetFactory newVintageDatesDatasetFactory(
        SparkSession sparkSession,
        List<VintageDatesSparkBeanSpecification> vintageDates
    ) {
        VintageDatesDatasetFactory result = new VintageDatesDatasetFactory(sparkSession);

        result.addBeans(vintageDates);

        return result;
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided.
     */
    public Dataset<VintageDatesSparkBeanSpecification> newVintageDatesDataset(SparkSession sparkSession) {
        return newVintageDatesDataset(sparkSession, new ArrayList<VintageDatesSparkBeanSpecification> ());
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified vintageDates.
     */
    public Dataset<VintageDatesSparkBeanSpecification> newVintageDatesDataset(
        SparkSession sparkSession,
        VintageDatesSparkBeanSpecification... vintageDates
    ) {
        return newVintageDatesDataset(sparkSession, Arrays.asList(vintageDates));
    }

    /**
     * Method returns a new instance of {@link Dataset} using the Spark session provided and
     * containing the specified vintageDates.
     */
    public Dataset<VintageDatesSparkBeanSpecification> newVintageDatesDataset(
        SparkSession sparkSession,
        List<VintageDatesSparkBeanSpecification> vintageDates
    ) {

        VintageDatesDatasetFactory result = new VintageDatesDatasetFactory(sparkSession);

        result.addBeans(vintageDates);

        return result.getInstance();
    }

    public static FREDClientDatasetBuilder initialize () {

        SpringApplicationBuilder builder = new SpringApplicationBuilder(FREDClientDatasetBuilder.class);

        ConfigurableApplicationContext applicationContext =
            builder.web(false).headless(true).registerShutdownHook(true).run(new String[]{});

        FREDClientDatasetBuilder result = applicationContext.getBean(FREDClientDatasetBuilder.class);

        return result;
    }

    @PreDestroy
    public void stop () {
        applicationContext.stop();
        applicationContext.close();
    }

    public static void main (String[] args) {

        String master = "local[*]";

        SparkConf sparkConf = new SparkConf()
            .setAppName(FREDClientDatasetBuilder.class.getName())
            .setMaster(master);

        JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);

        SparkSession sparkSession = new SparkSession (javaSparkContext.sc());

        FREDClientDatasetBuilder builder = FREDClientDatasetBuilder.initialize();

        Categories categories = builder.newQueryBuilder().category().withCategoryId(125).doGetAsCategories();

        Dataset<Categories> categoriesDataset = builder.newCategoriesDataset(sparkSession, categories);

        String[] columns = categoriesDataset.columns();

        for (String column : columns)
            log.info("column: " + column);

        sparkSession.close();
    }
}
