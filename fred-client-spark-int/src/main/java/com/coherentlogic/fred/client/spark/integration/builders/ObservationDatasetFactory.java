package com.coherentlogic.fred.client.spark.integration.builders;

import java.util.List;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import com.coherentlogic.fred.client.core.domain.Observation;

/**
 * A Dataset builder for {@link Observation} objects.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class ObservationDatasetFactory extends AbstractDatasetFactory<Observation> {

    static final Encoder<Observation> encoder = Encoders.bean(Observation.class);

    protected ObservationDatasetFactory(
        SparkSession sparkSession, Encoder<Observation> encoder, List<Observation> beans
    ) {
        super(sparkSession, encoder, beans);
    }

    public ObservationDatasetFactory(SparkSession sparkSession, List<Observation> beans) {
        super(sparkSession, encoder, beans);
    }

    public ObservationDatasetFactory(SparkSession sparkSession) {
        super(sparkSession, encoder);
    }
}
