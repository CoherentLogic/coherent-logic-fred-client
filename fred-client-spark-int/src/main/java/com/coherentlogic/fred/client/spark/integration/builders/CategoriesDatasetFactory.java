package com.coherentlogic.fred.client.spark.integration.builders;

import java.util.List;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import com.coherentlogic.fred.client.core.domain.Categories;

/**
 * A Dataset builder for {@link Categories} objects.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class CategoriesDatasetFactory extends AbstractDatasetFactory<Categories> {

    static final Encoder<Categories> encoder = Encoders.bean(Categories.class);

    protected CategoriesDatasetFactory(SparkSession sparkSession, Encoder<Categories> encoder, List<Categories> beans) {
        super(sparkSession, encoder, beans);
    }

    public CategoriesDatasetFactory(SparkSession sparkSession, List<Categories> beans) {
        super(sparkSession, encoder, beans);
    }

    public CategoriesDatasetFactory(SparkSession sparkSession) {
        super(sparkSession, encoder);
    }

//    @Override
//    public AbstractDatasetFactory<Categories> addBeans(Categories... beans) {
//        return super.addBeans(beans);
//    }
//
//    @Override
//    public AbstractDatasetFactory<Categories> addBeans(List<Categories> beans) {
//        return super.addBeans(beans);
//    }
}
