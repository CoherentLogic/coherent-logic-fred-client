
package com.coherentlogic.fred.client.spark.integration.builders;

import java.util.List;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import com.coherentlogic.fred.client.core.domain.TagsSparkBeanSpecification;

/**
 * A Dataset builder for {@link TagsSparkBeanSpecification} objects.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class TagsDatasetFactory extends AbstractDatasetFactory<TagsSparkBeanSpecification> {

    static final Encoder<TagsSparkBeanSpecification> encoder = Encoders.bean(TagsSparkBeanSpecification.class);

    public TagsDatasetFactory(SparkSession sparkSession, List<TagsSparkBeanSpecification> beans) {
        super(sparkSession, encoder, beans);
    }

    public TagsDatasetFactory(SparkSession sparkSession) {
        super(sparkSession, encoder);
    }
}
