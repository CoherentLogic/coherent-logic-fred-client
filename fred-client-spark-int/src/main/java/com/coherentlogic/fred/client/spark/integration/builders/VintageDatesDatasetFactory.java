package com.coherentlogic.fred.client.spark.integration.builders;

import java.util.List;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import com.coherentlogic.fred.client.core.domain.VintageDatesSparkBeanSpecification;

/**
 * A Dataset builder for {@link VintageDatesSparkBeanSpecification} objects.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class VintageDatesDatasetFactory extends AbstractDatasetFactory<VintageDatesSparkBeanSpecification> {

    static final Encoder<VintageDatesSparkBeanSpecification> encoder = Encoders.bean(VintageDatesSparkBeanSpecification.class);

    public VintageDatesDatasetFactory(SparkSession sparkSession, List<VintageDatesSparkBeanSpecification> beans) {
        super(sparkSession, encoder, beans);
    }

    public VintageDatesDatasetFactory(SparkSession sparkSession) {
        super(sparkSession, encoder);
    }
}
