package com.coherentlogic.fred.client.spark.integration.builders;

import java.util.List;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import com.coherentlogic.fred.client.core.domain.SourcesSparkBeanSpecification;

/**
 * A Dataset builder for {@link SourcesSparkBeanSpecification} objects.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class SourcesDatasetFactory extends AbstractDatasetFactory<SourcesSparkBeanSpecification> {

    static final Encoder<SourcesSparkBeanSpecification> encoder = Encoders.bean(SourcesSparkBeanSpecification.class);

    public SourcesDatasetFactory(SparkSession sparkSession, List<SourcesSparkBeanSpecification> beans) {
        super(sparkSession, encoder, beans);
    }

    public SourcesDatasetFactory(SparkSession sparkSession) {
        super(sparkSession, encoder);
    }
}
