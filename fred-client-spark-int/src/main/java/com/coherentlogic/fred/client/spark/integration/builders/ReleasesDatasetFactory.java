package com.coherentlogic.fred.client.spark.integration.builders;

import java.util.List;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import com.coherentlogic.fred.client.core.domain.ReleasesSparkBeanSpecification;

/**
 * A Dataset builder for {@link ReleasesSparkBeanSpecification} objects.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class ReleasesDatasetFactory extends AbstractDatasetFactory<ReleasesSparkBeanSpecification> {

    static final Encoder<ReleasesSparkBeanSpecification> encoder = Encoders.bean(ReleasesSparkBeanSpecification.class);

    public ReleasesDatasetFactory(SparkSession sparkSession, List<ReleasesSparkBeanSpecification> beans) {
        super(sparkSession, encoder, beans);
    }

    public ReleasesDatasetFactory(SparkSession sparkSession) {
        super(sparkSession, encoder);
    }
}
