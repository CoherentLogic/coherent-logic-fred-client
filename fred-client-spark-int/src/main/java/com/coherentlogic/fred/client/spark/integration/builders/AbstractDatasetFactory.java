package com.coherentlogic.fred.client.spark.integration.builders;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.SparkSession;

import com.coherentlogic.coherent.data.adapter.core.factories.TypedFactory;

/**
 * Base class the provides methods for aggregating beans and then instantiating the {@link Dataset}.
 *
 * @param <T> The type of object the encoder encodes.
 *
 * @see <a href="https://github.com/scalapb/ScalaPB/issues/87">Incompatibility between enums and Spark SQL</a>
 * @see <a href="http://dev.bizo.com/2014/02/beware-enums-in-spark.html">Beware Java Enums in Spark</a>
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public abstract class AbstractDatasetFactory<T> implements TypedFactory<Dataset<T>> {

    private final Encoder<T> encoder;

    private final List<T> beans;

    private final SparkSession sparkSession;

    public AbstractDatasetFactory(SparkSession sparkSession, Encoder<T> encoder) {
        this (sparkSession, encoder, new ArrayList<T> ());
    }

    public AbstractDatasetFactory(SparkSession sparkSession, Encoder<T> encoder, List<T> beans) {
        this.sparkSession = sparkSession;
        this.encoder = encoder;
        this.beans = beans;
    }

    public AbstractDatasetFactory<T> addBeans (T... beans) {
        return addBeans (Arrays.asList(beans));
    }

    public AbstractDatasetFactory<T> addBeans (List<T> beans) {

        this.beans.addAll(beans);

        return this;
    }

    protected SparkSession getSparkSession() {
        return sparkSession;
    }

    protected Encoder<T> getEncoder() {
        return encoder;
    }

    protected List<T> getBeans() {
        return beans;
    }

    @Override
    public Dataset<T> getInstance() {
        return getSparkSession().createDataset(getBeans(), getEncoder());
    }

    public Dataset<T> getInstance(List<T> beans) {
        return getSparkSession().createDataset(beans, getEncoder());
    }
}
