package com.coherentlogic.fred.client.spark.integration.builders;

import java.util.List;

import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import com.coherentlogic.fred.client.core.domain.Category;

/**
 * A Dataset builder for {@link Category} objects.
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class CategoryDatasetFactory extends AbstractDatasetFactory<Category> {

    static final Encoder<Category> encoder = Encoders.bean(Category.class);

    protected CategoryDatasetFactory(SparkSession sparkSession, Encoder<Category> encoder, List<Category> beans) {
        super(sparkSession, encoder, beans);
    }

    public CategoryDatasetFactory(SparkSession sparkSession, List<Category> beans) {
        super(sparkSession, encoder, beans);
    }

    public CategoryDatasetFactory(SparkSession sparkSession) {
        super(sparkSession, encoder);
    }

//    @Override
//    public AbstractDatasetFactory<Categories> addBeans(Categories... beans) {
//        return super.addBeans(beans);
//    }
//
//    @Override
//    public AbstractDatasetFactory<Categories> addBeans(List<Categories> beans) {
//        return super.addBeans(beans);
//    }
}
