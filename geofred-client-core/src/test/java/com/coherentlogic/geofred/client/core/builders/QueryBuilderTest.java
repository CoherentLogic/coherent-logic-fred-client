package com.coherentlogic.geofred.client.core.builders;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.web.client.RestTemplate;

import com.coherentlogic.geofred.client.core.domain.SeriesData;
import com.coherentlogic.geofred.client.core.domain.SeriesGroups;
import com.coherentlogic.geofred.client.core.domain.Shapes;

public class QueryBuilderTest {

    static final String
        FRED_API_KEY = "FRED_API_KEY",
        GEOFRED_REST_TEMPLATE_ID = "geoFREDRestTemplate";

    /**
     * This value should be set in the environment properties of the operating
     * system. Make sure to restart your IDE and/or OS shell once this has been
     * set.
     */
    static final String API_KEY = System.getProperty(FRED_API_KEY);

    private final ApplicationContext context
        = new FileSystemXmlApplicationContext ("src/test/resources/spring/application-context.xml");

    private RestTemplate restTemplate = null;

    private QueryBuilder builder = null;

    @Before
    public void setUp () throws Exception {

        restTemplate = (RestTemplate) context.getBean (GEOFRED_REST_TEMPLATE_ID);

        builder = new QueryBuilder (restTemplate);
    }

    @After
    public void tearDown () throws Exception {
        restTemplate = null;
        builder = null;
    }

    @Test
    public void testGetShapes() {

        Shapes shapes = builder.shapesFile().withApiKey(API_KEY).withShapeTypeAsBEA().doGetAsShapes();

//        System.out.println("shapes: " + shapes);
    }

    @Test
    public void testGetSeriesGroups() {

        SeriesGroups result = builder
            .seriesGroup()
            .withApiKey(API_KEY)
            .withSeriesId("SMU56000000500000001a")
            .doGetAsSeriesGroups();

//        System.out.println("seriesGroups: " + result);
    }

    @Test
    public void testGetSeriesData() {

        SeriesData seriesData = builder
            .seriesData()
            .withApiKey(API_KEY)
            .withSeriesId("WIPCPI")
            .withStartDate("2012-01-01")
            .doGetAsSeriesData(
                data -> {

//                    System.out.println("data: " + data);

                    return data;
                }
            );

//        System.out.println("seriesData: " + seriesData);
    }

    /* This works:
     *
     * https://api.stlouisfed.org/geofred/regional/data?api_key=[redacted]&series_group=882&date=2013-01-01&region_type=state&units=Dollars&frequency=a&season=NSA
     * 
     * <series_data title="Per Capita Personal Income by State (Dollars)" region="state" seasonality="Not Seasonally Adjusted" units="Dollars" frequency="Annual">
     *     <observation date="2013" region="Alabama" code="01" value="35778" series_id="ALPCPI"/>
     *     <observation date="2013" region="Alaska" code="02" value="51455" series_id="AKPCPI"/>
     *     <observation date="2013" region="Arizona" code="04" value="36558" series_id="AZPCPI"/>
     *     ...
     * </series_data>
     */
    @Test
    public void testGetRegionalData() {

        SeriesData regionalData = builder
            .regionalData()
            .withApiKey(API_KEY)
            .withSeriesGroup("882")
            .withDate("2013-01-01")
            .withRegionTypeAsState()
            .withUnits("Dollars")
            .withFrequencyAsAnnually()
            .withSeasonAsNSA()
            .doGetAsSeriesData(
                    data -> {

//                        System.out.println("data: " + data);

                        return data;
                    }
                );

//        System.out.println("regionalData: " + regionalData);
    }
}
