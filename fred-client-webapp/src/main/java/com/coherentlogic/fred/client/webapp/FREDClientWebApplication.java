package com.coherentlogic.fred.client.webapp;

import java.io.File;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.coherentlogic.fred.client.core.builders.QueryBuilder;
import com.coherentlogic.fred.client.core.domain.FilterValue;
import com.coherentlogic.fred.client.core.domain.FilterVariable;
import com.coherentlogic.fred.client.core.domain.OrderBy;
import com.coherentlogic.fred.client.core.domain.SearchType;
import com.coherentlogic.fred.client.core.domain.Seriess;
import com.coherentlogic.fred.client.core.domain.SortOrder;
import com.coherentlogic.fred.client.core.factories.QueryBuilderFactory;
import com.vaadin.annotations.Title;
import com.vaadin.server.FileResource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.annotation.SpringViewDisplay;
import com.vaadin.ui.Button;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.Label;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.TextField;
//import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.vaadin.googleanalytics.tracking.GoogleAnalyticsTracker;

@SpringUI
@SpringViewDisplay
@Title("FRED Client UI")
@SpringBootApplication
/* If the database-related configuration is not excluded we will see the following exception when deploying on JBoss:
 *
 * 01:25:26,094 ERROR [org.springframework.boot.SpringApplication] (MSC service thread 1-9) Application startup failed: org.springframework.beans.factory.BeanCreationException: Error creating bean with name 'entityManagerFactory' defined in class path resource [org/springframework/boot/autoconfigure/orm/jpa/HibernateJpaAutoConfiguration.class]: Invocation of init method failed; nested exception is java.lang.NoSuchMethodError: org.jboss.logging.Logger.debugf(Ljava/lang/String;I)V
 */
@EnableAutoConfiguration//(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
@ComponentScan(basePackages="com.coherentlogic.fred.client")
public class FREDClientWebApplication extends UI {

    private static final Logger log = LoggerFactory.getLogger(FREDClientWebApplication.class);

    @Autowired
    private ApplicationContext applicationContext;

    @Override
    protected void init(VaadinRequest request) {

        log.info("init: method begins; request: " + request);

        VerticalLayout content = new VerticalLayout();
        setContent(content);

        GoogleAnalyticsTracker googleAnalyticsTracker = new GoogleAnalyticsTracker ("UA-1434183-2");

        googleAnalyticsTracker.extend(this);

        MenuBar menuBar = new MenuBar();

        MenuItem signInMenuItem = menuBar.addItem("Sign In", null, null);
        MenuItem registerMenuItem = menuBar.addItem("Register", null, null);

        String basepath = VaadinService.getCurrent().getBaseDirectory().getAbsolutePath();

        FileResource resource = new FileResource(new File(basepath + "/WEB-INF/images/logo-transparent-1260x290.png"));

        Embedded coherentLogicLogo = new Embedded("Coherent Logic Limited", resource);

        coherentLogicLogo.setWidth("126px");
        coherentLogicLogo.setHeight("29px");

        Label seriesSearchLabel = new Label ("Series Search Example");

        TextField searchText = new TextField("Search Text: (ie. 'money stock')");

        ComboBox<SearchType> searchType = new ComboBox<SearchType> ("Search Type (ie. 'fullText':");

        searchType.setItems(Arrays.asList(SearchType.fullText, SearchType.seriesId));

        DateField realtimeStart = new DateField("Realtime Start (ie. 'Jan 20 2001'):");
        DateField realtimeEnd = new DateField("Realtime End (ie. 'May 17 2004'):");

        realtimeStart.setValue(LocalDate.now());
        realtimeEnd.setValue(LocalDate.now());

        ComboBox<Integer> limit = new ComboBox<Integer> ("Limit (ie. '1000'):");

        limit.setItems(Arrays.asList(0, 50, 100, 250, 500, 1000, 5000, 10000));

        TextField offset = new TextField("Offset (ie. '1'):");

        ComboBox<OrderBy> orderBy = new ComboBox<OrderBy>("Order By: (ie. 'searchRank')");

        orderBy.setItems(
            Arrays.asList(
                OrderBy.frequency,
                OrderBy.lastUpdated,
                OrderBy.observationDate,
                OrderBy.observationEnd,
                OrderBy.observationStart,
                OrderBy.popularity,
                OrderBy.realtimeEnd,
                OrderBy.realtimeStart,
                OrderBy.releaseDate,
                OrderBy.releaseId,
                OrderBy.searchRank,
                OrderBy.seasonalAdjustment,
                OrderBy.seriesCount,
                OrderBy.seriesId,
                OrderBy.sourceId,
                OrderBy.title,
                OrderBy.units,
                OrderBy.vintageDate
            )
        );

        ComboBox<SortOrder> sortOrder = new ComboBox<SortOrder> ("Sort Order (ie. 'desc'):");

        sortOrder.setItems(Arrays.asList(SortOrder.asc, SortOrder.desc));

        ComboBox<FilterVariable> filterVariable = new ComboBox<FilterVariable> ("Filter Variable (ie. 'frequency') :");

        filterVariable.setItems(
            Arrays.asList(
                FilterVariable.frequency,
                FilterVariable.geography,
                FilterVariable.seasonalAdjustment,
                FilterVariable.units
            )
        );

        ComboBox<FilterValue> filterValue = new ComboBox<FilterValue> ("Filter Value (ie. 'all') :");

        filterValue.setItems (Arrays.asList(FilterValue.all, FilterValue.macro, FilterValue.regional));

        content.addComponent(menuBar);
        content.addComponent(coherentLogicLogo);
        content.addComponent(seriesSearchLabel);
        content.addComponent(searchText);
        content.addComponent(searchType);
        content.addComponent(realtimeStart);
        content.addComponent(realtimeEnd);
        content.addComponent(limit);
        content.addComponent(offset);
        content.addComponent(orderBy);
        content.addComponent(sortOrder);
        content.addComponent(filterVariable);
        content.addComponent(filterValue);

        content.addComponent(
            new Button("Execute query!",
                click -> {

                    log.info("execute query button clicked; click: " + click);

                    Date realtimeStartDate = Date.from(
                        realtimeStart
                            .getValue()
                            .atStartOfDay(
                                ZoneId.systemDefault()
                            ).toInstant()
                        );

                    log.info("realtimeStartDate: " + realtimeStartDate);

                    Date realtimeEndDate = Date.from(
                        realtimeEnd
                            .getValue()
                            .atStartOfDay(
                                ZoneId.systemDefault()
                            ).toInstant()
                        );

                    log.info("realtimeEndDate: " + realtimeEndDate);

                    log.info("applicationContext: " + applicationContext);

                    QueryBuilderFactory queryBuilderFactory = applicationContext.getBean(QueryBuilderFactory.class);

                    QueryBuilder builder = queryBuilderFactory.getInstance();

                    Seriess seriess = builder
                        .series()
                        .withSeriesId(searchText.getValue())
                        .withRealtimeStart(realtimeStartDate)
                        .withRealtimeEnd(realtimeEndDate)
                        .doGetAsSeriess();
//                  .search()
//                  .withSearchText(searchText.getValue())
//                  .withSearchType(searchType.getValue())
//                  .withRealtimeStart(realtimeStartDate)
//                  .withRealtimeEnd(realtimeEndDate)
//                  .withLimit(limit.getValue())
//                  .withOffset(Integer.valueOf(offset.getValue()))
//                  .withOrderBy(orderBy.getValue())
//                  .withSortOrder(sortOrder.getValue())
//                  .withFilterVariable(filterVariable.getValue())
//                  .withFilterValue(filterValue.getValue())

                    log.info("escapedURI: " + builder.getEscapedURI());

                    log.info("execute query button returns; seriess: " + seriess);

                    seriess.getSeriesList().forEach(
                        series -> {
                            log.info("series: " + series);
                        }
                    );
                }
            )
        );

        log.info("init: method ends.");
    }
}


//Seriess seriess = builder
//.series()
//.search()
//.withApiKey(API_KEY)
//.withSearchText("money stock")
//.withSearchType(SearchType.fullText)
//.withRealtimeStart(realtimeStart)
//.withRealtimeEnd(realtimeEnd)
//.withLimit(1000)
//.withOffset(1)
//.withOrderBy(OrderBy.searchRank)
//.withSortOrder(SortOrder.desc)
//.withFilterVariable(FilterVariable.frequency)
//.withFilterValue(FilterValue.all)
//.doGetAsSeriess();