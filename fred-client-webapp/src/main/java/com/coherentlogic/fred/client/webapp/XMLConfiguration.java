package com.coherentlogic.fred.client.webapp;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration // "classpath*:spring/api-key-beans.xml"
@ImportResource({"classpath*:spring/h2-jpa-beans.xml", "classpath*:spring/application-context.xml"})
public class XMLConfiguration {

}
