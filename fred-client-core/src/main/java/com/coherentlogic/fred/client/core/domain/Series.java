package com.coherentlogic.fred.client.core.domain;

import static com.coherentlogic.coherent.data.model.core.util.Constants.ID;
import static com.coherentlogic.coherent.data.model.core.util.Constants.VALUE;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.FREQUENCY_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.LAST_UPDATED_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.NOTES_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.POPULARITY_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.SEASONAL_ADJUSTMENT_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.SEASONAL_ADJUSTMENT_SHORT_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.TITLE_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.UNITS_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.UNITS_SHORT_PROPERTY;
import static com.coherentlogic.fred.client.core.util.Constants.FREQUENCY;
import static com.coherentlogic.fred.client.core.util.Constants.FREQUENCY_SHORT;
import static com.coherentlogic.fred.client.core.util.Constants.FREQUENCY_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.LAST_UPDATED;
import static com.coherentlogic.fred.client.core.util.Constants.NOTES;
import static com.coherentlogic.fred.client.core.util.Constants.OBSERVATION_END;
import static com.coherentlogic.fred.client.core.util.Constants.OBSERVATION_START;
import static com.coherentlogic.fred.client.core.util.Constants.POPULARITY;
import static com.coherentlogic.fred.client.core.util.Constants.SEASONAL_ADJUSTMENT;
import static com.coherentlogic.fred.client.core.util.Constants.SEASONAL_ADJUSTMENT_SHORT;
import static com.coherentlogic.fred.client.core.util.Constants.SERIES;
import static com.coherentlogic.fred.client.core.util.Constants.TITLE;
import static com.coherentlogic.fred.client.core.util.Constants.UNITS;
import static com.coherentlogic.fred.client.core.util.Constants.UNITS_SHORT;
import static com.coherentlogic.fred.client.core.util.Constants.UNITS_SHORT_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.UNITS_VALUE;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.IdentityValueSpecification;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.fred.client.core.converters.FrequencyEnumConverter;
import com.coherentlogic.fred.client.core.converters.PopularityConverter;
import com.coherentlogic.fred.client.core.util.Constants;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import com.thoughtworks.xstream.converters.basic.StringConverter;

/**
 * A class which represents an economic data series.
 *
 * @see <a href="https://api.stlouisfed.org/docs/fred/series.html">series</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=SERIES)
@XStreamAlias(SERIES)
@Visitable
public class Series extends SerializableBean implements
    IdentityValueSpecification<String, String>,
    RealtimeBoundSpecification,
    ObservationBoundSpecification {

    private static final long serialVersionUID = -1977820595461889063L;

    @XStreamAlias(ID)
    @XStreamAsAttribute
    @XStreamConverter(StringConverter.class)
    @Visitable
    private String id;

    @XStreamConverter(StringConverter.class)
    @Visitable
    private String value;

    @XStreamAlias(Constants.REALTIME_START)
    @XStreamAsAttribute
    @Visitable
    private Date realtimeStart = null;

    @XStreamAlias(Constants.REALTIME_END)
    @XStreamAsAttribute
    @Visitable
    private Date realtimeEnd = null;

    @XStreamAlias(TITLE)
    @XStreamAsAttribute
    @Visitable
    private String title = null;

    @XStreamAlias(OBSERVATION_START)
    @XStreamAsAttribute
    @Visitable
    private Date observationStart = null;

    @XStreamAlias(OBSERVATION_END)
    @XStreamAsAttribute
    @Visitable
    private Date observationEnd = null;

    @XStreamAlias(FREQUENCY_SHORT)
    @XStreamAsAttribute
    @XStreamConverter(FrequencyEnumConverter.class)
    @Visitable
    private Frequency frequency = null;

    @XStreamAlias(FREQUENCY)
    @XStreamAsAttribute
    @XStreamOmitField
    @Visitable
    private String frequencyLong = null;

    @XStreamAlias(UNITS)
    @XStreamAsAttribute
    @Visitable
    private String units = null;

    @XStreamAlias(UNITS_SHORT)
    @XStreamAsAttribute
    @Visitable
    private String unitsShort = null;

    @XStreamAlias(SEASONAL_ADJUSTMENT)
    @XStreamAsAttribute
    @Visitable
    private String seasonalAdjustment = null;

    @XStreamAlias(SEASONAL_ADJUSTMENT_SHORT)
    @XStreamAsAttribute
    @Visitable
    private String seasonalAdjustmentShort = null;

    @XStreamAlias(LAST_UPDATED)
    @XStreamAsAttribute
    @Visitable
    private Date lastUpdated = null;

    @XStreamAlias(POPULARITY)
    @XStreamAsAttribute
    @XStreamConverter(PopularityConverter.class)
    @Visitable
    private Integer popularity = null;

    @XStreamAlias(NOTES)
    @XStreamAsAttribute
    @Visitable
    private String notes = null;

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriesSpecification2#getRealtimeStart()
     */
    @Override
    public Date getRealtimeStart () {
        return clone (realtimeStart);
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriesSpecification2#setRealtimeStart(java.util.Date)
     */
    @Override
    public void setRealtimeStart(Date realtimeStart) {

        Date oldValue = this.realtimeStart;

        this.realtimeStart = clone (realtimeStart);

        firePropertyChange(
            REALTIME_START_PROPERTY,
            oldValue,
            realtimeStart
        );
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriesSpecification#getRealtimeEnd()
     */
    @Override
    public Date getRealtimeEnd () {
        return clone (realtimeEnd);
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriesSpecification2#setRealtimeEnd(java.util.Date)
     */
    @Override
    public void setRealtimeEnd(Date realtimeEnd) {

        Date oldValue = this.realtimeEnd;

        this.realtimeEnd = clone (realtimeEnd);

        firePropertyChange(
            REALTIME_END_PROPERTY,
            oldValue,
            realtimeEnd
        );
    }

    public String getTitle () {
        return title;
    }

    public void setTitle (String title) {

        String oldValue = this.title;

        this.title = title;

        firePropertyChange(
            TITLE_PROPERTY,
            oldValue,
            title
        );
    }

    public Date getObservationStart () {
        return clone (observationStart);
    }

    public void setObservationStart (Date observationStart) {

        Date oldValue = this.observationStart;

        this.observationStart = clone (observationStart);

        firePropertyChange(
            OBSERVATION_START_PROPERTY,
            oldValue,
            observationStart
        );
    }

    public Date getObservationEnd () {
        return clone (observationEnd);
    }

    public void setObservationEnd (Date observationEnd) {

        Date oldValue = this.observationEnd;

        this.observationEnd = clone (observationEnd);

        firePropertyChange(
            OBSERVATION_END_PROPERTY,
            oldValue,
            observationEnd
        );
    }

    @Column(name=FREQUENCY_VALUE)
    public Frequency getFrequency () {
        return frequency;
    }

    public void setFrequency (Frequency frequency) {

        Frequency oldValue = this.frequency;

        this.frequency = frequency;

        firePropertyChange(
            FREQUENCY_PROPERTY,
            oldValue,
            frequency
        );
    }

    public String getFrequencyLong() {
        return frequencyLong;
    }

    public void setFrequencyLong(String frequencyLong) {
        this.frequencyLong = frequencyLong;
    }

    @Column(name=UNITS_VALUE)
    public String getUnits () {
        return units;
    }

    public void setUnits (String units) {

        String oldValue = this.units;

        this.units = units;

        firePropertyChange(
            UNITS_PROPERTY,
            oldValue,
            units
        );
    }

    @Column(name=UNITS_SHORT_VALUE)
    public String getUnitsShort () {
        return unitsShort;
    }

    public void setUnitsShort (String unitsShort) {

        String oldValue = this.unitsShort;

        this.unitsShort = unitsShort;

        firePropertyChange(
            UNITS_SHORT_PROPERTY,
            oldValue,
            unitsShort
        );
    }

    public String getSeasonalAdjustment () {
        return seasonalAdjustment;
    }

    public void setSeasonalAdjustment (String seasonalAdjustment) {

        String oldValue = this.seasonalAdjustment;

        this.seasonalAdjustment = seasonalAdjustment;

        firePropertyChange(
            SEASONAL_ADJUSTMENT_PROPERTY,
            oldValue,
            seasonalAdjustment
        );
    }

    public String getSeasonalAdjustmentShort () {
        return seasonalAdjustmentShort;
    }

    public void setSeasonalAdjustmentShort (String seasonalAdjustmentShort) {

        String oldValue = this.seasonalAdjustmentShort;

        this.seasonalAdjustmentShort = seasonalAdjustmentShort;

        firePropertyChange(
            SEASONAL_ADJUSTMENT_SHORT_PROPERTY,
            oldValue,
            seasonalAdjustmentShort
        );
    }

    public Date getLastUpdated () {
        return clone (lastUpdated);
    }

    public void setLastUpdated (Date lastUpdated) {

        Date oldValue = this.lastUpdated;

        this.lastUpdated = clone (lastUpdated);

        firePropertyChange(
            LAST_UPDATED_PROPERTY,
            oldValue,
            lastUpdated
        );
    }

    public Integer getPopularity () {
        return popularity;
    }

    public void setPopularity (Integer popularity) {

        Integer oldValue = this.popularity;

        this.popularity = popularity;

        firePropertyChange(
            POPULARITY_PROPERTY,
            oldValue,
            popularity
        );
    }

    public String getNotes () {
        return notes;
    }

    public void setNotes (String notes) {

        String oldValue = this.notes;

        this.notes = notes;

        firePropertyChange(
            NOTES_PROPERTY,
            oldValue,
            notes
        );
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {

        String oldValue = this.id;

        this.id = id;

        firePropertyChange(ID, oldValue, id);
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriesSpecification2#getValue()
     */
    @Override
    public String getValue() {
        return value;
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriesSpecification2#setValue(java.lang.String)
     */
    @Override
    public void setValue(String value) {

        String oldValue = this.value;

        this.value = value;

        firePropertyChange(VALUE, oldValue, value);
    }
}
