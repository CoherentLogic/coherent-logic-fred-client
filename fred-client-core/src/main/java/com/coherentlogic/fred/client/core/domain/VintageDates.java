package com.coherentlogic.fred.client.core.domain;

import static com.coherentlogic.fred.client.core.domain.PropertyNames.VINTAGE_DATE_LIST_PROPERTY;
import static com.coherentlogic.fred.client.core.util.Constants.COUNT_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.DEFAULT_LIMIT;
import static com.coherentlogic.fred.client.core.util.Constants.LIMIT_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.ORDER_BY_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.VINTAGE_DATES;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.fred.client.core.util.Constants;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * A class which represents the dates in history when a series' data values were
 * revised or new data values were released.
 *
 * @see <a href="https://api.stlouisfed.org/docs/fred/series_vintagedates.html">
 * series_vintagedates</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=VINTAGE_DATES)
@XStreamAlias(VINTAGE_DATES)
@Visitable
public class VintageDates extends SerializableBean
    implements
        RealtimeBoundSpecification,
        PaginationSpecification,
        OrderBySpecification,
        SortOrderSpecification {

    private static final long serialVersionUID = 6819386088595842198L;

    static final String
        COUNT = "count",
        VINTAGE_DATE_LIST = "vintageDateList";

    @XStreamAlias(Constants.REALTIME_START)
    @XStreamAsAttribute
    @Visitable
    private Date realtimeStart = null;

    @XStreamAlias(Constants.REALTIME_END)
    @XStreamAsAttribute
    @Visitable
    private Date realtimeEnd = null;

    @XStreamAlias(Constants.SORT_ORDER)
    @XStreamAsAttribute
    @Visitable
    private SortOrder sortOrder = SortOrder.asc;

    @XStreamAlias(Constants.ORDER_BY)
    @XStreamAsAttribute
    @Visitable
    private OrderBy orderBy = null;

    @XStreamAlias(Constants.LIMIT)
    @XStreamAsAttribute
    @Visitable
    private long limit = DEFAULT_LIMIT;

    @XStreamAlias(Constants.OFFSET)
    @XStreamAsAttribute
    @Visitable
    private int offset = 0;

    @XStreamAlias(COUNT)
    @XStreamAsAttribute
    @Visitable
    private Integer count = 0;

    @XStreamImplicit
    @Visitable
    private List<VintageDate> vintageDateList = null;

    @OneToMany(cascade=CascadeType.ALL)
    public List<VintageDate> getVintageDateList() {
        return vintageDateList;
    }

    public void setVintageDateList(List<VintageDate> vintageDateList) {

        List<VintageDate> oldValue = this.vintageDateList;

        this.vintageDateList = vintageDateList;

        firePropertyChange(
            VINTAGE_DATE_LIST_PROPERTY,
            oldValue,
            vintageDateList
        );
    }

    @Override
    public void setSortOrder(SortOrder sortOrder) {

        SortOrder oldValue = this.sortOrder;

        this.sortOrder = sortOrder;

        firePropertyChange(
            SORT_ORDER_PROPERTY,
            oldValue,
            sortOrder
        );
    }

    @Override
    public SortOrder getSortOrder() {
        return sortOrder;
    }

    @Column(name=ORDER_BY_VALUE)
    @Override
    public OrderBy getOrderBy() {
        return orderBy;
    }

    @Override
    public void setOrderBy(OrderBy orderBy) {
        OrderBy oldValue = this.orderBy;

        this.orderBy = orderBy;

        firePropertyChange(
            ORDER_BY_PROPERTY,
            oldValue,
            orderBy
        );
    }

    @Override
    public void setLimit(long limit) {

        long oldValue = this.limit;

        this.limit = limit;

        firePropertyChange(LIMIT_PROPERTY, oldValue, limit);
    }

    @Override
    public void setOffset(int offset) {

        int oldValue = this.offset;

        this.offset = offset;

        firePropertyChange(OFFSET_PROPERTY, oldValue, offset);
    }

    @Column(name=LIMIT_VALUE)
    @Override
    public long getLimit() {
        return limit;
    }

    @Column(name = Constants.OFFSET_COLUMN) // offset restricted in H2.
    @Override
    public int getOffset() {
        return offset;
    }

    /**
     * @see com.coherentlogic.fred.client.core.domain.RealtimeBoundSpecification#setRealtimeStart(Date)
     */
    @Override
    public void setRealtimeStart(Date realtimeStart) {

        Date oldValue = this.realtimeStart;

        this.realtimeStart = clone (realtimeStart);

        firePropertyChange(
            REALTIME_START_PROPERTY,
            oldValue,
            realtimeStart
        );
    }

    /**
     * @see com.coherentlogic.fred.client.core.domain.RealtimeBoundSpecification#setRealtimeEnd(Date)
     */
    @Override
    public void setRealtimeEnd(Date realtimeEnd) {

        Date oldValue = this.realtimeEnd;

        this.realtimeEnd = clone (realtimeEnd);

        firePropertyChange(
            REALTIME_END_PROPERTY,
            oldValue,
            realtimeEnd
        );
    }

    @Override
    public Date getRealtimeStart() {
        return clone (realtimeStart);
    }

    @Override
    public Date getRealtimeEnd() {
        return clone (realtimeEnd);
    }

    @Column(name=COUNT_VALUE)
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {

        Integer oldValue = this.count;

        this.count = count;

        firePropertyChange(COUNT, oldValue, count);
    }

    /**
     * @see {@link SerializableBean#accept(Collection)
     */
    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        vintageDateList.forEach(
            (VintageDate entry) -> {
                visitors.forEach(visitor -> {visitor.accept(entry);});
            }
        );
    }
}
