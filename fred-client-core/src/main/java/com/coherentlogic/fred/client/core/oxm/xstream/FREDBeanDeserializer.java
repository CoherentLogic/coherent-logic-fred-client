package com.coherentlogic.fred.client.core.oxm.xstream;

import org.springframework.oxm.xstream.XStreamMarshaller;

import com.coherentlogic.coherent.data.adapter.core.xstream.BeanDeserializer;
import com.thoughtworks.xstream.XStream;

public class FREDBeanDeserializer extends BeanDeserializer {

    public FREDBeanDeserializer (XStreamMarshaller xStreamMarshaller) {
        this (xStreamMarshaller.getXStream());
    }

    public FREDBeanDeserializer(XStream xstream) {
        super(xstream);
    }
}