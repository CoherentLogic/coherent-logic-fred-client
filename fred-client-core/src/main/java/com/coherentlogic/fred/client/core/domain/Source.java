package com.coherentlogic.fred.client.core.domain;

import static com.coherentlogic.coherent.data.model.core.util.Constants.ID;
import static com.coherentlogic.coherent.data.model.core.util.Constants.VALUE;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.LINK_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.NAME_PROPERTY;
import static com.coherentlogic.fred.client.core.util.Constants.LINK;
import static com.coherentlogic.fred.client.core.util.Constants.NAME;
import static com.coherentlogic.fred.client.core.util.Constants.SOURCE;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.IdentityValueSpecification;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.fred.client.core.util.Constants;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.basic.StringConverter;

/**
 * A class which represents a source of economic data.
 *
 * @see <a href="https://api.stlouisfed.org/docs/fred/release_sources.html">
 * release_sources</a>
 * @see <a href="https://api.stlouisfed.org/docs/fred/sources.html">
 * sources</a>
 * @see <a href="https://api.stlouisfed.org/docs/fred/source.html">
 * source</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=SOURCE)
@XStreamAlias(SOURCE)
@Visitable
public class Source extends SerializableBean implements
    IdentityValueSpecification<String, String>,
    RealtimeBoundSpecification {

    private static final long serialVersionUID = 5030690758234873018L;

    @XStreamAlias(ID)
    @XStreamAsAttribute
    @XStreamConverter(StringConverter.class)
    @Visitable
    private String id;

    @XStreamConverter(StringConverter.class)
    @Visitable
    private String value;

    /**
     * For example "2008-07-29".
     */
    @XStreamAlias(Constants.REALTIME_START)
    @XStreamAsAttribute
    @Visitable
    private Date realtimeStart = null;

    /**
     * For example "2008-07-29".
     */
    @XStreamAlias(Constants.REALTIME_END)
    @XStreamAsAttribute
    @Visitable
    private Date realtimeEnd = null;

    /**
     * For example "Real Gross National Product".
     */
    @XStreamAlias(NAME)
    @XStreamAsAttribute
    @Visitable
    private String name = null;

    @XStreamAlias(LINK)
    @XStreamAsAttribute
    @Visitable
    private String link = null;

    /**
     * @see com.coherentlogic.fred.client.core.domain.RealtimeBoundSpecification#getRealtimeStart()
     */
    @Override
    public Date getRealtimeStart () {
        return clone (realtimeStart);
    }

    /**
     * @see com.coherentlogic.fred.client.core.domain.RealtimeBoundSpecification#setRealtimeStart(Date)
     */
    @Override
    public void setRealtimeStart(Date realtimeStart) {

        Date oldValue = this.realtimeStart;

        this.realtimeStart = clone (realtimeStart);

        firePropertyChange(
            REALTIME_START_PROPERTY,
            oldValue,
            realtimeStart
        );
    }

    /**
     * @see com.coherentlogic.fred.client.core.domain.RealtimeBoundSpecification#getRealtimeEnd()
     */
    @Override
    public Date getRealtimeEnd () {
        return clone (realtimeEnd);
    }

    /**
     * @see com.coherentlogic.fred.client.core.domain.RealtimeBoundSpecification#setRealtimeEnd(Date)
     */
    @Override
    public void setRealtimeEnd(Date realtimeEnd) {

        Date oldValue = this.realtimeEnd;

        this.realtimeEnd = clone (realtimeEnd);

        firePropertyChange(
            REALTIME_END_PROPERTY,
            oldValue,
            realtimeEnd
        );
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {

        String oldValue = this.name;

        this.name = name;

        firePropertyChange(
            NAME_PROPERTY,
            oldValue,
            name
        );
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {

        String oldValue = this.link;

        this.link = link;

        firePropertyChange(
            LINK_PROPERTY,
            oldValue,
            link
        );
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {

        String oldValue = this.id;

        this.id = id;

        firePropertyChange(ID, oldValue, id);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {

        String oldValue = this.value;

        this.value = value;

        firePropertyChange(VALUE, oldValue, value);
    }
}
