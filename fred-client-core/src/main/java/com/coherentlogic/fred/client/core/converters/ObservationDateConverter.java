package com.coherentlogic.fred.client.core.converters;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

import com.thoughtworks.xstream.converters.basic.DateConverter;

/**
 * The observation value can be "." and hence we need to treat this conversion
 * as a special case, since the BigDecimalConverter will throw an exception if
 * that string is passed to it.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class ObservationDateConverter extends DateConverter {

    private static final DateTimeFormatter FORMATTER
        = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            .withLocale(Locale.ROOT);
//            .withZone(ZoneId.of("UTC"));

    @Override
    public Object fromString(String value) {

        LocalDate temp = LocalDate.parse(value, FORMATTER);

        return Date.from(temp.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }
}
