package com.coherentlogic.fred.client.core.domain;

import static com.coherentlogic.coherent.data.model.core.util.Constants.ID;
import static com.coherentlogic.coherent.data.model.core.util.Constants.VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.CATEGORIES;

import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.IdentityValueSpecification;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.converters.basic.StringConverter;

/**
 * A domain class which represents <a href="https://research.stlouisfed.org/fred2/categories">categories</a> of
 * economic data.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=CATEGORIES)
@XStreamAlias(CATEGORIES)
@Visitable
public class Categories extends SerializableBean
    implements IdentityValueSpecification<String, String> {

    private static final long serialVersionUID = -8593888059697867997L;

    static final String CATEGORY_LIST_PROPERTY = "categoryList";

    @XStreamAlias(ID)
    @XStreamAsAttribute
    @XStreamConverter(StringConverter.class)
    @Visitable
    private String id;

    @XStreamConverter(StringConverter.class)
    @Visitable
    private String value;

    @XStreamImplicit
    @Visitable
    private List<Category> categoryList = null;

    /**
     * Getter method for the list of {@link Category} objects.
     */
    @OneToMany(cascade=CascadeType.ALL)
    public List<Category> getCategoryList () {
        return categoryList;
    }

    /**
     * Setter method for the list of {@link Category} objects.
     */
    public void setCategoryList (List<Category> categoryList) {

        List<Category> oldValue = this.categoryList;

        this.categoryList = categoryList;

        firePropertyChange(CATEGORY_LIST_PROPERTY, oldValue, categoryList);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {

        String oldValue = this.id;

        this.id = id;

        firePropertyChange(ID, oldValue, id);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {

        String oldValue = this.value;

        this.value = value;

        firePropertyChange(VALUE, oldValue, value);
    }

    /**
     * @see {@link SerializableBean#accept(Collection)
     */
    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(
            (serializableBean, visitorCollection) -> {

                super.accept(visitors);

                categoryList.forEach(
                    (Category entry) -> {
                        visitors.forEach(visitor -> {visitor.accept(entry);});
                    }
                );
            }
        );
    }
}
