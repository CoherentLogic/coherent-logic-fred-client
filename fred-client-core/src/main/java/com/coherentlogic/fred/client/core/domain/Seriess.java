package com.coherentlogic.fred.client.core.domain;

import static com.coherentlogic.fred.client.Constants.FILE_TYPE;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.COUNT_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.FILE_TYPE_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.OUTPUT_TYPE_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.SERIES_LIST_PROPERTY;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.UNITS_PROPERTY;
import static com.coherentlogic.fred.client.core.util.Constants.COUNT;
import static com.coherentlogic.fred.client.core.util.Constants.DEFAULT_LIMIT;
import static com.coherentlogic.fred.client.core.util.Constants.FILTER_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.FILTER_VARIABLE;
import static com.coherentlogic.fred.client.core.util.Constants.LIMIT_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.ORDER_BY_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.OUTPUT_TYPE;
import static com.coherentlogic.fred.client.core.util.Constants.SERIESS;
import static com.coherentlogic.fred.client.core.util.Constants.UNITS;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.fred.client.core.util.Constants;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * A class which represents an economic data series.
 *
 * @see <a href="https://api.stlouisfed.org/docs/fred/category_series.html">
 * category_series</a>
 * @see <a href="https://api.stlouisfed.org/docs/fred/release_series.html">
 * release_series</a>
 * @see <a href="https://api.stlouisfed.org/docs/fred/series.html">series</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=SERIESS)
@XStreamAlias(SERIESS)
@Visitable
public class Seriess extends SerializableBean
    implements RealtimeBoundSpecification,
        FilterSpecification,
        PaginationSpecification,
        OrderBySpecification,
        SortOrderSpecification {

    private static final long serialVersionUID = 4924681115628356708L;

    @XStreamAlias(Constants.REALTIME_START)
    @XStreamAsAttribute
    @Visitable
    private Date realtimeStart = null;

    @XStreamAlias(Constants.REALTIME_END)
    @XStreamAsAttribute
    @Visitable
    private Date realtimeEnd = null;

    @XStreamAlias(Constants.SORT_ORDER)
    @XStreamAsAttribute
    @Visitable
    private SortOrder sortOrder = SortOrder.asc;

    @XStreamAlias(Constants.ORDER_BY)
    @XStreamAsAttribute
    @Visitable
    private OrderBy orderBy = OrderBy.seriesId;

    @XStreamAlias(Constants.LIMIT)
    @XStreamAsAttribute
    @Visitable
    private long limit = DEFAULT_LIMIT;

    @XStreamAlias(Constants.OFFSET)
    @XStreamAsAttribute
    @Visitable
    private int offset = 0;

    @XStreamAlias(UNITS)
    @XStreamAsAttribute
    @Visitable
    private Unit units = null;

    @XStreamAlias(OUTPUT_TYPE)
    @XStreamAsAttribute
    @Visitable
    private OutputType outputType = null;

    @XStreamAlias(FILE_TYPE)
    @XStreamAsAttribute
    @Visitable
    private FileType fileType = null;

    @XStreamAlias(COUNT)
    @XStreamAsAttribute
    @Visitable
    private int count = 0;

    @XStreamAlias(FILTER_VARIABLE)
    @XStreamAsAttribute
    @Visitable
    private FilterVariable filterVariable = null;

    @XStreamAlias(FILTER_VALUE)
    @XStreamAsAttribute
    @Visitable
    private FilterValue filterValue = null;

    @XStreamImplicit
    @Visitable
    private List<Series> seriesList = null;

    @OneToMany(targetEntity=Series.class, cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    public List<Series> getSeriesList () {
        return seriesList;
    }

    public void setSeriesList (List<Series> seriesList) {

        List<Series> oldValue = this.seriesList;

        this.seriesList = seriesList;

        firePropertyChange(
            SERIES_LIST_PROPERTY,
            oldValue,
            seriesList
        );
    }

    @Override
    public void setRealtimeStart(Date realtimeStart) {

        Date oldValue = this.realtimeStart;

        this.realtimeStart = clone (realtimeStart);

        firePropertyChange(
            REALTIME_START_PROPERTY,
            oldValue,
            realtimeStart
        );
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriessSpecification2#setRealtimeEnd(java.util.Date)
     */
    @Override
    public void setRealtimeEnd(Date realtimeEnd) {

        Date oldValue = this.realtimeEnd;

        this.realtimeEnd = clone (realtimeEnd);

        firePropertyChange(
            REALTIME_END_PROPERTY,
            oldValue,
            realtimeEnd
        );
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriessSpecification#getRealtimeStart()
     */
    @Override
    public Date getRealtimeStart() {
        return clone (realtimeStart);
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriessSpecification#getRealtimeEnd()
     */
    @Override
    public Date getRealtimeEnd() {
        return clone (realtimeEnd);
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriessSpecification2#setSortOrder(com.coherentlogic.fred.client.core.domain.SortOrder)
     */
    @Override
    public void setSortOrder(SortOrder sortOrder) {

        SortOrder oldValue = this.sortOrder;

        this.sortOrder = sortOrder;

        firePropertyChange(
            SORT_ORDER_PROPERTY,
            oldValue,
            sortOrder
        );
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriessSpecification#getSortOrder()
     */
    @Override
    public SortOrder getSortOrder() {
        return sortOrder;
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriessSpecification#getOrderBy()
     */
    @Override
    @Column(name=ORDER_BY_VALUE)
    public OrderBy getOrderBy() {
        return orderBy;
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriessSpecification2#setOrderBy(com.coherentlogic.fred.client.core.domain.OrderBy)
     */
    @Override
    public void setOrderBy(OrderBy orderBy) {

        OrderBy oldValue = this.orderBy;

        this.orderBy = orderBy;

        firePropertyChange(
            ORDER_BY_PROPERTY,
            oldValue,
            orderBy
        );
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriessSpecification2#setLimit(long)
     */
    @Override
    public void setLimit(long limit) {

        long oldValue = this.limit;

        this.limit = limit;

        firePropertyChange(
            LIMIT_PROPERTY,
            oldValue,
            limit
        );
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriessSpecification2#setOffset(int)
     */
    @Override
    public void setOffset(int offset) {
        int oldValue = this.offset;

        this.offset = offset;

        firePropertyChange(
            OFFSET_PROPERTY,
            oldValue,
            offset
        );
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriessSpecification#getLimit()
     */
    @Override
    @Column(name=LIMIT_VALUE)
    public long getLimit() {
        return limit;
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriessSpecification#getOffset()
     */
    @Override
    @Column(name = Constants.OFFSET_COLUMN) // offset restricted in H2.
    public int getOffset() {
        return offset;
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.SeriessSpecification#getFilterVariable()
     */
    @Override
    public FilterVariable getFilterVariable() {
        return filterVariable;
    }

    public void setFilterVariable(FilterVariable filterVariable) {

        FilterVariable oldValue = this.filterVariable;

        this.filterVariable = filterVariable;

        firePropertyChange(
            FILTER_VARIABLE_PROPERTY,
            oldValue,
            filterVariable
        );
    }

    public FilterValue getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(FilterValue filterValue) {

        FilterValue oldValue = this.filterValue;

        this.filterValue = filterValue;

        firePropertyChange(
            FILTER_VALUE_PROPERTY,
            oldValue,
            filterValue
        );
    }

    public Unit getUnits() {
        return units;
    }

    public void setUnits(Unit units) {

        Unit oldValue = this.units;

        this.units = units;

        firePropertyChange(
            UNITS_PROPERTY,
            oldValue,
            units
        );
    }

    public OutputType getOutputType() {
        return outputType;
    }

    public void setOutputType(OutputType outputType) {

        OutputType oldValue = this.outputType;

        this.outputType = outputType;

        firePropertyChange(
            OUTPUT_TYPE_PROPERTY,
            oldValue,
            outputType
        );
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {

        FileType oldValue = this.fileType;

        this.fileType = fileType;

        firePropertyChange(
            FILE_TYPE_PROPERTY,
            oldValue,
            fileType
        );
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {

        Integer oldValue = this.count;

        this.count = count;

        firePropertyChange(COUNT_PROPERTY, oldValue, count);
    }

    /**
     * @see {@link SerializableBean#accept(Collection)
     */
    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        seriesList.forEach(
            (Series entry) -> {
                visitors.forEach(visitor -> {visitor.accept((Series) entry);});
            }
        );
    }
}
