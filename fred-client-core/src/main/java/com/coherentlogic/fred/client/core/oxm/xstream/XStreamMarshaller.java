package com.coherentlogic.fred.client.core.oxm.xstream;

import com.coherentlogic.fred.client.core.domain.AggregationMethod;
import com.coherentlogic.fred.client.core.domain.Categories;
import com.coherentlogic.fred.client.core.domain.Category;
import com.coherentlogic.fred.client.core.domain.CountableSpecification;
import com.coherentlogic.fred.client.core.domain.FileType;
import com.coherentlogic.fred.client.core.domain.FilterSpecification;
import com.coherentlogic.fred.client.core.domain.FilterValue;
import com.coherentlogic.fred.client.core.domain.FilterVariable;
import com.coherentlogic.fred.client.core.domain.Frequency;
import com.coherentlogic.fred.client.core.domain.Message;
import com.coherentlogic.fred.client.core.domain.Observation;
import com.coherentlogic.fred.client.core.domain.ObservationBoundSpecification;
import com.coherentlogic.fred.client.core.domain.Observations;
import com.coherentlogic.fred.client.core.domain.OrderBy;
import com.coherentlogic.fred.client.core.domain.OrderBySpecification;
import com.coherentlogic.fred.client.core.domain.OutputType;
import com.coherentlogic.fred.client.core.domain.PaginationSpecification;
import com.coherentlogic.fred.client.core.domain.PropertyNames;
import com.coherentlogic.fred.client.core.domain.RealtimeBoundSpecification;
import com.coherentlogic.fred.client.core.domain.Release;
import com.coherentlogic.fred.client.core.domain.ReleaseDate;
import com.coherentlogic.fred.client.core.domain.ReleaseDates;
import com.coherentlogic.fred.client.core.domain.Releases;
import com.coherentlogic.fred.client.core.domain.SearchType;
import com.coherentlogic.fred.client.core.domain.Series;
import com.coherentlogic.fred.client.core.domain.Seriess;
import com.coherentlogic.fred.client.core.domain.SortOrder;
import com.coherentlogic.fred.client.core.domain.SortOrderSpecification;
import com.coherentlogic.fred.client.core.domain.Source;
import com.coherentlogic.fred.client.core.domain.Sources;
import com.coherentlogic.fred.client.core.domain.Tag;
import com.coherentlogic.fred.client.core.domain.TagGroupId;
import com.coherentlogic.fred.client.core.domain.Tags;
import com.coherentlogic.fred.client.core.domain.Unit;
import com.coherentlogic.fred.client.core.domain.VintageDate;
import com.coherentlogic.fred.client.core.domain.VintageDates;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.security.NoTypePermission;

/**
 * 
 */
public class XStreamMarshaller extends org.springframework.oxm.xstream.XStreamMarshaller {

	/**
     * @see https://groups.google.com/forum/#!topic/xstream-user/wiKfdJPL8aY
     * 
     * @throws com.thoughtworks.xstream.security.ForbiddenClassException
     */
    @Override
    protected void customizeXStream(XStream xstream) {

        super.customizeXStream(xstream);

        xstream.addPermission(NoTypePermission.NONE);

        xstream.allowTypes(
            new Class[] {
                Categories.class,
                Category.class,
                CountableSpecification.class,
                FileType.class,
                FilterSpecification.class,
                FilterValue.class,
                FilterVariable.class,
                Message.class,
                Observation.class,
                ObservationBoundSpecification.class,
                Observations.class,
                OrderBy.class,
                OrderBySpecification.class,
                OutputType.class,
                PaginationSpecification.class,
                PropertyNames.class,
                RealtimeBoundSpecification.class,
                Release.class,
                ReleaseDate.class,
                ReleaseDates.class,
                Releases.class,
                SearchType.class,
                Series.class,
                Seriess.class,
                SortOrder.class,
                SortOrderSpecification.class,
                Source.class,
                Sources.class,
                Tag.class,
                TagGroupId.class,
                Tags.class,
                Unit.class,
                VintageDate.class,
                VintageDates.class,
                AggregationMethod.class,
                FileType.class,
                Frequency.class
            }
        );
    }
}
