package com.coherentlogic.fred.client.core.domain;

import static com.coherentlogic.coherent.data.model.core.util.Constants.ID;
import static com.coherentlogic.coherent.data.model.core.util.Constants.VALUE;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.NAME_PROPERTY;
import static com.coherentlogic.fred.client.core.util.Constants.CATEGORY;
import static com.coherentlogic.fred.client.core.util.Constants.NAME;
import static com.coherentlogic.fred.client.core.util.Constants.PARENT_ID;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.IdentityValueSpecification;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.converters.basic.StringConverter;

/**
 * A domain class which represents a category of economic data.
 *
 * Note that instances of this class will not have an id provided by the FRED web services, however this class
 * implements {@link com.coherentlogic.coherent.data.model.core.domain.IdentityValueSpecification} because we will need
 * a primary key when saving instances of this class to the database.
 *
 * @see <a href="https://api.stlouisfed.org/docs/fred/category.html">category</a>
 * @see <a href="https://api.stlouisfed.org/docs/fred/category_children.html">category_children</a>
 * @see <a href="https://api.stlouisfed.org/docs/fred/category_related.html">category_related</a>
 * @see <a href="https://api.stlouisfed.org/docs/fred/series_categories.html">series_categories</a>
 * @see <a href="https://research.stlouisfed.org/fred2/categories/">categories</a>
 * @see <a href="https://research.stlouisfed.org/">research.stlouisfed.org</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=CATEGORY)
@XStreamAlias(CATEGORY)
@Visitable
public class Category extends SerializableBean implements IdentityValueSpecification<String, String> {

    private static final long serialVersionUID = 2622455224381507777L;

    static final String PARENT_ID_PROPERTY = "parentId";

    @XStreamAlias(ID)
    @XStreamAsAttribute
    @XStreamConverter(StringConverter.class)
    @Visitable
    private String id;

    @XStreamConverter(StringConverter.class)
    @Visitable
    private String value;

    @XStreamAlias(NAME)
    @XStreamAsAttribute
    @Visitable
    private String name = null;

    @XStreamAlias(PARENT_ID)
    @XStreamAsAttribute
    @Visitable
    private String parentId = null;

    /**
     * Getter method for the name property.
     */
    public String getName () {
        return name;
    }

    /**
     * Setter method for the name property.
     */
    public void setName (String name) {

        String oldValue = this.name;

        this.name = name;

        firePropertyChange(NAME_PROPERTY, oldValue, name);
    }

    /**
     * Getter method for the parent id property.
     */
    public String getParentId () {
        return parentId;
    }

    /**
     * Setter method for the parent id property.
     */
    public void setParentId (String parentId) {

        String oldValue = this.parentId;

        this.parentId = parentId;

        firePropertyChange(PARENT_ID_PROPERTY, oldValue, parentId);
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {

        String oldValue = this.id;

        this.id = id;

        firePropertyChange(ID, oldValue, id);
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {

        String oldValue = this.value;

        this.value = value;

        firePropertyChange(VALUE, oldValue, value);
    }
}
