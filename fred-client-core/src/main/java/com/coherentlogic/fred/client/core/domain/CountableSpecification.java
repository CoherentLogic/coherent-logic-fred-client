package com.coherentlogic.fred.client.core.domain;

/**
 * Specification for (mainly) Spark beans. This is due to the following: for count, in particular (albeit not limited
 * to unfortunately), if the setter method is not also included an exception will be thrown when the Encoder.bean method
 * is invoked that looks like:
 *
 * "Cannot infer type for class [target bean classname] because it is not bean-compliant"
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 *
 * @param <T>
 */
public interface CountableSpecification<T> {

    T getCount();

    void setCount(T count);
}
