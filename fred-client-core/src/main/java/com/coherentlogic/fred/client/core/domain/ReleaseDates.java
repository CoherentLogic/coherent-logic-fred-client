package com.coherentlogic.fred.client.core.domain;

import static com.coherentlogic.coherent.data.model.core.util.Constants.ID;
import static com.coherentlogic.coherent.data.model.core.util.Constants.VALUE;
import static com.coherentlogic.fred.client.core.domain.PropertyNames.COUNT_PROPERTY;
import static com.coherentlogic.fred.client.core.util.Constants.COUNT;
import static com.coherentlogic.fred.client.core.util.Constants.COUNT_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.DEFAULT_LIMIT;
import static com.coherentlogic.fred.client.core.util.Constants.LIMIT_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.ORDER_BY_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.RELEASE_DATES;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.IdentityValueSpecification;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.fred.client.core.util.Constants;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.converters.basic.StringConverter;

/**
 * A class which represents the release dates for all releases of economic data.
 *
 * @see <a href="https://api.stlouisfed.org/docs/fred/releases_dates.html">releases_dates</a>
 * @see <a href="https://api.stlouisfed.org/docs/fred/release_dates.html">release_dates</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=RELEASE_DATES)
@XStreamAlias(RELEASE_DATES)
@Visitable
public class ReleaseDates extends SerializableBean implements
    IdentityValueSpecification<String, String>,
    RealtimeBoundSpecification,
    PaginationSpecification,
    OrderBySpecification,
    SortOrderSpecification {

    private static final long serialVersionUID = 2905630043088967146L;

    static final String RELEASE_DATE_LIST_PROPERTY = "releaseDateList";

    @XStreamAlias(ID)
    @XStreamAsAttribute
    @XStreamConverter(StringConverter.class)
    @Visitable
    private String id;

    @XStreamConverter(StringConverter.class)
    @Visitable
    private String value;

    @XStreamImplicit
    @Visitable
    private List<ReleaseDate> releaseDateList = null;

    @XStreamAlias(Constants.REALTIME_START)
    @XStreamAsAttribute
    @Visitable
    private Date realtimeStartDate = null;

    @XStreamAlias(Constants.REALTIME_END)
    @XStreamAsAttribute
    @Visitable
    private Date realtimeEndDate = null;

    @XStreamAlias(Constants.SORT_ORDER)
    @XStreamAsAttribute
    @Visitable
    private SortOrder sortOrder = SortOrder.asc;

    @XStreamAlias(Constants.ORDER_BY)
    @XStreamAsAttribute
    @Visitable
    private OrderBy orderBy = null;

    @XStreamAlias(Constants.LIMIT)
    @XStreamAsAttribute
    @Visitable
    private long limit = DEFAULT_LIMIT;

    @XStreamAlias(Constants.OFFSET)
    @XStreamAsAttribute
    @Visitable
    private int offset = 0;

    @XStreamAlias(COUNT)
    @XStreamAsAttribute
    @Visitable
    private Integer count = 0;

    /**
     * Setter method for the sort order.
     */
    @Override
    public void setSortOrder(SortOrder sortOrder) {

        SortOrder oldValue = this.sortOrder;

        this.sortOrder = sortOrder;

        firePropertyChange(
            SORT_ORDER_PROPERTY, oldValue, sortOrder);
    }

    /**
     * Getter method for the sort order.
     */
    @Override
    public SortOrder getSortOrder() {
        return sortOrder;
    }

    /**
     * Getter method for the order-by value.
     */
    @Column(name=ORDER_BY_VALUE)
    @Override
    public OrderBy getOrderBy() {
        return orderBy;
    }

    /**
     * Setter method for the  order-by value.
     */
    @Override
    public void setOrderBy(OrderBy orderBy) {

        OrderBy oldValue = this.orderBy;

        this.orderBy = orderBy;

        firePropertyChange(ORDER_BY_PROPERTY, oldValue, orderBy);
    }

    /**
     * @see com.coherentlogic.fred.client.core.domain.PaginationSpecification#setLimit(long)
     */
    @Override
    public void setLimit(long limit) {

        long oldValue = this.limit;

        this.limit = limit;

        firePropertyChange(LIMIT_PROPERTY, oldValue, limit);
    }

    @Override
    public void setOffset(int offset) {

        int oldValue = this.offset;

        this.offset = offset;

        firePropertyChange(OFFSET_PROPERTY, oldValue, offset);
    }


    @Column(name=LIMIT_VALUE)
    @Override
    public long getLimit() {
        return limit;
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.ReleaseDatesSparkBeanSpecification#getOffset()
     */
    @Override
    @Column(name = Constants.OFFSET_COLUMN) // offset restricted in H2.
    public int getOffset() {
        return offset;
    }

    @OneToMany(cascade=CascadeType.ALL)
    public List<ReleaseDate> getReleaseDateList() {
        return releaseDateList;
    }

    /**
     * Setter method for the list of {@link ReleaseDate} objects.
     */
    public void setReleaseDateList(List<ReleaseDate> releaseDateList) {

        List<ReleaseDate> oldValue = this.releaseDateList;

        this.releaseDateList = releaseDateList;

        firePropertyChange(RELEASE_DATE_LIST_PROPERTY, oldValue, releaseDateList);
    }

    @Column(name=COUNT_VALUE)
    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {

        Integer oldValue = this.count;

        this.count = count;

        firePropertyChange(COUNT_PROPERTY, oldValue, count);
    }

    /**
     * @see com.coherentlogic.fred.client.core.domain.RealtimeBoundSpecification#setRealtimeStart(Date)
     */
    @Override
    public void setRealtimeStart(Date realtimeStartDate) {

        Date oldValue = this.realtimeStartDate;

        this.realtimeStartDate = clone (realtimeStartDate);

        firePropertyChange(
            REALTIME_START_PROPERTY, oldValue, realtimeStartDate);
    }

    /**
     * @see com.coherentlogic.fred.client.core.domain.RealtimeBoundSpecification#setRealtimeEnd(Date)
     */
    @Override
    public void setRealtimeEnd(Date realtimeEndDate) {

        Date oldValue = this.realtimeEndDate;

        this.realtimeEndDate = clone (realtimeEndDate);

        firePropertyChange(REALTIME_END_PROPERTY, oldValue, realtimeEndDate);
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.ReleaseDatesSparkBeanSpecification#getRealtimeStart()
     */
    @Override
    public Date getRealtimeStart() {
        return clone (realtimeStartDate);
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.ReleaseDatesSparkBeanSpecification#getRealtimeEnd()
     */
    @Override
    public Date getRealtimeEnd() {
        return clone (realtimeEndDate);
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.ReleaseDatesSparkBeanSpecification#getId()
     */
    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {

        String oldValue = this.id;

        this.id = id;

        firePropertyChange(ID, oldValue, id);
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.ReleaseDatesSparkBeanSpecification#getValue()
     */
    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {

        String oldValue = this.value;

        this.value = value;

        firePropertyChange(VALUE, oldValue, value);
    }

    /**
     * @see {@link SerializableBean#accept(Collection)
     */
    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        releaseDateList.forEach(
            (ReleaseDate entry) -> {
                visitors.forEach(visitor -> {visitor.accept(entry);});
            }
        );
    }
}
