package com.coherentlogic.fred.client.core.domain;

import static com.coherentlogic.fred.client.core.domain.PropertyNames.COUNT_PROPERTY;
import static com.coherentlogic.fred.client.core.util.Constants.COUNT;
import static com.coherentlogic.fred.client.core.util.Constants.COUNT_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.LIMIT_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.ORDER_BY_VALUE;
import static com.coherentlogic.fred.client.core.util.Constants.RELEASES;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.coherentlogic.coherent.data.model.core.annotations.Visitable;
import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.fred.client.core.util.Constants;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

/**
 * A class which represents the releases of an economic data series.
 *
 * @see <a href="https://api.stlouisfed.org/docs/fred/releases.html">releases</a>
 * @see <a href="https://api.stlouisfed.org/docs/fred/release.html">release</a>
 * @see <a href="https://api.stlouisfed.org/docs/fred/series_release.html">
 * series_release</a>
 * @see <a href="https://api.stlouisfed.org/docs/fred/source_releases.html">
 * source_releases</a>
 * @see <a href="https://research.stlouisfed.org/fred2/releases/">releases</a>
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Entity
@Table(name=RELEASES)
@XStreamAlias(RELEASES)
@Visitable
public class Releases extends SerializableBean
    implements
        RealtimeBoundSpecification,
        OrderBySpecification,
        SortOrderSpecification,
        PaginationSpecification {

    private static final long serialVersionUID = -1490415291101451411L;

    static final String RELEASE_LIST_PROPERTY = "releaseList";

    @XStreamImplicit
    @Visitable
    private List<Release> releaseList = null;

    @XStreamAlias(Constants.REALTIME_START)
    @XStreamAsAttribute
    @Visitable
    private Date realtimeStart = null;

    @XStreamAlias(Constants.REALTIME_END)
    @XStreamAsAttribute
    @Visitable
    private Date realtimeEnd = null;

    @XStreamAlias(COUNT)
    @XStreamAsAttribute
    @Visitable
    private Integer count = 0;

    @XStreamAlias(Constants.LIMIT)
    @XStreamAsAttribute
    @Visitable
    private long limit = 0;

    @XStreamAlias(Constants.OFFSET)
    @XStreamAsAttribute
    @Visitable
    private int offset = 0;

    @XStreamAlias(Constants.SORT_ORDER)
    @XStreamAsAttribute
    @Visitable
    private SortOrder sortOrder = null;

    @XStreamAlias(Constants.ORDER_BY)
    @XStreamAsAttribute
    @Visitable
    private OrderBy orderBy = null;

    @OneToMany(cascade=CascadeType.ALL)
    public List<Release> getReleaseList () {
        return releaseList;
    }

    public void setReleaseList (List<Release> releaseList) {

        List<Release> oldValue = this.releaseList;

        this.releaseList = releaseList;

        firePropertyChange(RELEASE_LIST_PROPERTY, oldValue, releaseList);
    }

    public Date getRealtimeStart() {
        return clone (realtimeStart);
    }

    /**
     * @see com.coherentlogic.fred.client.core.domain.RealtimeBoundSpecification#setRealtimeStart(Date)
     */
    @Override
    public void setRealtimeStart(Date realtimeStart) {

        Date oldValue = this.realtimeStart;

        this.realtimeStart = clone (realtimeStart);

        firePropertyChange(
            REALTIME_START_PROPERTY,
            oldValue,
            realtimeStart
        );
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.ReleasesSparkBeanSpecification#getRealtimeEnd()
     */
    @Override
    public Date getRealtimeEnd() {
        return clone (realtimeEnd);
    }

    /**
     * @see com.coherentlogic.fred.client.core.domain.RealtimeBoundSpecification#setRealtimeEnd(Date)
     */
    @Override
    public void setRealtimeEnd(Date realtimeEnd) {

        Date oldValue = this.realtimeEnd;

        this.realtimeEnd = clone (realtimeEnd);

        firePropertyChange(
            REALTIME_END_PROPERTY,
            oldValue,
            realtimeEnd
        );
    }

    @Override
    public void setLimit(long limit) {

        long oldValue = this.limit;

        this.limit = limit;

        firePropertyChange(LIMIT_PROPERTY, oldValue, limit);
    }

    @Override
    public void setOffset(int offset) {

        int oldValue = this.offset;

        this.offset = offset;

        firePropertyChange(OFFSET_PROPERTY, oldValue, offset);
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.ReleasesSparkBeanSpecification#getLimit()
     */
    @Override
    @Column(name=LIMIT_VALUE)
    public long getLimit() {
        return limit;
    }

    /* (non-Javadoc)
     * @see com.coherentlogic.fred.client.core.domain.ReleasesSparkBeanSpecification#getOffset()
     */
    @Override
    @Column(name = Constants.OFFSET_COLUMN) // offset restricted in H2.
    public int getOffset() {
        return offset;
    }

    @Override
    public void setSortOrder(SortOrder sortOrder) {
        SortOrder oldValue = this.sortOrder;

        this.sortOrder = sortOrder;

        firePropertyChange(
            SORT_ORDER_PROPERTY,
            oldValue,
            sortOrder
        );
    }

    @Override
    public SortOrder getSortOrder() {
        return sortOrder;
    }

    @Column(name=ORDER_BY_VALUE)
    @Override
    public OrderBy getOrderBy() {
        return orderBy;
    }

    @Override
    public void setOrderBy(OrderBy orderBy) {
        OrderBy oldValue = this.orderBy;

        this.orderBy = orderBy;

        firePropertyChange(
            ORDER_BY_PROPERTY,
            oldValue,
            orderBy
        );
    }

    @Column(name=COUNT_VALUE)
    public Integer getCount() {
        return count;
    }

    /**
     * Setter method for the count value.
     */
    public void setCount(Integer count) {

        Integer oldValue = this.count;

        this.count = count;

        firePropertyChange(COUNT_PROPERTY, oldValue, count);
    }

    /**
     * @see {@link SerializableBean#accept(Collection)
     */
    @Override
    public void accept(Collection<Consumer<SerializableBean>> visitors) {

        super.accept(visitors);

        releaseList.forEach(
            (Release entry) -> {
                visitors.forEach(visitor -> {visitor.accept(entry);});
            }
        );
    }
}
