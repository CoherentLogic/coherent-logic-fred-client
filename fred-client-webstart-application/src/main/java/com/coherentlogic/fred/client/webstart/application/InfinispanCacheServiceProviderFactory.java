package com.coherentlogic.fred.client.webstart.application;

import org.infinispan.manager.EmbeddedCacheManager;
import org.infinispan.notifications.Listener;
import org.infinispan.notifications.cachelistener.annotation.CacheEntryVisited;
import org.infinispan.notifications.cachelistener.event.CacheEntryVisitedEvent;
import org.infinispan.notifications.cachemanagerlistener.annotation.ViewChanged;
import org.infinispan.notifications.cachemanagerlistener.event.ViewChangedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.coherentlogic.coherent.data.model.core.domain.SerializableBean;
import com.coherentlogic.infinispan.cache.providers.DefaultInfinispanCacheServiceProviderFactory;

/**
 * Defines an Infinispan cache manager with the cache mode set to DIST_SYNC (distributed sync) and 
 *
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class InfinispanCacheServiceProviderFactory extends DefaultInfinispanCacheServiceProviderFactory {

    public static final String DEFAULT_FRED_CACHE_NAME = "fredCache";

    public InfinispanCacheServiceProviderFactory(EmbeddedCacheManager cacheManager) {
        this (cacheManager, DEFAULT_FRED_CACHE_NAME);
    }

    public InfinispanCacheServiceProviderFactory(
        EmbeddedCacheManager cacheManager,
        String defaultCacheName
    ) {
        super(cacheManager, defaultCacheName);
    }
}

@Listener
class FREDClusterAndCacheListener {

    private static final Logger log = LoggerFactory.getLogger(FREDClusterAndCacheListener.class);

    @ViewChanged
    public void onViewChanged(ViewChangedEvent event) {

        log.info("viewChanged: method begins; event: " + event);

        event
            .getOldMembers()
            .forEach(
                member -> {
                    log.info("old member: " + member);
                }
            );

        event
            .getNewMembers()
            .forEach(
                member -> {
                    log.info("old member: " + member);
                }
            );

        log.info("viewChanged: method ends.");
    }

    @CacheEntryVisited
    public void onCacheEntryVisited (
        CacheEntryVisitedEvent<String, SerializableBean> cacheEntryVisitedEvent
    ) {
        log.info("onCacheEntryVisited: method invoked; cacheEntryVisitedEvent: " + cacheEntryVisitedEvent);
    }
}

/*
import org.infinispan.spring.provider.SpringEmbeddedCacheManager;

    public InfinispanCacheServiceProviderFactory(SpringEmbeddedCacheManager cacheManager) {
        this (cacheManager, DEFAULT_FRED_CACHE_NAME);
    }

    public InfinispanCacheServiceProviderFactory(
        SpringEmbeddedCacheManager cacheManager,
        String defaultCacheName
    ) {
        super(cacheManager, defaultCacheName);
    }
}

@Listener
class FREDClusterAndCacheListener {

    private static final Logger log = LoggerFactory.getLogger(FREDClusterAndCacheListener.class);

    @ViewChanged
    public void onViewChanged(ViewChangedEvent event) {

        log.info("viewChanged: method begins; event: " + event);

        event
            .getOldMembers()
            .forEach(
                member -> {
                    log.info("old member: " + member);
                }
            );

        event
            .getNewMembers()
            .forEach(
                member -> {
                    log.info("old member: " + member);
                }
            );

        log.info("viewChanged: method ends.");
    }

    @CacheEntryVisited
    public void onCacheEntryVisited (
        CacheEntryVisitedEvent<String, SerializableBean> cacheEntryVisitedEvent
    ) {
        log.info("onCacheEntryVisited: method invoked; cacheEntryVisitedEvent: " + cacheEntryVisitedEvent);
    }
}
*/