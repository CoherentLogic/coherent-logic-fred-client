package com.coherentlogic.fred.client.spark.integration.examples;

import java.util.Calendar;
import java.util.Date;

//import org.apache.spark.SparkConf;
//import org.apache.spark.api.java.JavaSparkContext;
//import org.apache.spark.sql.Dataset;
//import org.apache.spark.sql.SparkSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

//import com.coherentlogic.fred.client.core.domain.Categories;
//import com.coherentlogic.fred.client.core.domain.Seriess;
import com.coherentlogic.fred.client.core.factories.QueryBuilderFactory;
//import com.coherentlogic.fred.client.spark.integration.builders.CategoriesDatasetFactory;
//import com.coherentlogic.fred.client.spark.integration.builders.SeriessDatasetFactory;

/**
 * An example that queries the FRED servers for a seriess object and then saves
 * the result to an instance of the H2 database running in memory.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan(basePackages="com.coherentlogic.fred.client")
public class Main implements CommandLineRunner {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    private static final String DEFAULT_APP_CTX_PATH = "spring/spark/application-context.xml";

    @Autowired
    private QueryBuilderFactory queryBuilderFactory;

//    public SparkDatasetBuilderExample(
//        QueryBuilder queryBuilder
//    ) {
//        super();
//        this.queryBuilder = queryBuilder;
//    }

    static Date getDate (int year, int month, int day) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        return calendar.getTime();
    }

//    Dataset<Categories> getCategoriesDataset (SparkSession sparkSession) {
//
//        Categories result = queryBuilderFactory.getInstance()
//            .category()
//            .withCategoryId(125)
//            .doGetAsCategories();
//
//        CategoriesDatasetFactory categoriesDatasetFactory = new CategoriesDatasetFactory(sparkSession);
//
//        Dataset<Categories> resultantDataset = categoriesDatasetFactory
//            .addBeans(result).getInstance();
//
//        log.info("----- resultantDataset: " + resultantDataset.count());
//
//        return resultantDataset;
//    }
//
//    Dataset<Seriess> getSeriessDataset (SparkSession sparkSession) {
//
//        Date realtimeStart = getDate (2001, Calendar.JANUARY, 20);
//        Date realtimeEnd = getDate (2004, Calendar.MAY, 17);
//
//        Seriess result = queryBuilderFactory.getInstance()
//            .series()
//            .withSeriesId("GNPCA")
//            .withRealtimeStart(realtimeStart)
//            .withRealtimeEnd(realtimeEnd)
//            .doGetAsSeriess ();
//
//        SeriessDatasetFactory seriessDatasetFactory = new SeriessDatasetFactory(sparkSession);
//        Dataset<Seriess> resultantDataset = seriessDatasetFactory
//            .addBeans(result).getInstance();
//        
//        return resultantDataset;
//    }

//    Dataset<Categories> getObservationsDataset (SparkSession sparkSession) {
//
//        Categories result = queryBuilderFactory.getInstance()
//            .category()
//            .withCategoryId(125)
//            .doGetAsCategories();
//
//        CategoriesDatasetFactory categoriesDatasetFactory = new CategoriesDatasetFactory(sparkSession);
//
//        Dataset<Categories> resultantDataset = categoriesDatasetFactory
//            .addBeans(result).getInstance();
//
//        log.info("----- resultantDataset: " + resultantDataset.count());
//
//        return resultantDataset;
//    }

    @Override
    public void run(String... arg0) throws Exception {

//        String master = "local[*]";
//
//        SparkConf sparkConf = new SparkConf()
//            .setAppName(Main.class.getName())
////            .set("spark.executor.memory", "8g")
//            .setMaster(master);
//
//        //SQLContext sqlContext = new SQLContext(javaSparkContext);
//
//        JavaSparkContext javaSparkContext = new JavaSparkContext(sparkConf);
//
//        SparkSession sparkSession = new SparkSession (javaSparkContext.sc());
//
//        Date realtimeStart = getDate (2001, Calendar.JANUARY, 20);
//        Date realtimeEnd = getDate (2004, Calendar.MAY, 17);
//
//        /* Note that the following VM argument must be set: -DFRED_API_KEY=[your FRED API key]
//         */
//        Seriess result = queryBuilderFactory.getInstance()
//            .series()
//            .withSeriesId("GNPCA")
//            .withRealtimeStart(realtimeStart)
//            .withRealtimeEnd(realtimeEnd)
//            .doGetAsSeriess ();
//
//        SeriessDatasetFactory seriessDatasetFactory = new SeriessDatasetFactory(sparkSession);
//        Dataset<Seriess> resultantDataset = seriessDatasetFactory
//            .addBeans(result).getInstance();
//
////        Categories result = queryBuilderFactory.getInstance()
////            .category()
////            .withCategoryId(125)
////            .doGetAsCategories();
////
////        CategoriesDatasetFactory categoriesDatasetFactory = new CategoriesDatasetFactory(sparkSession);
////
////        Dataset<Categories> resultantDataset = categoriesDatasetFactory
////            .addBeans(result).getInstance();
//
//        getCategoriesDataset (sparkSession).show();
//
//        sparkSession.close();
    }

    /**
     * When running this example, it may be necessary to increase the memory available, as is demonstrated below
     * (added as a VM argument):
     *
     * -Xmx16g
     */
    public static void main (String[] unused) throws InterruptedException {

        try {

            SpringApplicationBuilder builder =
                new SpringApplicationBuilder (Main.class);

            builder
                .web(false)
                .headless(false)
                .registerShutdownHook(true)
                .run(unused);

        } catch (Throwable thrown) {
            log.error("SparkDatasetBuilderExample.main caught an exception.", thrown);
        }

        Thread.sleep(60 * 1000 * 5); // 5 minutes, so if the computer locks up we're not waiting forever.

        System.exit(-9999);
    }
}

//PropertyDescriptor[] props = Introspector.getBeanInfo(SeriessSpecification.class).getPropertyDescriptors();
//for(PropertyDescriptor prop:props) {
//    System.out.println(prop.getDisplayName());
//    System.out.println("\t"+prop.getReadMethod());
//    System.out.println("\t"+prop.getWriteMethod());
//}

//SQLContext sqlContext = new SQLContext(sparkSession);

//WORKING WITH NESTED BEANS DON'T DELETE!
//Dataset<FooBean> resultantDataset = sqlContext.createDataset(Arrays.asList(new FooBean ()), Encoders.bean(FooBean.class));

//List<RealtimeBoundSpecification> bbb = (List) result.getSeriesList();
//WORKS
//Dataset<PaginationSpecification> resultantDataset = sqlContext.createDataset(Arrays.asList((PaginationSpecification)result), Encoders.bean(PaginationSpecification.class));