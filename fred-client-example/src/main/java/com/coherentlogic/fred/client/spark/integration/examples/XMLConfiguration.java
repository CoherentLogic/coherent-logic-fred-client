package com.coherentlogic.fred.client.spark.integration.examples;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * "classpath*:/spring/spark/api-key-beans.xml", 
 * 
 * @author <a href="https://www.linkedin.com/in/thomasfuller">Thomas P. Fuller</a>
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
@Configuration
@ImportResource({"classpath*:/spring/spark/h2-jpa-beans.xml", "classpath*:/spring/spark/application-context.xml"})
public class XMLConfiguration {

}
