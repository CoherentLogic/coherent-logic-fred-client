package com.coherentlogic.fred.client.db.integration;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.coherentlogic.fred.client.core.builders.QueryBuilder;
import com.coherentlogic.fred.client.core.domain.FilterValue;
import com.coherentlogic.fred.client.core.domain.FilterVariable;
import com.coherentlogic.fred.client.core.domain.OrderBy;
import com.coherentlogic.fred.client.core.domain.SearchType;
import com.coherentlogic.fred.client.core.domain.Seriess;
import com.coherentlogic.fred.client.core.domain.SortOrder;
import com.coherentlogic.fred.client.db.integration.services.SeriessService;

/**
 * An example that queries the FRED servers for a seriess object and then saves the result to an instance of the H2
 * database running in memory.
 *
 * @TODO Consider putting this class in its own module.
 * @TODO Review the application contexts this class is loading as IIRC it's loading the Spark app context as well and
 *       that's not appropriate here for this example.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    private static final String DEFAULT_APP_CTX_PATH =
        "spring/db/application-context.xml", BASIC_EXAMPLE = "basicExample";

    private final QueryBuilder seriessRequestBuilder;

    private final SeriessService seriessService;

    public Main(
        QueryBuilder seriessRequestBuilder,
        SeriessService seriessService
    ) {
        super();
        this.seriessRequestBuilder = seriessRequestBuilder;
        this.seriessService = seriessService;
    }

    static Date getDate (int year, int month, int day) {

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        return calendar.getTime();
    }

    public Seriess querySeriess () {
        Date realtimeStart = getDate (2001, Calendar.JANUARY, 20);
        Date realtimeEnd = getDate (2004, Calendar.MAY, 17);

        Seriess result = seriessRequestBuilder
            .withSearchText("money stock")
            .withSearchType(SearchType.fullText)
            .withRealtimeStart(realtimeStart)
            .withRealtimeEnd(realtimeEnd)
            .withLimit(1000)
            .withOffset(1)
            .withOrderBy(OrderBy.searchRank)
            .withSortOrder(SortOrder.desc)
            .withFilterVariable(FilterVariable.frequency)
            .withFilterValue(FilterValue.all)
            .doGetAsSeriess ();

        return result;
    }

    public void save (Seriess seriess) {
        seriessService.save(seriess);
    }

    public static void main (String[] unused) {

        AbstractApplicationContext applicationContext =
            new ClassPathXmlApplicationContext (DEFAULT_APP_CTX_PATH);

        applicationContext.registerShutdownHook();

        Main basicExample =
            (Main) applicationContext.getBean(BASIC_EXAMPLE);

        /* Note that the following VM argument must be set: -DFRED_API_KEY=[your FRED API key]
         */
        Seriess seriess = basicExample.querySeriess();

        basicExample.save(seriess);

        applicationContext.close ();
    }
}
