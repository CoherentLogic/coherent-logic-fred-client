package com.coherentlogic.fred.client.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.coherentlogic.fred.client.core.domain.Seriess;

/**
 * A simple application which calls the seriesService.getSeries method and then
 * prints out the Seriess returned.
 *
 * NOTE: You will need to add your own apiKey in the application-context.xml
 * file, otherwise this won't work.
 *
 * @author <a href="mailto:support@coherentlogic.com">Support</a>
 */
public class Main {

    public static void main(String[] args) {

        // Note that the application-context.xml is based very closely on the
        // one we use in fred-client-core-it.
        ApplicationContext context = new FileSystemXmlApplicationContext (
            "src/main/resources/spring/basic-example/application-context.xml");

        SeriesService seriesService =
            (SeriesService) context.getBean("seriesService");

        String seriesId = "GNPCA", // GNPCA / canada
            realtimeStartDate = "2001-01-20",
            realtimeEndDate = "2004-05-17";

        /* Note that the following VM argument must be set: -DFRED_API_KEY=[your FRED API key]
         */
        Seriess seriess = seriesService.getSeriess(
            seriesId,
            realtimeStartDate,
            realtimeEndDate);

        System.err.println ("Seriess: " + seriess);
    }
}
